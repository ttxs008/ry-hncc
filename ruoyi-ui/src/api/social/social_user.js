import request from '@/utils/request'

// 查询社交登录用户列表
export function listSocial_user(query) {
  return request({
    url: '/social/social_user/list',
    method: 'get',
    params: query
  })
}

// 查询社交登录用户详细
export function getSocial_user(id) {
  return request({
    url: '/social/social_user/' + id,
    method: 'get'
  })
}

// 新增社交登录用户
export function addSocial_user(data) {
  return request({
    url: '/social/social_user',
    method: 'post',
    data: data
  })
}

// 修改社交登录用户
export function updateSocial_user(data) {
  return request({
    url: '/social/social_user',
    method: 'put',
    data: data
  })
}

// 删除社交登录用户
export function delSocial_user(id) {
  return request({
    url: '/social/social_user/' + id,
    method: 'delete'
  })
}

// 导出社交登录用户
export function exportSocial_user(query) {
  return request({
    url: '/social/social_user/export',
    method: 'get',
    params: query
  })
}