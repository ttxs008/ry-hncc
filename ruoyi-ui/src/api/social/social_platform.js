import request from '@/utils/request'

// 查询社交应用列表
export function listSocial_platform(query) {
  return request({
    url: '/social/social_platform/list',
    method: 'get',
    params: query
  })
}

// 查询社交应用详细
export function getSocial_platform(platformName) {
  return request({
    url: '/social/social_platform/' + platformName,
    method: 'get'
  })
}

// 新增社交应用
export function addSocial_platform(data) {
  return request({
    url: '/social/social_platform',
    method: 'post',
    data: data
  })
}

// 修改社交应用
export function updateSocial_platform(data) {
  return request({
    url: '/social/social_platform',
    method: 'put',
    data: data
  })
}

// 删除社交应用
export function delSocial_platform(platformName) {
  return request({
    url: '/social/social_platform/' + platformName,
    method: 'delete'
  })
}

// 导出社交应用
export function exportSocial_platform(query) {
  return request({
    url: '/social/social_platform/export',
    method: 'get',
    params: query
  })
}