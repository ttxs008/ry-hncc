import request from '@/utils/request'

// 查询成药药房药品  (患者,已发药/未发药)
export function listPharmacy_drug(data) {
  return request({
    url: '/dms/pharmacyDrug/list',
    method: 'post',
    data: data
  })
}

// 查询草药药房药品  (患者,已发药/未发药)
export function herbalistPharmacy_drug(data) {
  return request({
    url: '/dms/pharmacyDrug/herbalist',
    method: 'post',
    data: data
  })
}
