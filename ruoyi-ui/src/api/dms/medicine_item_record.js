import request from '@/utils/request'

// 查询成药项记录列表
export function listMedicine_item_record(query) {
  return request({
    url: '/dms/medicine_item_record/list',
    method: 'get',
    params: query
  })
}

// 查询成药项记录详细
export function getMedicine_item_record(id) {
  return request({
    url: '/dms/medicine_item_record/' + id,
    method: 'get'
  })
}

// 新增成药项记录
export function addMedicine_item_record(data) {
  return request({
    url: '/dms/medicine_item_record',
    method: 'post',
    data: data
  })
}

// 修改成药项记录
export function updateMedicine_item_record(data) {
  return request({
    url: '/dms/medicine_item_record',
    method: 'put',
    data: data
  })
}

// 删除成药项记录
export function delMedicine_item_record(id) {
  return request({
    url: '/dms/medicine_item_record/' + id,
    method: 'delete'
  })
}

// 导出成药项记录
export function exportMedicine_item_record(query) {
  return request({
    url: '/dms/medicine_item_record/export',
    method: 'get',
    params: query
  })
}
