import request from '@/utils/request'

// 查询药品模版列表
export function listDrug_model(query) {
  return request({
    url: '/dms/drug_model/list',
    method: 'get',
    params: query
  })
}

// 查询药品模版详细
export function getDrug_model(id) {
  return request({
    url: '/dms/drug_model/' + id,
    method: 'get'
  })
}

// 新增药品模版
export function addDrug_model(data) {
  return request({
    url: '/dms/drug_model',
    method: 'post',
    data: data
  })
}

// 修改药品模版
export function updateDrug_model(data) {
  return request({
    url: '/dms/drug_model',
    method: 'put',
    data: data
  })
}

// 删除药品模版
export function delDrug_model(id) {
  return request({
    url: '/dms/drug_model/' + id,
    method: 'delete'
  })
}

// 导出药品模版
export function exportDrug_model(query) {
  return request({
    url: '/dms/drug_model/export',
    method: 'get',
    params: query
  })
}