import request from '@/utils/request'

// 查询草药模版项列表
export function listHerbal_model_item(query) {
  return request({
    url: '/dms/herbal_model_item/list',
    method: 'get',
    params: query
  })
}

// 查询草药模版项详细
export function getHerbal_model_item(id) {
  return request({
    url: '/dms/herbal_model_item/' + id,
    method: 'get'
  })
}

// 新增草药模版项
export function addHerbal_model_item(data) {
  return request({
    url: '/dms/herbal_model_item',
    method: 'post',
    data: data
  })
}

// 修改草药模版项
export function updateHerbal_model_item(data) {
  return request({
    url: '/dms/herbal_model_item',
    method: 'put',
    data: data
  })
}

// 删除草药模版项
export function delHerbal_model_item(id) {
  return request({
    url: '/dms/herbal_model_item/' + id,
    method: 'delete'
  })
}

// 导出草药模版项
export function exportHerbal_model_item(query) {
  return request({
    url: '/dms/herbal_model_item/export',
    method: 'get',
    params: query
  })
}

//通过模板Id查询药品信息
export function getHerbal_model_item_by_modelId(id) {
  return request({
    url: '/dms/herbal_model_item/queryDrugByModelId/' + id,
    method: 'get',
  })
}
