import request from '@/utils/request'


// 查询就诊(门诊)信息列表
export function listRegistration(query) {
  return request({
    url: '/dms/registration/list',
    method: 'get',
    params: query
  })
}

// 查询就诊(门诊)信息详细
export function getRegistration(id) {
  return request({
    url: '/dms/registration/' + id,
    method: 'get'
  })
}

// 新增就诊(门诊)信息
export function addRegistration(data) {
  return request({
    url: '/dms/registration',
    method: 'post',
    data: data
  })
}

// 修改就诊(门诊)信息
export function updateRegistration(data) {
  return request({
    url: '/dms/registration',
    method: 'put',
    data: data
  })
}

// 删除就诊(门诊)信息
export function delRegistration(id) {
  return request({
    url: '/dms/registration/' + id,
    method: 'delete'
  })
}

// 导出就诊(门诊)信息
export function exportRegistration(query) {
  return request({
    url: '/dms/registration/export',
    method: 'get',
    params: query
  })
}

// 查询门诊挂号收费信息列表
export function RegistrationPatientList(query) {
  return request({
    url: '/dms/registration/listByNameAndNo',
    method: 'get',
    params: query
  })
}

// 挂号处理
export function handleRegistration(data) {
  return request({
    url: '/dms/registration/handleRegistration',
    method: 'post',
    data: data
  })
}

// 退号处理
export function handleBounce(data) {
  return request({
    url: '/dms/registration/handleBounce',
    method: 'post',
    data: data
  })
}

// 缴费列表
export function payList(query) {
  return request({
    url: '/dms/registration/payList',
    method: 'get',
    params: query
  })
}

// 处理缴费
export function handlePayment(data) {
  return request({
    url: '/dms/registration/handlePayment',
    method: 'post',
    data: data
  })
}

// 退费列表
export function refundList(query) {
  return request({
    url: '/dms/registration/refundList',
    method: 'get',
    params: query
  })
}


// 退费缴费
export function handleRefund(data) {
  return request({
    url: '/dms/registration/handleRefund',
    method: 'post',
    data: data
  })
}
