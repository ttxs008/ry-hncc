import request from '@/utils/request'

// 查询检查项检验项处置项记录(开立的)列表
export function listNon_drug_item_record(query) {
  return request({
    url: '/dms/non_drug_item_record/list',
    method: 'get',
    params: query
  })
}

// 查询检查项检验项处置项记录(开立的)详细
export function getNon_drug_item_record(id) {
  return request({
    url: '/dms/non_drug_item_record/' + id,
    method: 'get'
  })
}

// 新增检查项检验项处置项记录(开立的)
export function addNon_drug_item_record(data) {
  return request({
    url: '/dms/non_drug_item_record',
    method: 'post',
    data: data
  })
}

// 修改检查项检验项处置项记录(开立的)
export function updateNon_drug_item_record(data) {
  return request({
    url: '/dms/non_drug_item_record/edit',
    method: 'put',
    data: data
  })
}

// 删除检查项检验项处置项记录(开立的)
export function delNon_drug_item_record(id) {
  return request({
    url: '/dms/non_drug_item_record/' + id,
    method: 'delete'
  })
}

// 导出检查项检验项处置项记录(开立的)
export function exportNon_drug_item_record(query) {
  return request({
    url: '/dms/non_drug_item_record/export',
    method: 'get',
    params: query
  })
}

// 查询处置项记录(开立的)列表
export function dealWithListNon_drug_item_record(query) {
  return request({
    url: '/dms/non_drug_item_record/dealWithList',
    method: 'get',
    params: query
  })
}

// 修改检查结果
export function updateNon_drug_item_record_check_result(data) {
  return request({
    url: '/dms/non_drug_item_record/editCheckResult',
    method: 'put',
    data: data
  })
}

// 暂存入Redis
export function save(query,registrationId,status) {
  return request({
    url: '/dms/non_drug_item_record/redisSave/'+registrationId+"/"+status,
    method: 'post',
    data: query
  })
}

// 根据KEY删除Redis
export function del(query,registrationId,status) {
  return request({
    url: '/dms/non_drug_item_record/redisDel/'+registrationId+"/"+status,
    method: 'post',
    data: query
  })
}

// 根据挂号Id查询所对应的Redis信息
export function getRedis(registrationId,status) {
  return request({
    url: '/dms/non_drug_item_record/redisGet/'+registrationId+"/"+status,
    method: 'post'
  })
}
