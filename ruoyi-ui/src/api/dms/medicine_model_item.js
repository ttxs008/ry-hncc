import request from '@/utils/request'

// 查询成药模版列表
export function listMedicine_model_item(query) {
  return request({
    url: '/dms/medicine_model_item/list',
    method: 'get',
    params: query
  })
}

// 查询成药模版详细
export function getMedicine_model_item(id) {
  return request({
    url: '/dms/medicine_model_item/' + id,
    method: 'get'
  })
}

// 新增成药模版
export function addMedicine_model_item(data) {
  return request({
    url: '/dms/medicine_model_item',
    method: 'post',
    data: data
  })
}

// 修改成药模版
export function updateMedicine_model_item(data) {
  return request({
    url: '/dms/medicine_model_item',
    method: 'put',
    data: data
  })
}

// 删除成药模版
export function delMedicine_model_item(id) {
  return request({
    url: '/dms/medicine_model_item/' + id,
    method: 'delete'
  })
}

// 导出成药模版
export function exportMedicine_model_item(query) {
  return request({
    url: '/dms/medicine_model_item/export',
    method: 'get',
    params: query
  })
}

//通过模板Id查询药品信息
export function getMedicine_model_item_by_modelId(id) {
  return request({
    url: '/dms/medicine_model_item/queryDrugByModelId/' + id,
    method: 'get',
  })
}
