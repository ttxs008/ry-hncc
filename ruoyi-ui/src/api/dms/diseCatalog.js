import request from '@/utils/request'

// 查询诊断类型（疾病）目录列表
export function listDiseCatalog(query) {
  return request({
    url: '/dms/diseCatalog/list',
    method: 'get',
    params: query
  })
}

// 查询诊断类型（疾病）目录详细
export function getDiseCatalog(id) {
  return request({
    url: '/dms/diseCatalog/' + id,
    method: 'get'
  })
}

// 新增诊断类型（疾病）目录
export function addDiseCatalog(data) {
  return request({
    url: '/dms/diseCatalog',
    method: 'post',
    data: data
  })
}

// 修改诊断类型（疾病）目录
export function updateDiseCatalog(data) {
  return request({
    url: '/dms/diseCatalog',
    method: 'put',
    data: data
  })
}

// 删除诊断类型（疾病）目录
export function delDiseCatalog(id) {
  return request({
    url: '/dms/diseCatalog/' + id,
    method: 'delete'
  })
}

// 导出诊断类型（疾病）目录
export function exportDiseCatalog(query) {
  return request({
    url: '/dms/diseCatalog/export',
    method: 'get',
    params: query
  })
}