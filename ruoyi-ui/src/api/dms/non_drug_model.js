import request from '@/utils/request'

// 查询非药品模版列表
export function listNon_drug_model(query) {
  return request({
    url: '/dms/non_drug_model/list',
    method: 'get',
    params: query
  })
}

// 查询非药品模版详细
export function getNon_drug_model(id) {
  return request({
    url: '/dms/non_drug_model/' + id,
    method: 'get'
  })
}

// 新增非药品模版
export function addNon_drug_model(data) {
  return request({
    url: '/dms/non_drug_model',
    method: 'post',
    data: data
  })
}

// 修改非药品模版
export function updateNon_drug_model(data) {
  return request({
    url: '/dms/non_drug_model',
    method: 'put',
    data: data
  })
}

// 删除非药品模版
export function delNon_drug_model(id) {
  return request({
    url: '/dms/non_drug_model/' + id,
    method: 'delete'
  })
}

// 导出非药品模版
export function exportNon_drug_model(query) {
  return request({
    url: '/dms/non_drug_model/export',
    method: 'get',
    params: query
  })
}