import request from '@/utils/request'

// 病例模版目录列表
export function listDms_case_model_catalog(query) {
  return request({
    url: '/dms/dms_case_model_catalog/list',
    method: 'get',
    params: query
  })
}

// 查询病例模版目录详细
export function getDms_case_model_catalog(id) {
  return request({
    url: '/dms/dms_case_model_catalog/' + id,
    method: 'get'
  })
}

// 新增病例模版目录
export function addDms_case_model_catalog(data) {
  return request({
    url: '/dms/dms_case_model_catalog',
    method: 'post',
    data: data
  })
}

// 修改病例模版目录
export function updateDms_case_model_catalog(data) {
  return request({
    url: '/dms/dms_case_model_catalog',
    method: 'put',
    data: data
  })
}

// 删除病例模版目录
export function delDms_case_model_catalog(id) {
  return request({
    url: '/dms/dms_case_model_catalog/' + id,
    method: 'delete'
  })
}

// 导出病例模版目录
export function exportDms_case_model_catalog(query) {
  return request({
    url: '/dms/dms_case_model_catalog/export',
    method: 'get',
    params: query
  })
}
