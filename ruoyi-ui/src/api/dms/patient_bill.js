import request from '@/utils/request'

// 查询患者费用列表
export function list_patient_bill(query) {
  return request({
    url: '/dms/patient_bill/list',
    method: 'get',
    params: query
  })
}
