import request from '@/utils/request'

// 查询病例模版列表
export function listDms_case_model(query) {
  return request({
    url: '/dms/dms_case_model/list',
    method: 'get',
    params: query
  })
}

// 查询病例模版详细
export function getDms_case_model(id) {
  return request({
    url: '/dms/dms_case_model/' + id,
    method: 'get'
  })
}

// 新增病例模版
export function addDms_case_model(data) {
  return request({
    url: '/dms/dms_case_model',
    method: 'post',
    data: data
  })
}

// 修改病例模版
export function updateDms_case_model(data) {
  return request({
    url: '/dms/dms_case_model',
    method: 'put',
    data: data
  })
}

// 删除病例模版
export function delDms_case_model(id) {
  return request({
    url: '/dms/dms_case_model/' + id,
    method: 'delete'
  })
}

// 导出病例模版
export function exportDms_case_model(query) {
  return request({
    url: '/dms/dms_case_model/export',
    method: 'get',
    params: query
  })
}