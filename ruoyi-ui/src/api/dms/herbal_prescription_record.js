import request from '@/utils/request'

// 查询草处方列表
export function listHerbal_prescription_record(query) {
  return request({
    url: '/dms/herbal_prescription_record/list',
    method: 'get',
    params: query
  })
}

// 查询草处方详细
export function getHerbal_prescription_record(id) {
  return request({
    url: '/dms/herbal_prescription_record/' + id,
    method: 'get'
  })
}

// 新增草处方
export function addHerbal_prescription_record(data) {
  return request({
    url: '/dms/herbal_prescription_record',
    method: 'post',
    data: data
  })
}

// 修改草处方
export function updateHerbal_prescription_record(data) {
  return request({
    url: '/dms/herbal_prescription_record',
    method: 'put',
    data: data
  })
}

// 删除草处方
export function delHerbal_prescription_record(id) {
  return request({
    url: '/dms/herbal_prescription_record/' + id,
    method: 'delete'
  })
}

// 导出草处方
export function exportHerbal_prescription_record(query) {
  return request({
    url: '/dms/herbal_prescription_record/export',
    method: 'get',
    params: query
  })
}

// 修改草处方(发药)
export function updateHerbal_prescription_records(data) {
  return request({
    url: '/dms/herbal_prescription_record/edits',
    method: 'put',
    data: data
  })
}
