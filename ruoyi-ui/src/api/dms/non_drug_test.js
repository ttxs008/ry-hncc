import request from '@/utils/request'

// 查询非药品检验操作数据
export function listNon_drug_test(data) {
  return request({
    url: '/dms/non_drug_item_record/dealWithList',
    method: 'get',
    data: data
  })
}
