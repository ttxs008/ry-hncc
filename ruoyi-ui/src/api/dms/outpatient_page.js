import request from '@/utils/request'

export function listDms_case_model(query) {
  return request({
    url: '/dms/dms_case_model/list',
    method: 'get',
    params: query
  })
}
export function getDms_case_model(id) {
  return request({
    url: '/dms/dms_case_model/' + id,
    method: 'get'
  })
}
export function listDise(query) {
  return request({
    url: '/dms/dise/list',
    method: 'get',
    params: query
  })
}
export function getDise(id) {
  return request({
    url: '/dms/dise/' + id,
    method: 'get'
  })
}

export function listPatient(query) {
  return request({
    url: '/pms/patient/list',
    method: 'get',
    params: query
  })
}

export function getMedicineList(query) {
  return request({
    url: '/dms/used/getMedicineList',
    method: 'get',
    params: query
  })
}
