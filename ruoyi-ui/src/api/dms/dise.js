import request from '@/utils/request'

// 查询诊断类型(疾病)管理列表
export function listDise(query) {
  return request({
    url: '/dms/dise/list',
    method: 'get',
    params: query
  })
}

// 查询诊断类型(疾病)管理详细
export function getDise(id) {
  return request({
    url: '/dms/dise/' + id,
    method: 'get'
  })
}

// 新增诊断类型(疾病)管理
export function addDise(data) {
  return request({
    url: '/dms/dise',
    method: 'post',
    data: data
  })
}

// 修改诊断类型(疾病)管理
export function updateDise(data) {
  return request({
    url: '/dms/dise',
    method: 'put',
    data: data
  })
}

// 删除诊断类型(疾病)管理
export function delDise(id) {
  return request({
    url: '/dms/dise/' + id,
    method: 'delete'
  })
}

// 导出诊断类型(疾病)管理
export function exportDise(query) {
  return request({
    url: '/dms/dise/export',
    method: 'get',
    params: query
  })
}

