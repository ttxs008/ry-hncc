import request from '@/utils/request'

// 查询病历列表
export function listCase_history(query) {
  return request({
    url: '/dms/case_history/list',
    method: 'get',
    params: query
  })
}

// 查询病历详细
export function getCase_history(id) {
  return request({
    url: '/dms/case_history/' + id,
    method: 'get'
  })
}

// 新增病历
export function addCase_history(data) {
  return request({
    url: '/dms/case_history',
    method: 'post',
    data: data
  })
}

// 修改病历
export function updateCase_history(data) {
  return request({
    url: '/dms/case_history',
    method: 'put',
    data: data
  })
}

// 删除病历
export function delCase_history(id) {
  return request({
    url: '/dms/case_history/' + id,
    method: 'delete'
  })
}

// 导出病历
export function exportCase_history(query) {
  return request({
    url: '/dms/case_history/export',
    method: 'get',
    params: query
  })
}