import request from '@/utils/request'


export function listDms_case_model(query) {
  return request({
    url: '/dms/dms_case_model/list',
    method: 'get',
    params: query
  })
}
export function getDms_case_model(id) {
  return request({
    url: '/dms/dms_case_model/' + id,
    method: 'get'
  })
}
export function listDise(query) {
  return request({
    url: '/dms/dise/list',
    method: 'get',
    params: query
  })
}
export function getDise(id) {
  return request({
    url: '/dms/dise/' + id,
    method: 'get'
  })
}
export function listPatient(query) {
  return request({
    url: '/pms/patient/list',
    method: 'get',
    params: query
  })
}
export function getPatient(id) {
  return request({
    url: '/pms/patient/' + id,
    method: 'get'
  })
}
export function getPatientAndReg(pid) {
  return request({
    url: '/pms/patient/patAndReg/' + pid,
    method: 'get'
  })
}
// 新增病历
export function addCase_history(data) {
  return request({
    url: '/dms/case_history',
    method: 'post',
    data: data
  })
}
// 查询病历详细
export function getCase_patHistory(patientId) {
  return request({
    url: '/dms/case_history/patHis/' + patientId,
    method: 'get',
    params: patientId,
  })
}
//查询常用就诊模板
export function listNon_drug_model(query) {
  return request({
    url: '/dms/non_drug_model/list',
    method: 'get',
    params: query
  })
}

// 查询就诊(门诊)信息列表
export function listRegistration(query) {
  return request({
    url: '/dms/registration/list',
    method: 'get',
    params: query
  })
}

// 查询就诊(门诊)信息详细
export function getRegistration(id) {
  return request({
    url: '/dms/registration/' + id,
    method: 'get'
  })
}

// 新增就诊(门诊)信息
export function addRegistration(data) {
  return request({
    url: '/dms/registration',
    method: 'post',
    data: data
  })
}

// 修改就诊(门诊)信息
export function updateRegistration(data) {
  return request({
    url: '/dms/registration',
    method: 'put',
    data: data
  })
}

// 删除就诊(门诊)信息
export function delRegistration(id) {
  return request({
    url: '/dms/registration/' + id,
    method: 'delete'
  })
}

// 导出就诊(门诊)信息
export function exportRegistration(query) {
  return request({
    url: '/dms/registration/export',
    method: 'get',
    params: query
  })
}
