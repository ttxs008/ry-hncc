import request from '@/utils/request'

// 查询成药处方列表
export function listMedicine_prescription_record(query) {
  return request({
    url: '/dms/medicine_prescription_record/list',
    method: 'get',
    params: query
  })
}

// 查询成药处方详细
export function getMedicine_prescription_record(id) {
  return request({
    url: '/dms/medicine_prescription_record/' + id,
    method: 'get'
  })
}

// 新增成药处方
export function addMedicine_prescription_record(data) {
  return request({
    url: '/dms/medicine_prescription_record',
    method: 'post',
    data: data
  })
}

// 开立成药处方
export function prescribe_over_counter_medicines(registrationId, names) {
  return request({
    url: '/dms/medicine_prescription_record/prescribe/' + registrationId + "/" + names,
    method: 'post',
  })
}

// 作废成药处方
export function void_prescription_over_counter_medicines(registrationId, names) {
  return request({
    url: '/dms/medicine_prescription_record/voidPrescription/' + registrationId + "/" + names,
    method: 'post',
  })
}

// 修改成药处方
export function updateMedicine_prescription_record(data) {
  return request({
    url: '/dms/medicine_prescription_record',
    method: 'put',
    data: data
  })
}

// 修改成药处方
export function updateMedicine_prescription_records(data) {
  return request({
    url: '/dms/medicine_prescription_record/edits',
    method: 'put',
    data: data
  })
}

// 删除成药处方
export function delMedicine_prescription_record(id) {
  return request({
    url: '/dms/medicine_prescription_record/' + id,
    method: 'delete'
  })
}

// 删除成药处方（从redis缓存删除）
export function delMedicine_prescription_record_remove(registrationId, names) {
  return request({
    url: '/dms/medicine_prescription_record/del/' + registrationId + "/" + names,
    method: 'delete',
  })
}


// 导出成药处方
export function exportMedicine_prescription_record(query) {
  return request({
    url: '/dms/medicine_prescription_record/export',
    method: 'get',
    params: query
  })
}
