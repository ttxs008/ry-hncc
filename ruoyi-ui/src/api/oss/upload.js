import request from '@/utils/request'

// 上传文件到oss
export function upload(data) {
  return request({
    url: '/aliyun/oss/upload',
    method: 'post',
    params: data
  })
}
