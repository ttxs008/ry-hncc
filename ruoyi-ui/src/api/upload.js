import request from '@/utils/request'

export function policy(data) {
  return request({
    url: '/aliyun/oss/policy',
    method: 'get',
    data
  })
}


