import request from '@/utils/request'

// 查询挂号级别列表
export function listRegistration_rank(query) {
  return request({
    url: '/sms/registration_rank/list',
    method: 'get',
    params: query
  })
}

// 查询挂号级别详细
export function getRegistration_rank(id) {
  return request({
    url: '/sms/registration_rank/' + id,
    method: 'get'
  })
}

// 新增挂号级别
export function addRegistration_rank(data) {
  return request({
    url: '/sms/registration_rank',
    method: 'post',
    data: data
  })
}

// 修改挂号级别
export function updateRegistration_rank(data) {
  return request({
    url: '/sms/registration_rank',
    method: 'put',
    data: data
  })
}

// 删除挂号级别
export function delRegistration_rank(id) {
  return request({
    url: '/sms/registration_rank/' + id,
    method: 'delete'
  })
}

// 导出挂号级别
export function exportRegistration_rank(query) {
  return request({
    url: '/sms/registration_rank/export',
    method: 'get',
    params: query
  })
}

// 挂号状态修改
export function changeRankStatus(id, status) {
  const data = {
    id,
    status
  }
  return request({
    url: '/sms/registration_rank',
    method: 'put',
    data: data
  })
}
