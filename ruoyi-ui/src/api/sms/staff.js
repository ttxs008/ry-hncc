import request from '@/utils/request'

// 查询员工列表
export function listStaff(query) {
  return request({
    url: '/sms/staff/list',
    method: 'get',
    params: query
  })
}

// 查询员工详细
export function getStaff(id) {
  return request({
    url: '/sms/staff/' + id,
    method: 'get'
  })
}

// 新增员工
export function addStaff(data) {
  return request({
    url: '/sms/staff',
    method: 'post',
    data: data
  })
}

// 修改员工
export function updateStaff(data) {
  return request({
    url: '/sms/staff',
    method: 'put',
    data: data
  })
}

// 删除员工
export function delStaff(id) {
  return request({
    url: '/sms/staff/' + id,
    method: 'delete'
  })
}

// 导出员工
export function exportStaff(query) {
  return request({
    url: '/sms/staff/export',
    method: 'get',
    params: query
  })
}

// 查询员工列表 根据排班时间表
export function listStaffBySkd(query) {
  return request({
    url: '/sms/staff/listBySkd',
    method: 'get',
    params: query
  })
}
