import request from '@/utils/request'

// 查询排班规则详情列表
export function listSkd_rule_item(query) {
  return request({
    url: '/sms/skd_rule_item/list',
    method: 'get',
    params: query
  })
}

// 查询排班规则详情详细
export function getSkd_rule_item(id) {
  return request({
    url: '/sms/skd_rule_item/' + id,
    method: 'get'
  })
}

// 新增排班规则详情
export function addSkd_rule_item(data) {
  return request({
    url: '/sms/skd_rule_item',
    method: 'post',
    data: data
  })
}

// 修改排班规则详情
export function updateSkd_rule_item(data) {
  return request({
    url: '/sms/skd_rule_item',
    method: 'put',
    data: data
  })
}

// 删除排班规则详情
export function delSkd_rule_item(id) {
  return request({
    url: '/sms/skd_rule_item/' + id,
    method: 'delete'
  })
}

// 导出排班规则详情
export function exportSkd_rule_item(query) {
  return request({
    url: '/sms/skd_rule_item/export',
    method: 'get',
    params: query
  })
}