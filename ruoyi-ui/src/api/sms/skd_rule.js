import request from '@/utils/request'

// 查询排班规则列表
export function listSkd_rule(query) {
  return request({
    url: '/sms/skd_rule/list',
    method: 'get',
    params: query
  })
}

// 查询排班规则详细
export function getSkd_rule(id) {
  return request({
    url: '/sms/skd_rule/' + id,
    method: 'get'
  })
}

// 新增排班规则
export function addSkd_rule(data) {
  return request({
    url: '/sms/skd_rule',
    method: 'post',
    data: data
  })
}

// 修改排班规则
export function updateSkd_rule(data) {
  return request({
    url: '/sms/skd_rule',
    method: 'put',
    data: data
  })
}

// 删除排班规则
export function delSkd_rule(id) {
  return request({
    url: '/sms/skd_rule/' + id,
    method: 'delete'
  })
}

// 导出排班规则
export function exportSkd_rule(query) {
  return request({
    url: '/sms/skd_rule/export',
    method: 'get',
    params: query
  })
}

