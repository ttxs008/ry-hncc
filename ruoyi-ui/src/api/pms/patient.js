import request from '@/utils/request'

// 查询病人基本信息列表
export function listPatient(query) {
  return request({
    url: '/pms/patient/list',
    method: 'get',
    params: query
  })
}

// 查询病人基本信息详细
export function getPatient(id) {
  return request({
    url: '/pms/patient/' + id,
    method: 'get'
  })
}

// 新增病人基本信息
export function addPatient(data) {
  return request({
    url: '/pms/patient',
    method: 'post',
    data: data
  })
}

// 修改病人基本信息
export function updatePatient(data) {
  return request({
    url: '/pms/patient',
    method: 'put',
    data: data
  })
}

// 删除病人基本信息
export function delPatient(id) {
  return request({
    url: '/pms/patient/' + id,
    method: 'delete'
  })
}

// 导出病人基本信息
export function exportPatient(query) {
  return request({
    url: '/pms/patient/export',
    method: 'get',
    params: query
  })
}

// 查询病人基本信息详细 根据用户身份证号
export function getPatientByIdentificationNo(identificationNo) {
  return request({
    url: '/pms/patient/queryByIdentificationNo/' + identificationNo,
    method: 'get'
  })
}

// （成药）患者列表  根据成药状态 （2：未发药，3：已发药） 查询用户列表
export function medicinePatientList(data) {
  return request({
    url: '/pms/patient/medicinePatientList/',
    method: 'post',
    data: data
  })
}

// （草药）患者列表  根据草药状态 （2：未发药，3：已发药） 查询用户列表
export function herbalPatientList(data) {
  return request({
    url: '/pms/patient/herbalPatientList/',
    method: 'post',
    data: data
  })
}
