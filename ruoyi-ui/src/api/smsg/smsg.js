import request from '@/utils/request'

// 发送验证码
export function sendValidCode(data) {
  return request({
    url: '/smsg/sendValidCode',
    method: 'get',
    params: data
  })
}

export function smsLogin(data) {
  return request({
    url: '/smsg/login',
    method: 'post',
    params: data
  })
}
