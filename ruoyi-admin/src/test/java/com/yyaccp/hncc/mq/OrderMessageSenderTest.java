package com.yyaccp.hncc.mq;

import com.ruoyi.RuoYiApplication;
import com.yyaccp.hncc.mq.rabbit.OrderMessageSender;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * @author 天天向上 （john.yi@qq.com）
 * @date 2020/9/6.
 */
@SpringBootTest(classes = {RuoYiApplication.class})
@RunWith(SpringRunner.class)
public class OrderMessageSenderTest {
    @Autowired
    private OrderMessageSender orderMessageSender;

    @Test
    public void testSendNoDrugMessage() {
        OrderMessage orderMessage = new OrderMessage(33L,FeeType.CHECK, 15000L);
        orderMessageSender.sendMessage(orderMessage);
        orderMessage = new OrderMessage(34L,FeeType.TEST, 15000L);
        orderMessageSender.sendMessage(orderMessage);
        orderMessage = new OrderMessage(35L,FeeType.DISPOSITION, 15000L);
        orderMessageSender.sendMessage(orderMessage);
    }
    @Test
    public void testSendHerbalMessage() {
        OrderMessage orderMessage = new OrderMessage(1L,FeeType.HERBAL, 15000L);
        orderMessageSender.sendMessage(orderMessage);
    }
    @Test
    public void testSendMedicineMessage() {
        OrderMessage orderMessage = new OrderMessage(1L,FeeType.MEDICINE, 15000L);
        orderMessageSender.sendMessage(orderMessage);
    }
}
