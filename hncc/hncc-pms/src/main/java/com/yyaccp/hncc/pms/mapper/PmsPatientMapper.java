package com.yyaccp.hncc.pms.mapper;

import com.yyaccp.hncc.pms.domain.PmsPatient;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 病人基本信息Mapper接口
 *
 * @author ruoyi
 * @date 2020-08-27
 */
public interface PmsPatientMapper {
    /**
     * 查询病人基本信息
     * 
     * @param id 病人基本信息ID
     * @return 病人基本信息
     */
    public PmsPatient selectPmsPatientById(Long id);

    /**
     * 查询病人基本信息列表
     * 
     * @param pmsPatient 病人基本信息
     * @return 病人基本信息集合
     */
    public List<PmsPatient> selectPmsPatientList(PmsPatient pmsPatient);

    /**
     * 新增病人基本信息
     * 
     * @param pmsPatient 病人基本信息
     * @return 结果
     */
    public int insertPmsPatient(PmsPatient pmsPatient);

    /**
     * 修改病人基本信息
     * 
     * @param pmsPatient 病人基本信息
     * @return 结果
     */
    public int updatePmsPatient(PmsPatient pmsPatient);

    /**
     * 删除病人基本信息
     * 
     * @param id 病人基本信息ID
     * @return 结果
     */
    public int deletePmsPatientById(Long id);

    /**
     * 批量删除病人基本信息
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deletePmsPatientByIds(Long[] ids);
    /**
     * 两表连接查询病人基本信息和对应就诊信息
     */
    public PmsPatient selectPmsPatientRegistration(Long id);
    /**
     * 获取根据日期最大的病历号
     * @param date  容器
     * @return 病历号
     */
    public String selectMaxMedicalRecordNo(String date);

    /**
     * 查询病人基本信息  根据用户身份证号
     *
     * @param identificationNo 病人基本信息ID
     * @return 病人基本信息
     */
    public PmsPatient selectPmsPatientByIdentificationNo(@Param("identificationNo") String identificationNo);

    /**
     * （成药）患者列表
     */
    public List<PmsPatient> medicinePatientList(@Param("medicineStatus") Long medicineStatus, @Param("name") String name);

    /**
     * （草药）患者列表
     */
    List<PmsPatient> herbalPatientList(@Param("medicineStatus") Long medicineStatus, @Param("name") String name);
}
