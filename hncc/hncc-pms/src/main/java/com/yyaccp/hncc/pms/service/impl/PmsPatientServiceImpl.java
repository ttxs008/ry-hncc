package com.yyaccp.hncc.pms.service.impl;

import com.yyaccp.hncc.pms.domain.PmsPatient;
import com.yyaccp.hncc.pms.mapper.PmsPatientMapper;
import com.yyaccp.hncc.pms.service.IPmsPatientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 病人基本信息Service业务层处理
 * 
 * @author ruoyi
 * @date 2020-08-27
 */
@Service
public class PmsPatientServiceImpl implements IPmsPatientService 
{
    @Autowired
    private PmsPatientMapper pmsPatientMapper;

    /**
     * 查询病人基本信息
     * 
     * @param id 病人基本信息ID
     * @return 病人基本信息
     */
    @Override
    public PmsPatient selectPmsPatientById(Long id)
    {
        return pmsPatientMapper.selectPmsPatientById(id);
    }

    /**
     * 查询病人基本信息列表
     * 
     * @param pmsPatient 病人基本信息
     * @return 病人基本信息
     */
    @Override
    public List<PmsPatient> selectPmsPatientList(PmsPatient pmsPatient)
    {
        return pmsPatientMapper.selectPmsPatientList(pmsPatient);
    }

    /**
     * 新增病人基本信息
     * 
     * @param pmsPatient 病人基本信息
     * @return 结果
     */
    @Override
    public int insertPmsPatient(PmsPatient pmsPatient)
    {
        return pmsPatientMapper.insertPmsPatient(pmsPatient);
    }

    /**
     * 修改病人基本信息
     * 
     * @param pmsPatient 病人基本信息
     * @return 结果
     */
    @Override
    public int updatePmsPatient(PmsPatient pmsPatient)
    {
        return pmsPatientMapper.updatePmsPatient(pmsPatient);
    }

    /**
     * 批量删除病人基本信息
     * 
     * @param ids 需要删除的病人基本信息ID
     * @return 结果
     */
    @Override
    public int deletePmsPatientByIds(Long[] ids)
    {
        return pmsPatientMapper.deletePmsPatientByIds(ids);
    }

    /**
     * 删除病人基本信息信息
     * 
     * @param id 病人基本信息ID
     * @return 结果
     */
    @Override
    public int deletePmsPatientById(Long id)
    {
        return pmsPatientMapper.deletePmsPatientById(id);
    }

    @Override
    public PmsPatient selectPmsPatientRegistration(Long id) {
        return pmsPatientMapper.selectPmsPatientRegistration(id);
    }

    /**
     * 查询病人基本信息  根据用户身份证号
     *
     * @param identificationNo 身份证
     * @return 病人基本信息
     */
    @Override
    public PmsPatient selectPmsPatientByIdentificationNo(String identificationNo) {
        return pmsPatientMapper.selectPmsPatientByIdentificationNo(identificationNo);
    }

    @Override
    public List<PmsPatient> medicinePatientList(Long medicineStatus, String name) {
        return pmsPatientMapper.medicinePatientList(medicineStatus, name);
    }

    @Override
    public List<PmsPatient> herbalPatientList(Long medicineStatus, String name) {
        return pmsPatientMapper.herbalPatientList(medicineStatus, name);
    }
}
