package com.yyaccp.hncc.pms.controller;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.yyaccp.hncc.common.util.BeanCopierUtil;
import com.yyaccp.hncc.common.vo.pms.PmsPatientVO;
import com.yyaccp.hncc.pms.domain.PmsPatient;
import com.yyaccp.hncc.pms.service.IPmsPatientService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

//com.yyaccp.hncc.common模块的包引入

/**
 * 病人基本信息Controller
 *
 * @author ruoyi
 * @date 2020-08-27
 */
@Api(tags = "病人基本信息")
@RestController
@RequestMapping("/pms/patient")
public class PmsPatientController extends BaseController {
    @Autowired
    private IPmsPatientService pmsPatientService;

/**
 * 查询病人基本信息列表
 *
 */
@PreAuthorize("@ss.hasPermi('pms:patient:list')")
@GetMapping("/list")
@ApiOperation(value = "查询病人基本信息" , notes = "查询所有病人基本信息" ,
        code = 200, produces = "application/json" , protocols = "Http" ,
        response = TableDataInfo.class, httpMethod = "GET")
@ApiResponses({
        @ApiResponse(code = 204, message = "操作已经执行成功，但是没有返回数据"),
        @ApiResponse(code = 303, message = "重定向"),
        @ApiResponse(code = 400, message = "参数列表错误（缺少，格式不匹配）"),
        @ApiResponse(code = 500, message = "系统内部错误"),
        @ApiResponse(code = 404, message = "资源，服务未找到"),
        @ApiResponse(code = 200, message = "操作成功"),
        @ApiResponse(code = 401, message = "未授权"),
        @ApiResponse(code = 403, message = "访问受限，授权过期")
})
@ApiParam(name = "PmsPatientVO对象" , type = "PmsPatientVO")
        public TableDataInfo list(PmsPatientVO pmsPatientVO) {
        //将Vo转化为实体
        PmsPatient pmsPatient=BeanCopierUtil.copy(pmsPatientVO,PmsPatient. class);
        startPage();
        List<PmsPatient> list = pmsPatientService.selectPmsPatientList(pmsPatient);
        TableDataInfo tableDataInfo = getDataTable(list);
        //替换集合
        tableDataInfo.setRows(BeanCopierUtil.copy(tableDataInfo.getRows(), PmsPatientVO.class));
        return tableDataInfo;
    }
    
    /**
     * 导出病人基本信息列表
     */
    @PreAuthorize("@ss.hasPermi('pms:patient:export')")
    @Log(title = "病人基本信息" , businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    @ApiOperation(value = "导出病人基本信息表" , notes = "导出所有病人基本信息" ,
            code = 200, produces = "application/json" , protocols = "Http" ,
            response = TableDataInfo.class, httpMethod = "GET")
    @ApiResponses({
            @ApiResponse(code = 204, message = "操作已经执行成功，但是没有返回数据"),
            @ApiResponse(code = 303, message = "重定向"),
            @ApiResponse(code = 400, message = "参数列表错误（缺少，格式不匹配）"),
            @ApiResponse(code = 500, message = "系统内部错误"),
            @ApiResponse(code = 404, message = "资源，服务未找到"),
            @ApiResponse(code = 200, message = "操作成功"),
            @ApiResponse(code = 401, message = "未授权"),
            @ApiResponse(code = 403, message = "访问受限，授权过期")
    })
    @ApiParam(name = "PmsPatientVO对象" , type = "PmsPatientVO")
    public AjaxResult export(PmsPatientVO pmsPatientVO) {
        //将VO转化为实体
        PmsPatient pmsPatient=BeanCopierUtil.copy(pmsPatientVO,PmsPatient. class);
        List<PmsPatientVO> list = BeanCopierUtil.copy(pmsPatientService.selectPmsPatientList(pmsPatient),PmsPatientVO.class);
        ExcelUtil<PmsPatientVO> util = new ExcelUtil<PmsPatientVO>(PmsPatientVO.class);
        return util.exportExcel(list, "patient");
    }

    /**
     * 获取病人基本信息详细信息
     */
    @PreAuthorize("@ss.hasPermi('pms:patient:query')")
    @GetMapping(value = "/{id}")
    @ApiOperation(value = "获取病人基本信息详细信息" ,
            notes = "根据病人基本信息id获取科室信息" , code = 200,
            produces = "application/json" , protocols = "Http" , response = AjaxResult.class,
            httpMethod = "GET")
    @ApiResponses({
            @ApiResponse(code = 204, message = "操作已经执行成功，但是没有返回数据"),
            @ApiResponse(code = 303, message = "重定向"),
            @ApiResponse(code = 400, message = "参数列表错误（缺少，格式不匹配）"),
            @ApiResponse(code = 500, message = "系统内部错误"),
            @ApiResponse(code = 404, message = "资源，服务未找到"),
            @ApiResponse(code = 200, message = "操作成功"),
            @ApiResponse(code = 401, message = "未授权"),
            @ApiResponse(code = 403, message = "访问受限，授权过期")
    })
    @ApiParam(name = "pmsPatient.id" , type = "Long")
    public AjaxResult getInfo(@PathVariable("id") Long id) {
        return AjaxResult.success(BeanCopierUtil.copy(pmsPatientService.selectPmsPatientById(id),PmsPatientVO.class));
    }

    /**
     * 新增病人基本信息
     */
    @PreAuthorize("@ss.hasPermi('pms:patient:add')")
    @Log(title = "新增病人基本信息" , businessType = BusinessType.INSERT)
    @PostMapping
    @ApiOperation(value = "新增病人基本信息信息" , notes = "新增病人基本信息信息" , code = 200,
            produces = "application/json" , protocols = "Http" , response = AjaxResult.class,
            httpMethod = "POST")
    @ApiResponses({
            @ApiResponse(code = 204, message = "操作已经执行成功，但是没有返回数据"),
            @ApiResponse(code = 303, message = "重定向"),
            @ApiResponse(code = 400, message = "参数列表错误（缺少，格式不匹配）"),
            @ApiResponse(code = 500, message = "系统内部错误"),
            @ApiResponse(code = 404, message = "资源，服务未找到"),
            @ApiResponse(code = 200, message = "操作成功"),
            @ApiResponse(code = 401, message = "未授权"),
            @ApiResponse(code = 403, message = "访问受限，授权过期")
    })
    @ApiParam(name = "PmsPatientVO对象" , type = "PmsPatientVO")
    public AjaxResult add(@RequestBody PmsPatientVO pmsPatientVO) {
        return toAjax(pmsPatientService.insertPmsPatient(BeanCopierUtil.copy(pmsPatientVO,PmsPatient. class)));
    }

    /**
     * 修改病人基本信息
     */
    @PreAuthorize("@ss.hasPermi('pms:patient:edit')")
    @Log(title = "病人基本信息" , businessType = BusinessType.UPDATE)
    @PutMapping
    @ApiOperation(value = "修改病人基本信息信息" , notes = "修改病人基本信息信息" , code = 200,
            produces = "application/json" , protocols = "Http" , response = AjaxResult.class,
            httpMethod = "PUT")
    @ApiResponses({
            @ApiResponse(code = 204, message = "操作已经执行成功，但是没有返回数据"),
            @ApiResponse(code = 303, message = "重定向"),
            @ApiResponse(code = 400, message = "参数列表错误（缺少，格式不匹配）"),
            @ApiResponse(code = 500, message = "系统内部错误"),
            @ApiResponse(code = 404, message = "资源，服务未找到"),
            @ApiResponse(code = 200, message = "操作成功"),
            @ApiResponse(code = 401, message = "未授权"),
            @ApiResponse(code = 403, message = "访问受限，授权过期")
    })
    @ApiParam(name = "PmsPatientVO对象" , type = "PmsPatientVO")
    public AjaxResult edit(@RequestBody PmsPatientVO pmsPatientVO) {
        return toAjax(pmsPatientService.updatePmsPatient(BeanCopierUtil.copy(pmsPatientVO,PmsPatient. class)));
    }

    /**
     * 删除病人基本信息
     */
    @PreAuthorize("@ss.hasPermi('pms:patient:remove')")
    @Log(title = "病人基本信息" , businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    @ApiOperation(value = "删除病人基本信息信息" , notes = "删除病人基本信息信息" , code = 200,
            produces = "application/json" , protocols = "Http" , response = AjaxResult.class,
            httpMethod = "DELETE")
    @ApiResponses({
            @ApiResponse(code = 204, message = "操作已经执行成功，但是没有返回数据"),
            @ApiResponse(code = 303, message = "重定向"),
            @ApiResponse(code = 400, message = "参数列表错误（缺少，格式不匹配）"),
            @ApiResponse(code = 500, message = "系统内部错误"),
            @ApiResponse(code = 404, message = "资源，服务未找到"),
            @ApiResponse(code = 200, message = "操作成功"),
            @ApiResponse(code = 401, message = "未授权"),
            @ApiResponse(code = 403, message = "访问受限，授权过期")
    })
    @ApiParam(name = "pmsPatient.id" , type = "Long[]")
    public AjaxResult remove(@PathVariable Long[] ids) {
        return toAjax(pmsPatientService.deletePmsPatientByIds(ids));
    }

    /**
     * 双表查询病人信息和对应就诊详细信息
     */
    @PreAuthorize("@ss.hasPermi('pms:patient:patientReglist')")
    @GetMapping(value = "/patAndReg/{pid}")
    @ApiOperation(value = "获取病人基本信息详细信息以及对应就诊信息(一部分)" ,
            notes = "根据病人基本信息id获取科室信息以及对应就诊信息(一部分)" , code = 200,
            produces = "application/json" , protocols = "Http" , response = AjaxResult.class,
            httpMethod = "GET")
    @ApiResponses({
            @ApiResponse(code = 204, message = "操作已经执行成功，但是没有返回数据"),
            @ApiResponse(code = 303, message = "重定向"),
            @ApiResponse(code = 400, message = "参数列表错误（缺少，格式不匹配）"),
            @ApiResponse(code = 500, message = "系统内部错误"),
            @ApiResponse(code = 404, message = "资源，服务未找到"),
            @ApiResponse(code = 200, message = "操作成功"),
            @ApiResponse(code = 401, message = "未授权"),
            @ApiResponse(code = 403, message = "访问受限，授权过期")
    })
    @ApiParam(name = "pmsPatient.id" , type = "Long")
    public AjaxResult selectPatient(@PathVariable("pid") Long id) {
        return AjaxResult.success(BeanCopierUtil.copy(pmsPatientService.selectPmsPatientRegistration(id),PmsPatientVO.class));
    }
    /**
     * 获取病人基本信息详细信息 根据身份证
     */
    @PreAuthorize("@ss.hasPermi('pms:patient:queryByIdentificationNo')")
    @GetMapping(value = "queryByIdentificationNo/{identificationNo}")
    @ApiOperation(value = "获取病人基本信息详细信息" ,
            notes = "根据病人基本信息identificationNo获取科室信息" , code = 200,
            produces = "application/json" , protocols = "Http" , response = AjaxResult.class,
            httpMethod = "GET")
    @ApiResponses({
            @ApiResponse(code = 204, message = "操作已经执行成功，但是没有返回数据"),
            @ApiResponse(code = 303, message = "重定向"),
            @ApiResponse(code = 400, message = "参数列表错误（缺少，格式不匹配）"),
            @ApiResponse(code = 500, message = "系统内部错误"),
            @ApiResponse(code = 404, message = "资源，服务未找到"),
            @ApiResponse(code = 200, message = "操作成功"),
            @ApiResponse(code = 401, message = "未授权"),
            @ApiResponse(code = 403, message = "访问受限，授权过期")
    })
    @ApiParam(name = "pmsPatient.identificationNo", type = "String")
    public AjaxResult getInfoByIdentificationNo(@PathVariable("identificationNo") String identificationNo) {
        return AjaxResult.success(BeanCopierUtil.copy(pmsPatientService.selectPmsPatientByIdentificationNo(identificationNo), PmsPatientVO.class));
    }

    /**
     * （成药）患者列表  根据成药状态 （2：未发药，3：已发药） 查询用户列表
     *
     * @return
     */
    @PostMapping(value = "/medicinePatientList/")
    public AjaxResult medicinePatientList(@RequestBody Map<String, Object> data) {
        String patientName = (String) data.get("patientName");
        Integer statusI = (Integer) data.get("medicineStatus");
        return AjaxResult.success(BeanCopierUtil.copy(pmsPatientService.medicinePatientList(statusI.longValue(), patientName), PmsPatientVO.class));
    }

    /**
     * （成药）患者列表  根据成药状态 （2：未发药，3：已发药） 查询用户列表
     *
     * @return
     */
    @PostMapping(value = "/herbalPatientList/")
    public AjaxResult herbalPatientList(@RequestBody Map<String, Object> data) {
        String patientName = (String) data.get("patientName");
        Integer statusI = (Integer) data.get("medicineStatus");
        return AjaxResult.success(BeanCopierUtil.copy(pmsPatientService.herbalPatientList(statusI.longValue(), patientName), PmsPatientVO.class));
    }
}
