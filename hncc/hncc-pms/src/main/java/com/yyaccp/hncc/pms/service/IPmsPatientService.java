package com.yyaccp.hncc.pms.service;

import com.yyaccp.hncc.pms.domain.PmsPatient;

import java.util.List;

/**
 * 病人基本信息Service接口
 *
 * @author ruoyi
 * @date 2020-08-27
 */
public interface IPmsPatientService {
    /**
     * 查询病人基本信息
     * 
     * @param id 病人基本信息ID
     * @return 病人基本信息
     */
    public PmsPatient selectPmsPatientById(Long id);

    /**
     * 查询病人基本信息列表
     * 
     * @param pmsPatient 病人基本信息
     * @return 病人基本信息集合
     */
    public List<PmsPatient> selectPmsPatientList(PmsPatient pmsPatient);

    /**
     * 新增病人基本信息
     * 
     * @param pmsPatient 病人基本信息
     * @return 结果
     */
    public int insertPmsPatient(PmsPatient pmsPatient);

    /**
     * 修改病人基本信息
     * 
     * @param pmsPatient 病人基本信息
     * @return 结果
     */
    public int updatePmsPatient(PmsPatient pmsPatient);

    /**
     * 批量删除病人基本信息
     * 
     * @param ids 需要删除的病人基本信息ID
     * @return 结果
     */
    public int deletePmsPatientByIds(Long[] ids);

    /**
     * 删除病人基本信息信息
     * 
     * @param id 病人基本信息ID
     * @return 结果
     */
    public int deletePmsPatientById(Long id);

    /**
     * 查询病人信息和对应门诊信息
     * 两表连接
     */
    public PmsPatient selectPmsPatientRegistration(Long id);

    /**
     * 查询病人基本信息  根据用户身份证号
     *
     * @param identificationNo 身份证
     * @return 病人基本信息
     */
    public PmsPatient selectPmsPatientByIdentificationNo(String identificationNo);

    /**
     * （成药）患者列表
     */
    public List<PmsPatient> medicinePatientList(Long medicineStatus, String name);

    /**
     * （草药）患者列表
     */
    public List<PmsPatient> herbalPatientList(Long medicineStatus, String name);
}
