-- 菜单 SQL
insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('科室', '2000', '1', 'dept', 'pms/dept/index', 1, 'C', '0', '0', 'pms:dept:list', '#', 'admin', '2018-03-01', 'ry', '2018-03-01', '科室菜单');

-- 按钮父菜单ID
SELECT @parentId := LAST_INSERT_ID();

-- 按钮 SQL
insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('科室查询', @parentId, '1',  '#', '', 1,  'F', '0',  '0', 'pms:dept:query',        '#', 'admin', '2018-03-01', 'ry', '2018-03-01', '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('科室新增', @parentId, '2',  '#', '', 1,  'F', '0',  '0', 'pms:dept:add',          '#', 'admin', '2018-03-01', 'ry', '2018-03-01', '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('科室修改', @parentId, '3',  '#', '', 1,  'F', '0',  '0', 'pms:dept:edit',         '#', 'admin', '2018-03-01', 'ry', '2018-03-01', '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('科室删除', @parentId, '4',  '#', '', 1,  'F', '0',  '0', 'pms:dept:remove',       '#', 'admin', '2018-03-01', 'ry', '2018-03-01', '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('科室导出', @parentId, '5',  '#', '', 1,  'F', '0',  '0', 'pms:dept:export',       '#', 'admin', '2018-03-01', 'ry', '2018-03-01', '');