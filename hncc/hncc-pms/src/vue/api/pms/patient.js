import request from '@/utils/request'

// 查询病人基本信息列表
export function listPatient(query) {
  return request({
    url: '/pms/patient/list',
    method: 'get',
    params: query
  })
}

// 查询病人基本信息详细
export function getPatient(id) {
  return request({
    url: '/pms/patient/' + id,
    method: 'get'
  })
}

// 新增病人基本信息
export function addPatient(data) {
  return request({
    url: '/pms/patient',
    method: 'post',
    data: data
  })
}

// 修改病人基本信息
export function updatePatient(data) {
  return request({
    url: '/pms/patient',
    method: 'put',
    data: data
  })
}

// 删除病人基本信息
export function delPatient(id) {
  return request({
    url: '/pms/patient/' + id,
    method: 'delete'
  })
}

// 导出病人基本信息
export function exportPatient(query) {
  return request({
    url: '/pms/patient/export',
    method: 'get',
    params: query
  })
}