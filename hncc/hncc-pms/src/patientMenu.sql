-- 菜单 SQL
insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('病人基本信息', '2000', '1', 'patient', 'pms/patient/index', 1, 'C', '0', '0', 'pms:patient:list', '#', 'admin', '2018-03-01', 'ry', '2018-03-01', '病人基本信息菜单');

-- 按钮父菜单ID
SELECT @parentId := LAST_INSERT_ID();

-- 按钮 SQL
insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('病人基本信息查询', @parentId, '1',  '#', '', 1,  'F', '0',  '0', 'pms:patient:query',        '#', 'admin', '2018-03-01', 'ry', '2018-03-01', '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('病人基本信息新增', @parentId, '2',  '#', '', 1,  'F', '0',  '0', 'pms:patient:add',          '#', 'admin', '2018-03-01', 'ry', '2018-03-01', '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('病人基本信息修改', @parentId, '3',  '#', '', 1,  'F', '0',  '0', 'pms:patient:edit',         '#', 'admin', '2018-03-01', 'ry', '2018-03-01', '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('病人基本信息删除', @parentId, '4',  '#', '', 1,  'F', '0',  '0', 'pms:patient:remove',       '#', 'admin', '2018-03-01', 'ry', '2018-03-01', '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('病人基本信息导出', @parentId, '5',  '#', '', 1,  'F', '0',  '0', 'pms:patient:export',       '#', 'admin', '2018-03-01', 'ry', '2018-03-01', '');
INSERT INTO `sys_menu`( `menu_name`, `parent_id`, `order_num`, `path`, `component`, `is_frame`, `menu_type`, `visible`, `status`, `perms`, `icon`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`) VALUES ('查询病人基本信息和对应就诊信息', @parentId, 6, '#', '', 1, 'F', '0', '0', 'pms:patient:patientReglist', '#', 'admin', '2018-03-01 00:00:00', 'ry', '2018-03-01 00:00:00', '');
--读卡权限
INSERT INTO `hncc`.`sys_menu`(`menu_id`, `menu_name`, `parent_id`, `order_num`, `path`, `component`, `is_frame`, `menu_type`, `visible`, `status`, `perms`, `icon`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`) VALUES (2163, '病人基本信息详情根据病人对象', 2138, 6, '#', NULL, 1, 'F', '0', '0', 'pms:patient:queryByIdentificationNo', '#', 'admin', '2020-08-26 23:06:12', '何磊', '2020-08-26 23:06:22', '');

