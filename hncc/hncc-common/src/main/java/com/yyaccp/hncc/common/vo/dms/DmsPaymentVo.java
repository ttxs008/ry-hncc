package com.yyaccp.hncc.common.vo.dms;

import io.swagger.annotations.ApiModelProperty;

import java.math.BigDecimal;
import java.util.List;

/**
 * 缴费对象 DmsPaymentVo
 * @author 何磊
 * @date 2020/9/9
 */
public class DmsPaymentVo {

    /** 缴费项集合 */
    @ApiModelProperty(value = "缴费项集合",dataType ="List",example = "")
    List<DmsPayVo> payList;

    /** 总金额 */
    @ApiModelProperty(value = "总金额",dataType ="BigDecimal",example = "")
    BigDecimal amount;

    /** 支付类别ID */
    @ApiModelProperty(value = "支付类别ID",dataType ="Long",example = "")
    Long catId;

    /** 挂号ID */
    @ApiModelProperty(value = "挂号ID",dataType ="Long",example = "")
    Long registerId;

    /** 发票号 */
    @ApiModelProperty(value = "发票号",dataType ="Long",example = "")
    Long invoiceNo;


    public List<DmsPayVo> getPayList() {
        return payList;
    }

    public void setPayList(List<DmsPayVo> payList) {
        this.payList = payList;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public Long getCatId() {
        return catId;
    }

    public void setCatId(Long catId) {
        this.catId = catId;
    }

    public Long getRegisterId() {
        return registerId;
    }

    public void setRegisterId(Long registerId) {
        this.registerId = registerId;
    }

    public Long getInvoiceNo() {
        return invoiceNo;
    }

    public void setInvoiceNo(Long invoiceNo) {
        this.invoiceNo = invoiceNo;
    }

    @Override
    public String toString() {
        return "DmsPaymentVo{" +
                "payList=" + payList +
                ", amount=" + amount +
                ", catId=" + catId +
                ", registerId=" + registerId +
                ", invoiceNo=" + invoiceNo +
                '}';
    }
}
