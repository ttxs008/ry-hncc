package com.yyaccp.hncc.common.vo.dms;

import java.math.BigDecimal;
import com.ruoyi.common.core.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * 草处方对象 dms_herbal_prescription_record
 *
 * @author ruoyi
 * @date 2020-09-07
 */
@ApiModel(description = "草处方" , value = "草处方对象VO")
public class DmsHerbalPrescriptionRecordVO extends BaseEntity {

                        /** id */
            @ApiModelProperty(value = "id" , example = "")
                            private Long id;

                                /** 草药处方状态：status  0作废 1未缴费 2已缴费 3已过期 */
            @ApiModelProperty(value = "草药处方状态：status  0作废 1未缴费 2已缴费 3已过期" , example = "")
                                                                                                @Excel(name = "草药处方状态：status  0作废 1未缴费 2已缴费 3已过期")
                                                private Integer status;

                                            /** 总金额 */
            @ApiModelProperty(value = "总金额" , example = "")
                                                                                                @Excel(name = "总金额")
                                                private BigDecimal amount;

                                /** 处方名 */
            @ApiModelProperty(value = "处方名" , example = "")
                                                                                                @Excel(name = "处方名")
                                                private String name;

                                /** 治法 */
            @ApiModelProperty(value = "治法" , example = "")
                                                                                                @Excel(name = "治法")
                                                private String therapy;

                                /** 治法详情 */
            @ApiModelProperty(value = "治法详情" , example = "")
                                                                                                @Excel(name = "治法详情")
                                                private String therapyDetails;

                                /** 医嘱 */
            @ApiModelProperty(value = "医嘱" , example = "")
                                                                                                @Excel(name = "医嘱")
                                                private String medicalAdvice;

                                /** 付数 */
            @ApiModelProperty(value = "付数" , example = "")
                                                                                                @Excel(name = "付数")
                                                private Long pairNum;

                                /** 门诊Id(挂号号id) */
            @ApiModelProperty(value = "门诊Id(挂号号id)" , example = "")
                                                                                                @Excel(name = "门诊Id(挂号号id)")
                                                private Long registrationId;

                                /** 频次 */
            @ApiModelProperty(value = "频次" , example = "")
                                                                                                @Excel(name = "频次")
                                                private Integer frequency;

                                /** 用法 */
            @ApiModelProperty(value = "用法" , example = "")
                                                                                                @Excel(name = "用法")
                                                private Integer usageMeans;

                                /** 类型（普诊） */
            @ApiModelProperty(value = "类型（普诊）" , example = "")
                                                                                                @Excel(name = "类型" , readConverterExp = "普=诊")
                                                private Integer type;

                                /** 开立人 */
            @ApiModelProperty(value = "开立人" , example = "")
                                                                                                @Excel(name = "开立人")
                                                private Long createStaffId;

                                                            public void setId(Long id) {
                this.id = id;
            }

            public Long getId() {
                return id;
            }
                                                        public void setStatus(Integer status) {
                this.status = status;
            }

            public Integer getStatus() {
                return status;
            }
                                                                    public void setAmount(BigDecimal amount) {
                this.amount = amount;
            }

            public BigDecimal getAmount() {
                return amount;
            }
                                                        public void setName(String name) {
                this.name = name;
            }

            public String getName() {
                return name;
            }
                                                        public void setTherapy(String therapy) {
                this.therapy = therapy;
            }

            public String getTherapy() {
                return therapy;
            }
                                                        public void setTherapyDetails(String therapyDetails) {
                this.therapyDetails = therapyDetails;
            }

            public String getTherapyDetails() {
                return therapyDetails;
            }
                                                        public void setMedicalAdvice(String medicalAdvice) {
                this.medicalAdvice = medicalAdvice;
            }

            public String getMedicalAdvice() {
                return medicalAdvice;
            }
                                                        public void setPairNum(Long pairNum) {
                this.pairNum = pairNum;
            }

            public Long getPairNum() {
                return pairNum;
            }
                                                        public void setRegistrationId(Long registrationId) {
                this.registrationId = registrationId;
            }

            public Long getRegistrationId() {
                return registrationId;
            }
                                                        public void setFrequency(Integer frequency) {
                this.frequency = frequency;
            }

            public Integer getFrequency() {
                return frequency;
            }
                                                        public void setUsageMeans(Integer usageMeans) {
                this.usageMeans = usageMeans;
            }

            public Integer getUsageMeans() {
                return usageMeans;
            }
                                                        public void setType(Integer type) {
                this.type = type;
            }

            public Integer getType() {
                return type;
            }
                                                        public void setCreateStaffId(Long createStaffId) {
                this.createStaffId = createStaffId;
            }

            public Long getCreateStaffId() {
                return createStaffId;
            }
            
    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
                                                                .append("id" , getId())
                                                                        .append("status" , getStatus())
                                                                        .append("createTime" , getCreateTime())
                                                                        .append("amount" , getAmount())
                                                                        .append("name" , getName())
                                                                        .append("therapy" , getTherapy())
                                                                        .append("therapyDetails" , getTherapyDetails())
                                                                        .append("medicalAdvice" , getMedicalAdvice())
                                                                        .append("pairNum" , getPairNum())
                                                                        .append("registrationId" , getRegistrationId())
                                                                        .append("frequency" , getFrequency())
                                                                        .append("usageMeans" , getUsageMeans())
                                                                        .append("type" , getType())
                                                                        .append("createStaffId" , getCreateStaffId())
                                    .toString();
    }
}
