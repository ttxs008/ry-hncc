package com.yyaccp.hncc.common.vo.dms;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.common.core.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.dozer.Mapping;

import java.math.BigDecimal;
import java.util.Date;

/**
 * 成药模版对象 dms_medicine_model_item
 *
 * @author 周某
 * @date 2020-08-30
 */
@ApiModel(description = "成药模版", value = "成药模版对象VO")
public class DmsMedicineModelItemVO extends BaseEntity {

    /**
     * id
     */
    @ApiModelProperty(value = "id", example = "")
    private Long id;

    /**
     * 模板id
     */
    @ApiModelProperty(value = "模板id", example = "")
    @Excel(name = "模板id")
    private Long modelId;

    /**
     * 状态
     */
    @ApiModelProperty(value = "状态", example = "")
    @Excel(name = "状态")
    private Integer status;

    /**
     * 药品id
     */
    @ApiModelProperty(value = "药品id", example = "")
    @Excel(name = "药品id")
    private Long drugId;

    /**
     * 用法
     */
    @ApiModelProperty(value = "用法", example = "")
    @Excel(name = "用法")
    private Integer medicineUsage;

    /**
     * 频率
     */
    @ApiModelProperty(value = "频率", example = "")
    @Excel(name = "频率")
    private Integer frequency;

    /**
     * 天数
     */
    @ApiModelProperty(value = "天数", example = "")
    @Excel(name = "天数")
    private Long days;

    /**
     * 数量
     */
    @ApiModelProperty(value = "数量", example = "")
    @Excel(name = "数量")
    private Long num;

    /**
     * 医嘱
     */
    @ApiModelProperty(value = "医嘱", example = "")
    @Excel(name = "医嘱")
    private String medicalAdvice;

    /**
     * 用量
     */
    @ApiModelProperty(value = "用量", example = "")
    @Excel(name = "用量")
    private Long usageNum;

    /**
     * 用法
     */
    @ApiModelProperty(value = "用法", example = "")
    @Excel(name = "用法")
    private Integer usageMeans;

    /**
     * 单位
     */
    @ApiModelProperty(value = "单位", example = "")
    @Excel(name = "单位")
    private Integer usageNumUnit;

    /**
     *药品DmsDrug的外加列
     */
    /** 药品编码 */
    @Mapping("drug.code")
    private String drugCode;

    /** 药品名称 */
    @Mapping("drug.name")
    private String drugName;

    /** 药品规格 */
    @Mapping("drug.format")
    private String drugFormat;

    /** 药品单价 */
    @Mapping("drug.price")
    private BigDecimal drugPrice;

    /** 包装单位 */
    @Mapping("drug.unit")
    private String drugUnit;

    /** 生产厂家 */
    @Mapping("drug.manufacturer")
    private String drugManufacturer;

    /** 药品剂型 */
    @Mapping("drug.dosageId")
    private Long drugDosageId;

    /** 药品类型 */
    @Mapping("drug.typeId")
    private Long drugTypeId;

    /** 拼音助记码 */
    @Mapping("drug.mnemonicCode")
    private String drugMnemonicCode;

    /** 创建时间 */
    @Mapping("drug.createDate")
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date drugCreateDate;

    /** 库存 */
    @Mapping("drug.stock")
    private Long drugStock;

    /** 通用名 */
    @Mapping("drug.genericName")
    private String drugGenericName;

    /** 状态 */
    @Mapping("drug.status")
    private Integer drugStatus;

    public String getDrugCode() {
        return drugCode;
    }

    public void setDrugCode(String drugCode) {
        this.drugCode = drugCode;
    }

    public String getDrugName() {
        return drugName;
    }

    public void setDrugName(String drugName) {
        this.drugName = drugName;
    }

    public String getDrugFormat() {
        return drugFormat;
    }

    public void setDrugFormat(String drugFormat) {
        this.drugFormat = drugFormat;
    }

    public BigDecimal getDrugPrice() {
        return drugPrice;
    }

    public void setDrugPrice(BigDecimal drugPrice) {
        this.drugPrice = drugPrice;
    }

    public String getDrugUnit() {
        return drugUnit;
    }

    public void setDrugUnit(String drugUnit) {
        this.drugUnit = drugUnit;
    }

    public String getDrugManufacturer() {
        return drugManufacturer;
    }

    public void setDrugManufacturer(String drugManufacturer) {
        this.drugManufacturer = drugManufacturer;
    }

    public Long getDrugDosageId() {
        return drugDosageId;
    }

    public void setDrugDosageId(Long drugDosageId) {
        this.drugDosageId = drugDosageId;
    }

    public Long getDrugTypeId() {
        return drugTypeId;
    }

    public void setDrugTypeId(Long drugTypeId) {
        this.drugTypeId = drugTypeId;
    }

    public String getDrugMnemonicCode() {
        return drugMnemonicCode;
    }

    public void setDrugMnemonicCode(String drugMnemonicCode) {
        this.drugMnemonicCode = drugMnemonicCode;
    }

    public Date getDrugCreateDate() {
        return drugCreateDate;
    }

    public void setDrugCreateDate(Date drugCreateDate) {
        this.drugCreateDate = drugCreateDate;
    }

    public Long getDrugStock() {
        return drugStock;
    }

    public void setDrugStock(Long drugStock) {
        this.drugStock = drugStock;
    }

    public String getDrugGenericName() {
        return drugGenericName;
    }

    public void setDrugGenericName(String drugGenericName) {
        this.drugGenericName = drugGenericName;
    }

    public Integer getDrugStatus() {
        return drugStatus;
    }

    public void setDrugStatus(Integer drugStatus) {
        this.drugStatus = drugStatus;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setModelId(Long modelId) {
        this.modelId = modelId;
    }

    public Long getModelId() {
        return modelId;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getStatus() {
        return status;
    }

    public void setDrugId(Long drugId) {
        this.drugId = drugId;
    }

    public Long getDrugId() {
        return drugId;
    }

    public void setMedicineUsage(Integer medicineUsage) {
        this.medicineUsage = medicineUsage;
    }

    public Integer getMedicineUsage() {
        return medicineUsage;
    }

    public void setFrequency(Integer frequency) {
        this.frequency = frequency;
    }

    public Integer getFrequency() {
        return frequency;
    }

    public void setDays(Long days) {
        this.days = days;
    }

    public Long getDays() {
        return days;
    }

    public void setNum(Long num) {
        this.num = num;
    }

    public Long getNum() {
        return num;
    }

    public void setMedicalAdvice(String medicalAdvice) {
        this.medicalAdvice = medicalAdvice;
    }

    public String getMedicalAdvice() {
        return medicalAdvice;
    }

    public void setUsageNum(Long usageNum) {
        this.usageNum = usageNum;
    }

    public Long getUsageNum() {
        return usageNum;
    }

    public void setUsageMeans(Integer usageMeans) {
        this.usageMeans = usageMeans;
    }

    public Integer getUsageMeans() {
        return usageMeans;
    }

    public void setUsageNumUnit(Integer usageNumUnit) {
        this.usageNumUnit = usageNumUnit;
    }

    public Integer getUsageNumUnit() {
        return usageNumUnit;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
                .append("id", getId())
                .append("modelId", getModelId())
                .append("status", getStatus())
                .append("drugId", getDrugId())
                .append("medicineUsage", getMedicineUsage())
                .append("frequency", getFrequency())
                .append("days", getDays())
                .append("num", getNum())
                .append("medicalAdvice", getMedicalAdvice())
                .append("usageNum", getUsageNum())
                .append("usageMeans", getUsageMeans())
                .append("usageNumUnit", getUsageNumUnit())
                .toString();
    }
}
