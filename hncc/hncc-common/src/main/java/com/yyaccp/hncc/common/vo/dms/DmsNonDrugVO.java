package com.yyaccp.hncc.common.vo.dms;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.common.annotation.Excel;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.math.BigDecimal;
import java.util.Date;

/**
 * 非药品收费项目VO对象
 */
@ApiModel(description = "非药品收费项目对象",value = "非药品收费项目对象")
public class DmsNonDrugVO {

    /**
     * id
     */
    private Long id;

    /**
     * 项目编码
     */
    @Excel(name = "项目编码")
    private String code;

    /**
     * 项目名称
     */
    @Excel(name = "项目名称")
    private String name;

    /**
     * 规格
     */
    @Excel(name = "规格")
    private String format;

    /**
     * 价格
     */
    @Excel(name = "价格")
    private BigDecimal price;

    /**
     * 所属费用科目ID
     */
    @Excel(name = "所属费用科目ID")
    private Long expClassId;

    /**
     * 拼音助记码
     */
    @Excel(name = "拼音助记码")
    private String mnemonicCode;

    /**
     * 1检查2检验3处置
     */
    @Excel(name = "1检查2检验3处置")
    private Long recordType;

    /**
     * 创建时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "创建时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date createDate;

    /**
     * 状态
     */
    @Excel(name = "状态")
    private Long status;

    /**
     * 执行科室ID
     */
    @Excel(name = "执行科室ID")
    private Long deptId;

    /**
     * 执行科室名称
     */
    @Excel(name = "执行科室名称")
    private String deptName;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFormat() {
        return format;
    }

    public void setFormat(String format) {
        this.format = format;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public Long getExpClassId() {
        return expClassId;
    }

    public void setExpClassId(Long expClassId) {
        this.expClassId = expClassId;
    }

    public String getMnemonicCode() {
        return mnemonicCode;
    }

    public void setMnemonicCode(String mnemonicCode) {
        this.mnemonicCode = mnemonicCode;
    }

    public Long getRecordType() {
        return recordType;
    }

    public void setRecordType(Long recordType) {
        this.recordType = recordType;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Long getStatus() {
        return status;
    }

    public void setStatus(Long status) {
        this.status = status;
    }

    public Long getDeptId() {
        return deptId;
    }

    public void setDeptId(Long deptId) {
        this.deptId = deptId;
    }

    public String getDeptName() {
        return deptName;
    }

    public void setDeptName(String deptName) {
        this.deptName = deptName;
    }
}
