package com.yyaccp.hncc.common.vo.dms;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.common.core.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * 就诊(门诊)信息对象 dms_registration
 *
 * @author ruoyi
 * @date 2020-08-20
 */
@ApiModel(description = "就诊(门诊)信息" , value = "就诊(门诊)信息对象VO")
public class DmsRegistrationVO extends BaseEntity {

                        /** id */
            @ApiModelProperty(value = "id" , example = "")
                            private Long id;

                                /** 患者id */
            @ApiModelProperty(value = "患者id" , example = "")
                                                                                                @Excel(name = "患者id")
                                                private Long patientId;
    @ApiModelProperty(value = "患者名称" , example = "")
    @Excel(name = "患者名称")
    private String name;
    @ApiModelProperty(value = "" , example = "")
    @Excel(name = "病例号")
    private String medicalRecordNo;

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    @ApiModelProperty(value = "性别" , example = "")
    @Excel(name = "性别")
    private String gender;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMedicalRecordNo() {
        return medicalRecordNo;
    }

    public void setMedicalRecordNo(String medicalRecordNo) {
        this.medicalRecordNo = medicalRecordNo;
    }

    /** 诊断状态 */
            @ApiModelProperty(value = "诊断状态" , example = "")
                                                                                                @Excel(name = "诊断状态")
                                                private Integer endAttendance;

                                /** 状态 */
            @ApiModelProperty(value = "状态" , example = "")
                                                                                                @Excel(name = "状态")
                                                private Integer status;

                                /** 排班id */
            @ApiModelProperty(value = "排班id" , example = "")
                                                                                                @Excel(name = "排班id")
                                                private Long skdId;

                                /** 是否需要病历本 */
            @ApiModelProperty(value = "是否需要病历本" , example = "")
                                                                                                @Excel(name = "是否需要病历本")
                                                private Integer needBook;

                                /** 绑定状态 */
            @ApiModelProperty(value = "绑定状态" , example = "")
                                                                                                @Excel(name = "绑定状态")
                                                private Integer bindStatus;

                                /** 科室id */
            @ApiModelProperty(value = "科室id" , example = "")
                                                                                                @Excel(name = "科室id")
                                                private Long deptId;

                                /** 就诊日期 */
            @ApiModelProperty(value = "就诊日期" , example = "")
                                                                                                @JsonFormat(pattern = "yyyy-MM-dd")
                    @Excel(name = "就诊日期" , width = 30, dateFormat = "yyyy-MM-dd")
                                                private Date attendanceDate;

                                /** 就诊年龄 */
            @ApiModelProperty(value = "就诊年龄" , example = "")
                                                                                                @Excel(name = "就诊年龄")
                                                private String patientAgeStr;

                                                            public void setId(Long id) {
                this.id = id;
            }

            public Long getId() {
                return id;
            }
                                                        public void setPatientId(Long patientId) {
                this.patientId = patientId;
            }

            public Long getPatientId() {
                return patientId;
            }
                                                                    public void setEndAttendance(Integer endAttendance) {
                this.endAttendance = endAttendance;
            }

            public Integer getEndAttendance() {
                return endAttendance;
            }
                                                        public void setStatus(Integer status) {
                this.status = status;
            }

            public Integer getStatus() {
                return status;
            }
                                                        public void setSkdId(Long skdId) {
                this.skdId = skdId;
            }

            public Long getSkdId() {
                return skdId;
            }
                                                        public void setNeedBook(Integer needBook) {
                this.needBook = needBook;
            }

            public Integer getNeedBook() {
                return needBook;
            }
                                                        public void setBindStatus(Integer bindStatus) {
                this.bindStatus = bindStatus;
            }

            public Integer getBindStatus() {
                return bindStatus;
            }
                                                        public void setDeptId(Long deptId) {
                this.deptId = deptId;
            }

            public Long getDeptId() {
                return deptId;
            }
                                                        public void setAttendanceDate(Date attendanceDate) {
                this.attendanceDate = attendanceDate;
            }

            public Date getAttendanceDate() {
                return attendanceDate;
            }
                                                        public void setPatientAgeStr(String patientAgeStr) {
                this.patientAgeStr = patientAgeStr;
            }

            public String getPatientAgeStr() {
                return patientAgeStr;
            }
            
    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
                                                                .append("id" , getId())
                                                                        .append("patientId" , getPatientId())
                                                                        .append("createTime" , getCreateTime())
                                                                        .append("endAttendance" , getEndAttendance())
                                                                        .append("status" , getStatus())
                                                                        .append("skdId" , getSkdId())
                                                                        .append("needBook" , getNeedBook())
                                                                        .append("bindStatus" , getBindStatus())
                                                                        .append("deptId" , getDeptId())
                                                                        .append("attendanceDate" , getAttendanceDate())
                                                                        .append("patientAgeStr" , getPatientAgeStr())
                                                                        .append("name", getName())
                                                                        .append("medicalRecordNo", getMedicalRecordNo())
                .append("gender", getGender())
                                    .toString();
    }
}
