package com.yyaccp.hncc.common.vo.bms;

import java.math.BigDecimal;
import com.ruoyi.common.core.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * 发票对象 bms_invoice_record
 *
 * @author 何磊
 * @date 2020-08-27
 */
@ApiModel(description = "发票" , value = "发票对象VO")
public class BmsInvoiceRecordVO extends BaseEntity {

                        /** id */
            @ApiModelProperty(value = "id" , example = "")
                            private Long id;

                                            /** 发票号 */
            @ApiModelProperty(value = "发票号" , example = "")
                                                                                                @Excel(name = "发票号")
                                                private Long invoiceNo;

                                /** 账单id */
            @ApiModelProperty(value = "账单id" , example = "")
                                                                                                @Excel(name = "账单id")
                                                private Long billId;

                                /** 总金额 */
            @ApiModelProperty(value = "总金额" , example = "")
                                                                                                @Excel(name = "总金额")
                                                private BigDecimal amount;

                                /** 冻结状态 */
            @ApiModelProperty(value = "冻结状态" , example = "")
                                                                                                @Excel(name = "冻结状态")
                                                private Integer freezeStatus;

                                /** 关联发票id */
            @ApiModelProperty(value = "关联发票id" , example = "")
                                                                                                @Excel(name = "关联发票id")
                                                private Long associateId;

                                /** 对账人id */
            @ApiModelProperty(value = "对账人id" , example = "")
                                                                                                @Excel(name = "对账人id")
                                                private Long operatorId;

                                /** 支付类别id */
            @ApiModelProperty(value = "支付类别id" , example = "")
                                                                                                @Excel(name = "支付类别id")
                                                private Long settlementCatId;

                                /** 所属日结记录id */
            @ApiModelProperty(value = "所属日结记录id" , example = "")
                                                                                                @Excel(name = "所属日结记录id")
                                                private Long settleRecordId;

                                /** $column.columnComment */
            @ApiModelProperty(value = "$column.columnComment" , example = "")
                                                                                                @Excel(name = "所属日结记录id")
                                                private String itemList;

                                /** 类型 */
            @ApiModelProperty(value = "类型" , example = "")
                                                                                                @Excel(name = "类型")
                                                private Integer type;

                                                            public void setId(Long id) {
                this.id = id;
            }

            public Long getId() {
                return id;
            }
                                                                    public void setInvoiceNo(Long invoiceNo) {
                this.invoiceNo = invoiceNo;
            }

            public Long getInvoiceNo() {
                return invoiceNo;
            }
                                                        public void setBillId(Long billId) {
                this.billId = billId;
            }

            public Long getBillId() {
                return billId;
            }
                                                        public void setAmount(BigDecimal amount) {
                this.amount = amount;
            }

            public BigDecimal getAmount() {
                return amount;
            }
                                                        public void setFreezeStatus(Integer freezeStatus) {
                this.freezeStatus = freezeStatus;
            }

            public Integer getFreezeStatus() {
                return freezeStatus;
            }
                                                        public void setAssociateId(Long associateId) {
                this.associateId = associateId;
            }

            public Long getAssociateId() {
                return associateId;
            }
                                                        public void setOperatorId(Long operatorId) {
                this.operatorId = operatorId;
            }

            public Long getOperatorId() {
                return operatorId;
            }
                                                        public void setSettlementCatId(Long settlementCatId) {
                this.settlementCatId = settlementCatId;
            }

            public Long getSettlementCatId() {
                return settlementCatId;
            }
                                                        public void setSettleRecordId(Long settleRecordId) {
                this.settleRecordId = settleRecordId;
            }

            public Long getSettleRecordId() {
                return settleRecordId;
            }
                                                        public void setItemList(String itemList) {
                this.itemList = itemList;
            }

            public String getItemList() {
                return itemList;
            }
                                                        public void setType(Integer type) {
                this.type = type;
            }

            public Integer getType() {
                return type;
            }
            
    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
                                                                .append("id" , getId())
                                                                        .append("createTime" , getCreateTime())
                                                                        .append("invoiceNo" , getInvoiceNo())
                                                                        .append("billId" , getBillId())
                                                                        .append("amount" , getAmount())
                                                                        .append("freezeStatus" , getFreezeStatus())
                                                                        .append("associateId" , getAssociateId())
                                                                        .append("operatorId" , getOperatorId())
                                                                        .append("settlementCatId" , getSettlementCatId())
                                                                        .append("settleRecordId" , getSettleRecordId())
                                                                        .append("itemList" , getItemList())
                                                                        .append("type" , getType())
                                    .toString();
    }
}
