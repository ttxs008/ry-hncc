package com.yyaccp.hncc.common.vo.dms;

import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * 诊断类型（疾病）目录对象 dms_dise_catalog
 * 
 * @author ruoyi
 * @date 2020-08-12
 */
@ApiModel(description = "诊断类型（疾病）目录对象" ,value = "诊断类型（疾病）目录对象VO")
public class DmsDiseCatalogVO
{
    /** id */
    @ApiModelProperty(value = "对象id",example = "1")
    private Long id;

    /** 疾病分类名 */
    @Excel(name = "疾病分类名")
    @ApiModelProperty(value = "疾病分类名",example = "白")
    private String name;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setName(String name) 
    {
        this.name = name;
    }

    public String getName() 
    {
        return name;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("name", getName())
            .toString();
    }
}
