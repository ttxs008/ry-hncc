package com.yyaccp.hncc.common.util;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author 何磊
 * @date 2020/8/28
 */
public class DateToNoUtil {

    /**
     * 日期编码格式
     */
    public static String YYYYMMDDHHMMSS = "yyyyMMddHHmmss";

    public static String YYYYMMDDHHMM = "yyyyMMddHHmm";

    public static String YYYYMMDD = "yyyyMMdd";


    /**
     *  根据当前日期生成单号  前面补充0
     * @param pattern       模式格式
     * @param initLength    长度为 4  不够使用 0 补充
     * @param initNo        初始单号
     * @return  单号
     */
    public static String DateToNoAndIncrease(String pattern,Integer initLength,String initNo){
        SimpleDateFormat sdf =  new SimpleDateFormat(pattern);
        String dataFormat = sdf.format(new Date());
        int num=1;
        if(initNo!=null){
                num = Integer.parseInt(initNo.substring(dataFormat.length()))+1;
        }
        String noSuffix=  String.format("%0"+initLength+"d",num);
        return dataFormat+noSuffix;
    }
}
