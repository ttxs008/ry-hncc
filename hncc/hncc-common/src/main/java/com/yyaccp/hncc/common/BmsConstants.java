package com.yyaccp.hncc.common;

@Deprecated
public interface BmsConstants {

    /**
     * 发票类型-正常
     */
    Integer INVOICE_TYPE_NORMAL = 1;

    /**
     * 发票类型-冲红
     */
    Integer INVOICE_TYPE_RED = 2;

    /**
     * 发票类型-退费
     */
    Integer INVOICE_TYPE_REFUND = 3;
}
