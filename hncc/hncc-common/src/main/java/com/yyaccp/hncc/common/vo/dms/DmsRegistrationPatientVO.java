package com.yyaccp.hncc.common.vo.dms;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.entity.SysDept;
import com.yyaccp.hncc.common.vo.pms.PmsPatientVO;
import io.swagger.annotations.ApiModelProperty;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @author 何磊
 * @date 2020/8/30
 */
public class DmsRegistrationPatientVO {

    /**
     * id
     */
    @ApiModelProperty(value = "id", example = "")
    private Long id;

    /**
     * 诊断状态
     */
    @ApiModelProperty(value = "诊断状态", example = "")
    @Excel(name = "诊断状态")
    private Integer endAttendance;
    /**
     * 状态
     */
    @ApiModelProperty(value = "状态", example = "")
    @Excel(name = "状态")
    private Integer status;
    /**
     * 排班id
     */
    @ApiModelProperty(value = "排班id", example = "")
    @Excel(name = "排班id")
    private Long skdId;
    /**
     * 是否需要病历本
     */
    @ApiModelProperty(value = "是否需要病历本", example = "")
    @Excel(name = "是否需要病历本")
    private Integer needBook;
    /**
     * 绑定状态
     */
    @ApiModelProperty(value = "绑定状态", example = "")
    @Excel(name = "绑定状态")
    private Integer bindStatus;
    /**
     * 科室id
     */
    @ApiModelProperty(value = "科室id", example = "")
    @Excel(name = "科室id")
    private Long deptId;
    /**
     * 就诊日期
     */
    @ApiModelProperty(value = "就诊日期", example = "")
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "就诊日期", width = 30, dateFormat = "yyyy-MM-dd")
    private Date attendanceDate;
    /**
     * 就诊年龄
     */
    @ApiModelProperty(value = "就诊年龄", example = "")
    @Excel(name = "就诊年龄")
    private String patientAgeStr;

    /**
     * 挂号级别id
     */
    @Excel(name = "号别id")
    @ApiModelProperty(value = "号别id", example = "")
    private Long registrationRankId;

    /**
     * 午别
     */
    @Excel(name = "午别")
    @ApiModelProperty(value = "午别", example = "")
    private Long noon;
    /**
     * 医生id
     */
    @Excel(name = "医生id")
    @ApiModelProperty(value = "医生id", example = "")
    private Long staffId;

    /**
     * 应收金额
     */
    @Excel(name = "应收金额")
    @ApiModelProperty(value = "应收金额", example = "")
    private BigDecimal price;


    /**
     * 支付方式id
     */
    @Excel(name = "支付方式id")
    @ApiModelProperty(value = "支付方式id", example = "")
    private Long catId;

    /**
     * 发票号
     */
    @Excel(name = "发票号")
    @ApiModelProperty(value = "发票号", example = "")
    private Long invoiceNo;

    /**
     * 患者对象VO
     */
    @Excel(name = "患者对象VO")
    @ApiModelProperty(value = "患者对象VO", example = "")
    private PmsPatientVO pmsPatient;

    /**
     * 部门对象
     */
    @Excel(name = "部门对象")
    @ApiModelProperty(value = "部门对象", example = "")
    private SysDept sysDept;


    public SysDept getSysDept() {
        return sysDept;
    }

    public void setSysDept(SysDept sysDept) {
        this.sysDept = sysDept;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getEndAttendance() {
        return endAttendance;
    }

    public void setEndAttendance(Integer endAttendance) {
        this.endAttendance = endAttendance;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Long getSkdId() {
        return skdId;
    }

    public void setSkdId(Long skdId) {
        this.skdId = skdId;
    }

    public Integer getNeedBook() {
        return needBook;
    }

    public void setNeedBook(Integer needBook) {
        this.needBook = needBook;
    }

    public Integer getBindStatus() {
        return bindStatus;
    }

    public void setBindStatus(Integer bindStatus) {
        this.bindStatus = bindStatus;
    }

    public Long getDeptId() {
        return deptId;
    }

    public void setDeptId(Long deptId) {
        this.deptId = deptId;
    }

    public Date getAttendanceDate() {
        return attendanceDate;
    }

    public void setAttendanceDate(Date attendanceDate) {
        this.attendanceDate = attendanceDate;
    }

    public String getPatientAgeStr() {
        return patientAgeStr;
    }

    public void setPatientAgeStr(String patientAgeStr) {
        this.patientAgeStr = patientAgeStr;
    }



    public Long getRegistrationRankId() {
        return registrationRankId;
    }

    public void setRegistrationRankId(Long registrationRankId) {
        this.registrationRankId = registrationRankId;
    }

    public Long getNoon() {
        return noon;
    }

    public void setNoon(Long noon) {
        this.noon = noon;
    }

    public Long getStaffId() {
        return staffId;
    }

    public void setStaffId(Long staffId) {
        this.staffId = staffId;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public Long getCatId() {
        return catId;
    }

    public void setCatId(Long catId) {
        this.catId = catId;
    }

    public Long getInvoiceNo() {
        return invoiceNo;
    }

    public void setInvoiceNo(Long invoiceNo) {
        this.invoiceNo = invoiceNo;
    }

    public PmsPatientVO getPmsPatient() {
        return pmsPatient;
    }

    public void setPmsPatient(PmsPatientVO pmsPatient) {
        this.pmsPatient = pmsPatient;
    }

    @Override
    public String toString() {
        return "DmsRegistrationPatientVO{" +
                "id=" + id +
                ", endAttendance=" + endAttendance +
                ", status=" + status +
                ", skdId=" + skdId +
                ", needBook=" + needBook +
                ", bindStatus=" + bindStatus +
                ", deptId=" + deptId +
                ", attendanceDate=" + attendanceDate +
                ", patientAgeStr='" + patientAgeStr + '\'' +
                ", registrationRankId=" + registrationRankId +
                ", noon=" + noon +
                ", staffId=" + staffId +
                ", price=" + price +
                ", catId=" + catId +
                ", invoiceNo=" + invoiceNo +
                ", pmsPatient=" + pmsPatient +
                '}';
    }
}
