package com.yyaccp.hncc.common.vo.dms;

import com.ruoyi.common.core.domain.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.math.BigDecimal;

/**
 * @author 毛
 * @Date 2020/8/19
 */
@ApiModel(description = "常用项",value ="常用项VO" )
public class DmsFrequentUsedVO extends BaseEntity {


    @Override
    public String toString() {
        return "DmsFrequentUsedVO{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", price='" + price + '\'' +
                ", projectType='" + projectType + '\'' +
                ", projectCode='" + code + '\'' +
                '}';
    }



    /**
     * 项目id
     */
    @ApiModelProperty(value = "项目id",example = "1")
    private Integer id;
    /**
     * 项目名称
     */
    @ApiModelProperty(value = "项目名称",example = "普通诊断")
    private String name;
    /**
     * 项目单价
     */
    @ApiModelProperty(value = "项目单价",example = "888")
    private BigDecimal price ;
    /**
     * 项目编码
     */
    @ApiModelProperty(value = "项目编码",example = "20******")
    private String code;
    /**
     * 项目类别
     */
    private Integer projectType;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Integer getProjectType() {
        return projectType;
    }

    public void setProjectType(Integer projectType) {
        this.projectType = projectType;
    }
}
