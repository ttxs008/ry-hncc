package com.yyaccp.hncc.common.vo.dms;

import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * 非药品模版对象 dms_non_drug_model
 *
 * @author yugui
 * @date 2020-08-17
 */
@ApiModel(description = "非药品模版", value = "非药品模版对象VO")
public class DmsNonDrugModelVO extends BaseEntity {

    /**
     * 模版编号
     */
    @ApiModelProperty(value = "模版编号", example = "")
    private Long id;

    /**
     * 模版状态
     */
    @ApiModelProperty(value = "模版状态", example = "")
    private Long status;

    /**
     * 模版名称
     */
    @ApiModelProperty(value = "模版名称", example = "")
    @Excel(name = "模版名称")
    private String name;

    /**
     * 模版所包括非药品的id
     */
    @ApiModelProperty(value = "模版所包括非药品的id", example = "")
    @Excel(name = "模版所包括非药品的id")
    private String nonDrugIdList;

    /**
     * 模版范围
     */
    @ApiModelProperty(value = "模版范围", example = "")
    @Excel(name = "模版范围")
    private Long scope;

    /**
     * 所属人Id
     */
    @ApiModelProperty(value = "所属人Id", example = "")
    private Long ownId;

    /**
     * 模版简介
     */
    @ApiModelProperty(value = "模版简介", example = "")
    @Excel(name = "模版简介")
    private String aim;

    /**
     * 模版编码
     */
    @ApiModelProperty(value = "模版编码", example = "")
    @Excel(name = "模版编码")
    private String code;

    /**
     * 模版类型
     */
    @ApiModelProperty(value = "模版类型", example = "")
    @Excel(name = "模版类型")
    private Long type;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getStatus() {
        return status;
    }

    public void setStatus(Long status) {
        this.status = status;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNonDrugIdList() {
        return nonDrugIdList;
    }

    public void setNonDrugIdList(String nonDrugIdList) {
        this.nonDrugIdList = nonDrugIdList;
    }

    public Long getScope() {
        return scope;
    }

    public void setScope(Long scope) {
        this.scope = scope;
    }

    public Long getOwnId() {
        return ownId;
    }

    public void setOwnId(Long ownId) {
        this.ownId = ownId;
    }

    public String getAim() {
        return aim;
    }

    public void setAim(String aim) {
        this.aim = aim;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Long getType() {
        return type;
    }

    public void setType(Long type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
                .append("id", getId())
                .append("status", getStatus())
                .append("name", getName())
                .append("nonDrugIdList", getNonDrugIdList())
                .append("scope", getScope())
                .append("ownId", getOwnId())
                .append("aim", getAim())
                .append("createTime", getCreateTime())
                .append("code", getCode())
                .append("type", getType())
                .toString();
    }
}
