package com.yyaccp.hncc.common;

public interface DmsConstants {

    /**
     * 非药品收费项目 项目类型ID  1检查2检验3处置
     */
    @Deprecated
    Integer TYPE_EXAMINE = 1;
    @Deprecated
    Integer TYPE_INSPECT = 2;
    @Deprecated
    Integer TYPE_HANDLE = 3;

    /**
     * 诊断项目  数据字典中常用项编码为4
     */
    Integer TYPE_DIAGNOSE = 4;
    /**
     * 药品收费项目 药品类型ID  101西药102中成药103中药
     */
    @Deprecated
    Integer TYPE_WESTERN_MEDICINE = 101;
    @Deprecated
    Integer TYPE_CHINESE_PATENT_MEDICINE = 102;
    @Deprecated
    Integer TYPE_CHINESE_MEDICINE = 103;


    /**
     * 非药品收费项目 对应项目类型初始编码
     */
    Integer CODE_EXAMINE = 12000001;
    Integer CODE_INSPECT = 21000001;
    Integer CODE_HANDLE = 24000001;
    /**
     * 病例模板目录状态-启用
     */
    @Deprecated
    Integer CASE_MODEL_CATALOG_STATUS_ENABLED = 1;
    /**
     * 病例模板目录状态-禁用
     */
    @Deprecated
    Integer CASE_MODEL_CATALOG_STATUS_DISABLED = 0;
    /**
     * 病例模板目录类型-目录
     */
    @Deprecated
    Integer CASE_MODEL_CATALOG_TYPE_CATALOG = 1;
    /**
     * 病例模板目录类型-模板
     */
    @Deprecated
    Integer CASE_MODEL_CATALOG_TYPE_MODEL = 2;
    /**
     * 病例模板状态-启用
     */
    @Deprecated
    Integer CASE_MODEL_STATUS_ENABLED = 1;
    /**
     * 病例模板状态-禁用
     */
    @Deprecated
    Integer CASE_MODEL_STATUS_DISABLED = 0;
    /**
     * Redis存入的成药处方文件夹
     */
    String PRESCRIPTION_FOLDER = "medicine_prescription_records";
    /**
     * Redis存入的处置表文件夹
     */
    String DISPOSAL = "non_drug_item_record_DISPOSAL";
    /**
     * Redis存入的检查表文件夹
     */
    String  INSPECTION= "non_drug_item_record_INSPECTION";
    /**
     * Redis存入的检验表文件夹
     */
    String TEST = "non_drug_item_record_TEST";
    /**
     * Redis存入对应患者挂号ID下的key值
     */
    String CORRESPONDING_REGISTRATION = "registrationID_MPRs";


    /**
     *   挂号信息状态-未看诊
     */
    @Deprecated
    Integer REGISTRATION_STATUS_NOT_VISIT= 1;

    /**
     *  挂号信息状态-待收费
     */
    @Deprecated
    Integer REGISTRATION_STATUS_STAY_CHARGE= 2;

    /**
     *  挂号信息状态-诊毕
     */
    @Deprecated
    Integer REGISTRATION_STATUS_FINISH_VISIT= 3;

    /**
     *  挂号信息状态-已退号
     */
    @Deprecated
    Integer REGISTRATION_STATUS_YES_BOUNCE= 4;


}
