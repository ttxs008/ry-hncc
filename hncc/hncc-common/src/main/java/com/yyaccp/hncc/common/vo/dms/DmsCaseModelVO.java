package com.yyaccp.hncc.common.vo.dms;

import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 *
 * Created by 天天向上 （john.yi@qq.com） on 2020/8/17.
 */
@Data
@ApiModel(description = "病例模板对象，关联病例目录" ,value = "病例模板对象")
public class DmsCaseModelVO extends BaseEntity {
    /** 模板id */
    private Long id;

    /** 主诉 */
    @ApiModelProperty(name = "主诉", example = "无")
    private String chiefComplaint;

    /** 症状 */
    @ApiModelProperty(name = "症状", example = "无")
    private String historyOfPresentIllness;

    /** 历史治疗 */
    @ApiModelProperty(name = "历史治疗", example = "无")
    private String historyOfTreatment;

    /** 既往史 */
    @ApiModelProperty(name = "既往史", example = "无")
    private String pastHistory;

    /** 过敏史 */
    @ApiModelProperty(name = "过敏史", example = "无")
    private String allergies;

    /** 健康检查 */
    @ApiModelProperty(name = "健康检查", example = "无")
    private String healthCheckup;

    /** 初步诊断Id串 */
    @ApiModelProperty(name = "初步诊断Id串")
    private String priliminaryDiseIdList;

    /** 初诊诊断id串对应诊断名字串 */
    @ApiModelProperty(name = "初诊诊断id串对应诊断名字串")
    private String priliminaryDiseStrList;

    /** 病例模板名 */
    @ApiModelProperty(name = "病例模板名", example = "无")
    private String name;

    /** 状态 */
    @ApiModelProperty(name = "状态", example = "无")
    private Integer status;

}
