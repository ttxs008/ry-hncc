package com.yyaccp.hncc.common.vo.bms;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 结算类别对象 bms_settlement_cat
 *
 * @author ruoyi
 * @date 2020-08-15
 */
@ApiModel(description = "结账类型目录对象",value = "结账类型目录对象VO")
public class BmsSettlementCatVO extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** id */
    @ApiModelProperty(value = "结账类型id",example = "1")
    private Long id;

    /** 支付类别名字 */
    @Excel(name = "支付类别名字")
    @ApiModelProperty(value = "结账类别名称",example = "现金")
    private String name;

    /** 状态 */
    @Excel(name = "状态")
    @ApiModelProperty(value = "结账类型状态",example = "1")
    private Integer status;

    /** 代码 */
    @Excel(name = "代码")
    @ApiModelProperty(value = "结账类型代码",example = "cash")
    private String code;

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getId()
    {
        return id;
    }
    public void setName(String name)
    {
        this.name = name;
    }

    public String getName()
    {
        return name;
    }
    public void setStatus(Integer status)
    {
        this.status = status;
    }

    public Integer getStatus()
    {
        return status;
    }
    public void setCode(String code)
    {
        this.code = code;
    }

    public String getCode()
    {
        return code;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
                .append("id", getId())
                .append("name", getName())
                .append("status", getStatus())
                .append("code", getCode())
                .toString();
    }
}
