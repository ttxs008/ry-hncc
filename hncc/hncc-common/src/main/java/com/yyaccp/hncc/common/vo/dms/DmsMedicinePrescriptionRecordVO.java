package com.yyaccp.hncc.common.vo.dms;

import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.math.BigDecimal;
import java.util.List;

/**
 * 成药处方对象 dms_medicine_prescription_record
 *
 * @author ruoyi
 * @date 2020-08-26
 */
@ApiModel(description = "成药处方", value = "成药处方对象VO")
public class DmsMedicinePrescriptionRecordVO extends BaseEntity {

    /**
     * id
     */
    @ApiModelProperty(value = "id", example = "")
    private Long id;

    /**
     * 成药处方状态
     */
    @ApiModelProperty(value = "成药处方状态", example = "")
    @Excel(name = "成药处方状态")
    private Long status;

    /**
     * 总金额
     */
    @ApiModelProperty(value = "总金额", example = "")
    @Excel(name = "总金额")
    private BigDecimal amount;

    /**
     * 处方名
     */
    @ApiModelProperty(value = "处方名", example = "")
    @Excel(name = "处方名")
    private String name;

    /**
     * 挂号id
     */
    @ApiModelProperty(value = "挂号id", example = "")
    @Excel(name = "挂号id")
    private Long registrationId;

    /**
     * 退款状态
     */
    @ApiModelProperty(value = "退款状态", example = "")
    @Excel(name = "退款状态")
    private Long refundStatus;

    /**
     * 类型（普诊）
     */
    @ApiModelProperty(value = "类型（普诊）", example = "")
    @Excel(name = "类型", readConverterExp = "普=诊")
    private Long type;

    /**
     * 开立人
     */
    @ApiModelProperty(value = "开立人", example = "")
    @Excel(name = "开立人")
    private Long createStaffId;

    /**
     * 成药处方下的成药记录集合
     */
    private List<DmsMedicineItemRecordVO> medicineItemRecords;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getStatus() {
        return status;
    }

    public void setStatus(Long status) {
        this.status = status;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getRegistrationId() {
        return registrationId;
    }

    public void setRegistrationId(Long registrationId) {
        this.registrationId = registrationId;
    }

    public Long getRefundStatus() {
        return refundStatus;
    }

    public void setRefundStatus(Long refundStatus) {
        this.refundStatus = refundStatus;
    }

    public Long getType() {
        return type;
    }

    public void setType(Long type) {
        this.type = type;
    }

    public Long getCreateStaffId() {
        return createStaffId;
    }

    public void setCreateStaffId(Long createStaffId) {
        this.createStaffId = createStaffId;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
                .append("id", getId())
                .append("status", getStatus())
                .append("createTime", getCreateTime())
                .append("amount", getAmount())
                .append("name", getName())
                .append("registrationId", getRegistrationId())
                .append("refundStatus", getRefundStatus())
                .append("type", getType())
                .append("createStaffId", getCreateStaffId())
                .toString();
    }

    public List<DmsMedicineItemRecordVO> getMedicineItemRecords() {
        return medicineItemRecords;
    }

    public void setMedicineItemRecords(List<DmsMedicineItemRecordVO> medicineItemRecords) {
        this.medicineItemRecords = medicineItemRecords;
    }
}
