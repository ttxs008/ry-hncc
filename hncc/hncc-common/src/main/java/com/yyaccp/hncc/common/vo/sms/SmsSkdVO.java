package com.yyaccp.hncc.common.vo.sms;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.common.core.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * 排班时间对象 sms_skd
 *
 * @author ruoyi
 * @date 2020-08-23
 */
@ApiModel(description = "排班时间", value = "排班时间对象VO")
public class SmsSkdVO extends BaseEntity {

    /**
     * $column.columnComment
     */
    @ApiModelProperty(value = "$column.columnComment", example = "")
    private Long id;

    /**
     * 上班日期
     */
    @ApiModelProperty(value = "上班日期", example = "")
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "上班日期", width = 30, dateFormat = "yyyy-MM-dd")
    private Date date;

    /**
     * 状态
     */
    @ApiModelProperty(value = "状态", example = "")
    @Excel(name = "状态")
    private Integer status;

    /**
     * 剩余号数
     */
    @ApiModelProperty(value = "剩余号数", example = "")
    @Excel(name = "剩余号数")
    private Long remain;

    /**
     * 午别（0上午1下午）
     */
    @ApiModelProperty(value = "午别（0上午1下午）", example = "")
    @Excel(name = "午别", readConverterExp = "0=上午1下午")
    private Integer noon;

    /**
     * 员工id
     */
    @ApiModelProperty(value = "员工id", example = "")
    @Excel(name = "员工id")
    private Long staffId;

    /**
     * 科室id
     */
    @ApiModelProperty(value = "科室id", example = "")
    @Excel(name = "科室id")
    private Long deptId;

    /**
     * 挂号限额
     */
    @ApiModelProperty(value = "挂号限额", example = "")
    @Excel(name = "挂号限额")
    private Long skLimit;

    /**
     * 排版规则id
     */
    @ApiModelProperty(value = "排版规则id", example = "")
    @Excel(name = "排版规则id")
    private Long  smsSkdRuleId;

    /**
     * 开始日期
     */
    @ApiModelProperty(value = "开始日期", example = "")
    @Excel(name = "开始日期")
    private String createDate;

    /**
     * 结束日期
     */
    @ApiModelProperty(value = "结束日期", example = "")
    @Excel(name = "结束日期")
    private String endDate;

    /** 部门名称 */
    @Excel(name = "部门名称")
    private String deptName;

    /** 医生名称 */
    @Excel(name = "医生名称")
    private String userName;

    /** 挂号级别名称 */
    @Excel(name = "挂号级别名称")
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getDeptName() {
        return deptName;
    }

    public void setDeptName(String deptName) {
        this.deptName = deptName;
    }

    public Long getSmsSkdRuleId() {
        return smsSkdRuleId;
    }

    public void setSmsSkdRuleId(Long smsSkdRuleId) {
        this.smsSkdRuleId = smsSkdRuleId;
    }

    public String getCreateDate() {
        return createDate;
    }

    public void setCreateDate(String createDate) {
        this.createDate = createDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Date getDate() {
        return date;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getStatus() {
        return status;
    }

    public void setRemain(Long remain) {
        this.remain = remain;
    }

    public Long getRemain() {
        return remain;
    }

    public void setNoon(Integer noon) {
        this.noon = noon;
    }

    public Integer getNoon() {
        return noon;
    }

    public void setStaffId(Long staffId) {
        this.staffId = staffId;
    }

    public Long getStaffId() {
        return staffId;
    }

    public void setDeptId(Long deptId) {
        this.deptId = deptId;
    }

    public Long getDeptId() {
        return deptId;
    }

    public void setSkLimit(Long skLimit) {
        this.skLimit = skLimit;
    }

    public Long getSkLimit() {
        return skLimit;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
                .append("id", getId())
                .append("date", getDate())
                .append("status", getStatus())
                .append("remain", getRemain())
                .append("noon", getNoon())
                .append("staffId", getStaffId())
                .append("deptId", getDeptId())
                .append("skLimit", getSkLimit())
                .toString();
    }
}
