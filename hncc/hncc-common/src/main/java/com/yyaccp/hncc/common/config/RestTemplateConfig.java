package com.yyaccp.hncc.common.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.MediaType;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author 天天向上 （john.yi@qq.com）
 * @date 2020/8/21
 */
@Configuration
public class RestTemplateConfig {
    @Bean
    RestTemplate restTemplate() {
        RestTemplate restTemplate = new RestTemplate();
        List<ClientHttpRequestInterceptor> interceptors = new ArrayList<>();
        interceptors.add((httpRequest, bytes, clientHttpRequestExecution) -> {
            // 码云需要设置user-agent和accept才能获取token
            httpRequest.getHeaders().set("Accept", MediaType.APPLICATION_JSON_UTF8_VALUE);
            httpRequest.getHeaders().set("User-Agent","Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:78.0) Gecko/20100101 Firefox/78.0");
            return clientHttpRequestExecution.execute(httpRequest, bytes);
        });
        restTemplate.setInterceptors(interceptors);
        return restTemplate;
    }
}
