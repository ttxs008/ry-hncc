package com.yyaccp.hncc.common.vo.sms;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.common.core.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.dozer.Mapping;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

/**
 * 员工对象 sms_staff
 *
 * @author ruoyi
 * @date 2020-08-23
 */
@ApiModel(description = "员工", value = "员工对象VO")
public class SmsStaffVO extends BaseEntity {

    /**
     * id
     */
    @ApiModelProperty(value = "id", example = "")
    private Long id;

    /**
     * 登录名
     */
    @ApiModelProperty(value = "登录名", example = "")
    @Excel(name = "登录名")
    private String username;

    /**
     * 登录密码
     */
    @ApiModelProperty(value = "登录密码", example = "")
    @Excel(name = "登录密码")
    private String password;

    /**
     * 状态：0-&gt;有效;1-&gt;无效
     */
    @ApiModelProperty(value = "状态：0-&gt;有效;1-&gt;无效", example = "")
    @Excel(name = "状态：0-&gt;有效;1-&gt;无效")
    private Integer status;

    /**
     * 性别
     */
    @ApiModelProperty(value = "性别", example = "")
    @Excel(name = "性别")
    private Integer gender;

    /**
     * 是否参与排班：0-&gt;否;1-&gt;是
     */
    @ApiModelProperty(value = "是否参与排班：0-&gt;否;1-&gt;是", example = "")
    @Excel(name = "是否参与排班：0-&gt;否;1-&gt;是")
    private Integer skdFlag;

    /**
     * 职称
     */
    @ApiModelProperty(value = "职称", example = "")
    @Excel(name = "职称")
    private String title;

    /**
     * 真实姓名
     */
    @ApiModelProperty(value = "真实姓名", example = "")
    @Excel(name = "真实姓名")
    private String name;

    /**
     * 所在科室
     */
    @ApiModelProperty(value = "所在科室", example = "")
    @Excel(name = "所在科室")
    @Mapping("smsSkd.deptId")
    private Long deptId;

    /**
     * 角色
     */
    @ApiModelProperty(value = "角色", example = "")
    @Excel(name = "角色")
    private Long roleId;

    /**
     * 挂号级别id
     */
    @ApiModelProperty(value = "挂号级别id", example = "")
    @Excel(name = "挂号级别id")
    private Long registrationRankId;

    /**
     * 上班日期
     */
    @ApiModelProperty(value = "上班日期", example = "")
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "上班日期", width = 30, dateFormat = "yyyy-MM-dd")
    private Date date;


    /**
     * 午别（0上午1下午）
     */
    @ApiModelProperty(value = "午别（0上午1下午）", example = "")
    @Excel(name = "午别", readConverterExp = "0=上午1下午")
    @Mapping("smsSkd.noon")
    private Integer noon;

    /**
     * 就诊日期
     */
    @ApiModelProperty(value = "就诊日期", example = "")
    @Excel(name = "就诊日期", readConverterExp = "对应医生的上班时间")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @Mapping("smsSkd.date")
    private Date attendanceDate;


    /**
     *  排班时间id
     */
    @ApiModelProperty(value = "排班时间id", example = "")
    @Mapping("smsSkd.id")
    private Long skdId;

    public Long getSkdId() {
        return skdId;
    }

    public void setSkdId(Long skdId) {
        this.skdId = skdId;
    }
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getGender() {
        return gender;
    }

    public void setGender(Integer gender) {
        this.gender = gender;
    }

    public Integer getSkdFlag() {
        return skdFlag;
    }

    public void setSkdFlag(Integer skdFlag) {
        this.skdFlag = skdFlag;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getDeptId() {
        return deptId;
    }

    public void setDeptId(Long deptId) {
        this.deptId = deptId;
    }

    public Long getRoleId() {
        return roleId;
    }

    public void setRoleId(Long roleId) {
        this.roleId = roleId;
    }

    public Long getRegistrationRankId() {
        return registrationRankId;
    }

    public void setRegistrationRankId(Long registrationRankId) {
        this.registrationRankId = registrationRankId;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Integer getNoon() {
        return noon;
    }

    public void setNoon(Integer noon) {
        this.noon = noon;
    }

    public Date getAttendanceDate() {
        return attendanceDate;
    }

    public void setAttendanceDate(Date attendanceDate) {
        this.attendanceDate = attendanceDate;
    }

    @Override
    public String toString() {
        return "SmsStaffVO{" +
                "id=" + id +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", status=" + status +
                ", gender=" + gender +
                ", skdFlag=" + skdFlag +
                ", title='" + title + '\'' +
                ", name='" + name + '\'' +
                ", deptId=" + deptId +
                ", roleId=" + roleId +
                ", registrationRankId=" + registrationRankId +
                ", date=" + date +
                ", noon=" + noon +
                ", attendanceDate=" + attendanceDate +
                '}';
    }
}
