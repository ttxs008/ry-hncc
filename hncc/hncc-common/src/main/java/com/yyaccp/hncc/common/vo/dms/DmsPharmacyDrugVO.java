package com.yyaccp.hncc.common.vo.dms;

import io.swagger.annotations.ApiModelProperty;

/**
 * @Author 余归
 * @Date 2020/9/15 0015 14:43
 * @Version 1.0
 * 药房药品VO 专用于药房医生业务
 */
public class DmsPharmacyDrugVO {

    /**
     * 处方号
     */
    @ApiModelProperty(value = "处方号", example = "")
    private Long mprId;

    /**
     * 处方名
     */
    @ApiModelProperty(value = "处方名", example = "")
    private String mprName;

    /**
     * 药品名
     */
    @ApiModelProperty(value = "药品名", example = "")
    private String drugName;

    /**
     * 药品数量
     */
    @ApiModelProperty(value = "药品数量", example = "")
    private Integer num;

    /**
     * 单价
     */
    @ApiModelProperty(value = "单价", example = "")
    private Double price;

    /**
     * 开立医生
     */
    @ApiModelProperty(value = "开立医生", example = "")
    private String userName;

    /**
     * 频次
     */
    @ApiModelProperty(value = "频次", example = "")
    private Integer frequency;

    /**
     * 使用建议
     */
    @ApiModelProperty(value = "使用建议", example = "")
    private String medicalAdvice;

    /**
     * 状态
     */
    @ApiModelProperty(value = "状态", example = "")
    private Long status;

    public Long getMprId() {
        return mprId;
    }

    public void setMprId(Long mprId) {
        this.mprId = mprId;
    }

    public String getMprName() {
        return mprName;
    }

    public void setMprName(String mprName) {
        this.mprName = mprName;
    }

    public String getDrugName() {
        return drugName;
    }

    public void setDrugName(String drugName) {
        this.drugName = drugName;
    }

    public Integer getNum() {
        return num;
    }

    public void setNum(Integer num) {
        this.num = num;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public Integer getFrequency() {
        return frequency;
    }

    public void setFrequency(Integer frequency) {
        this.frequency = frequency;
    }

    public String getMedicalAdvice() {
        return medicalAdvice;
    }

    public void setMedicalAdvice(String medicalAdvice) {
        this.medicalAdvice = medicalAdvice;
    }

    public Long getStatus() {
        return status;
    }

    public void setStatus(Long status) {
        this.status = status;
    }

}
