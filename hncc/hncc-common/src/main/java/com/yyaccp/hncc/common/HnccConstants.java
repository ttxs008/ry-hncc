package com.yyaccp.hncc.common;

/**
 * @author  天天向上 （john.yi@qq.com）
 * @date 2020/8/21.
 * @see HnccStatus.Common
 */

/**
 * @see HnccStatus
 * @see HnccType
 */

public interface HnccConstants {

    /**
     * 启用状态
     */
    @Deprecated
    int STATUS_ENABLED = 1;
    /**
     * 禁用状态
     */
    @Deprecated
    int STATUS_DISABLED = 0;
}
