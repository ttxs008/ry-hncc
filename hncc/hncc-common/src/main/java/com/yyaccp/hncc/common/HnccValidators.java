package com.yyaccp.hncc.common;

import lombok.Getter;

/**
 * @author 天天向上 （john.yi@qq.com）
 * @date 2020/9/15.
 */
public interface HnccValidators {
    @Getter
    enum Regex implements  HnccValidators {
        NUMBER("[0-9]+","正整数正则"),
        MOBILE( "^((13[0-9])|(14[0,1,4-9])|(15[0-3,5-9])|(16[2,5,6,7])|(17[0-8])|(18[0-9])|(19[0-3,5-9]))\\d{8}$", "手机号码正则");

        private String value;
        private String label;
        Regex(String value, String label) {
            this.value = value;
            this.label = label;
        }
    }
}
