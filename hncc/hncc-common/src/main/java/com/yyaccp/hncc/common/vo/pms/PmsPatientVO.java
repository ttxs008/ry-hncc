package com.yyaccp.hncc.common.vo.pms;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.common.core.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * 病人基本信息对象 pms_patient
 *
 * @author ruoyi
 * @date 2020-08-27
 */
@ApiModel(description = "病人基本信息" , value = "病人基本信息对象VO")
public class PmsPatientVO extends BaseEntity {
        /** 年齡 */
        @ApiModelProperty(value = "年齡",example = "")
        private String patientAgeStr;


    /** id */
            @ApiModelProperty(value = "id" , example = "")
                            private Long id;

                                /** 姓名 */
            @ApiModelProperty(value = "姓名" , example = "")
                                                                                                @Excel(name = "姓名")
                                                private String name;

                                /** 出生日期 */
            @ApiModelProperty(value = "出生日期" , example = "")
                                                                                                @JsonFormat(pattern = "yyyy-MM-dd")
                    @Excel(name = "出生日期" , width = 30, dateFormat = "yyyy-MM-dd")
                                                private Date dateOfBirth;

                                /** 身份证号 */
            @ApiModelProperty(value = "身份证号" , example = "")
                                                                                                @Excel(name = "身份证号")
                                                private String identificationNo;

                                /** 家庭住址 */
            @ApiModelProperty(value = "家庭住址" , example = "")
                                                                                                @Excel(name = "家庭住址")
                                                private String homeAddress;

                                /** 电话号码 */
            @ApiModelProperty(value = "电话号码" , example = "")
                                                                                                @Excel(name = "电话号码")
                                                private String phoneNo;

                                /** 性别 */
            @ApiModelProperty(value = "性别" , example = "")
                                                                                                @Excel(name = "性别")
                                                private Integer gender;

                                /** 病历号 */
            @ApiModelProperty(value = "病历号" , example = "")
                                                                                                @Excel(name = "病历号")
                                                private String medicalRecordNo;


    public String getPatientAgeStr() {
        return patientAgeStr;
    }

    public void setPatientAgeStr(String patientAgeStr) {
        this.patientAgeStr = patientAgeStr;
    }
    public void setId(Long id) {
                this.id = id;
            }

            public Long getId() {
                return id;
            }
                                                        public void setName(String name) {
                this.name = name;
            }

            public String getName() {
                return name;
            }
                                                        public void setDateOfBirth(Date dateOfBirth) {
                this.dateOfBirth = dateOfBirth;
            }

            public Date getDateOfBirth() {
                return dateOfBirth;
            }
                                                        public void setIdentificationNo(String identificationNo) {
                this.identificationNo = identificationNo;
            }

            public String getIdentificationNo() {
                return identificationNo;
            }
                                                        public void setHomeAddress(String homeAddress) {
                this.homeAddress = homeAddress;
            }

            public String getHomeAddress() {
                return homeAddress;
            }
                                                        public void setPhoneNo(String phoneNo) {
                this.phoneNo = phoneNo;
            }

            public String getPhoneNo() {
                return phoneNo;
            }
                                                        public void setGender(Integer gender) {
                this.gender = gender;
            }

            public Integer getGender() {
                return gender;
            }
                                                        public void setMedicalRecordNo(String medicalRecordNo) {
                this.medicalRecordNo = medicalRecordNo;
            }

            public String getMedicalRecordNo() {
                return medicalRecordNo;
            }
            
    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
                                                                .append("id" , getId())
                                                                        .append("name" , getName())
                                                                        .append("dateOfBirth" , getDateOfBirth())
                                                                        .append("identificationNo" , getIdentificationNo())
                                                                        .append("homeAddress" , getHomeAddress())
                                                                        .append("phoneNo" , getPhoneNo())
                                                                        .append("gender" , getGender())
                                                                        .append("medicalRecordNo" , getMedicalRecordNo())
                                                                        .append("patientAgeStr",getPatientAgeStr())
                                    .toString();
    }
}
