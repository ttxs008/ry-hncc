package com.yyaccp.hncc.common.vo.dms;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;
import io.swagger.annotations.ApiModelProperty;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.math.BigDecimal;
import java.util.Date;

/**
 * 缴费项 对象 dms_pay
 *
 * @author 何磊
 * @date 2020-09-07
 */
public class DmsPayVo extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** id */
    @ApiModelProperty(value = "ID",dataType ="Long",example = "")
    private Long id;

    /** 项目名称 */
    @ApiModelProperty(value = "项目名称",dataType ="String",example = "")
    @Excel(name = "项目名称")
    private String name;

    /** 总金额 */
    @ApiModelProperty(value = "总金额",dataType ="BigDecimal",example = "")
    @Excel(name = "总金额")
    private BigDecimal amount;

    /** 开立时间 */
    @ApiModelProperty(value = "开立时间",dataType ="Date",example = "")
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "开立时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date createTime;


    /** 项目类型 */
    @ApiModelProperty(value = "项目类型",dataType ="String",example = "")
    @Excel(name = "项目类型")
    private Integer type;

    /** 状态 */
    @ApiModelProperty(value = "状态",dataType ="Integer",example = "")
    @Excel(name = "状态")
    private Integer status;



    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    @Override
    public Date getCreateTime() {
        return createTime;
    }

    @Override
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }


    @Override
    public String toString() {
        return "DmsPayVo{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", amount=" + amount +
                ", createTime=" + createTime +
                ", type=" + type +
                ", status=" + status +
                '}';
    }
}
