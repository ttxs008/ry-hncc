package com.yyaccp.hncc.common.vo.dms;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.common.core.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.dozer.Mapping;

import java.math.BigDecimal;
import java.util.Date;

/**
 * 草药模版项对象 dms_herbal_model_item
 *
 * @author 周某
 * @date 2020-08-17
 */
@ApiModel(description = "草药模版项" ,value = "草药模版项对象VO")
public class DmsHerbalModelItemVO  extends BaseEntity
{

    /** id */
    @ApiModelProperty(value = "id",example = "7")
    private Long id;

    /** 状态 */
    @ApiModelProperty(value = "状态",example = "1")
    @Excel(name = "状态")
    private Integer status;

    /** 说明 */
    @ApiModelProperty(value = "说明",example = "说明")
    @Excel(name = "说明")
    private String footnote;

    /** 药品id */
    @ApiModelProperty(value = "药品id",example = "7")
    @Excel(name = "药品id")
    private Long drugId;

    /** 用量 */
    @ApiModelProperty(value = "用量",example = "1")
    @Excel(name = "用量")
    private Long usageNum;

    /** 用量单位 */
    @ApiModelProperty(value = "用量单位",example = "")
    @Excel(name = "用量单位")
    private Integer usageNumUnit;

    /** 药品模板id */
    @ApiModelProperty(value = "药品模板id",example = "9")
    @Excel(name = "药品模板id")
    private Long modelId;

    /**
     *药品DmsDrug的外加列
     */
    /** 药品编码 */
    @Mapping("drug.code")
    private String drugCode;

    /** 药品名称 */
    @Mapping("drug.name")
    private String drugName;

    /** 药品规格 */
    @Mapping("drug.format")
    private String drugFormat;

    /** 药品单价 */
    @Mapping("drug.price")
    private BigDecimal drugPrice;

    /** 包装单位 */
    @Mapping("drug.unit")
    private String drugUnit;

    /** 生产厂家 */
    @Mapping("drug.manufacturer")
    private String drugManufacturer;

    /** 药品剂型 */
    @Mapping("drug.dosageId")
    private Long drugDosageId;

    /** 药品类型 */
    @Mapping("drug.typeId")
    private Long drugTypeId;

    /** 拼音助记码 */
    @Mapping("drug.mnemonicCode")
    private String drugMnemonicCode;

    /** 创建时间 */
    @Mapping("drug.createDate")
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date drugCreateDate;

    /** 库存 */
    @Mapping("drug.stock")
    private Long drugStock;

    /** 通用名 */
    @Mapping("drug.genericName")
    private String drugGenericName;

    /** 状态 */
    @Mapping("drug.status")
    private Integer drugStatus;

    /**
     * 药品模板外加列
     * */
    /**频次*/
    @Mapping("drugModel.frequency")
    private Integer drugModelFrequency;

    /**医嘱*/
    @Mapping("drugModel.medicalAdvice")
    private String drugModelMedicalAdvice;

    public String getDrugCode() {
        return drugCode;
    }

    public void setDrugCode(String drugCode) {
        this.drugCode = drugCode;
    }

    public String getDrugName() {
        return drugName;
    }

    public void setDrugName(String drugName) {
        this.drugName = drugName;
    }

    public String getDrugFormat() {
        return drugFormat;
    }

    public void setDrugFormat(String drugFormat) {
        this.drugFormat = drugFormat;
    }

    public BigDecimal getDrugPrice() {
        return drugPrice;
    }

    public void setDrugPrice(BigDecimal drugPrice) {
        this.drugPrice = drugPrice;
    }

    public String getDrugUnit() {
        return drugUnit;
    }

    public void setDrugUnit(String drugUnit) {
        this.drugUnit = drugUnit;
    }

    public String getDrugManufacturer() {
        return drugManufacturer;
    }

    public void setDrugManufacturer(String drugManufacturer) {
        this.drugManufacturer = drugManufacturer;
    }

    public Long getDrugDosageId() {
        return drugDosageId;
    }

    public void setDrugDosageId(Long drugDosageId) {
        this.drugDosageId = drugDosageId;
    }

    public Long getDrugTypeId() {
        return drugTypeId;
    }

    public void setDrugTypeId(Long drugTypeId) {
        this.drugTypeId = drugTypeId;
    }

    public String getDrugMnemonicCode() {
        return drugMnemonicCode;
    }

    public void setDrugMnemonicCode(String drugMnemonicCode) {
        this.drugMnemonicCode = drugMnemonicCode;
    }

    public Date getDrugCreateDate() {
        return drugCreateDate;
    }

    public void setDrugCreateDate(Date drugCreateDate) {
        this.drugCreateDate = drugCreateDate;
    }

    public Long getDrugStock() {
        return drugStock;
    }

    public void setDrugStock(Long drugStock) {
        this.drugStock = drugStock;
    }

    public String getDrugGenericName() {
        return drugGenericName;
    }

    public void setDrugGenericName(String drugGenericName) {
        this.drugGenericName = drugGenericName;
    }

    public Integer getDrugStatus() {
        return drugStatus;
    }

    public void setDrugStatus(Integer drugStatus) {
        this.drugStatus = drugStatus;
    }

    public Integer getDrugModelFrequency() {
        return drugModelFrequency;
    }

    public void setDrugModelFrequency(Integer drugModelFrequency) {
        this.drugModelFrequency = drugModelFrequency;
    }

    public String getDrugModelMedicalAdvice() {
        return drugModelMedicalAdvice;
    }

    public void setDrugModelMedicalAdvice(String drugModelMedicalAdvice) {
        this.drugModelMedicalAdvice = drugModelMedicalAdvice;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setStatus(Integer status) 
    {
        this.status = status;
    }

    public Integer getStatus() 
    {
        return status;
    }
    public void setFootnote(String footnote) 
    {
        this.footnote = footnote;
    }

    public String getFootnote() 
    {
        return footnote;
    }
    public void setDrugId(Long drugId) 
    {
        this.drugId = drugId;
    }

    public Long getDrugId() 
    {
        return drugId;
    }
    public void setUsageNum(Long usageNum) 
    {
        this.usageNum = usageNum;
    }

    public Long getUsageNum() 
    {
        return usageNum;
    }
    public void setUsageNumUnit(Integer usageNumUnit) 
    {
        this.usageNumUnit = usageNumUnit;
    }

    public Integer getUsageNumUnit() 
    {
        return usageNumUnit;
    }
    public void setModelId(Long modelId) 
    {
        this.modelId = modelId;
    }

    public Long getModelId() 
    {
        return modelId;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("status", getStatus())
            .append("footnote", getFootnote())
            .append("drugId", getDrugId())
            .append("usageNum", getUsageNum())
            .append("usageNumUnit", getUsageNumUnit())
            .append("modelId", getModelId())
            .toString();
    }
}
