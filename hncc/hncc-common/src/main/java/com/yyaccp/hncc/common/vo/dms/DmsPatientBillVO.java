package com.yyaccp.hncc.common.vo.dms;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.math.BigDecimal;

/**
 * Created by 余归 on 2020/8/24
 *
 * @author 余归
 * 患者账单VO对象
 */
@ApiModel(description = "患者账单VO对象", value = "患者账单VO对象")
public class DmsPatientBillVO {

    /**
     * 项目名
     */
    @ApiModelProperty(value = "项目名", dataType = "String", example = "无")
    private String itemName;

    /**
     * 规格
     */
    @ApiModelProperty(value = "规格", dataType = "String", example = "无")
    private String format;

    /**
     * 数量
     */
    @ApiModelProperty(value = "数量", dataType = "Integer", example = "无")
    private Integer amount;

    /**
     * 单价
     */
    @ApiModelProperty(value = "单价", dataType = "BigDecimal", example = "无")
    private BigDecimal price;

    /**
     * 总金额
     */
    @ApiModelProperty(value = "总金额", dataType = "BigDecimal", example = "无")
    private BigDecimal totalPrice;

    /**
     * 类型
     */
    @ApiModelProperty(value = "类型", dataType = "String", example = "无")
    private String type;

    /**
     * 状态
     */
    @ApiModelProperty(value = "状态", dataType = "String", example = "无")
    private String status;

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public String getFormat() {
        return format;
    }

    public void setFormat(String format) {
        this.format = format;
    }

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public BigDecimal getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(BigDecimal totalPrice) {
        this.totalPrice = totalPrice;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "DmsPatientBillVO{" +
                "itemName='" + itemName + '\'' +
                ", format='" + format + '\'' +
                ", amount=" + amount +
                ", price=" + price +
                ", totalPrice=" + totalPrice +
                ", type='" + type + '\'' +
                ", status='" + status + '\'' +
                '}';
    }
}
