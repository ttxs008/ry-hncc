package com.yyaccp.hncc.common.vo.oss;

import lombok.Data;
import lombok.ToString;

/**
 * 前端直传oss回调结果类
 *
 * @author 天天向上 （john.yi@qq.com）
 * @date 2020/8/23
 */
@Data
@ToString
public class OssCallbackResult {
    private String filename;
    private String size;
    private String mimeType;
    private String width;
    private String height;
}
