package com.yyaccp.hncc.common.vo.dms;

import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.TreeEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * Created by 天天向上 （john.yi@qq.com） on 2020/8/18.
 */
@Data
@ApiModel(value = "病例模板目录VO，如果类型是模板，则通过modelId关联病例模板")
public class DmsCaseModelCatalogVO extends TreeEntity<DmsCaseModelCatalogVO> {
    /** id */
    @ApiModelProperty(name = "id")
    private Long id;

    /** 层级 */
    @ApiModelProperty(name = "层级")
    private Integer level;

    /** 类型  */
    @ApiModelProperty(name = "类型")
    private Integer type;

    /** 状态 */
    @ApiModelProperty(name = "状态")
    private Integer status;

    /** 模板id */
    @ApiModelProperty(name = "名称")
    private Long modelId;

    /** 范围 */
    @ApiModelProperty(name = "范围")
    private Long scope;

    /** 所属人Id */
    @ApiModelProperty(name = "名称")
    private Long ownId;

    /** 名称 */
    @ApiModelProperty(name = "名称")
    private String name;
}
