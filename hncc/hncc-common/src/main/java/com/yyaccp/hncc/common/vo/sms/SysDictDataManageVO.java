package com.yyaccp.hncc.common.vo.sms;

/**
 * @Date 2020/8/19
 * @author 毛
 */
public class SysDictDataManageVO {
    @Override
    public String toString() {
        return "SysDictDataManageVO{" +
                "dataId=" + dataId +
                ", keyId=" + keyId +
                ", keyValue='" + keyValue + '\'' +
                '}';
    }

    public SysDictDataManageVO(Integer dataId, Integer keyId, String keyValue) {
        this.dataId = dataId;
        this.keyId = keyId;
        this.keyValue = keyValue;
    }


    public SysDictDataManageVO(){

    }
    public Integer getDataId() {
        return dataId;
    }

    public void setDataId(Integer dataId) {
        this.dataId = dataId;
    }

    public Integer getKeyId() {
        return keyId;
    }

    public void setKeyId(Integer keyId) {
        this.keyId = keyId;
    }

    public String getKeyValue() {
        return keyValue;
    }

    public void setKeyValue(String keyValue) {
        this.keyValue = keyValue;
    }

    /**
     * 数据字典id
     */
    private Integer dataId;
    /**
     * 数据字典键id
     */
    private Integer keyId;
    /**
     * 数据字典键value
     */
    private String keyValue;

}
