package com.yyaccp.hncc.common.vo.oss;

import lombok.Data;
import lombok.ToString;

/**
 * 前端直传oss签名结果类
 * @author 天天向上 （john.yi@qq.com）
 * @date 2020/8/23
 */
@Data
@ToString
public class OssPolicyResult {
    /**
     * oss的访问key
     */
    private String accessKeyId;
    /**
     * 请求策略
     */
    private String policy;
    /**
     * 签名
     */
    private String signature;
    /**
     * 存储的目录
     */
    private String dir;
    /**
     * host的格式为 bucketname.endpoint
     */
    private String host;
    /**
     * 签名过期时间
     */
    private String expire;
    /**
     * 回调内容
     */
    private String callbackBody;
}
