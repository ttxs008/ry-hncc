package com.yyaccp.hncc.common;

/**
 * Created by 天天向上 （john.yi@qq.com） on 2020/8/22.
 */
public interface SocailContants {
    /**
     * 三方登录默认角色的key
     */
    String SOCIAL_ROLE_KEY = "social";
    /**
     * 传递数据到哪台前端服务器的key
     */
    String WEB_SERVER_KEY = "domain";
    /**
     * 传递到前端的token变量名
     */
    String TOKEN_KEY = "token";
    /**
     * 传递到前端的error变量名
     */
    String ERROR_KEY = "error";
    /**
     * 钉钉登录成功消息码
     */
    String DINGTALK_ERRMSG_SUCCESS = "ok";
    /**
     * 钉钉登录成功状态码
     */
    Long DINGTALK_ERRCODE_SUCCESS = 0L;
}
