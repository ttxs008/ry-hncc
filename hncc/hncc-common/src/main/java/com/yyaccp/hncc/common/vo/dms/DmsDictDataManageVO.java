package com.yyaccp.hncc.common.vo.dms;

import com.ruoyi.common.core.domain.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * @author 毛
 * @Date 2020/8/20
 */
@ApiModel(description = "常用项",value ="常用项VO" )
public class DmsDictDataManageVO extends BaseEntity {

    @Override
    public String toString() {
        return "DmsDictDataManageVO{" +
                "dictCode=" + dictCode +
                ", dictLabel=" + dictLabel +
                ", dictValue='" + dictValue + '\'' +
                ", dictType='" + dictType + '\'' +
                '}';
    }

    /**
     * 数据字典id
     */
    @ApiModelProperty(value = "数据字典id",example = "1")
    private Long dictCode;

    /**
     * 数据字典键标签
     */
    @ApiModelProperty(value = "数据字典键标签",example = "草药")
    private String dictLabel;
    /**
     * 数据字典键value
     */
    @ApiModelProperty(value = "数据字典键value",example = "1")
    private String dictValue;

    /**
     * 数据字典类别
     */
    @ApiModelProperty(value = "数据字典类别",example = "1")
    private String dictType;


    public Long getDictCode() {
        return dictCode;
    }

    public void setDictCode(Long dictCode) {
        this.dictCode = dictCode;
    }

    public String getDictLabel() {
        return dictLabel;
    }

    public void setDictLabel(String dictLabel) {
        this.dictLabel = dictLabel;
    }

    public String getDictValue() {
        return dictValue;
    }

    public void setDictValue(String dictValue) {
        this.dictValue = dictValue;
    }

    public String getDictType() {
        return dictType;
    }

    public void setDictType(String dictType) {
        this.dictType = dictType;
    }
}
