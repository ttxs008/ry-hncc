package com.yyaccp.hncc.common;

import lombok.Getter;

/**
 * 统一管理状态相关常量
 * @author 天天向上 （john.yi@qq.com）
 * @date 2020/9/7.
 */
public interface HnccStatus {
    @Getter
    enum Common implements HnccStatus {
        ENABLED(1, "启用/正常"),
        DISABLED(0,"禁用");

        Common(int value, String label) {
            this.value = value;
            this.label = label;
        }

        private int value;
        private String label;
    }
    /**
     * 处方、处方明细状态（跟药品状态一样）
     * @see DrugItem
     */
    @Getter
    enum Prescription implements HnccStatus {
        DELETED(0, "作废/删除"),
        CREATED(1, "新建/未缴费"),
        PAID(2, "已缴费"),
        DISPENSED(3, "已发药"),
        REFUNDED_DRUG(4, "已退药"),
        REFUNDED(5, "已退费"),
        EXPIRED(6, "已过期");
        private int value;
        private String label;

        Prescription(int value, String label) {
            this.value = value;
            this.label = label;
        }
    }
    /**
     * 药品项目（支付订单）状态
     */
    @Getter
    enum DrugItem implements HnccStatus {
        DELETED(0, "作废/删除"),
        CREATED(1, "开立/未缴费"),
        PAID(2, "已缴费"),
        DISPENSED(3, "已发药"),
        REFUNDED_DRUG(4, "已退药"),
        REFUNDED(5, "已退费"),
        EXPIRED(6, "已过期");
        private int value;
        private String label;

        DrugItem(int value, String label) {
            this.value = value;
            this.label = label;
        }
    }
    /**
     * 非药品项目（支付订单）状态
     */
    @Getter
    enum NonDrugItem {
        DELETED(0,"作废/删除"),
        CREATED(1,"开立/未缴费"),
        PAID(2,"已缴费/未登记"),
        LOGGED(3,"已登记"),
        EXECUTED(4,"已执行"),
        REFUNDED(5, "已退费"),
        EXPIRED(6, "已过期");
        private int value;
        private String label;
        NonDrugItem(int value, String label) {
            this.value = value;
            this.label = label;
        }
    }

    /**
     * 挂号信息状态
     */
    @Getter
    enum Registration {
        NOT_VISIT(1,"未看诊"),
        STAY_CHARGE(2,"待收费"),
        FINISH_VISIT(3,"诊毕"),
        YES_BOUNCE(4,"已退号");
        private int value;
        private String label;
        Registration(int value, String label) {
            this.value = value;
            this.label = label;
        }
    }
}
