package com.yyaccp.hncc.common.vo.dms;

import io.swagger.annotations.ApiModelProperty;

import java.math.BigDecimal;
import java.util.List;

/**
 * 缴费对象 DmsPaymentVo
 * @author 何磊
 * @date 2020/9/9
 */
public class DmsHandleRefundVo {

    /** 退费项集合 */
    @ApiModelProperty(value = "退费项集合",dataType ="List",example = "")
    List<DmsRefundVo> refundList;

    /** 总金额 */
    @ApiModelProperty(value = "总金额",dataType ="BigDecimal",example = "")
    BigDecimal amount;

    /** 挂号ID */
    @ApiModelProperty(value = "挂号ID",dataType ="Long",example = "")
    Long registerId;

    /** 发票号 */
    @ApiModelProperty(value = "发票号",dataType ="Long",example = "")
    Long newInvoiceNo;

    public List<DmsRefundVo> getRefundList() {
        return refundList;
    }

    public void setRefundList(List<DmsRefundVo> refundList) {
        this.refundList = refundList;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public Long getRegisterId() {
        return registerId;
    }

    public void setRegisterId(Long registerId) {
        this.registerId = registerId;
    }

    public Long getNewInvoiceNo() {
        return newInvoiceNo;
    }

    public void setNewInvoiceNo(Long newInvoiceNo) {
        this.newInvoiceNo = newInvoiceNo;
    }

    @Override
    public String toString() {
        return "DmsHandleRefundVo{" +
                "refundList=" + refundList +
                ", amount=" + amount +
                ", registerId=" + registerId +
                ", newInvoiceNo=" + newInvoiceNo +
                '}';
    }
}
