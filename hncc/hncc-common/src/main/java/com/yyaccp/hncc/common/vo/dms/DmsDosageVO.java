package com.yyaccp.hncc.common.vo.dms;

import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;
import io.swagger.annotations.ApiModelProperty;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * 药品剂型对象 dms_dosage
 *
 * @author 周某
 * @date 2020-08-12
 */
public class DmsDosageVO extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** id */
    @ApiModelProperty(value = "ID",dataType ="Long",example = "110")
    private Long id;

    /** 编码 */
    @ApiModelProperty(value = "编码",dataType ="String",example = "ZJ")
    @Excel(name = "编码")
    private String code;

    /** 名字 */
    @ApiModelProperty(value = "名字",dataType ="String",example = "针剂")
    @Excel(name = "名字")
    private String name;

    /** 状态 */
    @ApiModelProperty(value = "状态",dataType ="Integer",example = "1")
    @Excel(name = "状态")
    private Integer status;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setCode(String code) 
    {
        this.code = code;
    }

    public String getCode() 
    {
        return code;
    }
    public void setName(String name) 
    {
        this.name = name;
    }

    public String getName() 
    {
        return name;
    }
    public void setStatus(Integer status) 
    {
        this.status = status;
    }

    public Integer getStatus() 
    {
        return status;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("code", getCode())
            .append("name", getName())
            .append("status", getStatus())
            .toString();
    }
}
