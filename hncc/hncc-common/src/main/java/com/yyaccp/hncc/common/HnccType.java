package com.yyaccp.hncc.common;

import lombok.Getter;

/**
 * 统一管理类型相关常量
 * @author 天天向上 （john.yi@qq.com）
 * @date 2020/9/7.
 */
public interface HnccType {
    /**
     * 非药品类型
     */
    @Getter
    enum NonDrug implements HnccType{
        CHECK(1, "检查"),
        TEST(2, "检验"),
        DISPOSITION(3, "处置");

        NonDrug(int value, String label) {
            this.value = value;
            this.label = label;
        }

        private int value;
        private String label;
    }

    /**
     * 药品类型
     */
    @Getter
    enum Drug implements HnccType{
        MEDICINE(101, "西药"),
        CHINESE_PATENT_MEDICINE(102, "中成药"),
        HERBAL(103, "草药");

        Drug(int value, String label) {
            this.value = value;
            this.label = label;
        }

        private int value;
        private String label;
    }

    /**
     *   发票类型
     */
    @Getter
    enum Invoice implements HnccType{
        NORMAL(1,"正常"),
        RED(2,"冲红"),
        REFUND(3,"退费");
        Invoice(int value, String label) {
            this.value = value;
            this.label = label;
        }

        private int value;
        private String label;
    }
    /**
     *  处方类型
     */
    @Getter
    enum Prescription implements HnccType{
        MEDICINE_PRESCRIPTION(4,"成药"),
        HERBAL_PRESCRIPTION(5,"草药");
        Prescription(int value, String label) {
            this.value = value;
            this.label = label;
        }

        private int value;
        private String label;
    }

}
