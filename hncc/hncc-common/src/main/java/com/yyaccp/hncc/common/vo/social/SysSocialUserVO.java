package com.yyaccp.hncc.common.vo.social;

import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * 社交登录用户对象 sys_social_user
 *
 * @author 天天向上
 * @date 2020-08-20
 */
@ApiModel(description = "社交登录用户", value = "社交登录用户对象VO")
public class SysSocialUserVO extends BaseEntity {

    /**
     * id
     */
    @ApiModelProperty(value = "id", example = "")
    private Long id;

    /**
     * 平台名
     */
    @ApiModelProperty(value = "平台名", example = "")
    @Excel(name = "平台名")
    private String platformName;

    /**
     * 平台用户id
     */
    @ApiModelProperty(value = "平台用户id", example = "")
    @Excel(name = "平台用户id")
    private Long socialId;

    /**
     * 登录名字
     */
    @ApiModelProperty(value = "登录名字", example = "")
    @Excel(name = "登录名字")
    private String socialLoginName;

    /**
     * 显示名字
     */
    @ApiModelProperty(value = "显示名字", example = "")
    @Excel(name = "显示名字")
    private String socialName;

    /**
     * 头像地址
     */
    @ApiModelProperty(value = "头像地址", example = "")
    @Excel(name = "头像地址")
    private String avatarUrl;

    /**
     * 关联用户id
     */
    @ApiModelProperty(value = "用户在当前开放应用内的唯一标识", example = "")
    @Excel(name = "用户在当前开放应用内的唯一标识")
    private Long openid;
    /**
     * 关联用户id
     */
    @ApiModelProperty(value = "用户在当前开放应用所属企业的唯一标识", example = "")
    @Excel(name = "用户在当前开放应用所属企业的唯一标识")
    private Long unionid;
    /**
     * 关联用户id
     */
    @ApiModelProperty(value = "关联用户id", example = "")
    @Excel(name = "关联用户id")
    private Long sysUserId;

    public void setId(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setPlatformName(String platformName) {
        this.platformName = platformName;
    }

    public String getPlatformName() {
        return platformName;
    }

    public void setSocialId(Long socialId) {
        this.socialId = socialId;
    }

    public Long getSocialId() {
        return socialId;
    }

    public void setSocialLoginName(String socialLoginName) {
        this.socialLoginName = socialLoginName;
    }

    public Long getOpenid() {
        return openid;
    }

    public void setOpenid(Long openid) {
        this.openid = openid;
    }

    public Long getUnionid() {
        return unionid;
    }

    public void setUnionid(Long unionid) {
        this.unionid = unionid;
    }

    public String getSocialLoginName() {
        return socialLoginName;
    }

    public void setSocialName(String socialName) {
        this.socialName = socialName;
    }

    public String getSocialName() {
        return socialName;
    }

    public void setAvatarUrl(String avatarUrl) {
        this.avatarUrl = avatarUrl;
    }

    public String getAvatarUrl() {
        return avatarUrl;
    }

    public void setSysUserId(Long sysUserId) {
        this.sysUserId = sysUserId;
    }

    public Long getSysUserId() {
        return sysUserId;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
                .append("id", getId())
                .append("platformName", getPlatformName())
                .append("socialId", getSocialId())
                .append("socialLoginName", getSocialLoginName())
                .append("socialName", getSocialName())
                .append("avatarUrl", getAvatarUrl())
                .append("sysUserId", getSysUserId())
                .toString();
    }
}
