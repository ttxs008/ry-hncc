package com.yyaccp.hncc.common.vo.oss;

import lombok.Data;

/**
 * 通过应用服务器上传到oss结果类
 *
 * @author 天天向上 （john.yi@qq.com）
 * @date 2020/8/22
 */
@Data
public class UploadResult {
    private String originalFilename;
    private String filename;
    private String url;
}
