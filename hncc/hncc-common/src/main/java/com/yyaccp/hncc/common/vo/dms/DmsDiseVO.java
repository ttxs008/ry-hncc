package com.yyaccp.hncc.common.vo.dms;

import com.ruoyi.common.annotation.Excel;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * 诊断类型(疾病)管理对象 dms_dise
 * 
 * @author ruoyi
 * @date 2020-08-12
 */

@ApiModel(description = "诊断类型(疾病)对象",value ="诊断类型(疾病)对象VO" )
public class DmsDiseVO
{

    /** id */
    @ApiModelProperty(value = "对象id",example = "1")
    private Long id;

    /** 目录id */
    @Excel(name = "目录id")
    @ApiModelProperty(value = "目录id",example = "1")
    private Long catId;

    /** 疾病编码 */
    @Excel(name = "疾病编码")
    @ApiModelProperty(value = "疾病编码",example = "AAX-AA")
    private String code;

    /** 疾病名称 */
    @Excel(name = "疾病名称")
    @ApiModelProperty(value = "疾病名称",example = "这是疾病的名称")
    private String name;

    /** ICD编码 */
    @Excel(name = "ICD编码")
    @ApiModelProperty(value = "ICD编码",example = "AZZZA222")
    private String icd;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setCatId(Long catId) 
    {
        this.catId = catId;
    }

    public Long getCatId() 
    {
        return catId;
    }
    public void setCode(String code) 
    {
        this.code = code;
    }

    public String getCode() 
    {
        return code;
    }
    public void setName(String name) 
    {
        this.name = name;
    }

    public String getName() 
    {
        return name;
    }
    public void setIcd(String icd) 
    {
        this.icd = icd;
    }

    public String getIcd() 
    {
        return icd;
    }


    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("catId", getCatId())
            .append("code", getCode())
            .append("name", getName())
            .append("icd", getIcd())
            .toString();
    }
}
