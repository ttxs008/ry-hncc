package com.yyaccp.hncc.mq.rabbit;

import com.yyaccp.hncc.mq.OrderMessage;
import com.yyaccp.hncc.mq.QueueEnum;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.stereotype.Component;

/**
 * @author 天天向上 （john.yi@qq.com）
 * @date 2020/9/6.
 */
@Component
@Slf4j
@RequiredArgsConstructor
public class OrderMessageSender {
    private final AmqpTemplate amqpTemplate;

    /**
     * 发送订单延迟取消消息到mq
     * @param orderMessage 订单消息对象
     */
    public void sendMessage(OrderMessage orderMessage) {
        amqpTemplate.convertAndSend(QueueEnum.QUEUE_TTL_ORDER_CANCEL.getExchange(),
                QueueEnum.QUEUE_TTL_ORDER_CANCEL.getRouteKey(),
                orderMessage,
                msg -> {
                    msg.getMessageProperties().setExpiration(String.valueOf(orderMessage.getDelayTimes()));
                    return msg;
                });
    }
}
