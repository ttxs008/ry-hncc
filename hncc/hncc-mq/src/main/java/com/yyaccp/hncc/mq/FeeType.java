package com.yyaccp.hncc.mq;

import lombok.Getter;

/**
 * rabbit mq 枚举常量，定义各种缴费消息的类型
 * @author 天天向上 （john.yi@qq.com）
 * @date 2020/9/5.
 */
@Getter
public enum FeeType {
    REGISTRATION(0, "registration", "挂号"),
    CHECK(1, "check", "检查"),
    TEST(2, "test", "检验"),
    DISPOSITION(3, "disposition", "处置"),
    HERBAL(4, "herbal", "草药"),
    MEDICINE(5, "medicine", "成药");

    private int index;
    private String name;
    private String nameCn;
    FeeType(int index, String name, String nameCn) {
        this.index = index;
        this.name = name;
        this.nameCn = nameCn;
    }

    public static FeeType fromIndex(int index) {
        for(FeeType feeType: FeeType.values()) {
            if(feeType.getIndex() == index) {
                return feeType;
            }
        }
        return null;
    }
}
