package com.yyaccp.hncc.mq;

import lombok.Data;
import lombok.ToString;

import java.io.Serializable;
import java.util.Date;

/**
 * @author 天天向上 （john.yi@qq.com）
 * @date 2020/9/6.
 */
@Data
@ToString
public class OrderMessage implements Serializable {
    private static final long serialVersionUID = 7838240825426169823L;
    private Long orderId;
    private FeeType feeType;
    private Date createTime = new Date();
    private Long delayTimes;

    public OrderMessage(Long orderId, FeeType feeType, Long delayTimes) {
        this.orderId = orderId;
        this.feeType = feeType;
        this.delayTimes = delayTimes;
    }
}
