package com.yyaccp.hncc.smsg.service;

import java.util.Map;

/**
 * @author 天天向上 （john.yi@qq.com）
 * @date 2020/9/8.
 */
public interface ShortMessageService {
    /**
     * 发送成功
     */
    String SUCCESS = "success";
    /**
     * 根据短信模板发送验证码短信（验证码、通知等短信）
     * 请根据参考各平台api传递参数
     * @param phoneNumbers 电话号码字符串，如果是多个，使用英文,分割
     * @param code 验证码
     * @return 成功返回success，否则返回错误消息
     */
    String sendValidCodeMessage(String phoneNumbers, String code);
    /**
     * 发送通用短信
     * 请根据参考各平台api传递参数
     * @param phoneNumbers 电话号码字符串，如果是多个，使用英文,分割
     * @param templateId 模板id
     * @param paramsMap 模板参数名字和值的map
     * @return 成功返回success，否则返回错误消息
     */
    String sendMessage(String phoneNumbers, String templateId, Map<String, String> paramsMap);
}
