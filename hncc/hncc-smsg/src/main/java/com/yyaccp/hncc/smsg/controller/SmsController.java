package com.yyaccp.hncc.smsg.controller;

import com.yyaccp.hncc.common.HnccValidators;
import com.yyaccp.hncc.smsg.service.impl.SmsService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author 天天向上 （john.yi@qq.com）
 * @date 2020/9/11.
 */
@RestController
@RequiredArgsConstructor
@Slf4j
@RequestMapping("/smsg")
public class SmsController {
    private final SmsService smsService;

    private static final String REGEX_MOBILE = HnccValidators.Regex.MOBILE.getValue();

    @GetMapping("/sendValidCode")
    public String sendValidCode(String phoneNumber) {
        if(!phoneNumber.matches(REGEX_MOBILE)) {
            return "手机号码不正确";
        }
        return smsService.sendValidCodeMessage(phoneNumber);
    }

    @PostMapping("/login")
    public String login(String phoneNumber, String validCode) {
        return smsService.login(phoneNumber, validCode);
    }
}
