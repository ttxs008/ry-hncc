package com.yyaccp.hncc.sms.mapper;

import java.util.List;
import com.yyaccp.hncc.sms.domain.SmsRegistrationRank;

/**
 * 挂号级别Mapper接口
 * 
 * @author ruoyi
 * @date 2020-08-08
 */
public interface SmsRegistrationRankMapper 
{
    /**
     * 查询挂号级别
     * 
     * @param id 挂号级别ID
     * @return 挂号级别
     */
    public SmsRegistrationRank selectSmsRegistrationRankById(Long id);

    /**
     * 查询挂号级别列表
     * 
     * @param smsRegistrationRank 挂号级别
     * @return 挂号级别集合
     */
    public List<SmsRegistrationRank> selectSmsRegistrationRankList(SmsRegistrationRank smsRegistrationRank);

    /**
     * 新增挂号级别
     * 
     * @param smsRegistrationRank 挂号级别
     * @return 结果
     */
    public int insertSmsRegistrationRank(SmsRegistrationRank smsRegistrationRank);

    /**
     * 修改挂号级别
     * 
     * @param smsRegistrationRank 挂号级别
     * @return 结果
     */
    public int updateSmsRegistrationRank(SmsRegistrationRank smsRegistrationRank);

    /**
     * 删除挂号级别
     * 
     * @param id 挂号级别ID
     * @return 结果
     */
    public int deleteSmsRegistrationRankById(Long id);

    /**
     * 批量删除挂号级别
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteSmsRegistrationRankByIds(Long[] ids);
}
