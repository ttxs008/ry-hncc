package com.yyaccp.hncc.sms.domain;

import java.math.BigDecimal;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 挂号级别对象 sms_registration_rank
 * 
 * @author ruoyi
 * @date 2020-08-08
 */
public class SmsRegistrationRank extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 号别id */
    private Long id;

    /** 号别编码 */
    @Excel(name = "号别编码")
    private String code;

    /** 号别名称 */
    @Excel(name = "号别名称")
    private String name;

    /** 显示顺序号 */
    @Excel(name = "显示顺序号")
    private Long seqNo;

    /** 挂号费 */
    @Excel(name = "挂号费")
    private BigDecimal price;

    /** 状态：0-&gt;无效;1-&gt;有效 */
    @Excel(name = "状态：0-&gt;无效;1-&gt;有效")
    private Integer status;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setCode(String code) 
    {
        this.code = code;
    }

    public String getCode() 
    {
        return code;
    }
    public void setName(String name) 
    {
        this.name = name;
    }

    public String getName() 
    {
        return name;
    }
    public void setSeqNo(Long seqNo) 
    {
        this.seqNo = seqNo;
    }

    public Long getSeqNo() 
    {
        return seqNo;
    }
    public void setPrice(BigDecimal price) 
    {
        this.price = price;
    }

    public BigDecimal getPrice() 
    {
        return price;
    }
    public void setStatus(Integer status) 
    {
        this.status = status;
    }

    public Integer getStatus() 
    {
        return status;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("code", getCode())
            .append("name", getName())
            .append("seqNo", getSeqNo())
            .append("price", getPrice())
            .append("status", getStatus())
            .toString();
    }
}
