package com.yyaccp.hncc.sms.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.yyaccp.hncc.sms.mapper.SmsStaffMapper;
import com.yyaccp.hncc.sms.domain.SmsStaff;
import com.yyaccp.hncc.sms.service.ISmsStaffService;

/**
 * 员工Service业务层处理
 * 
 * @author ruoyi
 * @date 2020-08-23
 */
@Service
public class SmsStaffServiceImpl implements ISmsStaffService 
{
    @Autowired
    private SmsStaffMapper smsStaffMapper;

    /**
     * 查询员工
     * 
     * @param id 员工ID
     * @return 员工
     */
    @Override
    public SmsStaff selectSmsStaffById(Long id)
    {
        return smsStaffMapper.selectSmsStaffById(id);
    }

    /**
     * 查询员工列表
     * 
     * @param smsStaff 员工
     * @return 员工
     */
    @Override
    public List<SmsStaff> selectSmsStaffList(SmsStaff smsStaff)
    {
        return smsStaffMapper.selectSmsStaffList(smsStaff);
    }

    /**
     * 新增员工
     * 
     * @param smsStaff 员工
     * @return 结果
     */
    @Override
    public int insertSmsStaff(SmsStaff smsStaff)
    {
        smsStaff.setCreateTime(DateUtils.getNowDate());
        return smsStaffMapper.insertSmsStaff(smsStaff);
    }

    /**
     * 修改员工
     * 
     * @param smsStaff 员工
     * @return 结果
     */
    @Override
    public int updateSmsStaff(SmsStaff smsStaff)
    {
        return smsStaffMapper.updateSmsStaff(smsStaff);
    }

    /**
     * 批量删除员工
     * 
     * @param ids 需要删除的员工ID
     * @return 结果
     */
    @Override
    public int deleteSmsStaffByIds(Long[] ids)
    {
        return smsStaffMapper.deleteSmsStaffByIds(ids);
    }

    /**
     * 删除员工信息
     * 
     * @param id 员工ID
     * @return 结果
     */
    @Override
    public int deleteSmsStaffById(Long id)
    {
        return smsStaffMapper.deleteSmsStaffById(id);
    }


    /**
     * 根据 排班时间表 查询 员工列表
     * @param staff  排班时间对象
     * @return
     */
    @Override
    public List<SmsStaff> selectSmsStaffBySkd(SmsStaff staff) {
        return smsStaffMapper.selectSmsStaffBySkd(staff);
    }
}
