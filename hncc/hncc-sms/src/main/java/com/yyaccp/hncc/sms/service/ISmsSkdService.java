package com.yyaccp.hncc.sms.service;

import java.util.List;
import com.yyaccp.hncc.sms.domain.SmsSkd;

/**
 * 排班时间Service接口
 * 
 * @author ruoyi
 * @date 2020-08-23
 */
public interface ISmsSkdService 
{
    /**
     * 查询排班时间
     * 
     * @param id 排班时间ID
     * @return 排班时间
     */
    public SmsSkd selectSmsSkdById(Long id);

    /**
     * 查询排班时间列表
     * 
     * @param smsSkd 排班时间
     * @return 排班时间集合
     */
    public List<SmsSkd> selectSmsSkdList(SmsSkd smsSkd);

    /**
     * 新增排班时间
     * 
     * @param smsSkd 排班时间
     * @return 结果
     */
    public int insertSmsSkd(SmsSkd smsSkd);

    /**
     * 修改排班时间
     * 
     * @param smsSkd 排班时间
     * @return 结果
     */
    public int updateSmsSkd(SmsSkd smsSkd);

    /**
     * 批量删除排班时间
     * 
     * @param ids 需要删除的排班时间ID
     * @return 结果
     */
    public int deleteSmsSkdByIds(Long[] ids);

    /**
     * 删除排班时间信息
     * 
     * @param id 排班时间ID
     * @return 结果
     */
    public int deleteSmsSkdById(Long id);
}
