package com.yyaccp.hncc.sms.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.yyaccp.hncc.sms.mapper.SmsSkdMapper;
import com.yyaccp.hncc.sms.domain.SmsSkd;
import com.yyaccp.hncc.sms.service.ISmsSkdService;

/**
 * 排班时间Service业务层处理
 * 
 * @author ruoyi
 * @date 2020-08-23
 */
@Service
public class SmsSkdServiceImpl implements ISmsSkdService 
{
    @Autowired
    private SmsSkdMapper smsSkdMapper;

    /**
     * 查询排班时间
     * 
     * @param id 排班时间ID
     * @return 排班时间
     */
    @Override
    public SmsSkd selectSmsSkdById(Long id)
    {
        return smsSkdMapper.selectSmsSkdById(id);
    }

    /**
     * 查询排班时间列表
     * 
     * @param smsSkd 排班时间
     * @return 排班时间
     */
    @Override
    public List<SmsSkd> selectSmsSkdList(SmsSkd smsSkd)
    {
        return smsSkdMapper.selectSmsSkdList(smsSkd);
    }

    /**
     * 新增排班时间
     * 
     * @param smsSkd 排班时间
     * @return 结果
     */
    @Override
    public int insertSmsSkd(SmsSkd smsSkd)
    {
        return smsSkdMapper.insertSmsSkd(smsSkd);
    }

    /**
     * 修改排班时间
     * 
     * @param smsSkd 排班时间
     * @return 结果
     */
    @Override
    public int updateSmsSkd(SmsSkd smsSkd)
    {
        return smsSkdMapper.updateSmsSkd(smsSkd);
    }

    /**
     * 批量删除排班时间
     * 
     * @param ids 需要删除的排班时间ID
     * @return 结果
     */
    @Override
    public int deleteSmsSkdByIds(Long[] ids)
    {
        return smsSkdMapper.deleteSmsSkdByIds(ids);
    }

    /**
     * 删除排班时间信息
     * 
     * @param id 排班时间ID
     * @return 结果
     */
    @Override
    public int deleteSmsSkdById(Long id)
    {
        return smsSkdMapper.deleteSmsSkdById(id);
    }
}
