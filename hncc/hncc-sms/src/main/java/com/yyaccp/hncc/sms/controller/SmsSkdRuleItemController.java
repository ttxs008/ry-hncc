package com.yyaccp.hncc.sms.controller;

import java.util.List;

import com.yyaccp.hncc.sms.domain.SmsSkdRule;
import com.yyaccp.hncc.sms.service.ISmsSkdRuleService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.yyaccp.hncc.sms.domain.SmsSkdRuleItem;
import com.yyaccp.hncc.sms.service.ISmsSkdRuleItemService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import io.swagger.annotations.*;
//com.yyaccp.hncc.common模块的包引入
import com.yyaccp.hncc.common.vo.sms.SmsSkdRuleItemVO;
import com.yyaccp.hncc.common.util.BeanCopierUtil;

import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 排班规则xController
 *
 * @author ruoyi
 * @date 2020-08-18
 */
@Api(tags = "排班规则详情")
@RestController
@RequestMapping("/sms/skd_rule_item")
public class SmsSkdRuleItemController extends BaseController {
    @Autowired
    private ISmsSkdRuleItemService smsSkdRuleItemService;

    @Autowired
    private ISmsSkdRuleService smsSkdRuleService;
    /**
     * 查询排班规则详情列表
     */
    @PreAuthorize("@ss.hasPermi('sms:skd_rule_item:list')")
    @GetMapping("/list")
    @ApiOperation(value = "查询排班规则详情", notes = "查询所有排班规则详情",
            code = 200, produces = "application/json", protocols = "Http",
            response = TableDataInfo.class, httpMethod = "GET")
    @ApiResponses({
            @ApiResponse(code = 204, message = "操作已经执行成功，但是没有返回数据"),
            @ApiResponse(code = 303, message = "重定向"),
            @ApiResponse(code = 400, message = "参数列表错误（缺少，格式不匹配）"),
            @ApiResponse(code = 500, message = "系统内部错误"),
            @ApiResponse(code = 404, message = "资源，服务未找到"),
            @ApiResponse(code = 200, message = "操作成功"),
            @ApiResponse(code = 401, message = "未授权"),
            @ApiResponse(code = 403, message = "访问受限，授权过期")
    })
    @ApiParam(name = "SmsSkdRuleItemVO对象", type = "SmsSkdRuleItemVO")
    public TableDataInfo list(SmsSkdRuleItemVO smsSkdRuleItemVO) {
        //将Vo转化为实体
        SmsSkdRuleItem smsSkdRuleItem = BeanCopierUtil.copy(smsSkdRuleItemVO, SmsSkdRuleItem.class);
        startPage();
        List<SmsSkdRuleItem> list = smsSkdRuleItemService.selectSmsSkdRuleItemList(smsSkdRuleItem);
        TableDataInfo tableDataInfo = getDataTable(list);
        //替换集合
        tableDataInfo.setRows(BeanCopierUtil.copy(tableDataInfo.getRows(), SmsSkdRuleItemVO.class));
        return tableDataInfo;
    }

    /**
     * 导出排班规则详情列表
     */
    @PreAuthorize("@ss.hasPermi('sms:skd_rule_item:export')")
    @Log(title = "排班规则详情", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    @ApiOperation(value = "导出排班规则详情表", notes = "导出所有排班规则详情",
            code = 200, produces = "application/json", protocols = "Http",
            response = TableDataInfo.class, httpMethod = "GET")
    @ApiResponses({
            @ApiResponse(code = 204, message = "操作已经执行成功，但是没有返回数据"),
            @ApiResponse(code = 303, message = "重定向"),
            @ApiResponse(code = 400, message = "参数列表错误（缺少，格式不匹配）"),
            @ApiResponse(code = 500, message = "系统内部错误"),
            @ApiResponse(code = 404, message = "资源，服务未找到"),
            @ApiResponse(code = 200, message = "操作成功"),
            @ApiResponse(code = 401, message = "未授权"),
            @ApiResponse(code = 403, message = "访问受限，授权过期")
    })
    @ApiParam(name = "SmsSkdRuleItemVO对象", type = "SmsSkdRuleItemVO")
    public AjaxResult export(SmsSkdRuleItemVO smsSkdRuleItemVO) {
        //将VO转化为实体
        SmsSkdRuleItem smsSkdRuleItem = BeanCopierUtil.copy(smsSkdRuleItemVO, SmsSkdRuleItem.class);
        List<SmsSkdRuleItemVO> list = BeanCopierUtil.copy(smsSkdRuleItemService.selectSmsSkdRuleItemList(smsSkdRuleItem), SmsSkdRuleItemVO.class);
        ExcelUtil<SmsSkdRuleItemVO> util = new ExcelUtil<SmsSkdRuleItemVO>(SmsSkdRuleItemVO.class);
        return util.exportExcel(list, "skd_rule_item");
    }

    /**
     * 获取排班规则详细信息
     */
    @PreAuthorize("@ss.hasPermi('sms:skd_rule_item:query')")
    @GetMapping(value = "/{id}")
    @ApiOperation(value = "获取排班规则详细信息",
            notes = "根据排班规则详情id获取科室信息", code = 200,
            produces = "application/json", protocols = "Http", response = AjaxResult.class,
            httpMethod = "GET")
    @ApiResponses({
            @ApiResponse(code = 204, message = "操作已经执行成功，但是没有返回数据"),
            @ApiResponse(code = 303, message = "重定向"),
            @ApiResponse(code = 400, message = "参数列表错误（缺少，格式不匹配）"),
            @ApiResponse(code = 500, message = "系统内部错误"),
            @ApiResponse(code = 404, message = "资源，服务未找到"),
            @ApiResponse(code = 200, message = "操作成功"),
            @ApiResponse(code = 401, message = "未授权"),
            @ApiResponse(code = 403, message = "访问受限，授权过期")
    })
    @ApiParam(name = "smsSkdRuleItem.id", type = "Long")
    public TableDataInfo getInfo(@PathVariable("id") Long id) {
        startPage();
        List<SmsSkdRuleItem> list = smsSkdRuleItemService.selectSmsSkdRuleItemById(id);
        TableDataInfo tableDataInfo = getDataTable(list);
        //替换集合
        tableDataInfo.setRows(BeanCopierUtil.copy(tableDataInfo.getRows(), SmsSkdRuleItemVO.class));
        return tableDataInfo;
    }

    /**
     * 新增排班规则详情
     */
    @PreAuthorize("@ss.hasPermi('sms:skd_rule_item:add')")
    @Log(title = "新增排班规则详情", businessType = BusinessType.INSERT)
    @PostMapping
    @ApiOperation(value = "新增排班规则详情信息", notes = "新增排班规则详情信息", code = 200,
            produces = "application/json", protocols = "Http", response = AjaxResult.class,
            httpMethod = "POST")
    @ApiResponses({
            @ApiResponse(code = 204, message = "操作已经执行成功，但是没有返回数据"),
            @ApiResponse(code = 303, message = "重定向"),
            @ApiResponse(code = 400, message = "参数列表错误（缺少，格式不匹配）"),
            @ApiResponse(code = 500, message = "系统内部错误"),
            @ApiResponse(code = 404, message = "资源，服务未找到"),
            @ApiResponse(code = 200, message = "操作成功"),
            @ApiResponse(code = 401, message = "未授权"),
            @ApiResponse(code = 403, message = "访问受限，授权过期")
    })
    @ApiParam(name = "SmsSkdRuleItemVO对象", type = "SmsSkdRuleItemVO")
    public AjaxResult add(@RequestBody List<SmsSkdRuleItemVO> data) {
        int count=0;
        SmsSkdRule smsSkdRule=new SmsSkdRule();
        smsSkdRule.setDescription(data.get(0).getDescription());
        smsSkdRule.setRuleName(data.get(0).getRuleName());
        smsSkdRule.setDeptId(data.get(0).getDeptId());
        smsSkdRuleService.insertSmsSkdRule(smsSkdRule);
        for (SmsSkdRuleItemVO list:data){
            count+=smsSkdRuleItemService.insertSmsSkdRuleItem(BeanCopierUtil.copy(list, SmsSkdRuleItem.class));
        }
        return toAjax(count);
    }

    /**
     * 修改排班规则详情
     */
    @PreAuthorize("@ss.hasPermi('sms:skd_rule_item:edit')")
    @Log(title = "排班规则详情", businessType = BusinessType.UPDATE)
    @PutMapping
    @ApiOperation(value = "修改排班规则详情信息", notes = "修改排班规则详情信息", code = 200,
            produces = "application/json", protocols = "Http", response = AjaxResult.class,
            httpMethod = "PUT")
    @ApiResponses({
            @ApiResponse(code = 204, message = "操作已经执行成功，但是没有返回数据"),
            @ApiResponse(code = 303, message = "重定向"),
            @ApiResponse(code = 400, message = "参数列表错误（缺少，格式不匹配）"),
            @ApiResponse(code = 500, message = "系统内部错误"),
            @ApiResponse(code = 404, message = "资源，服务未找到"),
            @ApiResponse(code = 200, message = "操作成功"),
            @ApiResponse(code = 401, message = "未授权"),
            @ApiResponse(code = 403, message = "访问受限，授权过期")
    })
    @ApiParam(name = "SmsSkdRuleItemVO对象", type = "SmsSkdRuleItemVO")
    public AjaxResult edit(@RequestBody List<SmsSkdRuleItemVO> data) {
        int num=0;
        SmsSkdRule smsSkdRule=new SmsSkdRule();
        smsSkdRule.setDescription(data.get(0).getDescription());
        smsSkdRule.setRuleName(data.get(0).getRuleName());
        smsSkdRule.setDeptId(data.get(0).getDeptId());
        smsSkdRule.setId(data.get(0).getSkRuleId());
        smsSkdRuleService.updateSmsSkdRule(smsSkdRule);
        for (SmsSkdRuleItemVO list:data){
            num+=smsSkdRuleItemService.updateSmsSkdRuleItem(BeanCopierUtil.copy(list, SmsSkdRuleItem.class));
        }
        return toAjax(num);
    }

    /**
     * 删除排班规则x
     */
    @PreAuthorize("@ss.hasPermi('sms:skd_rule_item:remove')")
    @Log(title = "排班规则详情", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    @ApiOperation(value = "删除排班规则x信息", notes = "删除排班规则详情信息", code = 200,
            produces = "application/json", protocols = "Http", response = AjaxResult.class,
            httpMethod = "DELETE")
    @ApiResponses({
            @ApiResponse(code = 204, message = "操作已经执行成功，但是没有返回数据"),
            @ApiResponse(code = 303, message = "重定向"),
            @ApiResponse(code = 400, message = "参数列表错误（缺少，格式不匹配）"),
            @ApiResponse(code = 500, message = "系统内部错误"),
            @ApiResponse(code = 404, message = "资源，服务未找到"),
            @ApiResponse(code = 200, message = "操作成功"),
            @ApiResponse(code = 401, message = "未授权"),
            @ApiResponse(code = 403, message = "访问受限，授权过期")
    })
    @ApiParam(name = "smsSkdRuleItem.id", type = "Long[]")
    public AjaxResult remove(@PathVariable Long[] ids) {
        return toAjax(smsSkdRuleItemService.deleteSmsSkdRuleItemByIds(ids));
    }
}
