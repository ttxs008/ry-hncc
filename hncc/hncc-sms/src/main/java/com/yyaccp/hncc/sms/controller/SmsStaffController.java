package com.yyaccp.hncc.sms.controller;

import java.util.List;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.yyaccp.hncc.sms.domain.SmsStaff;
import com.yyaccp.hncc.sms.service.ISmsStaffService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import io.swagger.annotations.*;
//com.yyaccp.hncc.common模块的包引入
import com.yyaccp.hncc.common.vo.sms.SmsStaffVO;
import com.yyaccp.hncc.common.util.BeanCopierUtil;

import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 员工Controller
 *
 * @author ruoyi
 * @date 2020-08-23
 */
@Api(tags = "员工")
@RestController
@RequestMapping("/sms/staff")
public class SmsStaffController extends BaseController {
    @Autowired
    private ISmsStaffService smsStaffService;

    /**
     * 查询员工列表
     */
    @PreAuthorize("@ss.hasPermi('sms:staff:list')")
    @GetMapping("/list")
    @ApiOperation(value = "查询员工", notes = "查询所有员工",
            code = 200, produces = "application/json", protocols = "Http",
            response = TableDataInfo.class, httpMethod = "GET")
    @ApiResponses({
            @ApiResponse(code = 204, message = "操作已经执行成功，但是没有返回数据"),
            @ApiResponse(code = 303, message = "重定向"),
            @ApiResponse(code = 400, message = "参数列表错误（缺少，格式不匹配）"),
            @ApiResponse(code = 500, message = "系统内部错误"),
            @ApiResponse(code = 404, message = "资源，服务未找到"),
            @ApiResponse(code = 200, message = "操作成功"),
            @ApiResponse(code = 401, message = "未授权"),
            @ApiResponse(code = 403, message = "访问受限，授权过期")
    })
    @ApiParam(name = "SmsStaffVO对象", type = "SmsStaffVO")
    public TableDataInfo list(SmsStaffVO smsStaffVO) {
        //将Vo转化为实体
        SmsStaff smsStaff = BeanCopierUtil.copy(smsStaffVO, SmsStaff.class);
        startPage();
        List<SmsStaff> list = smsStaffService.selectSmsStaffList(smsStaff);
        TableDataInfo tableDataInfo = getDataTable(list);
        //替换集合
        tableDataInfo.setRows(BeanCopierUtil.copy(tableDataInfo.getRows(), SmsStaffVO.class));
        return tableDataInfo;
    }

    /**
     * 导出员工列表
     */
    @PreAuthorize("@ss.hasPermi('sms:staff:export')")
    @Log(title = "员工", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    @ApiOperation(value = "导出员工表", notes = "导出所有员工",
            code = 200, produces = "application/json", protocols = "Http",
            response = TableDataInfo.class, httpMethod = "GET")
    @ApiResponses({
            @ApiResponse(code = 204, message = "操作已经执行成功，但是没有返回数据"),
            @ApiResponse(code = 303, message = "重定向"),
            @ApiResponse(code = 400, message = "参数列表错误（缺少，格式不匹配）"),
            @ApiResponse(code = 500, message = "系统内部错误"),
            @ApiResponse(code = 404, message = "资源，服务未找到"),
            @ApiResponse(code = 200, message = "操作成功"),
            @ApiResponse(code = 401, message = "未授权"),
            @ApiResponse(code = 403, message = "访问受限，授权过期")
    })
    @ApiParam(name = "SmsStaffVO对象", type = "SmsStaffVO")
    public AjaxResult export(SmsStaffVO smsStaffVO) {
        //将VO转化为实体
        SmsStaff smsStaff = BeanCopierUtil.copy(smsStaffVO, SmsStaff.class);
        List<SmsStaffVO> list = BeanCopierUtil.copy(smsStaffService.selectSmsStaffList(smsStaff), SmsStaffVO.class);
        ExcelUtil<SmsStaffVO> util = new ExcelUtil<SmsStaffVO>(SmsStaffVO.class);
        return util.exportExcel(list, "staff");
    }

    /**
     * 获取员工详细信息
     */
    @PreAuthorize("@ss.hasPermi('sms:staff:query')")
    @GetMapping(value = "/{id}")
    @ApiOperation(value = "获取员工详细信息",
            notes = "根据员工id获取科室信息", code = 200,
            produces = "application/json", protocols = "Http", response = AjaxResult.class,
            httpMethod = "GET")
    @ApiResponses({
            @ApiResponse(code = 204, message = "操作已经执行成功，但是没有返回数据"),
            @ApiResponse(code = 303, message = "重定向"),
            @ApiResponse(code = 400, message = "参数列表错误（缺少，格式不匹配）"),
            @ApiResponse(code = 500, message = "系统内部错误"),
            @ApiResponse(code = 404, message = "资源，服务未找到"),
            @ApiResponse(code = 200, message = "操作成功"),
            @ApiResponse(code = 401, message = "未授权"),
            @ApiResponse(code = 403, message = "访问受限，授权过期")
    })
    @ApiParam(name = "smsStaff.id", type = "Long")
    public AjaxResult getInfo(@PathVariable("id") Long id) {
        return AjaxResult.success(BeanCopierUtil.copy(smsStaffService.selectSmsStaffById(id), SmsStaffVO.class));
    }

    /**
     * 新增员工
     */
    @PreAuthorize("@ss.hasPermi('sms:staff:add')")
    @Log(title = "新增员工", businessType = BusinessType.INSERT)
    @PostMapping
    @ApiOperation(value = "新增员工信息", notes = "新增员工信息", code = 200,
            produces = "application/json", protocols = "Http", response = AjaxResult.class,
            httpMethod = "POST")
    @ApiResponses({
            @ApiResponse(code = 204, message = "操作已经执行成功，但是没有返回数据"),
            @ApiResponse(code = 303, message = "重定向"),
            @ApiResponse(code = 400, message = "参数列表错误（缺少，格式不匹配）"),
            @ApiResponse(code = 500, message = "系统内部错误"),
            @ApiResponse(code = 404, message = "资源，服务未找到"),
            @ApiResponse(code = 200, message = "操作成功"),
            @ApiResponse(code = 401, message = "未授权"),
            @ApiResponse(code = 403, message = "访问受限，授权过期")
    })
    @ApiParam(name = "SmsStaffVO对象", type = "SmsStaffVO")
    public AjaxResult add(@RequestBody SmsStaffVO smsStaffVO) {
        return toAjax(smsStaffService.insertSmsStaff(BeanCopierUtil.copy(smsStaffVO, SmsStaff.class)));
    }

    /**
     * 修改员工
     */
    @PreAuthorize("@ss.hasPermi('sms:staff:edit')")
    @Log(title = "员工", businessType = BusinessType.UPDATE)
    @PutMapping
    @ApiOperation(value = "修改员工信息", notes = "修改员工信息", code = 200,
            produces = "application/json", protocols = "Http", response = AjaxResult.class,
            httpMethod = "PUT")
    @ApiResponses({
            @ApiResponse(code = 204, message = "操作已经执行成功，但是没有返回数据"),
            @ApiResponse(code = 303, message = "重定向"),
            @ApiResponse(code = 400, message = "参数列表错误（缺少，格式不匹配）"),
            @ApiResponse(code = 500, message = "系统内部错误"),
            @ApiResponse(code = 404, message = "资源，服务未找到"),
            @ApiResponse(code = 200, message = "操作成功"),
            @ApiResponse(code = 401, message = "未授权"),
            @ApiResponse(code = 403, message = "访问受限，授权过期")
    })
    @ApiParam(name = "SmsStaffVO对象", type = "SmsStaffVO")
    public AjaxResult edit(@RequestBody SmsStaffVO smsStaffVO) {
        return toAjax(smsStaffService.updateSmsStaff(BeanCopierUtil.copy(smsStaffVO, SmsStaff.class)));
    }

    /**
     * 删除员工
     */
    @PreAuthorize("@ss.hasPermi('sms:staff:remove')")
    @Log(title = "员工", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    @ApiOperation(value = "删除员工信息", notes = "删除员工信息", code = 200,
            produces = "application/json", protocols = "Http", response = AjaxResult.class,
            httpMethod = "DELETE")
    @ApiResponses({
            @ApiResponse(code = 204, message = "操作已经执行成功，但是没有返回数据"),
            @ApiResponse(code = 303, message = "重定向"),
            @ApiResponse(code = 400, message = "参数列表错误（缺少，格式不匹配）"),
            @ApiResponse(code = 500, message = "系统内部错误"),
            @ApiResponse(code = 404, message = "资源，服务未找到"),
            @ApiResponse(code = 200, message = "操作成功"),
            @ApiResponse(code = 401, message = "未授权"),
            @ApiResponse(code = 403, message = "访问受限，授权过期")
    })
    @ApiParam(name = "smsStaff.id", type = "Long[]")
    public AjaxResult remove(@PathVariable Long[] ids) {
        return toAjax(smsStaffService.deleteSmsStaffByIds(ids));
    }



    /**
     * 查询员工列表 根据排班时间表
     *
     */
    @PreAuthorize("@ss.hasPermi('sms:staff:listBySkd')")
    @GetMapping("/listBySkd")
    @ApiOperation(value = "查询员工" , notes = "查询所有员工根据排班时间表" ,
            code = 200, produces = "application/json" , protocols = "Http" ,
            response = TableDataInfo.class, httpMethod = "GET")
    @ApiResponses({
            @ApiResponse(code = 204, message = "操作已经执行成功，但是没有返回数据"),
            @ApiResponse(code = 303, message = "重定向"),
            @ApiResponse(code = 400, message = "参数列表错误（缺少，格式不匹配）"),
            @ApiResponse(code = 500, message = "系统内部错误"),
            @ApiResponse(code = 404, message = "资源，服务未找到"),
            @ApiResponse(code = 200, message = "操作成功"),
            @ApiResponse(code = 401, message = "未授权"),
            @ApiResponse(code = 403, message = "访问受限，授权过期")
    })
    @ApiParam(name = "SmsStaffVO对象" , type = "SmsStaffVO")
    public TableDataInfo listBySkd(SmsStaffVO smsStaffVO) {
        //将Vo转化为实体
        SmsStaff smsStaff=BeanCopierUtil.copy(smsStaffVO,SmsStaff.class);
        startPage();
        List<SmsStaff> list = smsStaffService.selectSmsStaffBySkd(smsStaff);
        TableDataInfo tableDataInfo = getDataTable(list);
        //替换集合
        tableDataInfo.setRows(BeanCopierUtil.copy(tableDataInfo.getRows(), SmsStaffVO.class));
        return tableDataInfo;
    }

}
