package com.yyaccp.hncc.sms.service.impl;

import java.util.List;

import com.ruoyi.common.utils.SecurityUtils;
import com.yyaccp.hncc.sms.mapper.SmsSkdRuleMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.yyaccp.hncc.sms.mapper.SmsSkdRuleItemMapper;
import com.yyaccp.hncc.sms.domain.SmsSkdRuleItem;
import com.yyaccp.hncc.sms.service.ISmsSkdRuleItemService;

/**
 * 排班规则xService业务层处理
 * 
 * @author ruoyi
 * @date 2020-08-18
 */
@Service
public class SmsSkdRuleItemServiceImpl implements ISmsSkdRuleItemService 
{
    @Autowired
    private SmsSkdRuleItemMapper smsSkdRuleItemMapper;

    @Autowired
    private SmsSkdRuleMapper smsSkdRuleMapper;

    /**
     * 查询排班规则x
     * 
     * @param id 排班规则xID
     * @return 排班规则x
     */
    @Override
    public List<SmsSkdRuleItem> selectSmsSkdRuleItemById(Long id)
    {
        return smsSkdRuleItemMapper.selectSmsSkdRuleItemById(id);
    }

    /**
     * 查询排班规则x列表
     * 
     * @param smsSkdRuleItem 排班规则x
     * @return 排班规则x
     */
    @Override
    public List<SmsSkdRuleItem> selectSmsSkdRuleItemList(SmsSkdRuleItem smsSkdRuleItem)
    {
        return smsSkdRuleItemMapper.selectSmsSkdRuleItemList(smsSkdRuleItem);
    }

    /**
     * 新增排班规则x
     * 
     * @param smsSkdRuleItem 排班规则x
     * @return 结果
     */
    @Override
    public int insertSmsSkdRuleItem(SmsSkdRuleItem smsSkdRuleItem)
    {
        smsSkdRuleItem.setSkRuleId(smsSkdRuleMapper.selectByRuleId());
        smsSkdRuleItem.setStatus(1);
        return smsSkdRuleItemMapper.insertSmsSkdRuleItem(smsSkdRuleItem);
    }

    /**
     * 修改排班规则x
     * 
     * @param smsSkdRuleItem 排班规则x
     * @return 结果
     */
    @Override
    public int updateSmsSkdRuleItem(SmsSkdRuleItem smsSkdRuleItem)
    {
        return smsSkdRuleItemMapper.updateSmsSkdRuleItem(smsSkdRuleItem);
    }

    /**
     * 批量删除排班规则x
     * 
     * @param ids 需要删除的排班规则xID
     * @return 结果
     */
    @Override
    public int deleteSmsSkdRuleItemByIds(Long[] ids)
    {
        return smsSkdRuleItemMapper.deleteSmsSkdRuleItemByIds(ids);
    }

    /**
     * 删除排班规则x信息
     * 
     * @param id 排班规则xID
     * @return 结果
     */
    @Override
    public int deleteSmsSkdRuleItemById(Long id)
    {
        return smsSkdRuleItemMapper.deleteSmsSkdRuleItemById(id);
    }
}
