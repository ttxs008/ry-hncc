package com.yyaccp.hncc.sms.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 排班时间对象 sms_skd
 * 
 * @author ruoyi
 * @date 2020-08-23
 */
public class SmsSkd extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    private Long id;

    /** 上班日期 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "上班日期", width = 30, dateFormat = "yyyy-MM-dd")
    private Date date;

    /** 状态 */
    @Excel(name = "状态")
    private Integer status;

    /** 剩余号数 */
    @Excel(name = "剩余号数")
    private Long remain;

    /** 午别（0上午1下午） */
    @Excel(name = "午别", readConverterExp = "0=上午1下午")
    private Integer noon;

    /** 员工id */
    @Excel(name = "员工id")
    private Long staffId;

    /** 科室id */
    @Excel(name = "科室id")
    private Long deptId;

    /** 挂号限额 */
    @Excel(name = "挂号限额")
    private Long skLimit;

    /** 规则id */
    @Excel(name = "规则id")
    private Long smsSkdRuleId;

    /** 查询参数,排班开始时间,范围查询 */
    @Excel(name = "查询参数,排班开始时间,范围查询")
    private Date start;

    /** 部门名称 */
    @Excel(name = "部门名称")
    private String deptName;

    /** 医生名称 */
    @Excel(name = "医生名称")
    private String userName;

    /** 挂号级别名称 */
    @Excel(name = "挂号级别名称")
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getDeptName() {
        return deptName;
    }

    public void setDeptName(String deptName) {
        this.deptName = deptName;
    }

    public Long getSmsSkdRuleId() {
        return smsSkdRuleId;
    }

    public void setSmsSkdRuleId(Long smsSkdRuleId) {
        this.smsSkdRuleId = smsSkdRuleId;
    }

    public Date getStart() {
        return start;
    }

    public void setStart(Date start) {
        this.start = start;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setDate(Date date) 
    {
        this.date = date;
    }

    public Date getDate() 
    {
        return date;
    }
    public void setStatus(Integer status) 
    {
        this.status = status;
    }

    public Integer getStatus() 
    {
        return status;
    }
    public void setRemain(Long remain) 
    {
        this.remain = remain;
    }

    public Long getRemain() 
    {
        return remain;
    }
    public void setNoon(Integer noon) 
    {
        this.noon = noon;
    }

    public Integer getNoon() 
    {
        return noon;
    }
    public void setStaffId(Long staffId) 
    {
        this.staffId = staffId;
    }

    public Long getStaffId() 
    {
        return staffId;
    }
    public void setDeptId(Long deptId) 
    {
        this.deptId = deptId;
    }

    public Long getDeptId() 
    {
        return deptId;
    }
    public void setSkLimit(Long skLimit) 
    {
        this.skLimit = skLimit;
    }

    public Long getSkLimit() 
    {
        return skLimit;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("date", getDate())
            .append("status", getStatus())
            .append("remain", getRemain())
            .append("noon", getNoon())
            .append("staffId", getStaffId())
            .append("deptId", getDeptId())
            .append("skLimit", getSkLimit())
            .toString();
    }
}
