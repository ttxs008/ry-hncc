package com.yyaccp.hncc.sms.mapper;

import java.util.List;
import com.yyaccp.hncc.sms.domain.SmsSkdRuleItem;

/**
 * 排班规则xMapper接口
 * 
 * @author ruoyi
 * @date 2020-08-18
 */
public interface SmsSkdRuleItemMapper 
{
    /**
     * 查询排班规则x
     * 
     * @param id 排班规则xID
     * @return 排班规则x
     */
    public List<SmsSkdRuleItem> selectSmsSkdRuleItemById(Long id);

    /**
     * 查询排班规则x列表
     * 
     * @param smsSkdRuleItem 排班规则x
     * @return 排班规则x集合
     */
    public List<SmsSkdRuleItem> selectSmsSkdRuleItemList(SmsSkdRuleItem smsSkdRuleItem);

    /**
     * 新增排班规则x
     * 
     * @param smsSkdRuleItem 排班规则x
     * @return 结果
     */
    public int insertSmsSkdRuleItem(SmsSkdRuleItem smsSkdRuleItem);

    /**
     * 修改排班规则x
     * 
     * @param smsSkdRuleItem 排班规则x
     * @return 结果
     */
    public int updateSmsSkdRuleItem(SmsSkdRuleItem smsSkdRuleItem);

    /**
     * 删除排班规则x
     * 
     * @param id 排班规则xID
     * @return 结果
     */
    public int deleteSmsSkdRuleItemById(Long id);

    /**
     * 批量删除排班规则x
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteSmsSkdRuleItemByIds(Long[] ids);
}
