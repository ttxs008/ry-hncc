package com.yyaccp.hncc.sms.service.impl;

import java.util.Date;
import java.util.List;

import com.ruoyi.common.utils.SecurityUtils;
import com.yyaccp.hncc.sms.mapper.SmsSkdRuleItemMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.yyaccp.hncc.sms.mapper.SmsSkdRuleMapper;
import com.yyaccp.hncc.sms.domain.SmsSkdRule;
import com.yyaccp.hncc.sms.service.ISmsSkdRuleService;

/**
 * 排班规则Service业务层处理
 * 
 * @author ruoyi
 * @date 2020-08-08
 */
@Service
public class SmsSkdRuleServiceImpl implements ISmsSkdRuleService 
{
    @Autowired
    private SmsSkdRuleMapper smsSkdRuleMapper;

    @Autowired
    private SmsSkdRuleItemMapper smsSkdRuleItemMapper;

    /**
     * 查询排班规则
     * 
     * @param id 排班规则ID
     * @return 排班规则
     */
    @Override
    public SmsSkdRule selectSmsSkdRuleById(Long id)
    {
        return smsSkdRuleMapper.selectSmsSkdRuleById(id);
    }

    /**
     * 查询排班规则列表
     * 
     * @param smsSkdRule 排班规则
     * @return 排班规则
     */
    @Override
    public List<SmsSkdRule> selectSmsSkdRuleList(SmsSkdRule smsSkdRule)
    {
        return smsSkdRuleMapper.selectSmsSkdRuleList(smsSkdRule);
    }

    /**
     * 新增排班规则
     * 
     * @param smsSkdRule 排班规则
     * @return 结果
     */
    @Override
    public int insertSmsSkdRule(SmsSkdRule smsSkdRule)
    {
        Long userId = smsSkdRuleMapper.checkUserName(SecurityUtils.getUsername());
        smsSkdRule.setOperatorId(userId);
        smsSkdRule.setStatus(1);
        smsSkdRule.setOperateTime(new Date());
        return smsSkdRuleMapper.insertSmsSkdRule(smsSkdRule);
    }

    /**
     * 修改排班规则
     * 
     * @param smsSkdRule 排班规则
     * @return 结果
     */
    @Override
    public int updateSmsSkdRule(SmsSkdRule smsSkdRule)
    {
        return smsSkdRuleMapper.updateSmsSkdRule(smsSkdRule);
    }

    /**
     * 批量删除排班规则
     * 
     * @param ids 需要删除的排班规则ID
     * @return 结果
     */
    @Override
    public int deleteSmsSkdRuleByIds(Long[] ids)
    {
        smsSkdRuleItemMapper.deleteSmsSkdRuleItemByIds(ids);
        return smsSkdRuleMapper.deleteSmsSkdRuleByIds(ids);
    }

    /**
     * 删除排班规则信息
     * 
     * @param id 排班规则ID
     * @return 结果
     */
    @Override
    public int deleteSmsSkdRuleById(Long id)
    {
        smsSkdRuleItemMapper.deleteSmsSkdRuleItemById(id);
        return smsSkdRuleMapper.deleteSmsSkdRuleById(id);
    }

    /**
     * 根据时间倒序查询最新的一条数据
     * @return
     */
    @Override
    public Long selectByRuleId() {
        return smsSkdRuleMapper.selectByRuleId();
    }

    @Override
    public Long checkUserName(String userName) {
        return smsSkdRuleMapper.checkUserName(userName);
    }
}
