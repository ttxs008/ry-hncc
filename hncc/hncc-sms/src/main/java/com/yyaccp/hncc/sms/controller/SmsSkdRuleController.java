package com.yyaccp.hncc.sms.controller;

import java.util.List;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.yyaccp.hncc.sms.domain.SmsSkdRule;
import com.yyaccp.hncc.sms.service.ISmsSkdRuleService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 排班规则Controller
 * 
 * @author ruoyi
 * @date 2020-08-08
 */
@RestController
@RequestMapping("/sms/skd_rule")
@Api("排班规则管理")
public class SmsSkdRuleController extends BaseController
{
    @Autowired
    private ISmsSkdRuleService smsSkdRuleService;

    /**
     * 查询排班规则列表
     */
    @ApiOperation("排班规则列表")
    @PreAuthorize("@ss.hasPermi('sms:skd_rule:list')")
    @GetMapping("/list")
    public TableDataInfo list(SmsSkdRule smsSkdRule)
    {
        startPage();
        List<SmsSkdRule> list = smsSkdRuleService.selectSmsSkdRuleList(smsSkdRule);
        return getDataTable(list);
    }

    /**
     * 导出排班规则列表
     */
    @PreAuthorize("@ss.hasPermi('sms:skd_rule:export')")
    @Log(title = "排班规则", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(SmsSkdRule smsSkdRule)
    {
        List<SmsSkdRule> list = smsSkdRuleService.selectSmsSkdRuleList(smsSkdRule);
        ExcelUtil<SmsSkdRule> util = new ExcelUtil<SmsSkdRule>(SmsSkdRule.class);
        return util.exportExcel(list, "skd_rule");
    }

    /**
     * 获取排班规则详细信息
     */
    @PreAuthorize("@ss.hasPermi('sms:skd_rule:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(smsSkdRuleService.selectSmsSkdRuleById(id));
    }

    /**
     * 新增排班规则
     */
    @PreAuthorize("@ss.hasPermi('sms:skd_rule:add')")
    @Log(title = "排班规则", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody SmsSkdRule smsSkdRule)
    {
        return toAjax(smsSkdRuleService.insertSmsSkdRule(smsSkdRule));
    }

    /**
     * 修改排班规则
     */
    @PreAuthorize("@ss.hasPermi('sms:skd_rule:edit')")
    @Log(title = "排班规则", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody SmsSkdRule smsSkdRule)
    {
        return toAjax(smsSkdRuleService.updateSmsSkdRule(smsSkdRule));
    }

    /**
     * 删除排班规则
     */
    @PreAuthorize("@ss.hasPermi('sms:skd_rule:remove')")
    @Log(title = "排班规则", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(smsSkdRuleService.deleteSmsSkdRuleByIds(ids));
    }
}
