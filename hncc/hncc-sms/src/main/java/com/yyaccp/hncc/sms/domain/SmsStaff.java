package com.yyaccp.hncc.sms.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 员工对象 sms_staff
 * 
 * @author ruoyi
 * @date 2020-08-23
 */
public class SmsStaff extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** id */
    private Long id;

    /** 登录名 */
    @Excel(name = "登录名")
    private String username;

    /** 登录密码 */
    @Excel(name = "登录密码")
    private String password;

    /** 状态：0-&gt;有效;1-&gt;无效 */
    @Excel(name = "状态：0-&gt;有效;1-&gt;无效")
    private Integer status;

    /** 性别 */
    @Excel(name = "性别")
    private Integer gender;

    /** 是否参与排班：0-&gt;否;1-&gt;是 */
    @Excel(name = "是否参与排班：0-&gt;否;1-&gt;是")
    private Integer skdFlag;

    /** 职称 */
    @Excel(name = "职称")
    private String title;

    /** 真实姓名 */
    @Excel(name = "真实姓名")
    private String name;

    /** 所在科室 */
    @Excel(name = "所在科室")
    private Long deptId;

    /** 角色 */
    @Excel(name = "角色")
    private Long roleId;

    /** 挂号级别id */
    @Excel(name = "挂号级别id")
    private Long registrationRankId;

    /** 对应 某一天 上午或下午的排班*/
    private SmsSkd smsSkd;

    public SmsSkd getSmsSkd() {
        return smsSkd;
    }

    public void setSmsSkd(SmsSkd smsSkd) {
        this.smsSkd = smsSkd;
    }

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setUsername(String username) 
    {
        this.username = username;
    }

    public String getUsername() 
    {
        return username;
    }
    public void setPassword(String password) 
    {
        this.password = password;
    }

    public String getPassword() 
    {
        return password;
    }
    public void setStatus(Integer status) 
    {
        this.status = status;
    }

    public Integer getStatus() 
    {
        return status;
    }
    public void setGender(Integer gender) 
    {
        this.gender = gender;
    }

    public Integer getGender() 
    {
        return gender;
    }
    public void setSkdFlag(Integer skdFlag) 
    {
        this.skdFlag = skdFlag;
    }

    public Integer getSkdFlag() 
    {
        return skdFlag;
    }
    public void setTitle(String title) 
    {
        this.title = title;
    }

    public String getTitle() 
    {
        return title;
    }
    public void setName(String name) 
    {
        this.name = name;
    }

    public String getName() 
    {
        return name;
    }
    public void setDeptId(Long deptId) 
    {
        this.deptId = deptId;
    }

    public Long getDeptId() 
    {
        return deptId;
    }
    public void setRoleId(Long roleId) 
    {
        this.roleId = roleId;
    }

    public Long getRoleId() 
    {
        return roleId;
    }
    public void setRegistrationRankId(Long registrationRankId) 
    {
        this.registrationRankId = registrationRankId;
    }

    public Long getRegistrationRankId() 
    {
        return registrationRankId;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("username", getUsername())
            .append("password", getPassword())
            .append("status", getStatus())
            .append("createTime", getCreateTime())
            .append("gender", getGender())
            .append("skdFlag", getSkdFlag())
            .append("title", getTitle())
            .append("name", getName())
            .append("deptId", getDeptId())
            .append("roleId", getRoleId())
            .append("registrationRankId", getRegistrationRankId())
            .toString();
    }
}
