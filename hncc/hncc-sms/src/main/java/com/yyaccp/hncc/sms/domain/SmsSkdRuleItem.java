package com.yyaccp.hncc.sms.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 排班规则x对象 sms_skd_rule_item
 * 
 * @author ruoyi
 * @date 2020-08-18
 */
public class SmsSkdRuleItem extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** id */
    private Long id;

    /** 医生id */
    @Excel(name = "医生id")
    private Long staffId;

    /** 一周中的排班时间 */
    @Excel(name = "一周中的排班时间")
    private String daysOfWeek;

    /** 状态 */
    @Excel(name = "状态")
    private Integer status;

    /** 挂号限额 */
    @Excel(name = "挂号限额")
    private Long skLimit;

    /** 排班规则id(FK) */
    @Excel(name = "排班规则id(FK)")
    private Long skRuleId;

    /** 医生名称 */
    @Excel(name = "医生名称")
    private String name;

    /** 科室id */
    @Excel(name = "科室id")
    private Long deptId;

    /** 部门名称 */
    @Excel(name = "部门名称")
    private String deptName;

    /** 规则名 */
    @Excel(name = "规则名")
    private String ruleName;

    /** 描述 */
    @Excel(name = "描述")
    private String description;

    public String getRuleName() {
        return ruleName;
    }

    public void setRuleName(String ruleName) {
        this.ruleName = ruleName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDeptName() {
        return deptName;
    }

    public void setDeptName(String deptName) {
        this.deptName = deptName;
    }

    public Long getDeptId() {
        return deptId;
    }

    public void setDeptId(Long deptId) {
        this.deptId = deptId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setStaffId(Long staffId) 
    {
        this.staffId = staffId;
    }

    public Long getStaffId() 
    {
        return staffId;
    }
    public void setDaysOfWeek(String daysOfWeek) 
    {
        this.daysOfWeek = daysOfWeek;
    }

    public String getDaysOfWeek() 
    {
        return daysOfWeek;
    }
    public void setStatus(Integer status) 
    {
        this.status = status;
    }

    public Integer getStatus() 
    {
        return status;
    }
    public void setSkLimit(Long skLimit) 
    {
        this.skLimit = skLimit;
    }

    public Long getSkLimit() 
    {
        return skLimit;
    }
    public void setSkRuleId(Long skRuleId) 
    {
        this.skRuleId = skRuleId;
    }

    public Long getSkRuleId() 
    {
        return skRuleId;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("staffId", getStaffId())
            .append("daysOfWeek", getDaysOfWeek())
            .append("status", getStatus())
            .append("skLimit", getSkLimit())
            .append("skRuleId", getSkRuleId())
            .toString();
    }
}
