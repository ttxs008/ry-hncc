package com.yyaccp.hncc.sms.service;

import java.util.List;
import com.yyaccp.hncc.sms.domain.SmsSkdRuleItem;

/**
 * 排班规则xService接口
 * 
 * @author ruoyi
 * @date 2020-08-18
 */
public interface ISmsSkdRuleItemService 
{
    /**
     * 查询排班规则x
     * 
     * @param id 排班规则xID
     * @return 排班规则x
     */
    public List<SmsSkdRuleItem> selectSmsSkdRuleItemById(Long id);

    /**
     * 查询排班规则x列表
     * 
     * @param smsSkdRuleItem 排班规则x
     * @return 排班规则x集合
     */
    public List<SmsSkdRuleItem> selectSmsSkdRuleItemList(SmsSkdRuleItem smsSkdRuleItem);

    /**
     * 新增排班规则x
     * 
     * @param smsSkdRuleItem 排班规则x
     * @return 结果
     */
    public int insertSmsSkdRuleItem(SmsSkdRuleItem smsSkdRuleItem);

    /**
     * 修改排班规则x
     * 
     * @param smsSkdRuleItem 排班规则x
     * @return 结果
     */
    public int updateSmsSkdRuleItem(SmsSkdRuleItem smsSkdRuleItem);

    /**
     * 批量删除排班规则x
     * 
     * @param ids 需要删除的排班规则xID
     * @return 结果
     */
    public int deleteSmsSkdRuleItemByIds(Long[] ids);

    /**
     * 删除排班规则x信息
     * 
     * @param id 排班规则xID
     * @return 结果
     */
    public int deleteSmsSkdRuleItemById(Long id);
}
