package com.yyaccp.hncc.sms.service;

import java.util.List;
import com.yyaccp.hncc.sms.domain.SmsStaff;

/**
 * 员工Service接口
 * 
 * @author ruoyi
 * @date 2020-08-23
 */
public interface ISmsStaffService 
{
    /**
     * 查询员工
     * 
     * @param id 员工ID
     * @return 员工
     */
    public SmsStaff selectSmsStaffById(Long id);

    /**
     * 查询员工列表
     * 
     * @param smsStaff 员工
     * @return 员工集合
     */
    public List<SmsStaff> selectSmsStaffList(SmsStaff smsStaff);

    /**
     * 新增员工
     * 
     * @param smsStaff 员工
     * @return 结果
     */
    public int insertSmsStaff(SmsStaff smsStaff);

    /**
     * 修改员工
     * 
     * @param smsStaff 员工
     * @return 结果
     */
    public int updateSmsStaff(SmsStaff smsStaff);

    /**
     * 批量删除员工
     * 
     * @param ids 需要删除的员工ID
     * @return 结果
     */
    public int deleteSmsStaffByIds(Long[] ids);

    /**
     * 删除员工信息
     * 
     * @param id 员工ID
     * @return 结果
     */
    public int deleteSmsStaffById(Long id);

    /**
     * 根据 排班时间表 查询 员工列表
     * @param staff  排班时间对象
     * @return
     */
    public List<SmsStaff> selectSmsStaffBySkd(SmsStaff staff);
}
