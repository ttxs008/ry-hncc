package com.yyaccp.hncc.sms.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.yyaccp.hncc.sms.mapper.SmsRegistrationRankMapper;
import com.yyaccp.hncc.sms.domain.SmsRegistrationRank;
import com.yyaccp.hncc.sms.service.ISmsRegistrationRankService;

/**
 * 挂号级别Service业务层处理
 * 
 * @author ruoyi
 * @date 2020-08-08
 */
@Service
public class SmsRegistrationRankServiceImpl implements ISmsRegistrationRankService 
{
    @Autowired
    private SmsRegistrationRankMapper smsRegistrationRankMapper;

    /**
     * 查询挂号级别
     * 
     * @param id 挂号级别ID
     * @return 挂号级别
     */
    @Override
    public SmsRegistrationRank selectSmsRegistrationRankById(Long id)
    {
        return smsRegistrationRankMapper.selectSmsRegistrationRankById(id);
    }

    /**
     * 查询挂号级别列表
     * 
     * @param smsRegistrationRank 挂号级别
     * @return 挂号级别
     */
    @Override
    public List<SmsRegistrationRank> selectSmsRegistrationRankList(SmsRegistrationRank smsRegistrationRank)
    {
        return smsRegistrationRankMapper.selectSmsRegistrationRankList(smsRegistrationRank);
    }

    /**
     * 新增挂号级别
     * 
     * @param smsRegistrationRank 挂号级别
     * @return 结果
     */
    @Override
    public int insertSmsRegistrationRank(SmsRegistrationRank smsRegistrationRank)
    {
        return smsRegistrationRankMapper.insertSmsRegistrationRank(smsRegistrationRank);
    }

    /**
     * 修改挂号级别
     * 
     * @param smsRegistrationRank 挂号级别
     * @return 结果
     */
    @Override
    public int updateSmsRegistrationRank(SmsRegistrationRank smsRegistrationRank)
    {
        return smsRegistrationRankMapper.updateSmsRegistrationRank(smsRegistrationRank);
    }

    /**
     * 批量删除挂号级别
     * 
     * @param ids 需要删除的挂号级别ID
     * @return 结果
     */
    @Override
    public int deleteSmsRegistrationRankByIds(Long[] ids)
    {
        return smsRegistrationRankMapper.deleteSmsRegistrationRankByIds(ids);
    }

    /**
     * 删除挂号级别信息
     * 
     * @param id 挂号级别ID
     * @return 结果
     */
    @Override
    public int deleteSmsRegistrationRankById(Long id)
    {
        return smsRegistrationRankMapper.deleteSmsRegistrationRankById(id);
    }
}
