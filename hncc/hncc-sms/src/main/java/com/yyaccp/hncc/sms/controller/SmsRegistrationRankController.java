package com.yyaccp.hncc.sms.controller;

import java.util.List;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.yyaccp.hncc.sms.domain.SmsRegistrationRank;
import com.yyaccp.hncc.sms.service.ISmsRegistrationRankService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 挂号级别Controller
 * 
 * @author ruoyi
 * @date 2020-08-08
 */
@RestController
@RequestMapping("/sms/registration_rank")
public class SmsRegistrationRankController extends BaseController
{
    @Autowired
    private ISmsRegistrationRankService smsRegistrationRankService;

    /**
     * 查询挂号级别列表
     */
    @PreAuthorize("@ss.hasPermi('sms:registration_rank:list')")
    @GetMapping("/list")
    public TableDataInfo list(SmsRegistrationRank smsRegistrationRank)
    {
        startPage();
        List<SmsRegistrationRank> list = smsRegistrationRankService.selectSmsRegistrationRankList(smsRegistrationRank);
        return getDataTable(list);
    }

    /**
     * 导出挂号级别列表
     */
    @PreAuthorize("@ss.hasPermi('sms:registration_rank:export')")
    @Log(title = "挂号级别", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(SmsRegistrationRank smsRegistrationRank)
    {
        List<SmsRegistrationRank> list = smsRegistrationRankService.selectSmsRegistrationRankList(smsRegistrationRank);
        ExcelUtil<SmsRegistrationRank> util = new ExcelUtil<SmsRegistrationRank>(SmsRegistrationRank.class);
        return util.exportExcel(list, "registration_rank");
    }

    /**
     * 获取挂号级别详细信息
     */
    @PreAuthorize("@ss.hasPermi('sms:registration_rank:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(smsRegistrationRankService.selectSmsRegistrationRankById(id));
    }

    /**
     * 新增挂号级别
     */
    @PreAuthorize("@ss.hasPermi('sms:registration_rank:add')")
    @Log(title = "挂号级别", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody SmsRegistrationRank smsRegistrationRank)
    {
        return toAjax(smsRegistrationRankService.insertSmsRegistrationRank(smsRegistrationRank));
    }

    /**
     * 修改挂号级别
     */
    @PreAuthorize("@ss.hasPermi('sms:registration_rank:edit')")
    @Log(title = "挂号级别", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody SmsRegistrationRank smsRegistrationRank)
    {
        return toAjax(smsRegistrationRankService.updateSmsRegistrationRank(smsRegistrationRank));
    }

    /**
     * 删除挂号级别
     */
    @PreAuthorize("@ss.hasPermi('sms:registration_rank:remove')")
    @Log(title = "挂号级别", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(smsRegistrationRankService.deleteSmsRegistrationRankByIds(ids));
    }
}
