package com.yyaccp.hncc.sms.mapper;

import java.util.List;
import com.yyaccp.hncc.sms.domain.SmsSkdRule;

/**
 * 排班规则Mapper接口
 * 
 * @author ruoyi
 * @date 2020-08-08
 */
public interface SmsSkdRuleMapper 
{
    /**
     * 查询排班规则
     * 
     * @param id 排班规则ID
     * @return 排班规则
     */
    public SmsSkdRule selectSmsSkdRuleById(Long id);

    /**
     * 查询排班规则列表
     * 
     * @param smsSkdRule 排班规则
     * @return 排班规则集合
     */
    public List<SmsSkdRule> selectSmsSkdRuleList(SmsSkdRule smsSkdRule);

    /**
     * 新增排班规则
     * 
     * @param smsSkdRule 排班规则
     * @return 结果
     */
    public int insertSmsSkdRule(SmsSkdRule smsSkdRule);

    /**
     * 修改排班规则
     * 
     * @param smsSkdRule 排班规则
     * @return 结果
     */
    public int updateSmsSkdRule(SmsSkdRule smsSkdRule);

    /**
     * 删除排班规则
     * 
     * @param id 排班规则ID
     * @return 结果
     */
    public int deleteSmsSkdRuleById(Long id);

    /**
     * 批量删除排班规则
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteSmsSkdRuleByIds(Long[] ids);

    /**
     * 根据时间倒序查询最新的一条数据
     * @return
     */
    public Long selectByRuleId();

    /**
     * 根据当前登录的用户名查询id
     * @param userName
     * @return
     */
    public Long checkUserName(String userName);
}
