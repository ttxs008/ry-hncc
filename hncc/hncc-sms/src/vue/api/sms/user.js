import request from '@/utils/request'

// 查询用户信息列表
export function listSysUserManage(query) {
  return request({
    url: '/sms/user/list',
    method: 'get',
    params: query
  })
}

// 查询用户信息详细
export function getSysUserManage(userId) {
  return request({
    url: '/sms/user/' + userId,
    method: 'get'
  })
}

// 新增用户信息
export function addSysUserManage(data) {
  return request({
    url: '/sms/user',
    method: 'post',
    data: data
  })
}

// 修改用户信息
export function updateSysUserManage(data) {
  return request({
    url: '/sms/user',
    method: 'put',
    data: data
  })
}

// 删除用户信息
export function delSysUserManage(userId) {
  return request({
    url: '/sms/user/' + userId,
    method: 'delete'
  })
}

// 导出用户信息
export function exportSysUserManage(query) {
  return request({
    url: '/sms/user/export',
    method: 'get',
    params: query
  })
}
