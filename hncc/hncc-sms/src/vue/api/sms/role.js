import request from '@/utils/request'

// 查询角色列表列表
export function listRole(query) {
  return request({
    url: '/sms/role/list',
    method: 'get',
    params: query
  })
}

// 查询角色列表详细
export function getRole(roleId) {
  return request({
    url: '/sms/role/' + roleId,
    method: 'get'
  })
}

// 新增角色列表
export function addRole(data) {
  return request({
    url: '/sms/role',
    method: 'post',
    data: data
  })
}

// 修改角色列表
export function updateRole(data) {
  return request({
    url: '/sms/role',
    method: 'put',
    data: data
  })
}

// 删除角色列表
export function delRole(roleId) {
  return request({
    url: '/sms/role/' + roleId,
    method: 'delete'
  })
}

// 导出角色列表
export function exportRole(query) {
  return request({
    url: '/sms/role/export',
    method: 'get',
    params: query
  })
}