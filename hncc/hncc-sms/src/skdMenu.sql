-- 菜单 SQL
insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('排班时间', '2131', '1', 'skd', 'sms/skd/index', 1, 'C', '0', '0', 'sms:skd:list', '#', 'admin', '2018-03-01', 'ry', '2018-03-01', '排班时间菜单');

-- 按钮父菜单ID
SELECT @parentId := LAST_INSERT_ID();

-- 按钮 SQL
insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('排班时间查询', @parentId, '1',  '#', '', 1,  'F', '0',  '0', 'sms:skd:query',        '#', 'admin', '2018-03-01', 'ry', '2018-03-01', '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('排班时间新增', @parentId, '2',  '#', '', 1,  'F', '0',  '0', 'sms:skd:add',          '#', 'admin', '2018-03-01', 'ry', '2018-03-01', '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('排班时间修改', @parentId, '3',  '#', '', 1,  'F', '0',  '0', 'sms:skd:edit',         '#', 'admin', '2018-03-01', 'ry', '2018-03-01', '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('排班时间删除', @parentId, '4',  '#', '', 1,  'F', '0',  '0', 'sms:skd:remove',       '#', 'admin', '2018-03-01', 'ry', '2018-03-01', '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('排班时间导出', @parentId, '5',  '#', '', 1,  'F', '0',  '0', 'sms:skd:export',       '#', 'admin', '2018-03-01', 'ry', '2018-03-01', '');