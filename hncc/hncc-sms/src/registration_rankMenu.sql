-- 菜单 SQL
insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('挂号级别', '2000', '1', 'registration_rank', 'sms/registration_rank/index', 1, 'C', '0', '0', 'sms:registration_rank:list', '#', 'admin', '2018-03-01', 'ry', '2018-03-01', '挂号级别菜单');

-- 按钮父菜单ID
SELECT @parentId := LAST_INSERT_ID();

-- 按钮 SQL
insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('挂号级别查询', @parentId, '1',  '#', '', 1,  'F', '0',  '0', 'sms:registration_rank:query',        '#', 'admin', '2018-03-01', 'ry', '2018-03-01', '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('挂号级别新增', @parentId, '2',  '#', '', 1,  'F', '0',  '0', 'sms:registration_rank:add',          '#', 'admin', '2018-03-01', 'ry', '2018-03-01', '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('挂号级别修改', @parentId, '3',  '#', '', 1,  'F', '0',  '0', 'sms:registration_rank:edit',         '#', 'admin', '2018-03-01', 'ry', '2018-03-01', '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('挂号级别删除', @parentId, '4',  '#', '', 1,  'F', '0',  '0', 'sms:registration_rank:remove',       '#', 'admin', '2018-03-01', 'ry', '2018-03-01', '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('挂号级别导出', @parentId, '5',  '#', '', 1,  'F', '0',  '0', 'sms:registration_rank:export',       '#', 'admin', '2018-03-01', 'ry', '2018-03-01', '');