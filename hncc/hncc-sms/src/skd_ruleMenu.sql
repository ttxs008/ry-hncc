--先导入排版规则表--
INSERT INTO `hncc`.`sys_menu`(`menu_id`, `menu_name`, `parent_id`, `order_num`, `path`, `component`, `is_frame`, `menu_type`, `visible`, `status`, `perms`, `icon`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`) VALUES (2007, '排班管理', 0, 1, 'skd', NULL, 1, 'M', '0', '0', NULL, 'chart', 'admin', '2020-08-08 14:20:39', '', NULL, '');

-- 菜单 SQL
insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('排班规则', '2000', '1', 'skd_rule', 'sms/skd_rule/index', 1, 'C', '0', '0', 'sms:skd_rule:list', '#', 'admin', '2018-03-01', 'ry', '2018-03-01', '排班规则菜单');

-- 按钮父菜单ID
SELECT @parentId := LAST_INSERT_ID();

-- 按钮 SQL
insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('排班规则查询', @parentId, '1',  '#', '', 1,  'F', '0',  '0', 'sms:skd_rule:query',        '#', 'admin', '2018-03-01', 'ry', '2018-03-01', '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('排班规则新增', @parentId, '2',  '#', '', 1,  'F', '0',  '0', 'sms:skd_rule:add',          '#', 'admin', '2018-03-01', 'ry', '2018-03-01', '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('排班规则修改', @parentId, '3',  '#', '', 1,  'F', '0',  '0', 'sms:skd_rule:edit',         '#', 'admin', '2018-03-01', 'ry', '2018-03-01', '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('排班规则删除', @parentId, '4',  '#', '', 1,  'F', '0',  '0', 'sms:skd_rule:remove',       '#', 'admin', '2018-03-01', 'ry', '2018-03-01', '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('排班规则导出', @parentId, '5',  '#', '', 1,  'F', '0',  '0', 'sms:skd_rule:export',       '#', 'admin', '2018-03-01', 'ry', '2018-03-01', '');