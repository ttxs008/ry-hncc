-- 菜单 SQL
insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('发票', '3', '1', 'invoice_record', 'bms/invoice_record/index', 1, 'C', '0', '0', 'bms:invoice_record:list', '#', 'admin', '2018-03-01', 'ry', '2018-03-01', '发票菜单');

-- 按钮父菜单ID
SELECT @parentId := LAST_INSERT_ID();

-- 按钮 SQL
insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('发票查询', @parentId, '1',  '#', '', 1,  'F', '0',  '0', 'bms:invoice_record:query',        '#', 'admin', '2018-03-01', 'ry', '2018-03-01', '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('发票新增', @parentId, '2',  '#', '', 1,  'F', '0',  '0', 'bms:invoice_record:add',          '#', 'admin', '2018-03-01', 'ry', '2018-03-01', '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('发票修改', @parentId, '3',  '#', '', 1,  'F', '0',  '0', 'bms:invoice_record:edit',         '#', 'admin', '2018-03-01', 'ry', '2018-03-01', '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('发票删除', @parentId, '4',  '#', '', 1,  'F', '0',  '0', 'bms:invoice_record:remove',       '#', 'admin', '2018-03-01', 'ry', '2018-03-01', '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('发票导出', @parentId, '5',  '#', '', 1,  'F', '0',  '0', 'bms:invoice_record:export',       '#', 'admin', '2018-03-01', 'ry', '2018-03-01', '');


-- 发票号 获取
INSERT INTO `hncc`.`sys_menu`(`menu_id`, `menu_name`, `parent_id`, `order_num`, `path`, `component`, `is_frame`, `menu_type`, `visible`, `status`, `perms`, `icon`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`) VALUES (2177, '发票号查询', 2170, 6, '#', NULL, 1, 'F', '0', '0', 'bms:invoice_record:queryInvoiceNo', '#', 'admin', '2020-08-29 00:42:09', '何磊', '2020-08-29 00:42:17', '');
-- 发票对象查询根据挂号ID
INSERT INTO `hncc`.`sys_menu`(`menu_id`, `menu_name`, `parent_id`, `order_num`, `path`, `component`, `is_frame`, `menu_type`, `visible`, `status`, `perms`, `icon`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`) VALUES (2194, '发票对象查询根据挂号ID', 2170, 7, '#', NULL, 1, 'F', '0', '0', 'bms:invoice_record:queryByRegistrationId', '#', 'admin', '2020-09-04 17:00:41', '何磊', '2020-09-04 17:00:49', '');
