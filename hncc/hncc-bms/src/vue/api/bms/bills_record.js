import request from '@/utils/request'

// 查询医院账单流水列表
export function listBills_record(query) {
  return request({
    url: '/bms/bills_record/list',
    method: 'get',
    params: query
  })
}

// 查询医院账单流水详细
export function getBills_record(id) {
  return request({
    url: '/bms/bills_record/' + id,
    method: 'get'
  })
}

// 新增医院账单流水
export function addBills_record(data) {
  return request({
    url: '/bms/bills_record',
    method: 'post',
    data: data
  })
}

// 修改医院账单流水
export function updateBills_record(data) {
  return request({
    url: '/bms/bills_record',
    method: 'put',
    data: data
  })
}

// 删除医院账单流水
export function delBills_record(id) {
  return request({
    url: '/bms/bills_record/' + id,
    method: 'delete'
  })
}

// 导出医院账单流水
export function exportBills_record(query) {
  return request({
    url: '/bms/bills_record/export',
    method: 'get',
    params: query
  })
}