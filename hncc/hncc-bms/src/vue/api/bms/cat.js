import request from '@/utils/request'

// 查询结算类别列表
export function listCat(query) {
  return request({
    url: '/bms/cat/list',
    method: 'get',
    params: query
  })
}

// 查询结算类别详细
export function getCat(id) {
  return request({
    url: '/bms/cat/' + id,
    method: 'get'
  })
}

// 新增结算类别
export function addCat(data) {
  return request({
    url: '/bms/cat',
    method: 'post',
    data: data
  })
}

// 修改结算类别
export function updateCat(data) {
  return request({
    url: '/bms/cat',
    method: 'put',
    data: data
  })
}

// 删除结算类别
export function delCat(id) {
  return request({
    url: '/bms/cat/' + id,
    method: 'delete'
  })
}

// 导出结算类别
export function exportCat(query) {
  return request({
    url: '/bms/cat/export',
    method: 'get',
    params: query
  })
}