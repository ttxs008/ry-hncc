import request from '@/utils/request'

// 查询发票列表
export function listInvoice_record(query) {
  return request({
    url: '/bms/invoice_record/list',
    method: 'get',
    params: query
  })
}

// 查询发票详细
export function getInvoice_record(id) {
  return request({
    url: '/bms/invoice_record/' + id,
    method: 'get'
  })
}

// 新增发票
export function addInvoice_record(data) {
  return request({
    url: '/bms/invoice_record',
    method: 'post',
    data: data
  })
}

// 修改发票
export function updateInvoice_record(data) {
  return request({
    url: '/bms/invoice_record',
    method: 'put',
    data: data
  })
}

// 删除发票
export function delInvoice_record(id) {
  return request({
    url: '/bms/invoice_record/' + id,
    method: 'delete'
  })
}

// 导出发票
export function exportInvoice_record(query) {
  return request({
    url: '/bms/invoice_record/export',
    method: 'get',
    params: query
  })
}