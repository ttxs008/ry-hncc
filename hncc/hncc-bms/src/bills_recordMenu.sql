-- 菜单 SQL
insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('医院账单流水', '3', '1', 'bills_record', 'bms/bills_record/index', 1, 'C', '0', '0', 'bms:bills_record:list', '#', 'admin', '2018-03-01', 'ry', '2018-03-01', '医院账单流水菜单');

-- 按钮父菜单ID
SELECT @parentId := LAST_INSERT_ID();

-- 按钮 SQL
insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('医院账单流水查询', @parentId, '1',  '#', '', 1,  'F', '0',  '0', 'bms:bills_record:query',        '#', 'admin', '2018-03-01', 'ry', '2018-03-01', '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('医院账单流水新增', @parentId, '2',  '#', '', 1,  'F', '0',  '0', 'bms:bills_record:add',          '#', 'admin', '2018-03-01', 'ry', '2018-03-01', '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('医院账单流水修改', @parentId, '3',  '#', '', 1,  'F', '0',  '0', 'bms:bills_record:edit',         '#', 'admin', '2018-03-01', 'ry', '2018-03-01', '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('医院账单流水删除', @parentId, '4',  '#', '', 1,  'F', '0',  '0', 'bms:bills_record:remove',       '#', 'admin', '2018-03-01', 'ry', '2018-03-01', '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('医院账单流水导出', @parentId, '5',  '#', '', 1,  'F', '0',  '0', 'bms:bills_record:export',       '#', 'admin', '2018-03-01', 'ry', '2018-03-01', '');