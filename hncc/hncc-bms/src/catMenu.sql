-- 菜单 SQL
insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('结算类别', '3', '1', 'cat', 'bms/cat/index', 1, 'C', '0', '0', 'bms:cat:list', '#', 'admin', '2018-03-01', 'ry', '2018-03-01', '结算类别菜单');

-- 按钮父菜单ID
SELECT @parentId := LAST_INSERT_ID();

-- 按钮 SQL
insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('结算类别查询', @parentId, '1',  '#', '', 1,  'F', '0',  '0', 'bms:cat:query',        '#', 'admin', '2018-03-01', 'ry', '2018-03-01', '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('结算类别新增', @parentId, '2',  '#', '', 1,  'F', '0',  '0', 'bms:cat:add',          '#', 'admin', '2018-03-01', 'ry', '2018-03-01', '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('结算类别修改', @parentId, '3',  '#', '', 1,  'F', '0',  '0', 'bms:cat:edit',         '#', 'admin', '2018-03-01', 'ry', '2018-03-01', '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('结算类别删除', @parentId, '4',  '#', '', 1,  'F', '0',  '0', 'bms:cat:remove',       '#', 'admin', '2018-03-01', 'ry', '2018-03-01', '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('结算类别导出', @parentId, '5',  '#', '', 1,  'F', '0',  '0', 'bms:cat:export',       '#', 'admin', '2018-03-01', 'ry', '2018-03-01', '');