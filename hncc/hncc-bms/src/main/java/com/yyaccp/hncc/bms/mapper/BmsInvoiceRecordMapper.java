package com.yyaccp.hncc.bms.mapper;

import java.util.List;
import com.yyaccp.hncc.bms.domain.BmsInvoiceRecord;
import org.apache.ibatis.annotations.Param;

/**
 * 发票Mapper接口
 * 
 * @author 何磊
 * @date 2020-08-27
 */
public interface BmsInvoiceRecordMapper 
{
    /**
     * 查询发票
     * 
     * @param id 发票ID
     * @return 发票
     */
    public BmsInvoiceRecord selectBmsInvoiceRecordById(Long id);

    /**
     * 查询发票列表
     * 
     * @param bmsInvoiceRecord 发票
     * @return 发票集合
     */
    public List<BmsInvoiceRecord> selectBmsInvoiceRecordList(BmsInvoiceRecord bmsInvoiceRecord);

    /**
     * 新增发票
     * 
     * @param bmsInvoiceRecord 发票
     * @return 结果
     */
    public int insertBmsInvoiceRecord(BmsInvoiceRecord bmsInvoiceRecord);

    /**
     * 修改发票
     * 
     * @param bmsInvoiceRecord 发票
     * @return 结果
     */
    public int updateBmsInvoiceRecord(BmsInvoiceRecord bmsInvoiceRecord);

    /**
     * 删除发票
     * 
     * @param id 发票ID
     * @return 结果
     */
    public int deleteBmsInvoiceRecordById(Long id);

    /**
     * 批量删除发票
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteBmsInvoiceRecordByIds(Long[] ids);

    /**
     * 获取最大的发票号
     * @return 发票号
     */
    public String selectMaxInvoiceNo();

    /**
     * 根据挂号id 获取发票号
     * @param registrationId  挂号id
     * @return 发票号
     */
    public BmsInvoiceRecord queryByRegistrationId(@Param("registrationId") String registrationId);
}
