package com.yyaccp.hncc.bms.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.yyaccp.hncc.bms.mapper.BmsBillsRecordMapper;
import com.yyaccp.hncc.bms.domain.BmsBillsRecord;
import com.yyaccp.hncc.bms.service.IBmsBillsRecordService;

/**
 * 医院账单流水Service业务层处理
 * 
 * @author 何磊
 * @date 2020-08-27
 */
@Service
public class BmsBillsRecordServiceImpl implements IBmsBillsRecordService 
{
    @Autowired
    private BmsBillsRecordMapper bmsBillsRecordMapper;

    /**
     * 查询医院账单流水
     * 
     * @param id 医院账单流水ID
     * @return 医院账单流水
     */
    @Override
    public BmsBillsRecord selectBmsBillsRecordById(Long id)
    {
        return bmsBillsRecordMapper.selectBmsBillsRecordById(id);
    }

    /**
     * 查询医院账单流水列表
     * 
     * @param bmsBillsRecord 医院账单流水
     * @return 医院账单流水
     */
    @Override
    public List<BmsBillsRecord> selectBmsBillsRecordList(BmsBillsRecord bmsBillsRecord)
    {
        return bmsBillsRecordMapper.selectBmsBillsRecordList(bmsBillsRecord);
    }

    /**
     * 新增医院账单流水
     * 
     * @param bmsBillsRecord 医院账单流水
     * @return 结果
     */
    @Override
    public int insertBmsBillsRecord(BmsBillsRecord bmsBillsRecord)
    {
        bmsBillsRecord.setCreateTime(DateUtils.getNowDate());
        return bmsBillsRecordMapper.insertBmsBillsRecord(bmsBillsRecord);
    }

    /**
     * 修改医院账单流水
     * 
     * @param bmsBillsRecord 医院账单流水
     * @return 结果
     */
    @Override
    public int updateBmsBillsRecord(BmsBillsRecord bmsBillsRecord)
    {
        return bmsBillsRecordMapper.updateBmsBillsRecord(bmsBillsRecord);
    }

    /**
     * 批量删除医院账单流水
     * 
     * @param ids 需要删除的医院账单流水ID
     * @return 结果
     */
    @Override
    public int deleteBmsBillsRecordByIds(Long[] ids)
    {
        return bmsBillsRecordMapper.deleteBmsBillsRecordByIds(ids);
    }

    /**
     * 删除医院账单流水信息
     * 
     * @param id 医院账单流水ID
     * @return 结果
     */
    @Override
    public int deleteBmsBillsRecordById(Long id)
    {
        return bmsBillsRecordMapper.deleteBmsBillsRecordById(id);
    }
}
