package com.yyaccp.hncc.bms.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.yyaccp.hncc.bms.mapper.BmsSettlementCatMapper;
import com.yyaccp.hncc.bms.domain.BmsSettlementCat;
import com.yyaccp.hncc.bms.service.IBmsSettlementCatService;

/**
 * 结算类别Service业务层处理
 * 
 * @author ruoyi
 * @date 2020-08-15
 */
@Service
public class BmsSettlementCatServiceImpl implements IBmsSettlementCatService 
{
    @Autowired
    private BmsSettlementCatMapper bmsSettlementCatMapper;

    /**
     * 查询结算类别
     * 
     * @param id 结算类别ID
     * @return 结算类别
     */
    @Override
    public BmsSettlementCat selectBmsSettlementCatById(Long id)
    {
        return bmsSettlementCatMapper.selectBmsSettlementCatById(id);
    }

    /**
     * 查询结算类别列表
     * 
     * @param bmsSettlementCat 结算类别
     * @return 结算类别
     */
    @Override
    public List<BmsSettlementCat> selectBmsSettlementCatList(BmsSettlementCat bmsSettlementCat)
    {
        return bmsSettlementCatMapper.selectBmsSettlementCatList(bmsSettlementCat);
    }

    /**
     * 新增结算类别
     * 
     * @param bmsSettlementCat 结算类别
     * @return 结果
     */
    @Override
    public int insertBmsSettlementCat(BmsSettlementCat bmsSettlementCat)
    {
        return bmsSettlementCatMapper.insertBmsSettlementCat(bmsSettlementCat);
    }

    /**
     * 修改结算类别
     * 
     * @param bmsSettlementCat 结算类别
     * @return 结果
     */
    @Override
    public int updateBmsSettlementCat(BmsSettlementCat bmsSettlementCat)
    {
        return bmsSettlementCatMapper.updateBmsSettlementCat(bmsSettlementCat);
    }

    /**
     * 批量删除结算类别
     * 
     * @param ids 需要删除的结算类别ID
     * @return 结果
     */
    @Override
    public int deleteBmsSettlementCatByIds(Long[] ids)
    {
        return bmsSettlementCatMapper.deleteBmsSettlementCatByIds(ids);
    }

    /**
     * 删除结算类别信息
     * 
     * @param id 结算类别ID
     * @return 结果
     */
    @Override
    public int deleteBmsSettlementCatById(Long id)
    {
        return bmsSettlementCatMapper.deleteBmsSettlementCatById(id);
    }
}
