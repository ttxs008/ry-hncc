package com.yyaccp.hncc.bms.service;

import java.util.List;
import com.yyaccp.hncc.bms.domain.BmsSettlementCat;

/**
 * 结算类别Service接口
 * 
 * @author ruoyi
 * @date 2020-08-15
 */
public interface IBmsSettlementCatService 
{
    /**
     * 查询结算类别
     * 
     * @param id 结算类别ID
     * @return 结算类别
     */
    public BmsSettlementCat selectBmsSettlementCatById(Long id);

    /**
     * 查询结算类别列表
     * 
     * @param bmsSettlementCat 结算类别
     * @return 结算类别集合
     */
    public List<BmsSettlementCat> selectBmsSettlementCatList(BmsSettlementCat bmsSettlementCat);

    /**
     * 新增结算类别
     * 
     * @param bmsSettlementCat 结算类别
     * @return 结果
     */
    public int insertBmsSettlementCat(BmsSettlementCat bmsSettlementCat);

    /**
     * 修改结算类别
     * 
     * @param bmsSettlementCat 结算类别
     * @return 结果
     */
    public int updateBmsSettlementCat(BmsSettlementCat bmsSettlementCat);

    /**
     * 批量删除结算类别
     * 
     * @param ids 需要删除的结算类别ID
     * @return 结果
     */
    public int deleteBmsSettlementCatByIds(Long[] ids);

    /**
     * 删除结算类别信息
     * 
     * @param id 结算类别ID
     * @return 结果
     */
    public int deleteBmsSettlementCatById(Long id);
}
