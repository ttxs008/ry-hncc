package com.yyaccp.hncc.bms.domain;

import java.math.BigDecimal;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 发票对象 bms_invoice_record
 * 
 * @author 何磊
 * @date 2020-08-27
 */
public class BmsInvoiceRecord extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** id */
    private Long id;

    /** 发票号 */
    @Excel(name = "发票号")
    private Long invoiceNo;

    /** 账单id */
    @Excel(name = "账单id")
    private Long billId;

    /** 总金额 */
    @Excel(name = "总金额")
    private BigDecimal amount;

    /** 冻结状态 */
    @Excel(name = "冻结状态")
    private Integer freezeStatus;

    /** 关联发票id */
    @Excel(name = "关联发票id")
    private Long associateId;

    /** 对账人id */
    @Excel(name = "对账人id")
    private Long operatorId;

    /** 支付类别id */
    @Excel(name = "支付类别id")
    private Long settlementCatId;

    /** 所属日结记录id */
    @Excel(name = "所属日结记录id")
    private Long settleRecordId;

    /** $column.columnComment */
    @Excel(name = "所属日结记录id")
    private String itemList;

    /** 类型 */
    @Excel(name = "类型")
    private Integer type;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setInvoiceNo(Long invoiceNo) 
    {
        this.invoiceNo = invoiceNo;
    }

    public Long getInvoiceNo() 
    {
        return invoiceNo;
    }
    public void setBillId(Long billId) 
    {
        this.billId = billId;
    }

    public Long getBillId() 
    {
        return billId;
    }
    public void setAmount(BigDecimal amount) 
    {
        this.amount = amount;
    }

    public BigDecimal getAmount() 
    {
        return amount;
    }
    public void setFreezeStatus(Integer freezeStatus) 
    {
        this.freezeStatus = freezeStatus;
    }

    public Integer getFreezeStatus() 
    {
        return freezeStatus;
    }
    public void setAssociateId(Long associateId) 
    {
        this.associateId = associateId;
    }

    public Long getAssociateId() 
    {
        return associateId;
    }
    public void setOperatorId(Long operatorId) 
    {
        this.operatorId = operatorId;
    }

    public Long getOperatorId() 
    {
        return operatorId;
    }
    public void setSettlementCatId(Long settlementCatId) 
    {
        this.settlementCatId = settlementCatId;
    }

    public Long getSettlementCatId() 
    {
        return settlementCatId;
    }
    public void setSettleRecordId(Long settleRecordId) 
    {
        this.settleRecordId = settleRecordId;
    }

    public Long getSettleRecordId() 
    {
        return settleRecordId;
    }
    public void setItemList(String itemList) 
    {
        this.itemList = itemList;
    }

    public String getItemList() 
    {
        return itemList;
    }
    public void setType(Integer type) 
    {
        this.type = type;
    }

    public Integer getType() 
    {
        return type;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("createTime", getCreateTime())
            .append("invoiceNo", getInvoiceNo())
            .append("billId", getBillId())
            .append("amount", getAmount())
            .append("freezeStatus", getFreezeStatus())
            .append("associateId", getAssociateId())
            .append("operatorId", getOperatorId())
            .append("settlementCatId", getSettlementCatId())
            .append("settleRecordId", getSettleRecordId())
            .append("itemList", getItemList())
            .append("type", getType())
            .toString();
    }
}
