package com.yyaccp.hncc.bms.controller;

import java.util.List;

import com.yyaccp.hncc.common.util.BeanCopierUtil;
import com.yyaccp.hncc.common.vo.bms.BmsSettlementCatVO;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.yyaccp.hncc.bms.domain.BmsSettlementCat;
import com.yyaccp.hncc.bms.service.IBmsSettlementCatService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 结算类别Controller
 * 
 * @author ruoyi
 * @date 2020-08-15
 */
@RestController
@RequestMapping("/bms/cat")
public class BmsSettlementCatController extends BaseController
{
    @Autowired
    private IBmsSettlementCatService bmsSettlementCatService;

    /**
     * 查询结算类别列表
     */
    @ApiOperation(value = "查询",notes = "查询结算类别列表",response = TableDataInfo.class)
    @ApiResponses({
            @ApiResponse(code = 204,message = "操作已经执行成功，但是没有返回数据"),
            @ApiResponse(code = 303,message = "重定向"),
            @ApiResponse(code = 400,message = "参数列表错误（缺少，格式不匹配）"),
            @ApiResponse(code = 500,message = "系统内部错误"),
            @ApiResponse(code = 404,message = "资源，服务未找到"),
            @ApiResponse(code = 200,message = "操作成功"),
            @ApiResponse(code = 401,message = "未授权"),
            @ApiResponse(code = 403,message = "访问受限，授权过期")
    })
    @PreAuthorize("@ss.hasPermi('bms:cat:list')")
    @GetMapping("/list")
    public TableDataInfo list(BmsSettlementCatVO bmsSettlementCatVO)
    {
        startPage();
        //将vo转换成对应的实体类
        List<BmsSettlementCat> list = bmsSettlementCatService.selectBmsSettlementCatList(BeanCopierUtil.copy(bmsSettlementCatVO,BmsSettlementCat.class));
        //获取整个表格对象
        TableDataInfo tableDataInfo = getDataTable(list);
        //将实体集合替换成vo类
        tableDataInfo.setRows(BeanCopierUtil.copy(list,BmsSettlementCatVO.class));
        return tableDataInfo;
    }

    /**
     * 导出结算类别列表
     */
    @ApiOperation(value = "导出",notes = "导出结算类别列表",response = AjaxResult.class)
    @ApiResponses({
            @ApiResponse(code = 204,message = "操作已经执行成功，但是没有返回数据"),
            @ApiResponse(code = 303,message = "重定向"),
            @ApiResponse(code = 400,message = "参数列表错误（缺少，格式不匹配）"),
            @ApiResponse(code = 500,message = "系统内部错误"),
            @ApiResponse(code = 404,message = "资源，服务未找到"),
            @ApiResponse(code = 200,message = "操作成功"),
            @ApiResponse(code = 401,message = "未授权"),
            @ApiResponse(code = 403,message = "访问受限，授权过期")
    })
    @PreAuthorize("@ss.hasPermi('bms:cat:export')")
    @Log(title = "结算类别", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(BmsSettlementCatVO bmsSettlementCatVO)
    {
        List<BmsSettlementCat> list = bmsSettlementCatService.selectBmsSettlementCatList(BeanCopierUtil.copy(bmsSettlementCatVO,BmsSettlementCat.class));
        ExcelUtil<BmsSettlementCatVO> util = new ExcelUtil<BmsSettlementCatVO>(BmsSettlementCatVO.class);
        List<BmsSettlementCatVO> voList = BeanCopierUtil.copy(list,BmsSettlementCatVO.class);
        return util.exportExcel(voList, "cat");
    }

    /**
     * 获取结算类别详细信息
     */
    @ApiOperation(value = "详细信息",notes = "根据Id获取结算类别详情",response = AjaxResult.class)
    @ApiResponses({
            @ApiResponse(code = 204,message = "操作已经执行成功，但是没有返回数据"),
            @ApiResponse(code = 303,message = "重定向"),
            @ApiResponse(code = 400,message = "参数列表错误（缺少，格式不匹配）"),
            @ApiResponse(code = 500,message = "系统内部错误"),
            @ApiResponse(code = 404,message = "资源，服务未找到"),
            @ApiResponse(code = 200,message = "操作成功"),
            @ApiResponse(code = 401,message = "未授权"),
            @ApiResponse(code = 403,message = "访问受限，授权过期")
    })
    @PreAuthorize("@ss.hasPermi('bms:cat:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") @ApiParam(value ="结账类别id",name = "id",defaultValue = "1",required = true) Long id)
    {
        return AjaxResult.success(bmsSettlementCatService.selectBmsSettlementCatById(id));
    }

    /**
     * 新增结算类别
     */
    @ApiOperation(value = "新增",notes = "新增一条结账类型数据",response = AjaxResult.class)
    @ApiResponses({
            @ApiResponse(code = 204,message = "操作已经执行成功，但是没有返回数据"),
            @ApiResponse(code = 303,message = "重定向"),
            @ApiResponse(code = 400,message = "参数列表错误（缺少，格式不匹配）"),
            @ApiResponse(code = 500,message = " 系统内部错误"),
            @ApiResponse(code = 404,message = " 资源，服务未找到"),
            @ApiResponse(code = 200,message = " 操作成功")
    })
    @PreAuthorize("@ss.hasPermi('bms:cat:add')")
    @Log(title = "结算类别", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody  @ApiParam(value = "BmsSettlementCatVO", name ="结账类型对象VO",type = "BmsSettlementCatVO")BmsSettlementCatVO bmsSettlementCatVO)
    {
        return toAjax(bmsSettlementCatService.insertBmsSettlementCat(BeanCopierUtil.copy(bmsSettlementCatVO,BmsSettlementCat.class)));
    }

    /**
     * 修改结算类别
     */
    @ApiOperation(value = "修改",notes = "修改结账类别",response = AjaxResult.class)
    @ApiResponses({
            @ApiResponse(code = 204,message = "操作已经执行成功，但是没有返回数据"),
            @ApiResponse(code = 303,message = "重定向"),
            @ApiResponse(code = 400,message = "参数列表错误（缺少，格式不匹配）"),
            @ApiResponse(code = 500,message = " 系统内部错误"),
            @ApiResponse(code = 404,message = " 资源，服务未找到"),
            @ApiResponse(code = 200,message = " 操作成功")
    })
    @PreAuthorize("@ss.hasPermi('bms:cat:edit')")
    @Log(title = "结算类别", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody @ApiParam(value = "BmsSettlementCatVO", name ="结账类别对象VO",type = "BmsSettlementCatVO") BmsSettlementCatVO bmsSettlementCatVO)
    {
        return toAjax(bmsSettlementCatService.updateBmsSettlementCat(BeanCopierUtil.copy(bmsSettlementCatVO,BmsSettlementCat.class)));
    }

    /**
     * 删除结算类别
     */
    @ApiOperation(value = "删除",notes = "根据id删除结账类别",response = AjaxResult.class)
    @ApiResponses({
            @ApiResponse(code = 204,message = "操作已经执行成功，但是没有返回数据"),
            @ApiResponse(code = 303,message = "重定向"),
            @ApiResponse(code = 400,message = "参数列表错误（缺少，格式不匹配）"),
            @ApiResponse(code = 500,message = " 系统内部错误"),
            @ApiResponse(code = 404,message = " 资源，服务未找到"),
            @ApiResponse(code = 200,message = " 操作成功")
    })
    @PreAuthorize("@ss.hasPermi('bms:cat:remove')")
    @Log(title = "结算类别", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable @ApiParam(value = "ids,可多个id进行多条数据删除",name = "ids",defaultValue = "1",required = true) Long[] ids)
    {
        return toAjax(bmsSettlementCatService.deleteBmsSettlementCatByIds(ids));
    }
}
