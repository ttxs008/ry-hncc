package com.yyaccp.hncc.bms.service;

import java.util.List;
import com.yyaccp.hncc.bms.domain.BmsBillsRecord;

/**
 * 医院账单流水Service接口
 * 
 * @author 何磊
 * @date 2020-08-27
 */
public interface IBmsBillsRecordService 
{
    /**
     * 查询医院账单流水
     * 
     * @param id 医院账单流水ID
     * @return 医院账单流水
     */
    public BmsBillsRecord selectBmsBillsRecordById(Long id);

    /**
     * 查询医院账单流水列表
     * 
     * @param bmsBillsRecord 医院账单流水
     * @return 医院账单流水集合
     */
    public List<BmsBillsRecord> selectBmsBillsRecordList(BmsBillsRecord bmsBillsRecord);

    /**
     * 新增医院账单流水
     * 
     * @param bmsBillsRecord 医院账单流水
     * @return 结果
     */
    public int insertBmsBillsRecord(BmsBillsRecord bmsBillsRecord);

    /**
     * 修改医院账单流水
     * 
     * @param bmsBillsRecord 医院账单流水
     * @return 结果
     */
    public int updateBmsBillsRecord(BmsBillsRecord bmsBillsRecord);

    /**
     * 批量删除医院账单流水
     * 
     * @param ids 需要删除的医院账单流水ID
     * @return 结果
     */
    public int deleteBmsBillsRecordByIds(Long[] ids);

    /**
     * 删除医院账单流水信息
     * 
     * @param id 医院账单流水ID
     * @return 结果
     */
    public int deleteBmsBillsRecordById(Long id);
}
