package com.yyaccp.hncc.bms.controller;

import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.yyaccp.hncc.bms.domain.BmsInvoiceRecord;
import com.yyaccp.hncc.bms.service.IBmsInvoiceRecordService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import io.swagger.annotations.*;
//com.yyaccp.hncc.common模块的包引入
import com.yyaccp.hncc.common.vo.bms.BmsInvoiceRecordVO;
import com.yyaccp.hncc.common.util.BeanCopierUtil;

import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 发票Controller
 *
 * @author 何磊
 * @date 2020-08-27
 */
@Api(tags = "发票")
@RestController
@RequestMapping("/bms/invoice_record")
public class BmsInvoiceRecordController extends BaseController {
    @Autowired
    private IBmsInvoiceRecordService bmsInvoiceRecordService;

/**
 * 查询发票列表
 *
 */
@PreAuthorize("@ss.hasPermi('bms:invoice_record:list')")
@GetMapping("/list")
@ApiOperation(value = "查询发票" , notes = "查询所有发票" ,
        code = 200, produces = "application/json" , protocols = "Http" ,
        response = TableDataInfo.class, httpMethod = "GET")
@ApiResponses({
        @ApiResponse(code = 204, message = "操作已经执行成功，但是没有返回数据"),
        @ApiResponse(code = 303, message = "重定向"),
        @ApiResponse(code = 400, message = "参数列表错误（缺少，格式不匹配）"),
        @ApiResponse(code = 500, message = "系统内部错误"),
        @ApiResponse(code = 404, message = "资源，服务未找到"),
        @ApiResponse(code = 200, message = "操作成功"),
        @ApiResponse(code = 401, message = "未授权"),
        @ApiResponse(code = 403, message = "访问受限，授权过期")
})
@ApiParam(name = "BmsInvoiceRecordVO对象" , type = "BmsInvoiceRecordVO")
        public TableDataInfo list(BmsInvoiceRecordVO bmsInvoiceRecordVO) {
        //将Vo转化为实体
        BmsInvoiceRecord bmsInvoiceRecord=BeanCopierUtil.copy(bmsInvoiceRecordVO,BmsInvoiceRecord. class);
        startPage();
        List<BmsInvoiceRecord> list = bmsInvoiceRecordService.selectBmsInvoiceRecordList(bmsInvoiceRecord);
        TableDataInfo tableDataInfo = getDataTable(list);
        //替换集合
        tableDataInfo.setRows(BeanCopierUtil.copy(tableDataInfo.getRows(), BmsInvoiceRecordVO.class));
        return tableDataInfo;
    }
    
    /**
     * 导出发票列表
     */
    @PreAuthorize("@ss.hasPermi('bms:invoice_record:export')")
    @Log(title = "发票" , businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    @ApiOperation(value = "导出发票表" , notes = "导出所有发票" ,
            code = 200, produces = "application/json" , protocols = "Http" ,
            response = TableDataInfo.class, httpMethod = "GET")
    @ApiResponses({
            @ApiResponse(code = 204, message = "操作已经执行成功，但是没有返回数据"),
            @ApiResponse(code = 303, message = "重定向"),
            @ApiResponse(code = 400, message = "参数列表错误（缺少，格式不匹配）"),
            @ApiResponse(code = 500, message = "系统内部错误"),
            @ApiResponse(code = 404, message = "资源，服务未找到"),
            @ApiResponse(code = 200, message = "操作成功"),
            @ApiResponse(code = 401, message = "未授权"),
            @ApiResponse(code = 403, message = "访问受限，授权过期")
    })
    @ApiParam(name = "BmsInvoiceRecordVO对象" , type = "BmsInvoiceRecordVO")
    public AjaxResult export(BmsInvoiceRecordVO bmsInvoiceRecordVO) {
        //将VO转化为实体
        BmsInvoiceRecord bmsInvoiceRecord=BeanCopierUtil.copy(bmsInvoiceRecordVO,BmsInvoiceRecord. class);
        List<BmsInvoiceRecordVO> list = BeanCopierUtil.copy(bmsInvoiceRecordService.selectBmsInvoiceRecordList(bmsInvoiceRecord),BmsInvoiceRecordVO.class);
        ExcelUtil<BmsInvoiceRecordVO> util = new ExcelUtil<BmsInvoiceRecordVO>(BmsInvoiceRecordVO.class);
        return util.exportExcel(list, "invoice_record");
    }

    /**
     * 获取发票详细信息
     */
    @PreAuthorize("@ss.hasPermi('bms:invoice_record:query')")
    @GetMapping(value = "/{id}")
    @ApiOperation(value = "获取发票详细信息" ,
            notes = "根据发票id获取科室信息" , code = 200,
            produces = "application/json" , protocols = "Http" , response = AjaxResult.class,
            httpMethod = "GET")
    @ApiResponses({
            @ApiResponse(code = 204, message = "操作已经执行成功，但是没有返回数据"),
            @ApiResponse(code = 303, message = "重定向"),
            @ApiResponse(code = 400, message = "参数列表错误（缺少，格式不匹配）"),
            @ApiResponse(code = 500, message = "系统内部错误"),
            @ApiResponse(code = 404, message = "资源，服务未找到"),
            @ApiResponse(code = 200, message = "操作成功"),
            @ApiResponse(code = 401, message = "未授权"),
            @ApiResponse(code = 403, message = "访问受限，授权过期")
    })
    @ApiParam(name = "bmsInvoiceRecord.id" , type = "Long")
    public AjaxResult getInfo(@PathVariable("id") Long id) {
        return AjaxResult.success(BeanCopierUtil.copy(bmsInvoiceRecordService.selectBmsInvoiceRecordById(id),BmsInvoiceRecordVO.class));
    }

    /**
     * 新增发票
     */
    @PreAuthorize("@ss.hasPermi('bms:invoice_record:add')")
    @Log(title = "新增发票" , businessType = BusinessType.INSERT)
    @PostMapping
    @ApiOperation(value = "新增发票信息" , notes = "新增发票信息" , code = 200,
            produces = "application/json" , protocols = "Http" , response = AjaxResult.class,
            httpMethod = "POST")
    @ApiResponses({
            @ApiResponse(code = 204, message = "操作已经执行成功，但是没有返回数据"),
            @ApiResponse(code = 303, message = "重定向"),
            @ApiResponse(code = 400, message = "参数列表错误（缺少，格式不匹配）"),
            @ApiResponse(code = 500, message = "系统内部错误"),
            @ApiResponse(code = 404, message = "资源，服务未找到"),
            @ApiResponse(code = 200, message = "操作成功"),
            @ApiResponse(code = 401, message = "未授权"),
            @ApiResponse(code = 403, message = "访问受限，授权过期")
    })
    @ApiParam(name = "BmsInvoiceRecordVO对象" , type = "BmsInvoiceRecordVO")
    public AjaxResult add(@RequestBody BmsInvoiceRecordVO bmsInvoiceRecordVO) {
        return toAjax(bmsInvoiceRecordService.insertBmsInvoiceRecord(BeanCopierUtil.copy(bmsInvoiceRecordVO,BmsInvoiceRecord. class)));
    }

    /**
     * 修改发票
     */
    @PreAuthorize("@ss.hasPermi('bms:invoice_record:edit')")
    @Log(title = "发票" , businessType = BusinessType.UPDATE)
    @PutMapping
    @ApiOperation(value = "修改发票信息" , notes = "修改发票信息" , code = 200,
            produces = "application/json" , protocols = "Http" , response = AjaxResult.class,
            httpMethod = "PUT")
    @ApiResponses({
            @ApiResponse(code = 204, message = "操作已经执行成功，但是没有返回数据"),
            @ApiResponse(code = 303, message = "重定向"),
            @ApiResponse(code = 400, message = "参数列表错误（缺少，格式不匹配）"),
            @ApiResponse(code = 500, message = "系统内部错误"),
            @ApiResponse(code = 404, message = "资源，服务未找到"),
            @ApiResponse(code = 200, message = "操作成功"),
            @ApiResponse(code = 401, message = "未授权"),
            @ApiResponse(code = 403, message = "访问受限，授权过期")
    })
    @ApiParam(name = "BmsInvoiceRecordVO对象" , type = "BmsInvoiceRecordVO")
    public AjaxResult edit(@RequestBody BmsInvoiceRecordVO bmsInvoiceRecordVO) {
        return toAjax(bmsInvoiceRecordService.updateBmsInvoiceRecord(BeanCopierUtil.copy(bmsInvoiceRecordVO,BmsInvoiceRecord. class)));
    }

    /**
     * 删除发票
     */
    @PreAuthorize("@ss.hasPermi('bms:invoice_record:remove')")
    @Log(title = "发票" , businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    @ApiOperation(value = "删除发票信息" , notes = "删除发票信息" , code = 200,
            produces = "application/json" , protocols = "Http" , response = AjaxResult.class,
            httpMethod = "DELETE")
    @ApiResponses({
            @ApiResponse(code = 204, message = "操作已经执行成功，但是没有返回数据"),
            @ApiResponse(code = 303, message = "重定向"),
            @ApiResponse(code = 400, message = "参数列表错误（缺少，格式不匹配）"),
            @ApiResponse(code = 500, message = "系统内部错误"),
            @ApiResponse(code = 404, message = "资源，服务未找到"),
            @ApiResponse(code = 200, message = "操作成功"),
            @ApiResponse(code = 401, message = "未授权"),
            @ApiResponse(code = 403, message = "访问受限，授权过期")
    })
    @ApiParam(name = "bmsInvoiceRecord.id" , type = "Long[]")
    public AjaxResult remove(@PathVariable Long[] ids) {
        return toAjax(bmsInvoiceRecordService.deleteBmsInvoiceRecordByIds(ids));
    }

    /**
     * 获取发票号
     */
    @PreAuthorize("@ss.hasPermi('bms:invoice_record:queryInvoiceNo')")
    @GetMapping(value = "/queryInvoiceNo")
    @ApiOperation(value = "获取发票号" ,
            notes = "获取发票号,根据当前日期" , code = 200,
            produces = "application/json" , protocols = "Http" , response = AjaxResult.class,
            httpMethod = "GET")
    @ApiResponses({
            @ApiResponse(code = 204, message = "操作已经执行成功，但是没有返回数据"),
            @ApiResponse(code = 303, message = "重定向"),
            @ApiResponse(code = 400, message = "参数列表错误（缺少，格式不匹配）"),
            @ApiResponse(code = 500, message = "系统内部错误"),
            @ApiResponse(code = 404, message = "资源，服务未找到"),
            @ApiResponse(code = 200, message = "操作成功"),
            @ApiResponse(code = 401, message = "未授权"),
            @ApiResponse(code = 403, message = "访问受限，授权过期")
    })
    public AjaxResult getInvoiceNo() {
        return AjaxResult.success("获取成功",bmsInvoiceRecordService.selectMaxInvoiceNo());
    }


    /**
     * 获取发票号
     */
    @PreAuthorize("@ss.hasPermi('bms:invoice_record:queryByRegistrationId')")
    @GetMapping(value = "/queryByRegistrationId/{registrationId}")
    @ApiOperation(value = "获取发票对象根据挂号ID" ,
            notes = "获取发票对象,根据挂号ID" , code = 200,
            produces = "application/json" , protocols = "Http" , response = AjaxResult.class,
            httpMethod = "GET")
    @ApiResponses({
            @ApiResponse(code = 204, message = "操作已经执行成功，但是没有返回数据"),
            @ApiResponse(code = 303, message = "重定向"),
            @ApiResponse(code = 400, message = "参数列表错误（缺少，格式不匹配）"),
            @ApiResponse(code = 500, message = "系统内部错误"),
            @ApiResponse(code = 404, message = "资源，服务未找到"),
            @ApiResponse(code = 200, message = "操作成功"),
            @ApiResponse(code = 401, message = "未授权"),
            @ApiResponse(code = 403, message = "访问受限，授权过期")
    })
    public AjaxResult queryByRegistrationId(@PathVariable String registrationId) {
        return AjaxResult.success("获取成功", BeanCopierUtil.copy(bmsInvoiceRecordService.queryByRegistrationId(registrationId),BmsInvoiceRecordVO.class));
    }
}
