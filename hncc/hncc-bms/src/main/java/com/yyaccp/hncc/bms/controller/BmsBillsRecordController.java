package com.yyaccp.hncc.bms.controller;

import java.util.List;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.yyaccp.hncc.bms.domain.BmsBillsRecord;
import com.yyaccp.hncc.bms.service.IBmsBillsRecordService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import io.swagger.annotations.*;
//com.yyaccp.hncc.common模块的包引入
import com.yyaccp.hncc.common.vo.bms.BmsBillsRecordVO;
import com.yyaccp.hncc.common.util.BeanCopierUtil;

import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 医院账单流水Controller
 *
 * @author 何磊
 * @date 2020-08-27
 */
@Api(tags = "医院账单流水")
@RestController
@RequestMapping("/bms/bills_record")
public class BmsBillsRecordController extends BaseController {
    @Autowired
    private IBmsBillsRecordService bmsBillsRecordService;

/**
 * 查询医院账单流水列表
 *
 */
@PreAuthorize("@ss.hasPermi('bms:bills_record:list')")
@GetMapping("/list")
@ApiOperation(value = "查询医院账单流水" , notes = "查询所有医院账单流水" ,
        code = 200, produces = "application/json" , protocols = "Http" ,
        response = TableDataInfo.class, httpMethod = "GET")
@ApiResponses({
        @ApiResponse(code = 204, message = "操作已经执行成功，但是没有返回数据"),
        @ApiResponse(code = 303, message = "重定向"),
        @ApiResponse(code = 400, message = "参数列表错误（缺少，格式不匹配）"),
        @ApiResponse(code = 500, message = "系统内部错误"),
        @ApiResponse(code = 404, message = "资源，服务未找到"),
        @ApiResponse(code = 200, message = "操作成功"),
        @ApiResponse(code = 401, message = "未授权"),
        @ApiResponse(code = 403, message = "访问受限，授权过期")
})
@ApiParam(name = "BmsBillsRecordVO对象" , type = "BmsBillsRecordVO")
        public TableDataInfo list(BmsBillsRecordVO bmsBillsRecordVO) {
        //将Vo转化为实体
        BmsBillsRecord bmsBillsRecord=BeanCopierUtil.copy(bmsBillsRecordVO,BmsBillsRecord. class);
        startPage();
        List<BmsBillsRecord> list = bmsBillsRecordService.selectBmsBillsRecordList(bmsBillsRecord);
        TableDataInfo tableDataInfo = getDataTable(list);
        //替换集合
        tableDataInfo.setRows(BeanCopierUtil.copy(tableDataInfo.getRows(), BmsBillsRecordVO.class));
        return tableDataInfo;
    }
    
    /**
     * 导出医院账单流水列表
     */
    @PreAuthorize("@ss.hasPermi('bms:bills_record:export')")
    @Log(title = "医院账单流水" , businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    @ApiOperation(value = "导出医院账单流水表" , notes = "导出所有医院账单流水" ,
            code = 200, produces = "application/json" , protocols = "Http" ,
            response = TableDataInfo.class, httpMethod = "GET")
    @ApiResponses({
            @ApiResponse(code = 204, message = "操作已经执行成功，但是没有返回数据"),
            @ApiResponse(code = 303, message = "重定向"),
            @ApiResponse(code = 400, message = "参数列表错误（缺少，格式不匹配）"),
            @ApiResponse(code = 500, message = "系统内部错误"),
            @ApiResponse(code = 404, message = "资源，服务未找到"),
            @ApiResponse(code = 200, message = "操作成功"),
            @ApiResponse(code = 401, message = "未授权"),
            @ApiResponse(code = 403, message = "访问受限，授权过期")
    })
    @ApiParam(name = "BmsBillsRecordVO对象" , type = "BmsBillsRecordVO")
    public AjaxResult export(BmsBillsRecordVO bmsBillsRecordVO) {
        //将VO转化为实体
        BmsBillsRecord bmsBillsRecord=BeanCopierUtil.copy(bmsBillsRecordVO,BmsBillsRecord. class);
        List<BmsBillsRecordVO> list = BeanCopierUtil.copy(bmsBillsRecordService.selectBmsBillsRecordList(bmsBillsRecord),BmsBillsRecordVO.class);
        ExcelUtil<BmsBillsRecordVO> util = new ExcelUtil<BmsBillsRecordVO>(BmsBillsRecordVO.class);
        return util.exportExcel(list, "bills_record");
    }

    /**
     * 获取医院账单流水详细信息
     */
    @PreAuthorize("@ss.hasPermi('bms:bills_record:query')")
    @GetMapping(value = "/{id}")
    @ApiOperation(value = "获取医院账单流水详细信息" ,
            notes = "根据医院账单流水id获取科室信息" , code = 200,
            produces = "application/json" , protocols = "Http" , response = AjaxResult.class,
            httpMethod = "GET")
    @ApiResponses({
            @ApiResponse(code = 204, message = "操作已经执行成功，但是没有返回数据"),
            @ApiResponse(code = 303, message = "重定向"),
            @ApiResponse(code = 400, message = "参数列表错误（缺少，格式不匹配）"),
            @ApiResponse(code = 500, message = "系统内部错误"),
            @ApiResponse(code = 404, message = "资源，服务未找到"),
            @ApiResponse(code = 200, message = "操作成功"),
            @ApiResponse(code = 401, message = "未授权"),
            @ApiResponse(code = 403, message = "访问受限，授权过期")
    })
    @ApiParam(name = "bmsBillsRecord.id" , type = "Long")
    public AjaxResult getInfo(@PathVariable("id") Long id) {
        return AjaxResult.success(BeanCopierUtil.copy(bmsBillsRecordService.selectBmsBillsRecordById(id),BmsBillsRecordVO.class));
    }

    /**
     * 新增医院账单流水
     */
    @PreAuthorize("@ss.hasPermi('bms:bills_record:add')")
    @Log(title = "新增医院账单流水" , businessType = BusinessType.INSERT)
    @PostMapping
    @ApiOperation(value = "新增医院账单流水信息" , notes = "新增医院账单流水信息" , code = 200,
            produces = "application/json" , protocols = "Http" , response = AjaxResult.class,
            httpMethod = "POST")
    @ApiResponses({
            @ApiResponse(code = 204, message = "操作已经执行成功，但是没有返回数据"),
            @ApiResponse(code = 303, message = "重定向"),
            @ApiResponse(code = 400, message = "参数列表错误（缺少，格式不匹配）"),
            @ApiResponse(code = 500, message = "系统内部错误"),
            @ApiResponse(code = 404, message = "资源，服务未找到"),
            @ApiResponse(code = 200, message = "操作成功"),
            @ApiResponse(code = 401, message = "未授权"),
            @ApiResponse(code = 403, message = "访问受限，授权过期")
    })
    @ApiParam(name = "BmsBillsRecordVO对象" , type = "BmsBillsRecordVO")
    public AjaxResult add(@RequestBody BmsBillsRecordVO bmsBillsRecordVO) {
        return toAjax(bmsBillsRecordService.insertBmsBillsRecord(BeanCopierUtil.copy(bmsBillsRecordVO,BmsBillsRecord. class)));
    }

    /**
     * 修改医院账单流水
     */
    @PreAuthorize("@ss.hasPermi('bms:bills_record:edit')")
    @Log(title = "医院账单流水" , businessType = BusinessType.UPDATE)
    @PutMapping
    @ApiOperation(value = "修改医院账单流水信息" , notes = "修改医院账单流水信息" , code = 200,
            produces = "application/json" , protocols = "Http" , response = AjaxResult.class,
            httpMethod = "PUT")
    @ApiResponses({
            @ApiResponse(code = 204, message = "操作已经执行成功，但是没有返回数据"),
            @ApiResponse(code = 303, message = "重定向"),
            @ApiResponse(code = 400, message = "参数列表错误（缺少，格式不匹配）"),
            @ApiResponse(code = 500, message = "系统内部错误"),
            @ApiResponse(code = 404, message = "资源，服务未找到"),
            @ApiResponse(code = 200, message = "操作成功"),
            @ApiResponse(code = 401, message = "未授权"),
            @ApiResponse(code = 403, message = "访问受限，授权过期")
    })
    @ApiParam(name = "BmsBillsRecordVO对象" , type = "BmsBillsRecordVO")
    public AjaxResult edit(@RequestBody BmsBillsRecordVO bmsBillsRecordVO) {
        return toAjax(bmsBillsRecordService.updateBmsBillsRecord(BeanCopierUtil.copy(bmsBillsRecordVO,BmsBillsRecord. class)));
    }

    /**
     * 删除医院账单流水
     */
    @PreAuthorize("@ss.hasPermi('bms:bills_record:remove')")
    @Log(title = "医院账单流水" , businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    @ApiOperation(value = "删除医院账单流水信息" , notes = "删除医院账单流水信息" , code = 200,
            produces = "application/json" , protocols = "Http" , response = AjaxResult.class,
            httpMethod = "DELETE")
    @ApiResponses({
            @ApiResponse(code = 204, message = "操作已经执行成功，但是没有返回数据"),
            @ApiResponse(code = 303, message = "重定向"),
            @ApiResponse(code = 400, message = "参数列表错误（缺少，格式不匹配）"),
            @ApiResponse(code = 500, message = "系统内部错误"),
            @ApiResponse(code = 404, message = "资源，服务未找到"),
            @ApiResponse(code = 200, message = "操作成功"),
            @ApiResponse(code = 401, message = "未授权"),
            @ApiResponse(code = 403, message = "访问受限，授权过期")
    })
    @ApiParam(name = "bmsBillsRecord.id" , type = "Long[]")
    public AjaxResult remove(@PathVariable Long[] ids) {
        return toAjax(bmsBillsRecordService.deleteBmsBillsRecordByIds(ids));
    }
}
