package com.yyaccp.hncc.bms.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import com.yyaccp.hncc.common.util.DateToNoUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.yyaccp.hncc.bms.mapper.BmsInvoiceRecordMapper;
import com.yyaccp.hncc.bms.domain.BmsInvoiceRecord;
import com.yyaccp.hncc.bms.service.IBmsInvoiceRecordService;

/**
 * 发票Service业务层处理
 * 
 * @author 何磊
 * @date 2020-08-27
 */
@Service
public class BmsInvoiceRecordServiceImpl implements IBmsInvoiceRecordService 
{
    @Autowired
    private BmsInvoiceRecordMapper bmsInvoiceRecordMapper;

    /**
     * 查询发票
     * 
     * @param id 发票ID
     * @return 发票
     */
    @Override
    public BmsInvoiceRecord selectBmsInvoiceRecordById(Long id)
    {
        return bmsInvoiceRecordMapper.selectBmsInvoiceRecordById(id);
    }

    /**
     * 查询发票列表
     * 
     * @param bmsInvoiceRecord 发票
     * @return 发票
     */
    @Override
    public List<BmsInvoiceRecord> selectBmsInvoiceRecordList(BmsInvoiceRecord bmsInvoiceRecord)
    {
        return bmsInvoiceRecordMapper.selectBmsInvoiceRecordList(bmsInvoiceRecord);
    }

    /**
     * 新增发票
     * 
     * @param bmsInvoiceRecord 发票
     * @return 结果
     */
    @Override
    public int insertBmsInvoiceRecord(BmsInvoiceRecord bmsInvoiceRecord)
    {
        bmsInvoiceRecord.setCreateTime(DateUtils.getNowDate());
        return bmsInvoiceRecordMapper.insertBmsInvoiceRecord(bmsInvoiceRecord);
    }

    /**
     * 修改发票
     * 
     * @param bmsInvoiceRecord 发票
     * @return 结果
     */
    @Override
    public int updateBmsInvoiceRecord(BmsInvoiceRecord bmsInvoiceRecord)
    {
        return bmsInvoiceRecordMapper.updateBmsInvoiceRecord(bmsInvoiceRecord);
    }

    /**
     * 批量删除发票
     * 
     * @param ids 需要删除的发票ID
     * @return 结果
     */
    @Override
    public int deleteBmsInvoiceRecordByIds(Long[] ids)
    {
        return bmsInvoiceRecordMapper.deleteBmsInvoiceRecordByIds(ids);
    }

    /**
     * 删除发票信息
     * 
     * @param id 发票ID
     * @return 结果
     */
    @Override
    public int deleteBmsInvoiceRecordById(Long id)
    {
        return bmsInvoiceRecordMapper.deleteBmsInvoiceRecordById(id);
    }

    /**
     * 获取最发票号
     * */
    @Override
    public String selectMaxInvoiceNo() {
        String no  = bmsInvoiceRecordMapper.selectMaxInvoiceNo();
        return DateToNoUtil.DateToNoAndIncrease(DateToNoUtil.YYYYMMDD,6,no);
    }

    /**
     * 根据挂号id 获取发票号
     * @param registrationId  挂号id
     * @return 发票号
     */
    @Override
    public BmsInvoiceRecord queryByRegistrationId(String registrationId) {
        return bmsInvoiceRecordMapper.queryByRegistrationId(registrationId);
    }

}
