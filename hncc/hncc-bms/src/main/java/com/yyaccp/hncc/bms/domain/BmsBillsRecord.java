package com.yyaccp.hncc.bms.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 医院账单流水对象 bms_bills_record
 * 
 * @author 何磊
 * @date 2020-08-27
 */
public class BmsBillsRecord extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** id */
    private Long id;

    /** 账单类型 */
    @Excel(name = "账单类型")
    private Integer type;

    /** 流水号 */
    @Excel(name = "流水号")
    private String billNo;

    /** 状态 */
    @Excel(name = "状态")
    private Integer status;

    /** 发票数量 */
    @Excel(name = "发票数量")
    private Integer invoiceNum;

    /** 挂号id */
    @Excel(name = "挂号id")
    private Long registrationId;

    /** 记录列表 */
    @Excel(name = "记录列表")
    private String recordList;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setType(Integer type) 
    {
        this.type = type;
    }

    public Integer getType() 
    {
        return type;
    }
    public void setBillNo(String billNo) 
    {
        this.billNo = billNo;
    }

    public String getBillNo() 
    {
        return billNo;
    }
    public void setStatus(Integer status) 
    {
        this.status = status;
    }

    public Integer getStatus() 
    {
        return status;
    }
    public void setInvoiceNum(Integer invoiceNum) 
    {
        this.invoiceNum = invoiceNum;
    }

    public Integer getInvoiceNum() 
    {
        return invoiceNum;
    }
    public void setRegistrationId(Long registrationId) 
    {
        this.registrationId = registrationId;
    }

    public Long getRegistrationId() 
    {
        return registrationId;
    }
    public void setRecordList(String recordList) 
    {
        this.recordList = recordList;
    }

    public String getRecordList() 
    {
        return recordList;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("type", getType())
            .append("billNo", getBillNo())
            .append("createTime", getCreateTime())
            .append("status", getStatus())
            .append("invoiceNum", getInvoiceNum())
            .append("registrationId", getRegistrationId())
            .append("recordList", getRecordList())
            .toString();
    }
}
