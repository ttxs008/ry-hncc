package com.yyaccp.hncc.es.util;

import java.util.List;

/**
 * @author 天天向上 （john.yi@qq.com）
 * @date 2020/8/26.
 */
public interface BatchSaveCallback {
    /**
     * 调用service批量保存从数据库查询到的voList到es
     *
     * @param voList 从hncc获取的vo对象集合
     */
    void doInService(List voList);
}
