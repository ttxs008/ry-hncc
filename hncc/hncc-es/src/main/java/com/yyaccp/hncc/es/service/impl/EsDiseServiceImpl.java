package com.yyaccp.hncc.es.service.impl;

import com.yyaccp.hncc.es.domain.EsDise;
import com.yyaccp.hncc.es.repository.EsDiseRepository;
import com.yyaccp.hncc.es.service.IEsDiseService;
import com.yyaccp.hncc.es.util.EsImportTemplate;
import com.yyaccp.hncc.es.util.HighlightSearchResultMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.fetch.subphase.highlight.HighlightBuilder;
import org.elasticsearch.search.sort.SortBuilders;
import org.elasticsearch.search.sort.SortOrder;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.elasticsearch.core.ElasticsearchRestTemplate;
import org.springframework.data.elasticsearch.core.query.NativeSearchQuery;
import org.springframework.data.elasticsearch.core.query.NativeSearchQueryBuilder;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author 天天向上 （john.yi@qq.com）
 * @date 2020/8/25
 */
@Service
@RequiredArgsConstructor
@Slf4j
public class EsDiseServiceImpl implements IEsDiseService {
    /**
     * 通用的增删改查使用repository
     */
    private final EsDiseRepository esDiseRepository;
    /**
     * 自定义的查询使用elasticsearchRestTemplate
     * 新版本已经不推荐使用elasticsearchTemplate
     * 推荐使用elasticsearchRestTemplate
     */
    private final ElasticsearchRestTemplate elasticsearchRestTemplate;
    private final HighlightSearchResultMapper highlightSearchResultMapper;

    public static final String DISE_CODE = "code";
    public static final String DISE_ICD = "icd";
    public static final String DISE_NAME = "name";

    @Override
    public void saveAll(List<EsDise> esDiseList) {
        if (esDiseList.size() > EsImportTemplate.BULK_SIZE) {
            this.bulkIndex(esDiseList);
        } else {
            esDiseRepository.saveAll(esDiseList);
        }
    }

    @Override
    public void bulkIndex(List<EsDise> esDiseList) {
        if (!elasticsearchRestTemplate.indexExists(EsDise.class)) {
            elasticsearchRestTemplate.createIndex(EsDise.class);
        }
        new EsImportTemplate<EsDise>().bulkIndex(elasticsearchRestTemplate, esDiseList, false);
    }

    @Override
    public void delete() {
        elasticsearchRestTemplate.deleteIndex(EsDise.class);
    }

    @Override
    public Page<EsDise> query(String keyword, Integer start, Integer size, boolean highlight) {
        /* 1.创建QueryBuilder(即设置查询条件)这儿创建的是组合查询(也叫多条件查询),后面会介绍更多的查询方法
         * 组合查询BoolQueryBuilder
         * must(QueryBuilders)   :AND
         * mustNot(QueryBuilders):NOT
         * should:               :OR
         * termQuery             :精确查询，不分词
         * queryStringQuery      :text分词查询
         */
        BoolQueryBuilder queryBuilder = QueryBuilders.boolQuery();
        queryBuilder.should(QueryBuilders.termQuery(DISE_CODE, keyword))
                .should(QueryBuilders.termQuery(DISE_ICD, keyword))
                .should(QueryBuilders.queryStringQuery(keyword).defaultField(DISE_NAME));
        //设置分页(从第一页开始，一页显示10条)
        //注意开始是从0开始，有点类似sql中的方法limit 的查询
        Pageable pageable = PageRequest.of(start, size);
        //构建查询
        NativeSearchQueryBuilder nativeSearchQueryBuilder = new NativeSearchQueryBuilder();
        nativeSearchQueryBuilder
                // 查询条件
                .withQuery(queryBuilder)
                // 分页
                .withPageable(pageable)
                // 排序
                .withSort(SortBuilders.fieldSort(DISE_CODE).order(SortOrder.ASC));
        if (highlight) {
            // 高亮字段
            nativeSearchQueryBuilder.withHighlightFields(new HighlightBuilder.Field(DISE_ICD),
                    new HighlightBuilder.Field(DISE_CODE),
                    new HighlightBuilder.Field(DISE_NAME)
                            // 自定义高亮样式，上面两个字段使用默认的<em>标签
                            .preTags("<span style='color:red'>")
                            .postTags("</span>"));
        }
        NativeSearchQuery searchQuery = nativeSearchQueryBuilder.build();
        // 执行查询
        if (highlight) {
            return elasticsearchRestTemplate.queryForPage(searchQuery, EsDise.class, highlightSearchResultMapper);
        } else {
            return elasticsearchRestTemplate.queryForPage(searchQuery, EsDise.class);
        }
    }

    @Override
    public void deleteById(Long id) {
        this.esDiseRepository.deleteById(id);
    }

    @Override
    public void save(EsDise esDise) {
        this.esDiseRepository.save(esDise);
    }

    @Override
    public void update(EsDise esDise) {
        this.esDiseRepository.save(esDise);
    }

}
