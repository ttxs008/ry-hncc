package com.yyaccp.hncc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;

/**
 * @author 天天向上 （john.yi@qq.com）
 * @date 2020/8/25.
 */
@SpringBootApplication(exclude = {DataSourceAutoConfiguration.class})
public class EsApplication {
    public static void main(String[] args) {
        SpringApplication.run(EsApplication.class, args);
    }
}
