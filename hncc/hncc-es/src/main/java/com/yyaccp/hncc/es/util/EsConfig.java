package com.yyaccp.hncc.es.util;

import org.elasticsearch.client.RestHighLevelClient;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.elasticsearch.client.ClientConfiguration;
import org.springframework.data.elasticsearch.client.RestClients;

/**
 * es 7.x不推荐使用ElasticsearchTemplate以及在配置文件中配置es
 * 使用ElasticsearchRestTemplate和@Bean配置
 * @author 天天向上 （john.yi@qq.com）
 * @date 2020/8/31.
 */
@Configuration
public class EsConfig {
    @Bean
    RestHighLevelClient restHighLevelClient() {
        ClientConfiguration cfg = ClientConfiguration.builder()
                // rest端口9200，transport客户端使用的端口是9300(老版，还需配置cluster-name)
                .connectedTo("192.168.189.102:9200")
                .withConnectTimeout(5000)
                .build();
        return RestClients.create(cfg).rest();
    }
}
