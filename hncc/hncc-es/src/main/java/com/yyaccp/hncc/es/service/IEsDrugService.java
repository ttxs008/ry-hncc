package com.yyaccp.hncc.es.service;

import com.yyaccp.hncc.es.domain.EsDrug;
import org.springframework.data.domain.Page;

import java.util.List;

/**
 * @author 天天向上 （john.yi@qq.com）
 * @date 2020/8/25
 */
public interface IEsDrugService {
    /**
     * 批量添加数据到es(使用jpa的save方法，适用于小批量数据插入)
     *
     * @param esDrugList 药品document集合
     */
    void saveAll(List<EsDrug> esDrugList);

    /**
     * 批量添加数据到es(使用es的bulk方法，适用于大量数据插入)
     *
     * @param esDrugList 药品document集合
     */
    void bulkIndex(List<EsDrug> esDrugList);

    /**
     * 删除所有数据
     */
    void delete();

    /**
     * 根据关键字查询药品（manufacturer或code或name或genericName）
     *
     * @param keyword   查询关键字
     * @param start     页面，从0开始
     * @param size      每页显示几条
     * @param highlight true 高亮显示
     * @return 分页的药品数据
     */
    Page<EsDrug> query(String keyword, Integer start, Integer size, boolean highlight);

    /**
     * 保存药品到es
     * @param esDrug 药品document
     */
    void save(EsDrug esDrug);

    /**
     * 根据id删除
     * @param id 药品document id
     */
    void deleteById(Long id);

    /**
     * 修改药品
     * @param esDrug 药品document
     */
    void update(EsDrug esDrug);
}
