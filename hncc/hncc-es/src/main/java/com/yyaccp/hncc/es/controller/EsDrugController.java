package com.yyaccp.hncc.es.controller;

import com.alibaba.fastjson.JSON;
import com.ruoyi.common.core.domain.AjaxResult;
import com.yyaccp.hncc.common.vo.dms.DmsDrugVO;
import com.yyaccp.hncc.es.domain.EsDrug;
import com.yyaccp.hncc.es.service.IEsDrugService;
import com.yyaccp.hncc.es.util.EsImportTemplate;
import com.yyaccp.hncc.es.util.HttpHeadersUtil;
import io.jsonwebtoken.lang.Assert;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * @author 天天向上 （john.yi@qq.com）
 * @date 2020/8/25
 */
@RestController
@RequiredArgsConstructor
@Slf4j
@RequestMapping("/es/drug")
public class EsDrugController {
    @Value("${hncc.api.drug}")
    private String apiDrug;
    private final RestTemplate restTemplate;
    private final IEsDrugService esDrugService;
    private final HttpHeadersUtil httpHeadersUtil;
    /**
     * 通过http调用药品列表api，导入所有药品到es
     * 为微服务改造做准备，不直接依赖dms模块
     * 后续版本会使用alibaba canal实现数据库跟es数据同步
     *
     * @return 封装好数据的ajaxResult对象
     */
    @GetMapping("/importData")
    public AjaxResult importData(HttpServletRequest request) {
        // 远程调用api,目标网址要验证token，构造请求头设置token
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeadersUtil.withToken(httpHeaders);
        EsImportTemplate<DmsDrugVO> esImportTemplate = new EsImportTemplate<>(restTemplate, httpHeaders);
        esImportTemplate.batchSave(apiDrug, list -> {
            List<EsDrug> drugList = JSON.parseArray(JSON.toJSONString(list), EsDrug.class);
            esDrugService.saveAll(drugList);
        });
        return AjaxResult.success();
    }

    /**
     * 从es中分页查询疾病数据
     *
     * @param keyword   查询关键词
     * @param start     页面，从0开始
     * @param size      每页显示几条
     * @param highlight true 高亮显示匹配的关键字
     * @return AjaxResult
     */
    @GetMapping("/search")
    public AjaxResult search(String keyword, Integer start, Integer size, Boolean highlight) {
        Assert.notNull(keyword, "keyword不能为空");
        return AjaxResult.success(esDrugService.query(keyword, start, size, highlight != null));
    }

    /**
     * 添加药品到es
     * 目前在hncc中添加药品需要调用此方法，以确保mysql和es数据一致
     * 后续版本会使用alibaba canal实现数据库跟es数据同步
     * @param drugVO 药品vo
     * @return 封装好数据的ajaxResult对象
     */
    @PostMapping("")
    public AjaxResult add(@RequestBody DmsDrugVO drugVO) {
        EsDrug esDrug = JSON.parseObject(JSON.toJSONString(drugVO), EsDrug.class);
        esDrugService.save(esDrug);
        return AjaxResult.success(esDrug);
    }

    /**
     * 修改药品到es
     * 目前在hncc中修改药品需要调用此方法，以确保mysql和es数据一致
     * 后续版本会使用alibaba canal实现数据库跟es数据同步
     * @param drugVO 药品vo
     * @return 封装好数据的ajaxResult对象
     */
    @PutMapping("/{id}")
    public AjaxResult update(@PathVariable Long id, @RequestBody DmsDrugVO drugVO) {
        drugVO.setId(id);
        EsDrug esDrug = JSON.parseObject(JSON.toJSONString(drugVO), EsDrug.class);
        esDrugService.update(esDrug);
        return AjaxResult.success(esDrug);
    }

    /**
     * 根据id删除药品
     * 目前在hncc中删除药品时需要调用此方法，以确保mysql和es数据一致
     * 后续版本会使用alibaba canal实现数据库跟es数据同步
     * @param id 药品id
     * @return 封装好数据的ajaxResult对象
     */
    @DeleteMapping("/{id}")
    public AjaxResult delete(@PathVariable Long id) {
        esDrugService.delete();
        return AjaxResult.success();
    }
}
