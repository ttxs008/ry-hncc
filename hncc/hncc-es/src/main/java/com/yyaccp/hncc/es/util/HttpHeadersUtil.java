package com.yyaccp.hncc.es.util;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

/**
 * @author 天天向上 （john.yi@qq.com）
 * @date 2020/8/26.
 */
@Component
public class HttpHeadersUtil {
    @Value("${token.header}")
    private String headerName;
    public void withToken(HttpHeaders httpHeaders) {
        String token = ((ServletRequestAttributes)RequestContextHolder
                .getRequestAttributes()).getRequest().getHeader(headerName);
        httpHeaders.add(headerName, token);
    }
}
