package com.yyaccp.hncc.es.domain;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * @author 天天向上 （john.yi@qq.com）
 * @date 2020/8/26.
 */
@Setter
@Getter
@ToString
@Document(indexName = "hncc_drug", shards = 1, replicas = 0)
public class EsDrug implements EsIdable, Serializable {
    private static final long serialVersionUID = -1L;
    @Id
    private Long id;
    @Field(type = FieldType.Keyword)
    private String code;
    /**
     * 药品名，支持分词
     */
    @Field(analyzer = "ik_max_word", type = FieldType.Text)
    private String name;
    private String format;
    private BigDecimal price;
    private String unit;
    /**
     * 生产厂家，支持分词
     */
    @Field(analyzer = "ik_max_word", type = FieldType.Text)
    private String manufacturer;
    /**
     * 药品剂型
     */
    private Long dosageId;
    /**
     * 药品类型
     */
    private Long typeId;
    /**
     * 拼音助记码
     */
    private String mnemonicCode;
    private Date createDate;
    private Long stock;
    /**
     * 通用名，支持分词
     */
    @Field(analyzer = "ik_max_word", type = FieldType.Text)
    private String genericName;
    private Integer status;

    @Override
    public String getEsId() {
        return this.id + "";
    }
}
