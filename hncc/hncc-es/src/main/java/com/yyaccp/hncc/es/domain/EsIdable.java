package com.yyaccp.hncc.es.domain;

/**
 * 如果使用bulkIndex批量导入，并且需要从es实体类对象中获取数据库对应的id
 * 则该es实体类需要实现该接口
 *
 * @author 天天向上 （john.yi@qq.com）
 * @date 2020/8/26.
 */
public interface EsIdable {
    /**
     * 获取存入到es的对象id
     *
     * @return 存入到es中的document id
     */
    String getEsId();
}
