package com.yyaccp.hncc.es.controller;

import com.alibaba.fastjson.JSON;
import com.ruoyi.common.core.domain.AjaxResult;
import com.yyaccp.hncc.common.vo.dms.DmsDiseVO;
import com.yyaccp.hncc.es.domain.EsDise;
import com.yyaccp.hncc.es.service.IEsDiseService;
import com.yyaccp.hncc.es.util.EsImportTemplate;
import com.yyaccp.hncc.es.util.HttpHeadersUtil;
import io.jsonwebtoken.lang.Assert;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import javax.servlet.http.HttpServletRequest;

/**
 * @author 天天向上 （john.yi@qq.com）
 * @date 2020/8/25
 */
@RestController
@RequiredArgsConstructor
@Slf4j
@RequestMapping("/es/dise")
public class EsDiseController {
    @Value("${hncc.api.dise}")
    private String apiDise;

    private final RestTemplate restTemplate;
    private final IEsDiseService esDiseService;
    private final HttpHeadersUtil httpHeadersUtil;

    /**
     * 通过http调用疾病列表api，导入所有疾病到es
     * 为微服务改造做准备，不直接依赖dms模块
     * 后续版本会使用alibaba canal实现数据库跟es数据同步
     *
     * @return AjaxResult
     */
    @GetMapping("/importData")
    public AjaxResult importData(HttpServletRequest request) {
        // 远程调用api,目标网址要验证token，构造请求头设置token
        // 在微服务中可以通过app网关统一设置
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeadersUtil.withToken(httpHeaders);
        EsImportTemplate<DmsDiseVO> esImportTemplate = new EsImportTemplate<>(restTemplate, httpHeaders);
        esImportTemplate.batchSave(apiDise, list -> esDiseService.saveAll(JSON.parseArray(JSON.toJSONString(list), EsDise.class)));
        return AjaxResult.success();
    }

    /**
     * 从es中分页查询疾病数据
     *
     * @param keyword   查询关键词
     * @param start     页面，从0开始
     * @param size      每页显示几条
     * @param highlight true 高亮显示匹配的关键字
     * @return AjaxResult
     */
    @GetMapping("/search")
    public AjaxResult search(String keyword, Integer start, Integer size, Boolean highlight) {
        Assert.notNull(keyword, "keyword不能为空");
        return AjaxResult.success(esDiseService.query(keyword, start, size, highlight != null));
    }
    /**
     * 添加esDise到es
     * 目前在hncc中添加疾病时需要调用此方法，以确保mysql和es数据一致
     * 后续版本会使用alibaba canal实现数据库跟es数据同步
     * @param diseVO 疾病vo
     * @return 封装好数据的ajax对象
     */
    @PostMapping("")
    public AjaxResult add(@RequestBody DmsDiseVO diseVO) {
        EsDise esDise = JSON.parseObject(JSON.toJSONString(diseVO), EsDise.class);
        esDiseService.save(esDise);
        return AjaxResult.success(esDise);
    }

    /**
     * 修改esDise
     * 目前在hncc中修改疾病时需要调用此方法，以确保mysql和es数据一致
     * 后续版本会使用alibaba canal实现数据库跟es数据同步
     * @param diseVO 疾病vo
     * @return 封装好数据的ajax对象
     */
    @PutMapping("/{id}")
    public AjaxResult update(@PathVariable Long id, @RequestBody DmsDiseVO diseVO) {
        diseVO.setId(id);
        EsDise esDise = JSON.parseObject(JSON.toJSONString(diseVO), EsDise.class);
        esDiseService.update(esDise);
        return AjaxResult.success(esDise);
    }

    /**
     * 根据id删除疾病
     * 目前在hncc中删除疾病时需要调用此方法，以确保mysql和es数据一致
     * 后续版本会使用alibaba canal实现数据库跟es数据同步
     * @param id 疾病id
     * @return 封装好数据的ajax对象
     */
    @DeleteMapping("/{id}")
    public AjaxResult delete(@PathVariable Long id) {
        esDiseService.deleteById(id);
        return AjaxResult.success();
    }
}
