package com.yyaccp.hncc.es.domain;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;

import java.io.Serializable;

/**
 * @author 天天向上 （john.yi@qq.com）
 * @date 2020/8/26.
 */
@Setter
@Getter
@ToString
@Document(indexName = "hncc_dise", shards = 1, replicas = 0)
public class EsDise implements EsIdable, Serializable {
    private static final long serialVersionUID = -1L;
    @Id
    private Long id;
    private Long catId;
    @Field(type = FieldType.Keyword)
    private String code;
    @Field(type = FieldType.Text, analyzer = "ik_max_word")
    private String name;
    @Field(type = FieldType.Keyword)
    private String icd;
    private Integer status;

    @Override
    public String getEsId() {
        return this.id + "";
    }
}
