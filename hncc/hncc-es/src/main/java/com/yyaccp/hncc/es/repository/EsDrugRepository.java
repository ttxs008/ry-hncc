package com.yyaccp.hncc.es.repository;

import com.yyaccp.hncc.es.domain.EsDrug;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.stereotype.Repository;

/**
 * 包名字不要使用mapper，否则会自动生成mybatis实现类
 * 因为ApplicationConfig类配置了下面的扫描
 * MapperScan({"com.ruoyi.**.mapper","com.yyaccp.hncc.**.mapper"})
 * @author 天天向上 （john.yi@qq.com）
 * @date 2020/8/25
 */
@Repository
public interface EsDrugRepository extends ElasticsearchRepository<EsDrug, Long> {
}
