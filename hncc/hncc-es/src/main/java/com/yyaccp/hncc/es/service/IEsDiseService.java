package com.yyaccp.hncc.es.service;

import com.yyaccp.hncc.es.domain.EsDise;
import org.springframework.data.domain.Page;

import java.util.List;

/**
 * @author 天天向上 （john.yi@qq.com）
 * @date 2020/8/25
 */
public interface IEsDiseService {
    /**
     * 批量添加数据到es，使用默认的插入方法
     *
     * @param esDiseList 疾病document集合
     */
    void saveAll(List<EsDise> esDiseList);

    /**
     * 批量添加数据到es(使用es的bulk方法，适用于大量数据插入)
     *
     * @param esDiseList 疾病document集合
     */
    void bulkIndex(List<EsDise> esDiseList);

    /**
     * 删除所有数据
     */
    void delete();

    /**
     * 根据关键字查询疾病（icd或code或name，name支持分词查询）
     *
     * @param keyword   查询关键字
     * @param start     页面，从0开始
     * @param size      每页显示几条
     * @param highlight true 高亮显示
     * @return 疾病document分页对象
     */
    Page<EsDise> query(String keyword, Integer start, Integer size, boolean highlight);

    /**
     * 根据id删除疾病
     * @param id 疾病document id
     */
    void deleteById(Long id);

    /**
     * 保存疾病到es
     * @param esDise 疾病document对象
     */
    void save(EsDise esDise);

    /**
     * 修改疾病
     * @param esDise 疾病document对象
     */
    void update(EsDise esDise);
}
