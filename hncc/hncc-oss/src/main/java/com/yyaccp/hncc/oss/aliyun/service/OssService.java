package com.yyaccp.hncc.oss.aliyun.service;

import com.yyaccp.hncc.common.vo.oss.OssCallbackResult;
import com.yyaccp.hncc.common.vo.oss.OssPolicyResult;
import com.yyaccp.hncc.common.vo.oss.UploadResult;

import java.io.InputStream;
import java.io.OutputStream;
import java.util.Map;

/**
 * Created by 天天向上 （john.yi@qq.com） on 2020/8/18.
 */
public interface OssService {
    /**
     * 前端直传oss生成签名的方法
     * @return 签名结果
     */
    OssPolicyResult ossPolicy();

    /**
     * 前端直传oss回调方法
     * @param parameterMap
     * @return 回调结果
     */
    OssCallbackResult ossCallback(Map<String, String[]> parameterMap);

    /**
     * 普通方式上传文件到oss
     * @param originalFilename 原文件名
     * @param in 上传的文件流
     * @return 上传结果
     */
    UploadResult upload(String originalFilename, InputStream in);

    /**
     * 从oss下载指定key（文件名）的文件
     * @param key
     * @param outputStream
     * @return 成功返回success，否则返回error
     */
    String download(String key, OutputStream outputStream);
}
