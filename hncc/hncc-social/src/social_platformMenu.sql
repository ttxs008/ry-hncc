-- 菜单 SQL
insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('社交应用', '1', '1', 'social_platform', 'social/social_platform/index', 1, 'C', '0', '0', 'social:social_platform:list', '#', 'admin', '2018-03-01', 'ry', '2018-03-01', '社交应用菜单');

-- 按钮父菜单ID
SELECT @parentId := LAST_INSERT_ID();

-- 按钮 SQL
insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('社交应用查询', @parentId, '1',  '#', '', 1,  'F', '0',  '0', 'social:social_platform:query',        '#', 'admin', '2018-03-01', 'ry', '2018-03-01', '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('社交应用新增', @parentId, '2',  '#', '', 1,  'F', '0',  '0', 'social:social_platform:add',          '#', 'admin', '2018-03-01', 'ry', '2018-03-01', '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('社交应用修改', @parentId, '3',  '#', '', 1,  'F', '0',  '0', 'social:social_platform:edit',         '#', 'admin', '2018-03-01', 'ry', '2018-03-01', '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('社交应用删除', @parentId, '4',  '#', '', 1,  'F', '0',  '0', 'social:social_platform:remove',       '#', 'admin', '2018-03-01', 'ry', '2018-03-01', '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('社交应用导出', @parentId, '5',  '#', '', 1,  'F', '0',  '0', 'social:social_platform:export',       '#', 'admin', '2018-03-01', 'ry', '2018-03-01', '');