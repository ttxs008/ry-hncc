package com.yyaccp.hncc.social.controller;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.yyaccp.hncc.common.util.BeanCopierUtil;
import com.yyaccp.hncc.common.vo.social.SysSocialUserVO;
import com.yyaccp.hncc.social.domain.SysSocialUser;
import com.yyaccp.hncc.social.service.ISysSocialUserService;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

//com.yyaccp.hncc.common模块的包引入

/**
 * 社交登录用户Controller
 *
 * @author 天天向上
 * @date 2020-08-20
 */
@Api(tags = "社交登录用户")
@RestController
@RequestMapping("/social/social_user")
public class SysSocialUserController extends BaseController {
    @Autowired
    private ISysSocialUserService sysSocialUserService;

    /**
     * 查询社交登录用户列表
     */
    @PreAuthorize("@ss.hasPermi('social:social_user:list')")
    @GetMapping("/list")
    @ApiOperation(value = "查询社交登录用户", notes = "查询所有社交登录用户",
            code = 200, produces = "application/json", protocols = "Http",
            response = TableDataInfo.class, httpMethod = "GET")
    @ApiResponses({
            @ApiResponse(code = 204, message = "操作已经执行成功，但是没有返回数据"),
            @ApiResponse(code = 303, message = "重定向"),
            @ApiResponse(code = 400, message = "参数列表错误（缺少，格式不匹配）"),
            @ApiResponse(code = 500, message = "系统内部错误"),
            @ApiResponse(code = 404, message = "资源，服务未找到"),
            @ApiResponse(code = 200, message = "操作成功"),
            @ApiResponse(code = 401, message = "未授权"),
            @ApiResponse(code = 403, message = "访问受限，授权过期")
    })
    @ApiParam(name = "SysSocialUserVO对象", type = "SysSocialUserVO")
    public TableDataInfo list(SysSocialUserVO sysSocialUserVO) {
        //将Vo转化为实体
        SysSocialUser sysSocialUser = BeanCopierUtil.copy(sysSocialUserVO, SysSocialUser.class);
        startPage();
        List<SysSocialUser> list = sysSocialUserService.selectSysSocialUserList(sysSocialUser);
        TableDataInfo tableDataInfo = getDataTable(list);
        //替换集合
        tableDataInfo.setRows(BeanCopierUtil.copy(tableDataInfo.getRows(), SysSocialUserVO.class));
        return tableDataInfo;
    }

    /**
     * 导出社交登录用户列表
     */
    @PreAuthorize("@ss.hasPermi('social:social_user:export')")
    @Log(title = "社交登录用户", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    @ApiOperation(value = "导出社交登录用户表", notes = "导出所有社交登录用户",
            code = 200, produces = "application/json", protocols = "Http",
            response = TableDataInfo.class, httpMethod = "GET")
    @ApiResponses({
            @ApiResponse(code = 204, message = "操作已经执行成功，但是没有返回数据"),
            @ApiResponse(code = 303, message = "重定向"),
            @ApiResponse(code = 400, message = "参数列表错误（缺少，格式不匹配）"),
            @ApiResponse(code = 500, message = "系统内部错误"),
            @ApiResponse(code = 404, message = "资源，服务未找到"),
            @ApiResponse(code = 200, message = "操作成功"),
            @ApiResponse(code = 401, message = "未授权"),
            @ApiResponse(code = 403, message = "访问受限，授权过期")
    })
    @ApiParam(name = "SysSocialUserVO对象", type = "SysSocialUserVO")
    public AjaxResult export(SysSocialUserVO sysSocialUserVO) {
        //将VO转化为实体
        SysSocialUser sysSocialUser = BeanCopierUtil.copy(sysSocialUserVO, SysSocialUser.class);
        List<SysSocialUserVO> list = BeanCopierUtil.copy(sysSocialUserService.selectSysSocialUserList(sysSocialUser), SysSocialUserVO.class);
        ExcelUtil<SysSocialUserVO> util = new ExcelUtil<SysSocialUserVO>(SysSocialUserVO.class);
        return util.exportExcel(list, "social_user");
    }

    /**
     * 获取社交登录用户详细信息
     */
    @PreAuthorize("@ss.hasPermi('social:social_user:query')")
    @GetMapping(value = "/{id}")
    @ApiOperation(value = "获取社交登录用户详细信息",
            notes = "根据社交登录用户id获取科室信息", code = 200,
            produces = "application/json", protocols = "Http", response = AjaxResult.class,
            httpMethod = "GET")
    @ApiResponses({
            @ApiResponse(code = 204, message = "操作已经执行成功，但是没有返回数据"),
            @ApiResponse(code = 303, message = "重定向"),
            @ApiResponse(code = 400, message = "参数列表错误（缺少，格式不匹配）"),
            @ApiResponse(code = 500, message = "系统内部错误"),
            @ApiResponse(code = 404, message = "资源，服务未找到"),
            @ApiResponse(code = 200, message = "操作成功"),
            @ApiResponse(code = 401, message = "未授权"),
            @ApiResponse(code = 403, message = "访问受限，授权过期")
    })
    @ApiParam(name = "sysSocialUser.id", type = "Long")
    public AjaxResult getInfo(@PathVariable("id") Long id) {
        return AjaxResult.success(BeanCopierUtil.copy(sysSocialUserService.selectSysSocialUserById(id), SysSocialUserVO.class));
    }

    /**
     * 新增社交登录用户
     */
    @PreAuthorize("@ss.hasPermi('social:social_user:add')")
    @Log(title = "新增社交登录用户", businessType = BusinessType.INSERT)
    @PostMapping
    @ApiOperation(value = "新增社交登录用户信息", notes = "新增社交登录用户信息", code = 200,
            produces = "application/json", protocols = "Http", response = AjaxResult.class,
            httpMethod = "POST")
    @ApiResponses({
            @ApiResponse(code = 204, message = "操作已经执行成功，但是没有返回数据"),
            @ApiResponse(code = 303, message = "重定向"),
            @ApiResponse(code = 400, message = "参数列表错误（缺少，格式不匹配）"),
            @ApiResponse(code = 500, message = "系统内部错误"),
            @ApiResponse(code = 404, message = "资源，服务未找到"),
            @ApiResponse(code = 200, message = "操作成功"),
            @ApiResponse(code = 401, message = "未授权"),
            @ApiResponse(code = 403, message = "访问受限，授权过期")
    })
    @ApiParam(name = "SysSocialUserVO对象", type = "SysSocialUserVO")
    public AjaxResult add(@RequestBody SysSocialUserVO sysSocialUserVO) {
        return toAjax(sysSocialUserService.insertSysSocialUser(BeanCopierUtil.copy(sysSocialUserVO, SysSocialUser.class)));
    }

    /**
     * 修改社交登录用户
     */
    @PreAuthorize("@ss.hasPermi('social:social_user:edit')")
    @Log(title = "社交登录用户", businessType = BusinessType.UPDATE)
    @PutMapping
    @ApiOperation(value = "修改社交登录用户信息", notes = "修改社交登录用户信息", code = 200,
            produces = "application/json", protocols = "Http", response = AjaxResult.class,
            httpMethod = "PUT")
    @ApiResponses({
            @ApiResponse(code = 204, message = "操作已经执行成功，但是没有返回数据"),
            @ApiResponse(code = 303, message = "重定向"),
            @ApiResponse(code = 400, message = "参数列表错误（缺少，格式不匹配）"),
            @ApiResponse(code = 500, message = "系统内部错误"),
            @ApiResponse(code = 404, message = "资源，服务未找到"),
            @ApiResponse(code = 200, message = "操作成功"),
            @ApiResponse(code = 401, message = "未授权"),
            @ApiResponse(code = 403, message = "访问受限，授权过期")
    })
    @ApiParam(name = "SysSocialUserVO对象", type = "SysSocialUserVO")
    public AjaxResult edit(@RequestBody SysSocialUserVO sysSocialUserVO) {
        return toAjax(sysSocialUserService.updateSysSocialUser(BeanCopierUtil.copy(sysSocialUserVO, SysSocialUser.class)));
    }

    /**
     * 删除社交登录用户
     */
    @PreAuthorize("@ss.hasPermi('social:social_user:remove')")
    @Log(title = "社交登录用户", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    @ApiOperation(value = "删除社交登录用户信息", notes = "删除社交登录用户信息", code = 200,
            produces = "application/json", protocols = "Http", response = AjaxResult.class,
            httpMethod = "DELETE")
    @ApiResponses({
            @ApiResponse(code = 204, message = "操作已经执行成功，但是没有返回数据"),
            @ApiResponse(code = 303, message = "重定向"),
            @ApiResponse(code = 400, message = "参数列表错误（缺少，格式不匹配）"),
            @ApiResponse(code = 500, message = "系统内部错误"),
            @ApiResponse(code = 404, message = "资源，服务未找到"),
            @ApiResponse(code = 200, message = "操作成功"),
            @ApiResponse(code = 401, message = "未授权"),
            @ApiResponse(code = 403, message = "访问受限，授权过期")
    })
    @ApiParam(name = "sysSocialUser.id", type = "Long[]")
    public AjaxResult remove(@PathVariable Long[] ids) {
        return toAjax(sysSocialUserService.deleteSysSocialUserByIds(ids));
    }
}
