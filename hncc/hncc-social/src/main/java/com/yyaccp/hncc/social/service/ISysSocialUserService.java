package com.yyaccp.hncc.social.service;

import com.yyaccp.hncc.social.domain.SysSocialUser;

import java.util.List;

/**
 * 社交登录用户Service接口
 *
 * @author 天天向上
 * @date 2020-08-20
 */
public interface ISysSocialUserService {
    /**
     * 查询社交登录用户
     *
     * @param id 社交登录用户ID
     * @return 社交登录用户
     */
    SysSocialUser selectSysSocialUserById(Long id);

    /**
     * 查询社交登录用户列表
     *
     * @param sysSocialUser 社交登录用户
     * @return 社交登录用户集合
     */
    List<SysSocialUser> selectSysSocialUserList(SysSocialUser sysSocialUser);

    /**
     * 新增社交登录用户
     *
     * @param sysSocialUser 社交登录用户
     * @return 结果
     */
    int insertSysSocialUser(SysSocialUser sysSocialUser);

    /**
     * 修改社交登录用户
     *
     * @param sysSocialUser 社交登录用户
     * @return 结果
     */
    int updateSysSocialUser(SysSocialUser sysSocialUser);

    /**
     * 批量删除社交登录用户
     *
     * @param ids 需要删除的社交登录用户ID
     * @return 结果
     */
    int deleteSysSocialUserByIds(Long[] ids);

    /**
     * 删除社交登录用户信息
     *
     * @param id 社交登录用户ID
     * @return 结果
     */
    int deleteSysSocialUserById(Long id);
}
