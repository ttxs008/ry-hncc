package com.yyaccp.hncc.social.controller;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.yyaccp.hncc.common.util.BeanCopierUtil;
import com.yyaccp.hncc.common.vo.social.SysSocialPlatformVO;
import com.yyaccp.hncc.social.domain.SysSocialPlatform;
import com.yyaccp.hncc.social.service.ISysSocialPlatformService;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

//com.yyaccp.hncc.common模块的包引入

/**
 * 社交应用Controller
 *
 * @author 天天向上
 * @date 2020-08-20
 */
@Api(tags = "社交应用")
@RestController
@RequestMapping("/social/social_platform")
public class SysSocialPlatformController extends BaseController {
    @Autowired
    private ISysSocialPlatformService sysSocialPlatformService;

    /**
     * 查询社交应用列表
     */
    @PreAuthorize("@ss.hasPermi('social:social_platform:list')")
    @GetMapping("/list")
    @ApiOperation(value = "查询社交应用", notes = "查询所有社交应用",
            code = 200, produces = "application/json", protocols = "Http",
            response = TableDataInfo.class, httpMethod = "GET")
    @ApiResponses({
            @ApiResponse(code = 204, message = "操作已经执行成功，但是没有返回数据"),
            @ApiResponse(code = 303, message = "重定向"),
            @ApiResponse(code = 400, message = "参数列表错误（缺少，格式不匹配）"),
            @ApiResponse(code = 500, message = "系统内部错误"),
            @ApiResponse(code = 404, message = "资源，服务未找到"),
            @ApiResponse(code = 200, message = "操作成功"),
            @ApiResponse(code = 401, message = "未授权"),
            @ApiResponse(code = 403, message = "访问受限，授权过期")
    })
    @ApiParam(name = "SysSocialPlatformVO对象", type = "SysSocialPlatformVO")
    public TableDataInfo list(SysSocialPlatformVO sysSocialPlatformVO) {
        //将Vo转化为实体
        SysSocialPlatform sysSocialPlatform = BeanCopierUtil.copy(sysSocialPlatformVO, SysSocialPlatform.class);
        startPage();
        List<SysSocialPlatform> list = sysSocialPlatformService.selectSysSocialPlatformList(sysSocialPlatform);
        TableDataInfo tableDataInfo = getDataTable(list);
        //替换集合
        tableDataInfo.setRows(BeanCopierUtil.copy(tableDataInfo.getRows(), SysSocialPlatformVO.class));
        return tableDataInfo;
    }

    /**
     * 导出社交应用列表
     */
    @PreAuthorize("@ss.hasPermi('social:social_platform:export')")
    @Log(title = "社交应用", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    @ApiOperation(value = "导出社交应用表", notes = "导出所有社交应用",
            code = 200, produces = "application/json", protocols = "Http",
            response = TableDataInfo.class, httpMethod = "GET")
    @ApiResponses({
            @ApiResponse(code = 204, message = "操作已经执行成功，但是没有返回数据"),
            @ApiResponse(code = 303, message = "重定向"),
            @ApiResponse(code = 400, message = "参数列表错误（缺少，格式不匹配）"),
            @ApiResponse(code = 500, message = "系统内部错误"),
            @ApiResponse(code = 404, message = "资源，服务未找到"),
            @ApiResponse(code = 200, message = "操作成功"),
            @ApiResponse(code = 401, message = "未授权"),
            @ApiResponse(code = 403, message = "访问受限，授权过期")
    })
    @ApiParam(name = "SysSocialPlatformVO对象", type = "SysSocialPlatformVO")
    public AjaxResult export(SysSocialPlatformVO sysSocialPlatformVO) {
        //将VO转化为实体
        SysSocialPlatform sysSocialPlatform = BeanCopierUtil.copy(sysSocialPlatformVO, SysSocialPlatform.class);
        List<SysSocialPlatformVO> list = BeanCopierUtil.copy(sysSocialPlatformService.selectSysSocialPlatformList(sysSocialPlatform), SysSocialPlatformVO.class);
        ExcelUtil<SysSocialPlatformVO> util = new ExcelUtil<SysSocialPlatformVO>(SysSocialPlatformVO.class);
        return util.exportExcel(list, "social_platform");
    }

    /**
     * 获取社交应用详细信息
     */
    @PreAuthorize("@ss.hasPermi('social:social_platform:query')")
    @GetMapping(value = "/{platformName}")
    @ApiOperation(value = "获取社交应用详细信息",
            notes = "根据社交应用platformName获取科室信息", code = 200,
            produces = "application/json", protocols = "Http", response = AjaxResult.class,
            httpMethod = "GET")
    @ApiResponses({
            @ApiResponse(code = 204, message = "操作已经执行成功，但是没有返回数据"),
            @ApiResponse(code = 303, message = "重定向"),
            @ApiResponse(code = 400, message = "参数列表错误（缺少，格式不匹配）"),
            @ApiResponse(code = 500, message = "系统内部错误"),
            @ApiResponse(code = 404, message = "资源，服务未找到"),
            @ApiResponse(code = 200, message = "操作成功"),
            @ApiResponse(code = 401, message = "未授权"),
            @ApiResponse(code = 403, message = "访问受限，授权过期")
    })
    @ApiParam(name = "sysSocialPlatform.id", type = "Long")
    public AjaxResult getInfo(@PathVariable("platformName") String platformName) {
        return AjaxResult.success(BeanCopierUtil.copy(sysSocialPlatformService.selectSysSocialPlatformById(platformName), SysSocialPlatformVO.class));
    }

    /**
     * 新增社交应用
     */
    @PreAuthorize("@ss.hasPermi('social:social_platform:add')")
    @Log(title = "新增社交应用", businessType = BusinessType.INSERT)
    @PostMapping
    @ApiOperation(value = "新增社交应用信息", notes = "新增社交应用信息", code = 200,
            produces = "application/json", protocols = "Http", response = AjaxResult.class,
            httpMethod = "POST")
    @ApiResponses({
            @ApiResponse(code = 204, message = "操作已经执行成功，但是没有返回数据"),
            @ApiResponse(code = 303, message = "重定向"),
            @ApiResponse(code = 400, message = "参数列表错误（缺少，格式不匹配）"),
            @ApiResponse(code = 500, message = "系统内部错误"),
            @ApiResponse(code = 404, message = "资源，服务未找到"),
            @ApiResponse(code = 200, message = "操作成功"),
            @ApiResponse(code = 401, message = "未授权"),
            @ApiResponse(code = 403, message = "访问受限，授权过期")
    })
    @ApiParam(name = "SysSocialPlatformVO对象", type = "SysSocialPlatformVO")
    public AjaxResult add(@RequestBody SysSocialPlatformVO sysSocialPlatformVO) {
        return toAjax(sysSocialPlatformService.insertSysSocialPlatform(BeanCopierUtil.copy(sysSocialPlatformVO, SysSocialPlatform.class)));
    }

    /**
     * 修改社交应用
     */
    @PreAuthorize("@ss.hasPermi('social:social_platform:edit')")
    @Log(title = "社交应用", businessType = BusinessType.UPDATE)
    @PutMapping
    @ApiOperation(value = "修改社交应用信息", notes = "修改社交应用信息", code = 200,
            produces = "application/json", protocols = "Http", response = AjaxResult.class,
            httpMethod = "PUT")
    @ApiResponses({
            @ApiResponse(code = 204, message = "操作已经执行成功，但是没有返回数据"),
            @ApiResponse(code = 303, message = "重定向"),
            @ApiResponse(code = 400, message = "参数列表错误（缺少，格式不匹配）"),
            @ApiResponse(code = 500, message = "系统内部错误"),
            @ApiResponse(code = 404, message = "资源，服务未找到"),
            @ApiResponse(code = 200, message = "操作成功"),
            @ApiResponse(code = 401, message = "未授权"),
            @ApiResponse(code = 403, message = "访问受限，授权过期")
    })
    @ApiParam(name = "SysSocialPlatformVO对象", type = "SysSocialPlatformVO")
    public AjaxResult edit(@RequestBody SysSocialPlatformVO sysSocialPlatformVO) {
        return toAjax(sysSocialPlatformService.updateSysSocialPlatform(BeanCopierUtil.copy(sysSocialPlatformVO, SysSocialPlatform.class)));
    }

    /**
     * 删除社交应用
     */
    @PreAuthorize("@ss.hasPermi('social:social_platform:remove')")
    @Log(title = "社交应用", businessType = BusinessType.DELETE)
    @DeleteMapping("/{platformNames}")
    @ApiOperation(value = "删除社交应用信息", notes = "删除社交应用信息", code = 200,
            produces = "application/json", protocols = "Http", response = AjaxResult.class,
            httpMethod = "DELETE")
    @ApiResponses({
            @ApiResponse(code = 204, message = "操作已经执行成功，但是没有返回数据"),
            @ApiResponse(code = 303, message = "重定向"),
            @ApiResponse(code = 400, message = "参数列表错误（缺少，格式不匹配）"),
            @ApiResponse(code = 500, message = "系统内部错误"),
            @ApiResponse(code = 404, message = "资源，服务未找到"),
            @ApiResponse(code = 200, message = "操作成功"),
            @ApiResponse(code = 401, message = "未授权"),
            @ApiResponse(code = 403, message = "访问受限，授权过期")
    })
    @ApiParam(name = "sysSocialPlatform.id", type = "Long[]")
    public AjaxResult remove(@PathVariable String[] platformNames) {
        return toAjax(sysSocialPlatformService.deleteSysSocialPlatformByIds(platformNames));
    }
}
