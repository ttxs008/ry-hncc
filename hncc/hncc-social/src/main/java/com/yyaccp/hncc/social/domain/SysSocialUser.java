package com.yyaccp.hncc.social.domain;

import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * 社交登录用户对象 sys_social_user
 *
 * @author 天天向上
 * @date 2020-08-20
 */
public class SysSocialUser extends BaseEntity {
    private static final long serialVersionUID = 1L;

    /**
     * id
     */
    private Long id;

    /**
     * 平台名
     */
    @Excel(name = "平台名")
    private String platformName;

    /**
     * 平台用户id
     */
    @Excel(name = "平台用户id")
    private Long socialId;

    /**
     * 登录名字
     */
    @Excel(name = "登录名字")
    private String socialLoginName;

    /**
     * 显示名字
     */
    @Excel(name = "显示名字")
    private String socialName;

    /**
     * 头像地址
     */
    @Excel(name = "头像地址")
    private String avatarUrl;
    /**
     * 用户在当前开放应用内的唯一标识
     */
    private String openid;
    /**
     * 用户在当前开放应用所属企业的唯一标识
     */
    private String unionid;
    /**
     * 关联用户id
     */
    @Excel(name = "关联用户id")
    private Long sysUserId;

    public void setId(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setPlatformName(String platformName) {
        this.platformName = platformName;
    }

    public String getPlatformName() {
        return platformName;
    }

    public void setSocialId(Long socialId) {
        this.socialId = socialId;
    }

    public Long getSocialId() {
        return socialId;
    }

    public void setSocialLoginName(String socialLoginName) {
        this.socialLoginName = socialLoginName;
    }

    public String getSocialLoginName() {
        return socialLoginName;
    }

    public void setSocialName(String socialName) {
        this.socialName = socialName;
    }

    public String getSocialName() {
        return socialName;
    }

    public void setAvatarUrl(String avatarUrl) {
        this.avatarUrl = avatarUrl;
    }

    public String getAvatarUrl() {
        return avatarUrl;
    }

    public void setSysUserId(Long sysUserId) {
        this.sysUserId = sysUserId;
    }

    public Long getSysUserId() {
        return sysUserId;
    }

    public String getOpenid() {
        return openid;
    }

    public void setOpenid(String openid) {
        this.openid = openid;
    }

    public String getUnionid() {
        return unionid;
    }

    public void setUnionid(String unionid) {
        this.unionid = unionid;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
                .append("id", getId())
                .append("platformName", getPlatformName())
                .append("socialId", getSocialId())
                .append("socialLoginName", getSocialLoginName())
                .append("socialName", getSocialName())
                .append("avatarUrl", getAvatarUrl())
                .append("sysUserId", getSysUserId())
                .toString();
    }
}
