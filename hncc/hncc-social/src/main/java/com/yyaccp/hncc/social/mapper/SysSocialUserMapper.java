package com.yyaccp.hncc.social.mapper;

import com.yyaccp.hncc.social.domain.SysSocialUser;

import java.util.List;

/**
 * 社交登录用户Mapper接口
 *
 * @author 天天向上
 * @date 2020-08-20
 */
public interface SysSocialUserMapper {
    /**
     * 查询社交登录用户
     *
     * @param id 社交登录用户ID
     * @return 社交登录用户
     */
    public SysSocialUser selectSysSocialUserById(Long id);

    /**
     * 查询社交登录用户列表
     *
     * @param sysSocialUser 社交登录用户
     * @return 社交登录用户集合
     */
    public List<SysSocialUser> selectSysSocialUserList(SysSocialUser sysSocialUser);

    /**
     * 新增社交登录用户
     *
     * @param sysSocialUser 社交登录用户
     * @return 结果
     */
    public int insertSysSocialUser(SysSocialUser sysSocialUser);

    /**
     * 修改社交登录用户
     *
     * @param sysSocialUser 社交登录用户
     * @return 结果
     */
    public int updateSysSocialUser(SysSocialUser sysSocialUser);

    /**
     * 删除社交登录用户
     *
     * @param id 社交登录用户ID
     * @return 结果
     */
    public int deleteSysSocialUserById(Long id);

    /**
     * 批量删除社交登录用户
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteSysSocialUserByIds(Long[] ids);

    /**
     * 根据平台名字和相应的标识列查询对应的社交账号
     * 相应的标识列说明如下:
     *  github，gitee为对应的id
     *  dingtalk为openid
     *
     * @param sysSocialUser
     * @return
     */
    SysSocialUser selectSysSocialUser(SysSocialUser sysSocialUser);
}
