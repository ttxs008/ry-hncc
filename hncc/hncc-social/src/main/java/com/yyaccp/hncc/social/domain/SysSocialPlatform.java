package com.yyaccp.hncc.social.domain;

import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * 社交应用对象 sys_social_platform
 *
 * @author 天天向上
 * @date 2020-08-20
 */
public class SysSocialPlatform extends BaseEntity {
    private static final long serialVersionUID = 1L;

    /**
     * 平台名
     */
    private String platformName;

    /**
     * 服务器url
     */
    @Excel(name = "服务器url")
    private String serverUrl;

    /**
     * 授权应用id
     */
    @Excel(name = "授权应用id")
    private String clientId;

    /**
     * 授权应用秘钥
     */
    @Excel(name = "授权应用秘钥")
    private String clientSecret;

    /**
     * 回调地址
     */
    @Excel(name = "回调地址")
    private String callbackUrl;

    /**
     * 授权码路径
     */
    @Excel(name = "授权码路径")
    private String authorizePath;

    /**
     * accessToken路径
     */
    @Excel(name = "accessToken路径")
    private String tokenPath;

    /**
     * 用户信息url
     */
    @Excel(name = "用户信息url")
    private String userInfoUrl;

    /**
     * openId路径
     */
    @Excel(name = "openId路径")
    private String openIdPath;
    /**
     * 请求授权的范围
     */
    private String scope;
    /**
     * 盐值
     */
    private String salt;
    /**
     * 状态（0禁用1启用）
     */
    @Excel(name = "状态", readConverterExp = "0=禁用1启用")
    private Integer status;

    public void setPlatformName(String platformName) {
        this.platformName = platformName;
    }

    public String getPlatformName() {
        return platformName;
    }

    public void setServerUrl(String serverUrl) {
        this.serverUrl = serverUrl;
    }

    public String getServerUrl() {
        return serverUrl;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public String getClientId() {
        return clientId;
    }

    public void setClientSecret(String clientSecret) {
        this.clientSecret = clientSecret;
    }

    public String getClientSecret() {
        return clientSecret;
    }

    public void setCallbackUrl(String callbackUrl) {
        this.callbackUrl = callbackUrl;
    }

    public String getCallbackUrl() {
        return callbackUrl;
    }

    public void setAuthorizePath(String authorizePath) {
        this.authorizePath = authorizePath;
    }

    public String getAuthorizePath() {
        return authorizePath;
    }

    public void setTokenPath(String tokenPath) {
        this.tokenPath = tokenPath;
    }

    public String getTokenPath() {
        return tokenPath;
    }

    public void setUserInfoUrl(String userInfoUrl) {
        this.userInfoUrl = userInfoUrl;
    }

    public String getUserInfoUrl() {
        return userInfoUrl;
    }

    public void setOpenIdPath(String openIdPath) {
        this.openIdPath = openIdPath;
    }

    public String getOpenIdPath() {
        return openIdPath;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getStatus() {
        return status;
    }

    public String getScope() {
        return scope;
    }

    public void setScope(String scope) {
        this.scope = scope;
    }

    public String getSalt() {
        return salt;
    }

    public void setSalt(String salt) {
        this.salt = salt;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
                .append("platformName", getPlatformName())
                .append("serverUrl", getServerUrl())
                .append("clientId", getClientId())
                .append("clientSecret", getClientSecret())
                .append("callbackUrl", getCallbackUrl())
                .append("authorizePath", getAuthorizePath())
                .append("tokenPath", getTokenPath())
                .append("userInfoUrl", getUserInfoUrl())
                .append("openIdPath", getOpenIdPath())
                .append("status", getStatus())
                .toString();
    }
}
