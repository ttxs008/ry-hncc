package com.yyaccp.hncc.social.service;

import com.yyaccp.hncc.social.domain.SysSocialPlatform;

import java.util.List;

/**
 * 社交应用Service接口
 *
 * @author 天天向上
 * @date 2020-08-20
 */
public interface ISysSocialPlatformService {
    /**
     * 查询社交应用
     *
     * @param platformName 社交应用ID
     * @return 社交应用
     */
    SysSocialPlatform selectSysSocialPlatformById(String platformName);

    /**
     * 查询社交应用列表
     *
     * @param sysSocialPlatform 社交应用
     * @return 社交应用集合
     */
    List<SysSocialPlatform> selectSysSocialPlatformList(SysSocialPlatform sysSocialPlatform);

    /**
     * 新增社交应用
     *
     * @param sysSocialPlatform 社交应用
     * @return 结果
     */
    int insertSysSocialPlatform(SysSocialPlatform sysSocialPlatform);

    /**
     * 修改社交应用
     *
     * @param sysSocialPlatform 社交应用
     * @return 结果
     */
    int updateSysSocialPlatform(SysSocialPlatform sysSocialPlatform);

    /**
     * 批量删除社交应用
     *
     * @param platformNames 需要删除的社交应用ID
     * @return 结果
     */
    int deleteSysSocialPlatformByIds(String[] platformNames);

    /**
     * 删除社交应用信息
     *
     * @param platformName 社交应用ID
     * @return 结果
     */
    int deleteSysSocialPlatformById(String platformName);
}
