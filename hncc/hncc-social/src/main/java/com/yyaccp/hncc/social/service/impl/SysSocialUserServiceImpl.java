package com.yyaccp.hncc.social.service.impl;

import com.yyaccp.hncc.social.domain.SysSocialUser;
import com.yyaccp.hncc.social.mapper.SysSocialUserMapper;
import com.yyaccp.hncc.social.service.ISysSocialUserService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 社交登录用户Service业务层处理
 *
 * @author 天天向上
 * @date 2020-08-20
 */
@Service
@RequiredArgsConstructor
public class SysSocialUserServiceImpl implements ISysSocialUserService {
    private final SysSocialUserMapper sysSocialUserMapper;

    /**
     * 查询社交登录用户
     *
     * @param id 社交登录用户ID
     * @return 社交登录用户
     */
    @Override
    public SysSocialUser selectSysSocialUserById(Long id) {
        return sysSocialUserMapper.selectSysSocialUserById(id);
    }

    /**
     * 查询社交登录用户列表
     *
     * @param sysSocialUser 社交登录用户
     * @return 社交登录用户
     */
    @Override
    public List<SysSocialUser> selectSysSocialUserList(SysSocialUser sysSocialUser) {
        return sysSocialUserMapper.selectSysSocialUserList(sysSocialUser);
    }

    /**
     * 新增社交登录用户
     *
     * @param sysSocialUser 社交登录用户
     * @return 结果
     */
    @Override
    public int insertSysSocialUser(SysSocialUser sysSocialUser) {
        return sysSocialUserMapper.insertSysSocialUser(sysSocialUser);
    }

    /**
     * 修改社交登录用户
     *
     * @param sysSocialUser 社交登录用户
     * @return 结果
     */
    @Override
    public int updateSysSocialUser(SysSocialUser sysSocialUser) {
        return sysSocialUserMapper.updateSysSocialUser(sysSocialUser);
    }

    /**
     * 批量删除社交登录用户
     *
     * @param ids 需要删除的社交登录用户ID
     * @return 结果
     */
    @Override
    public int deleteSysSocialUserByIds(Long[] ids) {
        return sysSocialUserMapper.deleteSysSocialUserByIds(ids);
    }

    /**
     * 删除社交登录用户信息
     *
     * @param id 社交登录用户ID
     * @return 结果
     */
    @Override
    public int deleteSysSocialUserById(Long id) {
        return sysSocialUserMapper.deleteSysSocialUserById(id);
    }
}
