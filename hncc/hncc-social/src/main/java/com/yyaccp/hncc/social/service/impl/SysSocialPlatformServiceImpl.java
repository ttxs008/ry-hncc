package com.yyaccp.hncc.social.service.impl;

import com.yyaccp.hncc.social.domain.SysSocialPlatform;
import com.yyaccp.hncc.social.mapper.SysSocialPlatformMapper;
import com.yyaccp.hncc.social.service.ISysSocialPlatformService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 社交应用Service业务层处理
 *
 * @author 天天向上
 * @date 2020-08-20
 */
@Service
public class SysSocialPlatformServiceImpl implements ISysSocialPlatformService {
    @Autowired
    private SysSocialPlatformMapper sysSocialPlatformMapper;

    /**
     * 查询社交应用
     *
     * @param platformName 社交应用ID
     * @return 社交应用
     */
    @Override
    public SysSocialPlatform selectSysSocialPlatformById(String platformName) {
        return sysSocialPlatformMapper.selectSysSocialPlatformById(platformName);
    }

    /**
     * 查询社交应用列表
     *
     * @param sysSocialPlatform 社交应用
     * @return 社交应用
     */
    @Override
    public List<SysSocialPlatform> selectSysSocialPlatformList(SysSocialPlatform sysSocialPlatform) {
        return sysSocialPlatformMapper.selectSysSocialPlatformList(sysSocialPlatform);
    }

    /**
     * 新增社交应用
     *
     * @param sysSocialPlatform 社交应用
     * @return 结果
     */
    @Override
    public int insertSysSocialPlatform(SysSocialPlatform sysSocialPlatform) {
        return sysSocialPlatformMapper.insertSysSocialPlatform(sysSocialPlatform);
    }

    /**
     * 修改社交应用
     *
     * @param sysSocialPlatform 社交应用
     * @return 结果
     */
    @Override
    public int updateSysSocialPlatform(SysSocialPlatform sysSocialPlatform) {
        return sysSocialPlatformMapper.updateSysSocialPlatform(sysSocialPlatform);
    }

    /**
     * 批量删除社交应用
     *
     * @param platformNames 需要删除的社交应用ID
     * @return 结果
     */
    @Override
    public int deleteSysSocialPlatformByIds(String[] platformNames) {
        return sysSocialPlatformMapper.deleteSysSocialPlatformByIds(platformNames);
    }

    /**
     * 删除社交应用信息
     *
     * @param platformName 社交应用ID
     * @return 结果
     */
    @Override
    public int deleteSysSocialPlatformById(String platformName) {
        return sysSocialPlatformMapper.deleteSysSocialPlatformById(platformName);
    }
}
