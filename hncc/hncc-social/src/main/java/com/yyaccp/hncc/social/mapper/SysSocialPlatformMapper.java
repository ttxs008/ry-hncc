package com.yyaccp.hncc.social.mapper;

import com.yyaccp.hncc.social.domain.SysSocialPlatform;

import java.util.List;

/**
 * 社交应用Mapper接口
 *
 * @author 天天向上
 * @date 2020-08-20
 */
public interface SysSocialPlatformMapper {
    /**
     * 查询社交应用
     *
     * @param platformName 社交应用ID
     * @return 社交应用
     */
    public SysSocialPlatform selectSysSocialPlatformById(String platformName);

    /**
     * 查询社交应用列表
     *
     * @param sysSocialPlatform 社交应用
     * @return 社交应用集合
     */
    public List<SysSocialPlatform> selectSysSocialPlatformList(SysSocialPlatform sysSocialPlatform);

    /**
     * 新增社交应用
     *
     * @param sysSocialPlatform 社交应用
     * @return 结果
     */
    public int insertSysSocialPlatform(SysSocialPlatform sysSocialPlatform);

    /**
     * 修改社交应用
     *
     * @param sysSocialPlatform 社交应用
     * @return 结果
     */
    public int updateSysSocialPlatform(SysSocialPlatform sysSocialPlatform);

    /**
     * 删除社交应用
     *
     * @param platformName 社交应用ID
     * @return 结果
     */
    public int deleteSysSocialPlatformById(String platformName);

    /**
     * 批量删除社交应用
     *
     * @param platformNames 需要删除的数据ID
     * @return 结果
     */
    public int deleteSysSocialPlatformByIds(String[] platformNames);
}
