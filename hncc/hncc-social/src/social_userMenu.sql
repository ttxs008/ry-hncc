-- 菜单 SQL
insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('社交登录用户', '1', '1', 'social_user', 'social/social_user/index', 1, 'C', '0', '0', 'social:social_user:list', '#', 'admin', '2018-03-01', 'ry', '2018-03-01', '社交登录用户菜单');

-- 按钮父菜单ID
SELECT @parentId := LAST_INSERT_ID();

-- 按钮 SQL
insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('社交登录用户查询', @parentId, '1',  '#', '', 1,  'F', '0',  '0', 'social:social_user:query',        '#', 'admin', '2018-03-01', 'ry', '2018-03-01', '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('社交登录用户新增', @parentId, '2',  '#', '', 1,  'F', '0',  '0', 'social:social_user:add',          '#', 'admin', '2018-03-01', 'ry', '2018-03-01', '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('社交登录用户修改', @parentId, '3',  '#', '', 1,  'F', '0',  '0', 'social:social_user:edit',         '#', 'admin', '2018-03-01', 'ry', '2018-03-01', '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('社交登录用户删除', @parentId, '4',  '#', '', 1,  'F', '0',  '0', 'social:social_user:remove',       '#', 'admin', '2018-03-01', 'ry', '2018-03-01', '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('社交登录用户导出', @parentId, '5',  '#', '', 1,  'F', '0',  '0', 'social:social_user:export',       '#', 'admin', '2018-03-01', 'ry', '2018-03-01', '');