-- 菜单 SQL
insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('诊断类型（疾病）目录', '2000', '1', 'diseCatalog', 'dms/diseCatalog/index', 1, 'C', '0', '0', 'dms:diseCatalog:list', '#', 'admin', '2018-03-01', 'ry', '2018-03-01', '诊断类型（疾病）目录菜单');

-- 按钮父菜单ID
SELECT @parentId := LAST_INSERT_ID();

-- 按钮 SQL
insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('诊断类型（疾病）目录查询', @parentId, '1',  '#', '', 1,  'F', '0',  '0', 'dms:diseCatalog:query',        '#', 'admin', '2018-03-01', 'ry', '2018-03-01', '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('诊断类型（疾病）目录新增', @parentId, '2',  '#', '', 1,  'F', '0',  '0', 'dms:diseCatalog:add',          '#', 'admin', '2018-03-01', 'ry', '2018-03-01', '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('诊断类型（疾病）目录修改', @parentId, '3',  '#', '', 1,  'F', '0',  '0', 'dms:diseCatalog:edit',         '#', 'admin', '2018-03-01', 'ry', '2018-03-01', '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('诊断类型（疾病）目录删除', @parentId, '4',  '#', '', 1,  'F', '0',  '0', 'dms:diseCatalog:remove',       '#', 'admin', '2018-03-01', 'ry', '2018-03-01', '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('诊断类型（疾病）目录导出', @parentId, '5',  '#', '', 1,  'F', '0',  '0', 'dms:diseCatalog:export',       '#', 'admin', '2018-03-01', 'ry', '2018-03-01', '');