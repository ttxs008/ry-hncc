-- 菜单 SQL
insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('药品项目（包括了重要、中成药、草药）', '2020', '1', 'drug', 'dms/drug/index', 1, 'C', '0', '0', 'dms:drug:list', '#', 'admin', '2018-03-01', 'ry', '2018-03-01', '药品项目（包括了重要、中成药、草药）菜单');

-- 按钮父菜单ID
SELECT @parentId := LAST_INSERT_ID();

-- 按钮 SQL
insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('药品项目（包括了重要、中成药、草药）查询', @parentId, '1',  '#', '', 1,  'F', '0',  '0', 'dms:drug:query',        '#', 'admin', '2018-03-01', 'ry', '2018-03-01', '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('药品项目（包括了重要、中成药、草药）新增', @parentId, '2',  '#', '', 1,  'F', '0',  '0', 'dms:drug:add',          '#', 'admin', '2018-03-01', 'ry', '2018-03-01', '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('药品项目（包括了重要、中成药、草药）修改', @parentId, '3',  '#', '', 1,  'F', '0',  '0', 'dms:drug:edit',         '#', 'admin', '2018-03-01', 'ry', '2018-03-01', '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('药品项目（包括了重要、中成药、草药）删除', @parentId, '4',  '#', '', 1,  'F', '0',  '0', 'dms:drug:remove',       '#', 'admin', '2018-03-01', 'ry', '2018-03-01', '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('药品项目（包括了重要、中成药、草药）导出', @parentId, '5',  '#', '', 1,  'F', '0',  '0', 'dms:drug:export',       '#', 'admin', '2018-03-01', 'ry', '2018-03-01', '');