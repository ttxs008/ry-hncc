-- 菜单 SQL
insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('病历', '2000', '1', 'case_history', 'dms/case_history/index', 1, 'C', '0', '0', 'dms:case_history:list', '#', 'admin', '2018-03-01', 'ry', '2018-03-01', '病历菜单');

-- 按钮父菜单ID
SELECT @parentId := LAST_INSERT_ID();

-- 按钮 SQL
insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('病历查询', @parentId, '1',  '#', '', 1,  'F', '0',  '0', 'dms:case_history:query',        '#', 'admin', '2018-03-01', 'ry', '2018-03-01', '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('病历新增', @parentId, '2',  '#', '', 1,  'F', '0',  '0', 'dms:case_history:add',          '#', 'admin', '2018-03-01', 'ry', '2018-03-01', '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('病历修改', @parentId, '3',  '#', '', 1,  'F', '0',  '0', 'dms:case_history:edit',         '#', 'admin', '2018-03-01', 'ry', '2018-03-01', '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('病历删除', @parentId, '4',  '#', '', 1,  'F', '0',  '0', 'dms:case_history:remove',       '#', 'admin', '2018-03-01', 'ry', '2018-03-01', '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('病历导出', @parentId, '5',  '#', '', 1,  'F', '0',  '0', 'dms:case_history:export',       '#', 'admin', '2018-03-01', 'ry', '2018-03-01', '');
//新增方法
INSERT INTO `sys_menu`(`menu_name`, `parent_id`, `order_num`, `path`, `component`, `is_frame`, `menu_type`, `visible`, `status`, `perms`, `icon`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`)
VALUES ('挂号id查询病历', @parentId, 6, '#', '', 1, 'F', '0', '0', 'dms:case_history:patQuery', '#', 'admin', '2018-03-01 00:00:00', 'ry', '2018-03-01 00:00:00', '');
