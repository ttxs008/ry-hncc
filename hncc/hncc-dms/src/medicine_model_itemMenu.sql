-- 菜单 SQL
insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('成药模版', '2000', '1', 'medicine_model_item', 'dms/medicine_model_item/index', 1, 'C', '0', '0', 'dms:medicine_model_item:list', '#', 'admin', '2018-03-01', 'ry', '2018-03-01', '成药模版菜单');

-- 按钮父菜单ID
SELECT @parentId := LAST_INSERT_ID();

-- 按钮 SQL
insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('成药模版查询', @parentId, '1',  '#', '', 1,  'F', '0',  '0', 'dms:medicine_model_item:query',        '#', 'admin', '2018-03-01', 'ry', '2018-03-01', '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('成药模版新增', @parentId, '2',  '#', '', 1,  'F', '0',  '0', 'dms:medicine_model_item:add',          '#', 'admin', '2018-03-01', 'ry', '2018-03-01', '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('成药模版修改', @parentId, '3',  '#', '', 1,  'F', '0',  '0', 'dms:medicine_model_item:edit',         '#', 'admin', '2018-03-01', 'ry', '2018-03-01', '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('成药模版删除', @parentId, '4',  '#', '', 1,  'F', '0',  '0', 'dms:medicine_model_item:remove',       '#', 'admin', '2018-03-01', 'ry', '2018-03-01', '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('成药模版导出', @parentId, '5',  '#', '', 1,  'F', '0',  '0', 'dms:medicine_model_item:export',       '#', 'admin', '2018-03-01', 'ry', '2018-03-01', '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('成药：通过模板Id查询药品明细', 2000, '6',  'select_medicine_model_item_byModelID', 'dms/medicine_model_item/medicine_model_item_info', 1,  'C', '1',  '0', '',       '#', 'admin', '2018-03-01', 'ry', '2018-03-01', '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('成药：添加模板下的药品', 2000, '7',  'medicine_model_item_add_drug', 'dms/medicine_model_item/addDrugModel', 1,  'C', '1',  '0', '',       '#', 'admin', '2018-03-01', 'ry', '2018-03-01', '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('通过药品模板Id查询成药模板明细', @parentId, '8',  '#', '#', 1,  'F', '0',  '0', 'dms:medicine_model_item:queryDrugByModelId',       '#', 'admin', '2018-03-01', 'ry', '2018-03-01', '');