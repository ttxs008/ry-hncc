-- 菜单 SQL
insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('草处方', '3', '1', 'herbal_prescription_record', 'dms/herbal_prescription_record/index', 1, 'C', '0', '0', 'dms:herbal_prescription_record:list', '#', 'admin', '2018-03-01', 'ry', '2018-03-01', '草处方菜单');

-- 按钮父菜单ID
SELECT @parentId := LAST_INSERT_ID();

-- 按钮 SQL
insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('草处方查询', @parentId, '1',  '#', '', 1,  'F', '0',  '0', 'dms:herbal_prescription_record:query',        '#', 'admin', '2018-03-01', 'ry', '2018-03-01', '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('草处方新增', @parentId, '2',  '#', '', 1,  'F', '0',  '0', 'dms:herbal_prescription_record:add',          '#', 'admin', '2018-03-01', 'ry', '2018-03-01', '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('草处方修改', @parentId, '3',  '#', '', 1,  'F', '0',  '0', 'dms:herbal_prescription_record:edit',         '#', 'admin', '2018-03-01', 'ry', '2018-03-01', '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('草处方删除', @parentId, '4',  '#', '', 1,  'F', '0',  '0', 'dms:herbal_prescription_record:remove',       '#', 'admin', '2018-03-01', 'ry', '2018-03-01', '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('草处方导出', @parentId, '5',  '#', '', 1,  'F', '0',  '0', 'dms:herbal_prescription_record:export',       '#', 'admin', '2018-03-01', 'ry', '2018-03-01', '');