-- 菜单 SQL
insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('草药模版项', '2000', '1', 'herbal_model_item', 'dms/herbal_model_item/index', 1, 'C', '1', '0', 'dms:herbal_model_item:list', '#', 'admin', '2018-03-01', 'ry', '2018-03-01', '草药模版项菜单');

-- 按钮父菜单ID
SELECT @parentId := LAST_INSERT_ID();

-- 按钮 SQL
insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('草药模版项查询', @parentId, '1',  '#', '', 1,  'F', '0',  '0', 'dms:herbal_model_item:query',        '#', 'admin', '2018-03-01', 'ry', '2018-03-01', '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('草药模版项新增', @parentId, '2',  '#', '', 1,  'F', '0',  '0', 'dms:herbal_model_item:add',          '#', 'admin', '2018-03-01', 'ry', '2018-03-01', '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('草药模版项修改', @parentId, '3',  '#', '', 1,  'F', '0',  '0', 'dms:herbal_model_item:edit',         '#', 'admin', '2018-03-01', 'ry', '2018-03-01', '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('草药模版项删除', @parentId, '4',  '#', '', 1,  'F', '0',  '0', 'dms:herbal_model_item:remove',       '#', 'admin', '2018-03-01', 'ry', '2018-03-01', '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('草药模版项导出', @parentId, '5',  '#', '', 1,  'F', '0',  '0', 'dms:herbal_model_item:export',       '#', 'admin', '2018-03-01', 'ry', '2018-03-01', '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('草药模板下的药品查询', @parentId, '6',  '#', '', 1,  'F', '0',  '0', 'dms:herbal_model_item:queryDrugByModelId',       '#', 'admin', '2018-03-01', 'zhoujixin', '2018-03-01', '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('草药模板下的药品添加', '2000', '1',  'herbal_model_item_add_drug', 'dms/herbal_model_item/addDrugModel', 1,  'C', '1',  '0', '',       '#', 'admin', '2018-03-01', 'zhoujixin', '2018-03-01', '');