/**
  !!!!请先执行目录SQL!!!!
 */

--目录SQL
INSERT INTO `hncc`.`sys_menu`(`menu_id`, `menu_name`, `parent_id`, `order_num`, `path`, `component`, `is_frame`, `menu_type`, `visible`, `status`, `perms`, `icon`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`)
VALUES (0, '模版与常用项', 0, 2, 'template', NULL, 1, 'M', '0', '0', NULL, 'documentation', 'admin', '2020-08-16 22:12:23', '', NULL, '');

/**
==============================================================================================================================================================================================================================================================
 */

/**
  请将 菜单SQL parent_id ‘2094’ 改为 目录SQL自增id
 */

-- 菜单 SQL
insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('非药品模版', '2094', '1', 'non_drug_model', 'dms/non_drug_model/index', 1, 'C', '0', '0', 'dms:non_drug_model:list', '#', 'admin', '2018-03-01', 'ry', '2018-03-01', '非药品模版菜单');

/**
 添加和修改的挂载
 */
INSERT INTO `hncc`.`sys_menu`(`menu_id`, `menu_name`, `parent_id`, `order_num`, `path`, `component`, `is_frame`, `menu_type`, `visible`, `status`, `perms`, `icon`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`)
VALUES (0, '非药品模版新增', 2094, 1, 'add_non_drug_model', 'dms/non_drug_model/add_non_drug_model', 1, 'C', '1', '0', 'dms:non_drug_model:add', '#', 'admin', '2018-03-01 00:00:00', 'ry', '2018-03-01 00:00:00', '非药品模版新增');
INSERT INTO `hncc`.`sys_menu`(`menu_id`, `menu_name`, `parent_id`, `order_num`, `path`, `component`, `is_frame`, `menu_type`, `visible`, `status`, `perms`, `icon`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`)
VALUES (0, '非药品模版修改', 2094, 1, 'edit_non_drug_model', 'dms/non_drug_model/edit_non_drug_model', 1, 'C', '1', '0', 'dms:non_drug_model:edit', '#', 'admin', '2018-03-01 00:00:00', 'ry', '2018-03-01 00:00:00', '非药品模版修改');


-- 按钮父菜单ID
SELECT @parentId := LAST_INSERT_ID();

-- 按钮 SQL
insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('非药品模版查询', @parentId, '1',  '#', '', 1,  'F', '0',  '0', 'dms:non_drug_model:query',        '#', 'admin', '2018-03-01', 'ry', '2018-03-01', '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('非药品模版新增', @parentId, '2',  '#', '', 1,  'F', '0',  '0', 'dms:non_drug_model:add',          '#', 'admin', '2018-03-01', 'ry', '2018-03-01', '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('非药品模版修改', @parentId, '3',  '#', '', 1,  'F', '0',  '0', 'dms:non_drug_model:edit',         '#', 'admin', '2018-03-01', 'ry', '2018-03-01', '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('非药品模版删除', @parentId, '4',  '#', '', 1,  'F', '0',  '0', 'dms:non_drug_model:remove',       '#', 'admin', '2018-03-01', 'ry', '2018-03-01', '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('非药品模版导出', @parentId, '5',  '#', '', 1,  'F', '0',  '0', 'dms:non_drug_model:export',       '#', 'admin', '2018-03-01', 'ry', '2018-03-01', '');