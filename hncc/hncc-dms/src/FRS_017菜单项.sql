-- 菜单 SQL
insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('医生常用诊断（疾病）、、检查、检验、处置、药品查询', '1093', '1', 'used', 'dms/used/index', 1, 'C', '0', '0', 'dms:used:list', '#', 'admin', '2018-03-01', 'ry', '2018-03-01', '医生常用项菜单');

-- 按钮父菜单ID
SELECT @parentId := LAST_INSERT_ID();

-- 按钮 SQL
insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('医生常用诊断（疾病）、检查、检验、处置、药品查询', @parentId, '1',  '#', '', 1,  'F', '0',  '0', 'dms:used:query',        '#', 'admin', '2018-03-01', 'ry', '2018-03-01', '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('医生常用诊断（疾病）、检查、检验、处置、药品新增', @parentId, '2',  '#', '', 1,  'F', '0',  '0', 'dms:used:add',          '#', 'admin', '2018-03-01', 'ry', '2018-03-01', '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('医生常用诊断（疾病）、检查、检验、处置、药品修改', @parentId, '3',  '#', '', 1,  'F', '0',  '0', 'dms:used:edit',         '#', 'admin', '2018-03-01', 'ry', '2018-03-01', '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('医生常用诊断（疾病）、检查、检验、处置、药品删除', @parentId, '4',  '#', '', 1,  'F', '0',  '0', 'dms:used:remove',       '#', 'admin', '2018-03-01', 'ry', '2018-03-01', '');

