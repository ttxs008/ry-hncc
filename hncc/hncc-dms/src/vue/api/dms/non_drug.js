import request from '@/utils/request'

// 查询非药品收费项目列表
export function listNon_drug(query) {
    return request({
        url: '/dms/non_drug/list',
        method: 'get',
        params: query
    })
}

// 查询非药品收费项目详细
export function getNon_drug(id) {
    return request({
        url: '/dms/non_drug/' + id,
        method: 'get'
    })
}

// 新增非药品收费项目
export function addNon_drug(data) {
    return request({
        url: '/dms/non_drug',
        method: 'post',
        data: data
    })
}

// 修改非药品收费项目
export function updateNon_drug(data) {
    return request({
        url: '/dms/non_drug',
        method: 'put',
        data: data
    })
}

// 删除非药品收费项目
export function delNon_drug(id) {
    return request({
        url: '/dms/non_drug/' + id,
        method: 'delete'
    })
}

// 导出非药品收费项目
export function exportNon_drug(query) {
    return request({
        url: '/dms/non_drug/export',
        method: 'get',
        params: query
    })
}
