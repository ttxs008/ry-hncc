import request from '@/utils/request'

// 查询医生常用诊断（疾病）、检查检验(搁置)列表
export function listUsed(query) {
  return request({
    url: '/dms/used/list',
    method: 'get',
    params: query
  })
}

export function dictDataList(query) {
  return request({
    url: '/dms/used/dictDataList',
    method: 'get',

  })
}

// 查询所有常用诊断（疾病）、检查检验详细
export function allDataList(data) {
  return request({
    url: '/dms/used/allDataList',
    method: 'get',
    params:data
  })
}

// 新增医生常用诊断（疾病）、检查检验(搁置)
export function addUsed(data) {
  return request({
    url: '/dms/used/add/'+data,
    method: 'get',
  })
}

// 修改医生常用诊断（疾病）、检查检验(搁置)
export function updateUsed(data) {
  return request({
    url: '/dms/used',
    method: 'put',
    data: data
  })
}

// 删除医生常用诊断（疾病）、检查检验(搁置)
export function delUsed(data) {
  return request({
    url: '/dms/used/del/'+data,
    method: 'get',
  })
}

// 导出医生常用诊断（疾病）、检查检验(搁置)
export function exportUsed(query) {
  return request({
    url: '/dms/used/export',
    method: 'get',
    params: query
  })
}
