import request from '@/utils/request'

// 查询成药药品模版列表
export function listDrug_medicine_model(query) {
  return request({
    url: '/dms/drug_medicine_model/list',
    method: 'get',
    params: query
  })
}

// 查询成药药品模版详细
export function getDrug_medicine_model(id) {
  return request({
    url: '/dms/drug_medicine_model/' + id,
    method: 'get'
  })
}

// 新增成药药品模版
export function addDrug_medicine_model(data) {
  return request({
    url: '/dms/drug_medicine_model',
    method: 'post',
    data: data
  })
}

// 修改成药药品模版
export function updateDrug_medicine_model(data) {
  return request({
    url: '/dms/drug_medicine_model',
    method: 'put',
    data: data
  })
}

// 删除成药药品模版
export function delDrug_medicine_model(id) {
  return request({
    url: '/dms/drug_medicine_model/' + id,
    method: 'delete'
  })
}

// 导出成药药品模版
export function exportDrug_medicine_model(query) {
  return request({
    url: '/dms/drug_medicine_model/export',
    method: 'get',
    params: query
  })
}