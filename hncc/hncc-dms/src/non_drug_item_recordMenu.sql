INSERT INTO `hncc`.`sys_menu`(`menu_name`, `parent_id`, `order_num`, `path`, `component`, `is_frame`, `menu_type`, `visible`, `status`, `perms`, `icon`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`) VALUES ('门诊医技工作台', 2000, 3, 'non_drug_item_record', 'dms/non_drug_item_record/index', 1, 'C', '0', '0', 'dms:non_drug_item_record:list', '#', 'admin', '2020-09-03 09:47:09', 'admin', '2020-09-03 09:53:49', '');

-- 按钮父菜单ID
SELECT @parentId := LAST_INSERT_ID();

-- 菜单 SQL
insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('检查项检验项处置项记录(开立的)', @parentId, '1', 'non_drug_item_record', 'dms/non_drug_item_record/index', 1, 'C', '0', '0', 'dms:non_drug_item_record:list', '#', 'admin', '2018-03-01', 'ry', '2018-03-01', '检查项检验项处置项记录(开立的)菜单');

-- 按钮父菜单ID
SELECT @parentId := LAST_INSERT_ID();

-- 按钮 SQL
insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('检查项检验项处置项记录(开立的)查询', @parentId, '1',  '#', '', 1,  'F', '0',  '0', 'dms:non_drug_item_record:query',        '#', 'admin', '2018-03-01', 'ry', '2018-03-01', '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('检查项检验项处置项记录(开立的)新增', @parentId, '2',  '#', '', 1,  'F', '0',  '0', 'dms:non_drug_item_record:add',          '#', 'admin', '2018-03-01', 'ry', '2018-03-01', '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('检查项检验项处置项记录(开立的)修改', @parentId, '3',  '#', '', 1,  'F', '0',  '0', 'dms:non_drug_item_record:edit',         '#', 'admin', '2018-03-01', 'ry', '2018-03-01', '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('检查项检验项处置项记录(开立的)删除', @parentId, '4',  '#', '', 1,  'F', '0',  '0', 'dms:non_drug_item_record:remove',       '#', 'admin', '2018-03-01', 'ry', '2018-03-01', '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('检查项检验项处置项记录(开立的)导出', @parentId, '5',  '#', '', 1,  'F', '0',  '0', 'dms:non_drug_item_record:export',       '#', 'admin', '2018-03-01', 'ry', '2018-03-01', '');