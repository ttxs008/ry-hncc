-- 菜单 SQL
insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('非药品收费项目', '2020', '1', 'non_drug', 'dms/non_drug/index', 1, 'C', '0', '0', 'dms:non_drug:list', '#', 'admin', '2018-03-01', 'ry', '2018-03-01', '非药品收费项目菜单');

-- 按钮父菜单ID
SELECT @parentId := LAST_INSERT_ID();

-- 按钮 SQL
insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('非药品收费项目查询', @parentId, '1',  '#', '', 1,  'F', '0',  '0', 'dms:non_drug:query',        '#', 'admin', '2018-03-01', 'ry', '2018-03-01', '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('非药品收费项目新增', @parentId, '2',  '#', '', 1,  'F', '0',  '0', 'dms:non_drug:add',          '#', 'admin', '2018-03-01', 'ry', '2018-03-01', '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('非药品收费项目修改', @parentId, '3',  '#', '', 1,  'F', '0',  '0', 'dms:non_drug:edit',         '#', 'admin', '2018-03-01', 'ry', '2018-03-01', '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('非药品收费项目删除', @parentId, '4',  '#', '', 1,  'F', '0',  '0', 'dms:non_drug:remove',       '#', 'admin', '2018-03-01', 'ry', '2018-03-01', '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('非药品收费项目导出', @parentId, '5',  '#', '', 1,  'F', '0',  '0', 'dms:non_drug:export',       '#', 'admin', '2018-03-01', 'ry', '2018-03-01', '');