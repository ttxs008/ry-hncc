-- 菜单 SQL
insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('药品模版', '2125', '1', 'drug_model', 'dms/drug_model/index', 1, 'C', '0', '0', 'dms:drug_model:list', '#', 'admin', '2018-03-01', 'ry', '2018-03-01', '药品模版菜单');

-- 按钮父菜单ID
SELECT @parentId := LAST_INSERT_ID();

-- 按钮 SQL
insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('药品模版查询', @parentId, '1',  '#', '', 1,  'F', '0',  '0', 'dms:drug_model:query',        '#', 'admin', '2018-03-01', 'ry', '2018-03-01', '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('药品模版新增', @parentId, '2',  '#', '', 1,  'F', '0',  '0', 'dms:drug_model:add',          '#', 'admin', '2018-03-01', 'ry', '2018-03-01', '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('药品模版修改', @parentId, '3',  '#', '', 1,  'F', '0',  '0', 'dms:drug_model:edit',         '#', 'admin', '2018-03-01', 'ry', '2018-03-01', '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('药品模版删除', @parentId, '4',  '#', '', 1,  'F', '0',  '0', 'dms:drug_model:remove',       '#', 'admin', '2018-03-01', 'ry', '2018-03-01', '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('药品模版导出', @parentId, '5',  '#', '', 1,  'F', '0',  '0', 'dms:drug_model:export',       '#', 'admin', '2018-03-01', 'ry', '2018-03-01', '');