package com.yyaccp.hncc.dms.mapper;

import com.yyaccp.hncc.dms.domain.DmsPatientBill;

import java.util.List;

/**
 * Created by 余归 on 2020/8/24
 *
 * @author 余归
 */
public interface DmsPatientBillMapper {

    /**
     * 1.查询患者挂号id对应的非药品项目
     */
    List<DmsPatientBill> selectNonDrugs(Integer registrationId);


    /**
     * 2.查询患者挂号id对应的成药项目
     */
    List<DmsPatientBill> selectMedicineDrugs(Integer registrationId);

    /**
     * 3.查询患者挂号id对应的草药项目
     */
    List<DmsPatientBill> selectHerbalDrugs(Integer registrationId);

}
