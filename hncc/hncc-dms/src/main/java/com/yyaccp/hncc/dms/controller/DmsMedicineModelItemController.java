package com.yyaccp.hncc.dms.controller;

import java.util.List;

import com.yyaccp.hncc.common.vo.dms.DmsHerbalModelItemVO;
import com.yyaccp.hncc.dms.domain.DmsHerbalModelItem;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.yyaccp.hncc.dms.domain.DmsMedicineModelItem;
import com.yyaccp.hncc.dms.service.IDmsMedicineModelItemService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import io.swagger.annotations.*;
//com.yyaccp.hncc.common模块的包引入
import com.yyaccp.hncc.common.vo.dms.DmsMedicineModelItemVO;
import com.yyaccp.hncc.common.util.BeanCopierUtil;

import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 成药模版Controller
 *
 * @author 周某
 * @date 2020-08-30
 */
@Api(tags = "成药模版")
@RestController
@RequestMapping("/dms/medicine_model_item")
public class DmsMedicineModelItemController extends BaseController {
    @Autowired
    private IDmsMedicineModelItemService dmsMedicineModelItemService;

/**
 * 查询成药模版列表
 *
 */
@PreAuthorize("@ss.hasPermi('dms:medicine_model_item:list')")
@GetMapping("/list")
@ApiOperation(value = "查询成药模版" , notes = "查询所有成药模版" ,
        code = 200, produces = "application/json" , protocols = "Http" ,
        response = TableDataInfo.class, httpMethod = "GET")
@ApiResponses({
        @ApiResponse(code = 204, message = "操作已经执行成功，但是没有返回数据"),
        @ApiResponse(code = 303, message = "重定向"),
        @ApiResponse(code = 400, message = "参数列表错误（缺少，格式不匹配）"),
        @ApiResponse(code = 500, message = "系统内部错误"),
        @ApiResponse(code = 404, message = "资源，服务未找到"),
        @ApiResponse(code = 200, message = "操作成功"),
        @ApiResponse(code = 401, message = "未授权"),
        @ApiResponse(code = 403, message = "访问受限，授权过期")
})
@ApiParam(name = "DmsMedicineModelItemVO对象" , type = "DmsMedicineModelItemVO")
        public TableDataInfo list(DmsMedicineModelItemVO dmsMedicineModelItemVO) {
        //将Vo转化为实体
        DmsMedicineModelItem dmsMedicineModelItem=BeanCopierUtil.copy(dmsMedicineModelItemVO,DmsMedicineModelItem. class);
        startPage();
        List<DmsMedicineModelItem> list = dmsMedicineModelItemService.selectDmsMedicineModelItemList(dmsMedicineModelItem);
        TableDataInfo tableDataInfo = getDataTable(list);
        //替换集合
        tableDataInfo.setRows(BeanCopierUtil.copy(tableDataInfo.getRows(), DmsMedicineModelItemVO.class));
        return tableDataInfo;
    }
    
    /**
     * 导出成药模版列表
     */
    @PreAuthorize("@ss.hasPermi('dms:medicine_model_item:export')")
    @Log(title = "成药模版" , businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    @ApiOperation(value = "导出成药模版表" , notes = "导出所有成药模版" ,
            code = 200, produces = "application/json" , protocols = "Http" ,
            response = TableDataInfo.class, httpMethod = "GET")
    @ApiResponses({
            @ApiResponse(code = 204, message = "操作已经执行成功，但是没有返回数据"),
            @ApiResponse(code = 303, message = "重定向"),
            @ApiResponse(code = 400, message = "参数列表错误（缺少，格式不匹配）"),
            @ApiResponse(code = 500, message = "系统内部错误"),
            @ApiResponse(code = 404, message = "资源，服务未找到"),
            @ApiResponse(code = 200, message = "操作成功"),
            @ApiResponse(code = 401, message = "未授权"),
            @ApiResponse(code = 403, message = "访问受限，授权过期")
    })
    @ApiParam(name = "DmsMedicineModelItemVO对象" , type = "DmsMedicineModelItemVO")
    public AjaxResult export(DmsMedicineModelItemVO dmsMedicineModelItemVO) {
        //将VO转化为实体
        DmsMedicineModelItem dmsMedicineModelItem=BeanCopierUtil.copy(dmsMedicineModelItemVO,DmsMedicineModelItem. class);
        List<DmsMedicineModelItemVO> list = BeanCopierUtil.copy(dmsMedicineModelItemService.selectDmsMedicineModelItemList(dmsMedicineModelItem),DmsMedicineModelItemVO.class);
        ExcelUtil<DmsMedicineModelItemVO> util = new ExcelUtil<DmsMedicineModelItemVO>(DmsMedicineModelItemVO.class);
        return util.exportExcel(list, "medicine_model_item");
    }

    /**
     * 获取成药模版详细信息
     */
    @PreAuthorize("@ss.hasPermi('dms:medicine_model_item:query')")
    @GetMapping(value = "/{id}")
    @ApiOperation(value = "获取成药模版详细信息" ,
            notes = "根据成药模版id获取科室信息" , code = 200,
            produces = "application/json" , protocols = "Http" , response = AjaxResult.class,
            httpMethod = "GET")
    @ApiResponses({
            @ApiResponse(code = 204, message = "操作已经执行成功，但是没有返回数据"),
            @ApiResponse(code = 303, message = "重定向"),
            @ApiResponse(code = 400, message = "参数列表错误（缺少，格式不匹配）"),
            @ApiResponse(code = 500, message = "系统内部错误"),
            @ApiResponse(code = 404, message = "资源，服务未找到"),
            @ApiResponse(code = 200, message = "操作成功"),
            @ApiResponse(code = 401, message = "未授权"),
            @ApiResponse(code = 403, message = "访问受限，授权过期")
    })
    @ApiParam(name = "dmsMedicineModelItem.id" , type = "Long")
    public AjaxResult getInfo(@PathVariable("id") Long id) {
        return AjaxResult.success(BeanCopierUtil.copy(dmsMedicineModelItemService.selectDmsMedicineModelItemById(id),DmsMedicineModelItemVO.class));
    }

    /**
     * 新增成药模版
     */
    @PreAuthorize("@ss.hasPermi('dms:medicine_model_item:add')")
    @Log(title = "新增成药模版" , businessType = BusinessType.INSERT)
    @PostMapping
    @ApiOperation(value = "新增成药模版信息" , notes = "新增成药模版信息" , code = 200,
            produces = "application/json" , protocols = "Http" , response = AjaxResult.class,
            httpMethod = "POST")
    @ApiResponses({
            @ApiResponse(code = 204, message = "操作已经执行成功，但是没有返回数据"),
            @ApiResponse(code = 303, message = "重定向"),
            @ApiResponse(code = 400, message = "参数列表错误（缺少，格式不匹配）"),
            @ApiResponse(code = 500, message = "系统内部错误"),
            @ApiResponse(code = 404, message = "资源，服务未找到"),
            @ApiResponse(code = 200, message = "操作成功"),
            @ApiResponse(code = 401, message = "未授权"),
            @ApiResponse(code = 403, message = "访问受限，授权过期")
    })
    @ApiParam(name = "DmsMedicineModelItemVO对象" , type = "DmsMedicineModelItemVO")
    public AjaxResult add(@RequestBody DmsMedicineModelItemVO dmsMedicineModelItemVO) {
        return toAjax(dmsMedicineModelItemService.insertDmsMedicineModelItem(BeanCopierUtil.copy(dmsMedicineModelItemVO,DmsMedicineModelItem. class)));
    }

    /**
     * 修改成药模版
     */
    @PreAuthorize("@ss.hasPermi('dms:medicine_model_item:edit')")
    @Log(title = "成药模版" , businessType = BusinessType.UPDATE)
    @PutMapping
    @ApiOperation(value = "修改成药模版信息" , notes = "修改成药模版信息" , code = 200,
            produces = "application/json" , protocols = "Http" , response = AjaxResult.class,
            httpMethod = "PUT")
    @ApiResponses({
            @ApiResponse(code = 204, message = "操作已经执行成功，但是没有返回数据"),
            @ApiResponse(code = 303, message = "重定向"),
            @ApiResponse(code = 400, message = "参数列表错误（缺少，格式不匹配）"),
            @ApiResponse(code = 500, message = "系统内部错误"),
            @ApiResponse(code = 404, message = "资源，服务未找到"),
            @ApiResponse(code = 200, message = "操作成功"),
            @ApiResponse(code = 401, message = "未授权"),
            @ApiResponse(code = 403, message = "访问受限，授权过期")
    })
    @ApiParam(name = "DmsMedicineModelItemVO对象" , type = "DmsMedicineModelItemVO")
    public AjaxResult edit(@RequestBody DmsMedicineModelItemVO dmsMedicineModelItemVO) {
        return toAjax(dmsMedicineModelItemService.updateDmsMedicineModelItem(BeanCopierUtil.copy(dmsMedicineModelItemVO,DmsMedicineModelItem. class)));
    }

    /**
     * 删除成药模版
     */
    @PreAuthorize("@ss.hasPermi('dms:medicine_model_item:remove')")
    @Log(title = "成药模版" , businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    @ApiOperation(value = "删除成药模版信息" , notes = "删除成药模版信息" , code = 200,
            produces = "application/json" , protocols = "Http" , response = AjaxResult.class,
            httpMethod = "DELETE")
    @ApiResponses({
            @ApiResponse(code = 204, message = "操作已经执行成功，但是没有返回数据"),
            @ApiResponse(code = 303, message = "重定向"),
            @ApiResponse(code = 400, message = "参数列表错误（缺少，格式不匹配）"),
            @ApiResponse(code = 500, message = "系统内部错误"),
            @ApiResponse(code = 404, message = "资源，服务未找到"),
            @ApiResponse(code = 200, message = "操作成功"),
            @ApiResponse(code = 401, message = "未授权"),
            @ApiResponse(code = 403, message = "访问受限，授权过期")
    })
    @ApiParam(name = "dmsMedicineModelItem.id" , type = "Long[]")
    public AjaxResult remove(@PathVariable Long[] ids) {
        return toAjax(dmsMedicineModelItemService.deleteDmsMedicineModelItemByIds(ids));
    }

    /**
     * 通过模板Id查询药品模板明细
     */
    @PreAuthorize("@ss.hasPermi('dms:medicine_model_item:queryDrugByModelId')")
    @GetMapping(value = "/queryDrugByModelId/{id}")
    @ApiOperation(value = "通过模板Id查询药品信息",
            notes = "通过模板Id查询药品信息", code = 200,
            produces = "application/json",protocols = "Http",response = AjaxResult.class,
            httpMethod = "GET")
    @ApiResponses({
            @ApiResponse(code = 204,message = "操作已经执行成功，但是没有返回数据"),
            @ApiResponse(code = 303,message = "重定向"),
            @ApiResponse(code = 400,message = "参数列表错误（缺少，格式不匹配）"),
            @ApiResponse(code = 500,message = "系统内部错误"),
            @ApiResponse(code = 404,message = "资源，服务未找到"),
            @ApiResponse(code = 200,message = "操作成功"),
            @ApiResponse(code = 401,message = "未授权"),
            @ApiResponse(code = 403,message = "访问受限，授权过期")
    })
    @ApiParam(name = "ID", type = "Long")
    public TableDataInfo getDrugInfoByModelId(@PathVariable("id") Long id)
    {
        startPage();
        List<DmsMedicineModelItem> list = dmsMedicineModelItemService.getDrugInfoByModelId(id);
        TableDataInfo tableDataInfo=getDataTable(list);
        //替换集合
        tableDataInfo.setRows(BeanCopierUtil.copy(tableDataInfo.getRows(), DmsMedicineModelItemVO.class));
        return tableDataInfo;
    }
}
