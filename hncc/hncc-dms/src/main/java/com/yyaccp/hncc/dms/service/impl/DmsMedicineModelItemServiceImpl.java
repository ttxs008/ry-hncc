package com.yyaccp.hncc.dms.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.yyaccp.hncc.dms.mapper.DmsMedicineModelItemMapper;
import com.yyaccp.hncc.dms.domain.DmsMedicineModelItem;
import com.yyaccp.hncc.dms.service.IDmsMedicineModelItemService;

/**
 * 成药模版Service业务层处理
 * 
 * @author 周某
 * @date 2020-08-30
 */
@Service
public class DmsMedicineModelItemServiceImpl implements IDmsMedicineModelItemService 
{
    @Autowired
    private DmsMedicineModelItemMapper dmsMedicineModelItemMapper;

    /**
     * 查询成药模版
     * 
     * @param id 成药模版ID
     * @return 成药模版
     */
    @Override
    public DmsMedicineModelItem selectDmsMedicineModelItemById(Long id)
    {
        return dmsMedicineModelItemMapper.selectDmsMedicineModelItemById(id);
    }

    /**
     * 查询成药模版列表
     * 
     * @param dmsMedicineModelItem 成药模版
     * @return 成药模版
     */
    @Override
    public List<DmsMedicineModelItem> selectDmsMedicineModelItemList(DmsMedicineModelItem dmsMedicineModelItem)
    {
        return dmsMedicineModelItemMapper.selectDmsMedicineModelItemList(dmsMedicineModelItem);
    }

    /**
     * 新增成药模版
     * 
     * @param dmsMedicineModelItem 成药模版
     * @return 结果
     */
    @Override
    public int insertDmsMedicineModelItem(DmsMedicineModelItem dmsMedicineModelItem)
    {
        return dmsMedicineModelItemMapper.insertDmsMedicineModelItem(dmsMedicineModelItem);
    }

    /**
     * 修改成药模版
     * 
     * @param dmsMedicineModelItem 成药模版
     * @return 结果
     */
    @Override
    public int updateDmsMedicineModelItem(DmsMedicineModelItem dmsMedicineModelItem)
    {
        return dmsMedicineModelItemMapper.updateDmsMedicineModelItem(dmsMedicineModelItem);
    }

    /**
     * 批量删除成药模版
     * 
     * @param ids 需要删除的成药模版ID
     * @return 结果
     */
    @Override
    public int deleteDmsMedicineModelItemByIds(Long[] ids)
    {
        return dmsMedicineModelItemMapper.deleteDmsMedicineModelItemByIds(ids);
    }

    /**
     * 删除成药模版信息
     * 
     * @param id 成药模版ID
     * @return 结果
     */
    @Override
    public int deleteDmsMedicineModelItemById(Long id)
    {
        return dmsMedicineModelItemMapper.deleteDmsMedicineModelItemById(id);
    }

    @Override
    public List<DmsMedicineModelItem> getDrugInfoByModelId(Long id) {
        return dmsMedicineModelItemMapper.selectDrugInfoByModelId(id);
    }
}
