package com.yyaccp.hncc.dms.service.impl;

import com.yyaccp.hncc.dms.domain.DmsCaseModel;
import com.yyaccp.hncc.dms.mapper.DmsCaseModelMapper;
import com.yyaccp.hncc.dms.service.IDmsCaseModelService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 病例模版Service业务层处理
 *
 * @author 天天向上
 * @date 2020-08-15
 */
@Service
@RequiredArgsConstructor
public class DmsCaseModelServiceImpl implements IDmsCaseModelService {
    private final DmsCaseModelMapper dmsCaseModelMapper;

    /**
     * 查询病例模版
     *
     * @param id 病例模版ID
     * @return 病例模版
     */
    @Override
    public DmsCaseModel selectDmsCaseModelById(Long id) {
        return dmsCaseModelMapper.selectDmsCaseModelById(id);
    }

    /**
     * 查询病例模版列表
     *
     * @param dmsCaseModel 病例模版
     * @return 病例模版
     */
    @Override
    public List<DmsCaseModel> selectDmsCaseModelList(DmsCaseModel dmsCaseModel) {
        return dmsCaseModelMapper.selectDmsCaseModelList(dmsCaseModel);
    }

    /**
     * 新增病例模版
     *
     * @param dmsCaseModel 病例模版
     * @return 结果
     */
    @Override
    public int insertDmsCaseModel(DmsCaseModel dmsCaseModel) {
        return dmsCaseModelMapper.insertDmsCaseModel(dmsCaseModel);
    }

    /**
     * 修改病例模版
     *
     * @param dmsCaseModel 病例模版
     * @return 结果
     */
    @Override
    public int updateDmsCaseModel(DmsCaseModel dmsCaseModel) {
        return dmsCaseModelMapper.updateDmsCaseModel(dmsCaseModel);
    }

    /**
     * 批量删除病例模版
     *
     * @param ids 需要删除的病例模版ID
     * @return 结果
     */
    @Override
    public int deleteDmsCaseModelByIds(Long[] ids) {
        return dmsCaseModelMapper.deleteDmsCaseModelByIds(ids);
    }

    /**
     * 删除病例模版信息
     *
     * @param id 病例模版ID
     * @return 结果
     */
    @Override
    public int deleteDmsCaseModelById(Long id) {
        return dmsCaseModelMapper.deleteDmsCaseModelById(id);
    }
}
