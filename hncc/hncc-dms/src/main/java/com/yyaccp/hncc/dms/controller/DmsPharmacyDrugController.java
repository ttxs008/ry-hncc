package com.yyaccp.hncc.dms.controller;

import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.yyaccp.hncc.common.util.BeanCopierUtil;
import com.yyaccp.hncc.common.vo.dms.DmsPharmacyDrugVO;
import com.yyaccp.hncc.dms.service.IDmsPharmacyDrugService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.Map;

/**
 * @Author 余归
 * @Date 2020/9/15 0015 15:06
 * @Version 1.0
 */
@Api(tags = "药房药品  (患者,已发药/未发药)")
@RestController
@RequestMapping("/dms/pharmacyDrug")
public class DmsPharmacyDrugController extends BaseController {

    @Resource
    private IDmsPharmacyDrugService dmsPharmacyDrugService;

    /**
     * 查询成药药房药品  (患者,已发药/未发药)
     */
    //@PreAuthorize("@ss.hasPermi('dms:pharmacy_drug:list')")
    @PostMapping("/list")
    @ApiResponses({
            @ApiResponse(code = 204, message = "操作已经执行成功，但是没有返回数据"),
            @ApiResponse(code = 303, message = "重定向"),
            @ApiResponse(code = 400, message = "参数列表错误（缺少，格式不匹配）"),
            @ApiResponse(code = 500, message = "系统内部错误"),
            @ApiResponse(code = 404, message = "资源，服务未找到"),
            @ApiResponse(code = 200, message = "操作成功"),
            @ApiResponse(code = 401, message = "未授权"),
            @ApiResponse(code = 403, message = "访问受限，授权过期")
    })
    @ApiOperation(value = "查询成药药房药品  (患者,已发药/未发药)", notes = "查询成药药房药品  (患者,已发药/未发药)",
            code = 200, produces = "application/json", protocols = "Http",
            response = TableDataInfo.class, httpMethod = "POST")
    public AjaxResult list(@RequestBody Map<String, Object> data) {
        Long registrationId = (Long) data.get("registrationId");
        Integer status = (Integer) data.get("status");
        return AjaxResult.success(BeanCopierUtil.copy(dmsPharmacyDrugService.queryOverTheCounterPharmacyDrugs(registrationId, status.longValue()), DmsPharmacyDrugVO.class));
    }

    /**
     * 查询成药药房药品  (患者,已发药/未发药)
     */
    //@PreAuthorize("@ss.hasPermi('dms:pharmacy_drug:list')")
    @PostMapping("/herbalist")
    @ApiResponses({
            @ApiResponse(code = 204, message = "操作已经执行成功，但是没有返回数据"),
            @ApiResponse(code = 303, message = "重定向"),
            @ApiResponse(code = 400, message = "参数列表错误（缺少，格式不匹配）"),
            @ApiResponse(code = 500, message = "系统内部错误"),
            @ApiResponse(code = 404, message = "资源，服务未找到"),
            @ApiResponse(code = 200, message = "操作成功"),
            @ApiResponse(code = 401, message = "未授权"),
            @ApiResponse(code = 403, message = "访问受限，授权过期")
    })
    @ApiOperation(value = "查询草药药房药品  (患者,已发药/未发药)", notes = "查询成药药房药品  (患者,已发药/未发药)",
            code = 200, produces = "application/json", protocols = "Http",
            response = TableDataInfo.class, httpMethod = "POST")
    public AjaxResult medicinalHerbalist(@RequestBody Map<String, Object> data) {
        Long registrationId = (Long) data.get("registrationId");
        Integer status = (Integer) data.get("status");
        return AjaxResult.success(BeanCopierUtil.copy(dmsPharmacyDrugService.inquireAboutHerbalPharmacyDrugs(registrationId, status.longValue()), DmsPharmacyDrugVO.class));
    }

}
