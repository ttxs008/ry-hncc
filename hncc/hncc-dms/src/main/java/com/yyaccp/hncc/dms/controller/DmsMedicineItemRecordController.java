package com.yyaccp.hncc.dms.controller;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.yyaccp.hncc.common.util.BeanCopierUtil;
import com.yyaccp.hncc.common.vo.dms.DmsMedicineItemRecordVO;
import com.yyaccp.hncc.dms.domain.DmsMedicineItemRecord;
import com.yyaccp.hncc.dms.service.IDmsMedicineItemRecordService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

//com.yyaccp.hncc.common模块的包引入

/**
 * 成药项记录Controller
 *
 * @author 余归
 * @date 2020-09-02
 */
@Api(tags = "成药项记录")
@RestController
@RequestMapping("/dms/medicine_item_record")
public class DmsMedicineItemRecordController extends BaseController {
    @Autowired
    private IDmsMedicineItemRecordService dmsMedicineItemRecordService;

    /**
     * 查询成药项记录列表
     */
    //@PreAuthorize("@ss.hasPermi('dms:medicine_item_record:list')")
    @GetMapping("/list")
    @ApiOperation(value = "查询成药项记录", notes = "查询所有成药项记录",
            code = 200, produces = "application/json", protocols = "Http",
            response = TableDataInfo.class, httpMethod = "GET")
    @ApiResponses({
            @ApiResponse(code = 204, message = "操作已经执行成功，但是没有返回数据"),
            @ApiResponse(code = 303, message = "重定向"),
            @ApiResponse(code = 400, message = "参数列表错误（缺少，格式不匹配）"),
            @ApiResponse(code = 500, message = "系统内部错误"),
            @ApiResponse(code = 404, message = "资源，服务未找到"),
            @ApiResponse(code = 200, message = "操作成功"),
            @ApiResponse(code = 401, message = "未授权"),
            @ApiResponse(code = 403, message = "访问受限，授权过期")
    })
    @ApiParam(name = "DmsMedicineItemRecordVO对象", type = "DmsMedicineItemRecordVO")
    public TableDataInfo list(DmsMedicineItemRecordVO dmsMedicineItemRecordVO) {
        //将Vo转化为实体
        DmsMedicineItemRecord dmsMedicineItemRecord = BeanCopierUtil.copy(dmsMedicineItemRecordVO, DmsMedicineItemRecord.class);
        startPage();
        List<DmsMedicineItemRecord> list = dmsMedicineItemRecordService.selectDmsMedicineItemRecordList(dmsMedicineItemRecord);
        TableDataInfo tableDataInfo = getDataTable(list);
        //替换集合
        tableDataInfo.setRows(BeanCopierUtil.copy(tableDataInfo.getRows(), DmsMedicineItemRecordVO.class));
        return tableDataInfo;
    }

    /**
     * 导出成药项记录列表
     */
    //@PreAuthorize("@ss.hasPermi('dms:medicine_item_record:export')")
    @Log(title = "成药项记录", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    @ApiOperation(value = "导出成药项记录表", notes = "导出所有成药项记录",
            code = 200, produces = "application/json", protocols = "Http",
            response = TableDataInfo.class, httpMethod = "GET")
    @ApiResponses({
            @ApiResponse(code = 204, message = "操作已经执行成功，但是没有返回数据"),
            @ApiResponse(code = 303, message = "重定向"),
            @ApiResponse(code = 400, message = "参数列表错误（缺少，格式不匹配）"),
            @ApiResponse(code = 500, message = "系统内部错误"),
            @ApiResponse(code = 404, message = "资源，服务未找到"),
            @ApiResponse(code = 200, message = "操作成功"),
            @ApiResponse(code = 401, message = "未授权"),
            @ApiResponse(code = 403, message = "访问受限，授权过期")
    })
    @ApiParam(name = "DmsMedicineItemRecordVO对象", type = "DmsMedicineItemRecordVO")
    public AjaxResult export(DmsMedicineItemRecordVO dmsMedicineItemRecordVO) {
        //将VO转化为实体
        DmsMedicineItemRecord dmsMedicineItemRecord = BeanCopierUtil.copy(dmsMedicineItemRecordVO, DmsMedicineItemRecord.class);
        List<DmsMedicineItemRecordVO> list = BeanCopierUtil.copy(dmsMedicineItemRecordService.selectDmsMedicineItemRecordList(dmsMedicineItemRecord), DmsMedicineItemRecordVO.class);
        ExcelUtil<DmsMedicineItemRecordVO> util = new ExcelUtil<DmsMedicineItemRecordVO>(DmsMedicineItemRecordVO.class);
        return util.exportExcel(list, "medicine_item_record");
    }

    /**
     * 获取成药项记录详细信息
     */
    //@PreAuthorize("@ss.hasPermi('dms:medicine_item_record:query')")
    @GetMapping(value = "/{id}")
    @ApiOperation(value = "获取成药项记录详细信息",
            notes = "根据成药项记录id获取科室信息", code = 200,
            produces = "application/json", protocols = "Http", response = AjaxResult.class,
            httpMethod = "GET")
    @ApiResponses({
            @ApiResponse(code = 204, message = "操作已经执行成功，但是没有返回数据"),
            @ApiResponse(code = 303, message = "重定向"),
            @ApiResponse(code = 400, message = "参数列表错误（缺少，格式不匹配）"),
            @ApiResponse(code = 500, message = "系统内部错误"),
            @ApiResponse(code = 404, message = "资源，服务未找到"),
            @ApiResponse(code = 200, message = "操作成功"),
            @ApiResponse(code = 401, message = "未授权"),
            @ApiResponse(code = 403, message = "访问受限，授权过期")
    })
    @ApiParam(name = "dmsMedicineItemRecord.id", type = "Long")
    public AjaxResult getInfo(@PathVariable("id") Long id) {
        return AjaxResult.success(BeanCopierUtil.copy(dmsMedicineItemRecordService.selectDmsMedicineItemRecordById(id), DmsMedicineItemRecordVO.class));
    }

    /**
     * 新增成药项记录
     */
    //@PreAuthorize("@ss.hasPermi('dms:medicine_item_record:add')")
    @Log(title = "新增成药项记录", businessType = BusinessType.INSERT)
    @PostMapping
    @ApiOperation(value = "新增成药项记录信息", notes = "新增成药项记录信息", code = 200,
            produces = "application/json", protocols = "Http", response = AjaxResult.class,
            httpMethod = "POST")
    @ApiResponses({
            @ApiResponse(code = 204, message = "操作已经执行成功，但是没有返回数据"),
            @ApiResponse(code = 303, message = "重定向"),
            @ApiResponse(code = 400, message = "参数列表错误（缺少，格式不匹配）"),
            @ApiResponse(code = 500, message = "系统内部错误"),
            @ApiResponse(code = 404, message = "资源，服务未找到"),
            @ApiResponse(code = 200, message = "操作成功"),
            @ApiResponse(code = 401, message = "未授权"),
            @ApiResponse(code = 403, message = "访问受限，授权过期")
    })
    @ApiParam(name = "DmsMedicineItemRecordVO对象", type = "DmsMedicineItemRecordVO")
    public AjaxResult add(@RequestBody DmsMedicineItemRecordVO dmsMedicineItemRecordVO) {
        return toAjax(dmsMedicineItemRecordService.insertDmsMedicineItemRecord(BeanCopierUtil.copy(dmsMedicineItemRecordVO, DmsMedicineItemRecord.class)));
    }

    /**
     * 修改成药项记录
     */
    //@PreAuthorize("@ss.hasPermi('dms:medicine_item_record:edit')")
    @Log(title = "成药项记录", businessType = BusinessType.UPDATE)
    @PutMapping
    @ApiOperation(value = "修改成药项记录信息", notes = "修改成药项记录信息", code = 200,
            produces = "application/json", protocols = "Http", response = AjaxResult.class,
            httpMethod = "PUT")
    @ApiResponses({
            @ApiResponse(code = 204, message = "操作已经执行成功，但是没有返回数据"),
            @ApiResponse(code = 303, message = "重定向"),
            @ApiResponse(code = 400, message = "参数列表错误（缺少，格式不匹配）"),
            @ApiResponse(code = 500, message = "系统内部错误"),
            @ApiResponse(code = 404, message = "资源，服务未找到"),
            @ApiResponse(code = 200, message = "操作成功"),
            @ApiResponse(code = 401, message = "未授权"),
            @ApiResponse(code = 403, message = "访问受限，授权过期")
    })
    @ApiParam(name = "DmsMedicineItemRecordVO对象", type = "DmsMedicineItemRecordVO")
    public AjaxResult edit(@RequestBody DmsMedicineItemRecordVO dmsMedicineItemRecordVO) {
        return toAjax(dmsMedicineItemRecordService.updateDmsMedicineItemRecord(BeanCopierUtil.copy(dmsMedicineItemRecordVO, DmsMedicineItemRecord.class)));
    }

    /**
     * 删除成药项记录
     */
    //@PreAuthorize("@ss.hasPermi('dms:medicine_item_record:remove')")
    @Log(title = "成药项记录", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    @ApiOperation(value = "删除成药项记录信息", notes = "删除成药项记录信息", code = 200,
            produces = "application/json", protocols = "Http", response = AjaxResult.class,
            httpMethod = "DELETE")
    @ApiResponses({
            @ApiResponse(code = 204, message = "操作已经执行成功，但是没有返回数据"),
            @ApiResponse(code = 303, message = "重定向"),
            @ApiResponse(code = 400, message = "参数列表错误（缺少，格式不匹配）"),
            @ApiResponse(code = 500, message = "系统内部错误"),
            @ApiResponse(code = 404, message = "资源，服务未找到"),
            @ApiResponse(code = 200, message = "操作成功"),
            @ApiResponse(code = 401, message = "未授权"),
            @ApiResponse(code = 403, message = "访问受限，授权过期")
    })
    @ApiParam(name = "dmsMedicineItemRecord.id", type = "Long[]")
    public AjaxResult remove(@PathVariable Long[] ids) {
        return toAjax(dmsMedicineItemRecordService.deleteDmsMedicineItemRecordByIds(ids));
    }
}
