package com.yyaccp.hncc.dms.mapper;

import com.yyaccp.hncc.dms.domain.DmsDise;
import com.yyaccp.hncc.dms.domain.DmsNonDrug;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 诊断类型(疾病)管理Mapper接口
 * 
 * @author ruoyi
 * @date 2020-08-12
 */
public interface DmsDiseMapper 
{
    /**
     * 查询诊断类型(疾病)管理
     * 
     * @param id 诊断类型(疾病)管理ID
     * @return 诊断类型(疾病)管理
     */
    public DmsDise selectDmsDiseById(Long id);

    /**
     * 查询诊断类型(疾病)管理列表
     * 
     * @param dmsDise 诊断类型(疾病)管理
     * @return 诊断类型(疾病)管理集合
     */
    public List<DmsDise> selectDmsDiseList(DmsDise dmsDise);

    /**
     * 新增诊断类型(疾病)管理
     * 
     * @param dmsDise 诊断类型(疾病)管理
     * @return 结果
     */
    public int insertDmsDise(DmsDise dmsDise);

    /**
     * 修改诊断类型(疾病)管理
     * 
     * @param dmsDise 诊断类型(疾病)管理
     * @return 结果
     */
    public int updateDmsDise(DmsDise dmsDise);

    /**
     * 删除诊断类型(疾病)管理
     * 
     * @param id 诊断类型(疾病)管理ID
     * @return 结果
     */
    public int deleteDmsDiseById(Long id);

    /**
     * 批量删除诊断类型(疾病)管理
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteDmsDiseByIds(Long[] ids);

    /**
     * 批量删除诊断类型(疾病)管理
     *
     * @param catIds 需要删除的数据ID
     * @return 结果
     */
    public int deleteDmsDiseByCatIds(Long[] catIds);

    /**
     * 根据ids查询诊断类型管理集合
     * @param name 需要查询的数据ID
     * @param ids 需要查询的数据ID

     * @return 结果
     */
    public List<DmsDise> selectDmsDiseListByIdsOrName( @Param("ids") String[] ids, @Param("name")String name);

}
