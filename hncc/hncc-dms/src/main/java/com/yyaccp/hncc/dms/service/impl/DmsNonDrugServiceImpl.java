package com.yyaccp.hncc.dms.service.impl;


import com.yyaccp.hncc.common.DmsConstants;

import com.yyaccp.hncc.dms.domain.DmsNonDrug;
import com.yyaccp.hncc.dms.mapper.DmsNonDrugMapper;
import com.yyaccp.hncc.dms.service.IDmsNonDrugService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 非药品收费项目Service业务层处理
 *
 * @author yugui
 * @date 2020-08-11
 */
@Service
public class DmsNonDrugServiceImpl implements IDmsNonDrugService {

    //定义不同项目类型的自动增长列常量集合

    public final static Map<Integer, Integer> CODE_MAP = new HashMap<>();

    static {
        CODE_MAP.put(DmsConstants.TYPE_EXAMINE, DmsConstants.CODE_EXAMINE);
        CODE_MAP.put(DmsConstants.TYPE_INSPECT, DmsConstants.CODE_INSPECT);
        CODE_MAP.put(DmsConstants.TYPE_HANDLE, DmsConstants.CODE_HANDLE);
    }

    public final static Map<Integer, Integer> codeMap = new HashMap<>();

    static {
        codeMap.put(1, 12000001);
        codeMap.put(2, 21000001);
        codeMap.put(3, 24000001);
    }

    @Resource
    private DmsNonDrugMapper dmsNonDrugMapper;

    /**
     * 根据项目类型 查询项目编号
     */
    @Override
    public Integer selectItemNumber(Integer recordType) {
        return dmsNonDrugMapper.selectItemNumber(recordType);
    }

    /**
     * 查询非药品收费项目
     *
     * @param id 非药品收费项目ID
     * @return 非药品收费项目
     */
    @Override
    public DmsNonDrug selectDmsNonDrugById(Long id) {
        return dmsNonDrugMapper.selectDmsNonDrugById(id);
    }

    /**
     * 查询非药品收费项目列表
     *
     * @param dmsNonDrug 非药品收费项目
     * @return 非药品收费项目
     */
    @Override
    public List<DmsNonDrug> selectDmsNonDrugList(DmsNonDrug dmsNonDrug) {
        return dmsNonDrugMapper.selectDmsNonDrugList(dmsNonDrug);
    }

    /**
     * 新增非药品收费项目
     *
     * @param dmsNonDrug 非药品收费项目
     * @return 结果
     */
    @Override
    public int insertDmsNonDrug(DmsNonDrug dmsNonDrug) {
        //从对象拿到项目类型ID
        int recordType = dmsNonDrug.getRecordType().intValue();
        //根据项目类型定义获取最后一个对应的编码
        Integer code = selectItemNumber(recordType);
        //如果不为空,则自增
        //否则,使用常量数据
        if (code != null) {
            code++;
        } else {
            code = codeMap.get(recordType);
        }
        dmsNonDrug.setCode(String.valueOf(code));
        //创建时间
        dmsNonDrug.setCreateDate(new Date());
        //状态
        dmsNonDrug.setStatus(1L);
        code = codeMap.get(recordType);


        dmsNonDrug.setCode(String.valueOf(code));
        dmsNonDrug.setCreateDate(new Date());
        dmsNonDrug.setStatus(1L);

        return dmsNonDrugMapper.insertDmsNonDrug(dmsNonDrug);
    }

    /**
     * 修改非药品收费项目
     *
     * @param dmsNonDrug 非药品收费项目
     * @return 结果
     */
    @Override
    public int updateDmsNonDrug(DmsNonDrug dmsNonDrug) {
        return dmsNonDrugMapper.updateDmsNonDrug(dmsNonDrug);
    }

    /**
     * 批量删除非药品收费项目
     *
     * @param ids 需要删除的非药品收费项目ID
     * @return 结果
     */
    @Override
    public int deleteDmsNonDrugByIds(Long[] ids) {
        return dmsNonDrugMapper.deleteDmsNonDrugByIds(ids);
    }

    /**
     * 删除非药品收费项目信息
     *
     * @param id 非药品收费项目ID
     * @return 结果
     */
    @Override
    public int deleteDmsNonDrugById(Long id) {
        return dmsNonDrugMapper.deleteDmsNonDrugById(id);
    }

    /**
     * 根据ids查询非药品收费项目集合
     *
     * @param dmsNonDrugIds 需要查询的数据ID
     * @param recordType    需要查询的数据ID
     * @return 结果
     */
    @Override
    public List<DmsNonDrug> selectDmsNonDrugListByIdsOrTypeOrName(String dmsNonDrugIds, Integer recordType, String name) {

        return dmsNonDrugMapper.selectDmsNonDrugListByIdsOrTypeOrName(dmsNonDrugIds == null ? null : dmsNonDrugIds.split(","), recordType, name);
    }

}
