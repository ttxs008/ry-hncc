package com.yyaccp.hncc.dms.service;

import java.util.List;
import com.yyaccp.hncc.dms.domain.DmsDrugModel;

/**
 * 药品模版Service接口
 *
 * @author 周某
 * @date 2020-08-18
 */
public interface IDmsDrugModelService 
{
    /**
     * 查询药品模版
     * 
     * @param id 药品模版ID
     * @return 药品模版
     */
    public DmsDrugModel selectDmsDrugModelById(Long id);

    /**
     * 查询药品模版列表
     * 
     * @param dmsDrugModel 药品模版
     * @return 药品模版集合
     */
    public List<DmsDrugModel> selectDmsDrugModelList(DmsDrugModel dmsDrugModel);

    /**
     * 新增药品模版
     * 
     * @param dmsDrugModel 药品模版
     * @return 结果
     */
    public int insertDmsDrugModel(DmsDrugModel dmsDrugModel);

    /**
     * 修改药品模版
     * 
     * @param dmsDrugModel 药品模版
     * @return 结果
     */
    public int updateDmsDrugModel(DmsDrugModel dmsDrugModel);

    /**
     * 批量删除药品模版
     * 
     * @param ids 需要删除的药品模版ID
     * @return 结果
     */
    public int deleteDmsDrugModelByIds(Long[] ids);

    /**
     * 删除药品模版信息
     * 
     * @param id 药品模版ID
     * @return 结果
     */
    public int deleteDmsDrugModelById(Long id);
}
