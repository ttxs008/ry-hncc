package com.yyaccp.hncc.dms.controller;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.yyaccp.hncc.common.util.BeanCopierUtil;
import com.yyaccp.hncc.common.vo.dms.DmsNonDrugModelVO;
import com.yyaccp.hncc.dms.domain.DmsNonDrugModel;
import com.yyaccp.hncc.dms.service.IDmsNonDrugModelService;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

//com.yyaccp.hncc.common模块的包引入

/**
 * 非药品模版Controller
 *
 * @author yugui
 * @date 2020-08-17
 */
@Api(tags = "非药品模版")
@RestController
@RequestMapping("/dms/non_drug_model")
public class DmsNonDrugModelController extends BaseController {
    @Autowired
    private IDmsNonDrugModelService dmsNonDrugModelService;

    /**
     * 查询非药品模版列表
     */
    @PreAuthorize("@ss.hasPermi('dms:non_drug_model:list')")
    @GetMapping("/list")
    @ApiOperation(value = "查询非药品模版", notes = "查询所有非药品模版",
            code = 200, produces = "application/json", protocols = "Http",
            response = TableDataInfo.class, httpMethod = "GET")
    @ApiResponses({
            @ApiResponse(code = 204, message = "操作已经执行成功，但是没有返回数据"),
            @ApiResponse(code = 303, message = "重定向"),
            @ApiResponse(code = 400, message = "参数列表错误（缺少，格式不匹配）"),
            @ApiResponse(code = 500, message = "系统内部错误"),
            @ApiResponse(code = 404, message = "资源，服务未找到"),
            @ApiResponse(code = 200, message = "操作成功"),
            @ApiResponse(code = 401, message = "未授权"),
            @ApiResponse(code = 403, message = "访问受限，授权过期")
    })
    @ApiParam(name = "DmsNonDrugModelVO对象", type = "DmsNonDrugModelVO")
    public TableDataInfo list(DmsNonDrugModelVO dmsNonDrugModelVO) {
        //将Vo转化为实体
        DmsNonDrugModel dmsNonDrugModel = BeanCopierUtil.copy(dmsNonDrugModelVO, DmsNonDrugModel.class);
        startPage();
        List<DmsNonDrugModel> list = dmsNonDrugModelService.selectDmsNonDrugModelList(dmsNonDrugModel);
        TableDataInfo tableDataInfo = getDataTable(list);
        //替换集合
        tableDataInfo.setRows(BeanCopierUtil.copy(tableDataInfo.getRows(), DmsNonDrugModelVO.class));
        return tableDataInfo;
    }

    /**
     * 导出非药品模版列表
     */
    @PreAuthorize("@ss.hasPermi('dms:non_drug_model:export')")
    @Log(title = "非药品模版", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    @ApiOperation(value = "导出非药品模版表", notes = "导出所有非药品模版",
            code = 200, produces = "application/json", protocols = "Http",
            response = TableDataInfo.class, httpMethod = "GET")
    @ApiResponses({
            @ApiResponse(code = 204, message = "操作已经执行成功，但是没有返回数据"),
            @ApiResponse(code = 303, message = "重定向"),
            @ApiResponse(code = 400, message = "参数列表错误（缺少，格式不匹配）"),
            @ApiResponse(code = 500, message = "系统内部错误"),
            @ApiResponse(code = 404, message = "资源，服务未找到"),
            @ApiResponse(code = 200, message = "操作成功"),
            @ApiResponse(code = 401, message = "未授权"),
            @ApiResponse(code = 403, message = "访问受限，授权过期")
    })
    @ApiParam(name = "DmsNonDrugModelVO对象", type = "DmsNonDrugModelVO")
    public AjaxResult export(DmsNonDrugModelVO dmsNonDrugModelVO) {
        //将VO转化为实体
        DmsNonDrugModel dmsNonDrugModel = BeanCopierUtil.copy(dmsNonDrugModelVO, DmsNonDrugModel.class);
        List<DmsNonDrugModelVO> list = BeanCopierUtil.copy(dmsNonDrugModelService.selectDmsNonDrugModelList(dmsNonDrugModel), DmsNonDrugModelVO.class);
        ExcelUtil<DmsNonDrugModelVO> util = new ExcelUtil<DmsNonDrugModelVO>(DmsNonDrugModelVO.class);
        return util.exportExcel(list, "non_drug_model");
    }

    /**
     * 获取非药品模版详细信息
     */
    @PreAuthorize("@ss.hasPermi('dms:non_drug_model:query')")
    @GetMapping(value = "/{id}")
    @ApiOperation(value = "获取非药品模版详细信息",
            notes = "根据非药品模版id获取科室信息", code = 200,
            produces = "application/json", protocols = "Http", response = AjaxResult.class,
            httpMethod = "GET")
    @ApiResponses({
            @ApiResponse(code = 204, message = "操作已经执行成功，但是没有返回数据"),
            @ApiResponse(code = 303, message = "重定向"),
            @ApiResponse(code = 400, message = "参数列表错误（缺少，格式不匹配）"),
            @ApiResponse(code = 500, message = "系统内部错误"),
            @ApiResponse(code = 404, message = "资源，服务未找到"),
            @ApiResponse(code = 200, message = "操作成功"),
            @ApiResponse(code = 401, message = "未授权"),
            @ApiResponse(code = 403, message = "访问受限，授权过期")
    })
    @ApiParam(name = "dmsNonDrugModel.id", type = "Long")
    public AjaxResult getInfo(@PathVariable("id") Long id) {
        return AjaxResult.success(BeanCopierUtil.copy(dmsNonDrugModelService.selectDmsNonDrugModelById(id), DmsNonDrugModelVO.class));
    }

    /**
     * 新增非药品模版
     */
    @PreAuthorize("@ss.hasPermi('dms:non_drug_model:add')")
    @Log(title = "新增非药品模版", businessType = BusinessType.INSERT)
    @PostMapping
    @ApiOperation(value = "新增非药品模版信息", notes = "新增非药品模版信息", code = 200,
            produces = "application/json", protocols = "Http", response = AjaxResult.class,
            httpMethod = "POST")
    @ApiResponses({
            @ApiResponse(code = 204, message = "操作已经执行成功，但是没有返回数据"),
            @ApiResponse(code = 303, message = "重定向"),
            @ApiResponse(code = 400, message = "参数列表错误（缺少，格式不匹配）"),
            @ApiResponse(code = 500, message = "系统内部错误"),
            @ApiResponse(code = 404, message = "资源，服务未找到"),
            @ApiResponse(code = 200, message = "操作成功"),
            @ApiResponse(code = 401, message = "未授权"),
            @ApiResponse(code = 403, message = "访问受限，授权过期")
    })
    @ApiParam(name = "DmsNonDrugModelVO对象", type = "DmsNonDrugModelVO")
    public AjaxResult add(@RequestBody DmsNonDrugModelVO dmsNonDrugModelVO) {
        return toAjax(dmsNonDrugModelService.insertDmsNonDrugModel(BeanCopierUtil.copy(dmsNonDrugModelVO, DmsNonDrugModel.class)));
    }

    /**
     * 修改非药品模版
     */
    @PreAuthorize("@ss.hasPermi('dms:non_drug_model:edit')")
    @Log(title = "非药品模版", businessType = BusinessType.UPDATE)
    @PutMapping
    @ApiOperation(value = "修改非药品模版信息", notes = "修改非药品模版信息", code = 200,
            produces = "application/json", protocols = "Http", response = AjaxResult.class,
            httpMethod = "PUT")
    @ApiResponses({
            @ApiResponse(code = 204, message = "操作已经执行成功，但是没有返回数据"),
            @ApiResponse(code = 303, message = "重定向"),
            @ApiResponse(code = 400, message = "参数列表错误（缺少，格式不匹配）"),
            @ApiResponse(code = 500, message = "系统内部错误"),
            @ApiResponse(code = 404, message = "资源，服务未找到"),
            @ApiResponse(code = 200, message = "操作成功"),
            @ApiResponse(code = 401, message = "未授权"),
            @ApiResponse(code = 403, message = "访问受限，授权过期")
    })
    @ApiParam(name = "DmsNonDrugModelVO对象", type = "DmsNonDrugModelVO")
    public AjaxResult edit(@RequestBody DmsNonDrugModelVO dmsNonDrugModelVO) {
        return toAjax(dmsNonDrugModelService.updateDmsNonDrugModel(BeanCopierUtil.copy(dmsNonDrugModelVO, DmsNonDrugModel.class)));
    }

    /**
     * 删除非药品模版
     */
    @PreAuthorize("@ss.hasPermi('dms:non_drug_model:remove')")
    @Log(title = "非药品模版", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    @ApiOperation(value = "删除非药品模版信息", notes = "删除非药品模版信息", code = 200,
            produces = "application/json", protocols = "Http", response = AjaxResult.class,
            httpMethod = "DELETE")
    @ApiResponses({
            @ApiResponse(code = 204, message = "操作已经执行成功，但是没有返回数据"),
            @ApiResponse(code = 303, message = "重定向"),
            @ApiResponse(code = 400, message = "参数列表错误（缺少，格式不匹配）"),
            @ApiResponse(code = 500, message = "系统内部错误"),
            @ApiResponse(code = 404, message = "资源，服务未找到"),
            @ApiResponse(code = 200, message = "操作成功"),
            @ApiResponse(code = 401, message = "未授权"),
            @ApiResponse(code = 403, message = "访问受限，授权过期")
    })
    @ApiParam(name = "dmsNonDrugModel.id", type = "Long[]")
    public AjaxResult remove(@PathVariable Long[] ids) {
        return toAjax(dmsNonDrugModelService.deleteDmsNonDrugModelByIds(ids));
    }
}
