package com.yyaccp.hncc.dms.service;

import com.yyaccp.hncc.dms.domain.DmsHerbalPrescriptionRecord;

import java.util.List;

/**
 * 草处方Service接口
 * 
 * @author ruoyi
 * @date 2020-09-07
 */
public interface IDmsHerbalPrescriptionRecordService 
{
    /**
     * 查询草处方
     * 
     * @param id 草处方ID
     * @return 草处方
     */
    public DmsHerbalPrescriptionRecord selectDmsHerbalPrescriptionRecordById(Long id);

    /**
     * 查询草处方列表
     * 
     * @param dmsHerbalPrescriptionRecord 草处方
     * @return 草处方集合
     */
    public List<DmsHerbalPrescriptionRecord> selectDmsHerbalPrescriptionRecordList(DmsHerbalPrescriptionRecord dmsHerbalPrescriptionRecord);

    /**
     * 新增草处方
     * 
     * @param dmsHerbalPrescriptionRecord 草处方
     * @return 结果
     */
    public int insertDmsHerbalPrescriptionRecord(DmsHerbalPrescriptionRecord dmsHerbalPrescriptionRecord);

    /**
     * 修改草处方
     * 
     * @param dmsHerbalPrescriptionRecord 草处方
     * @return 结果
     */
    public int updateDmsHerbalPrescriptionRecord(DmsHerbalPrescriptionRecord dmsHerbalPrescriptionRecord);

    /**
     * 批量删除草处方
     * 
     * @param ids 需要删除的草处方ID
     * @return 结果
     */
    public int deleteDmsHerbalPrescriptionRecordByIds(Long[] ids);

    /**
     * 删除草处方信息
     *
     * @param id 草处方ID
     * @return 结果
     */
    public int deleteDmsHerbalPrescriptionRecordById(Long id);

    /**
     * 批量修改草处方（发药）
     *
     * @param ids 需要删除的草处方ID
     * @return 结果
     */
    public int batchRevisionOfDraftPrescriptions(List<Long> ids, Integer status);
}
