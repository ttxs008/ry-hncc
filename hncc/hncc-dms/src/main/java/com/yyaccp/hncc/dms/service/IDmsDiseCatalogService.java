package com.yyaccp.hncc.dms.service;



import com.yyaccp.hncc.dms.domain.DmsDiseCatalog;

import java.util.List;

/**
 * 诊断类型（疾病）目录Service接口
 * 
 * @author ruoyi
 * @date 2020-08-12
 */
public interface IDmsDiseCatalogService 
{
    /**
     * 查询诊断类型（疾病）目录
     * 
     * @param id 诊断类型（疾病）目录ID
     * @return 诊断类型（疾病）目录
     */
    public DmsDiseCatalog selectDmsDiseCatalogById(Long id);

    /**
     * 查询诊断类型（疾病）目录列表
     * 
     * @param dmsDiseCatalog 诊断类型（疾病）目录
     * @return 诊断类型（疾病）目录集合
     */
    public List<DmsDiseCatalog> selectDmsDiseCatalogList(DmsDiseCatalog dmsDiseCatalog);

    /**
     * 新增诊断类型（疾病）目录
     * 
     * @param dmsDiseCatalog 诊断类型（疾病）目录
     * @return 结果
     */
    public int insertDmsDiseCatalog(DmsDiseCatalog dmsDiseCatalog);

    /**
     * 修改诊断类型（疾病）目录
     * 
     * @param dmsDiseCatalog 诊断类型（疾病）目录
     * @return 结果
     */
    public int updateDmsDiseCatalog(DmsDiseCatalog dmsDiseCatalog);

    /**
     * 批量删除诊断类型（疾病）目录
     * 
     * @param ids 需要删除的诊断类型（疾病）目录ID
     * @return 结果
     */
    public int deleteDmsDiseCatalogByIds(Long[] ids);

    /**
     * 删除诊断类型（疾病）目录信息
     * 
     * @param id 诊断类型（疾病）目录ID
     * @return 结果
     */
    public int deleteDmsDiseCatalogById(Long id);
}
