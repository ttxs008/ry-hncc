package com.yyaccp.hncc.dms.domain;

import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * 成药项记录对象 dms_medicine_item_record
 *
 * @author 余归
 * @date 2020-09-02
 */
public class DmsMedicineItemRecord extends BaseEntity {
    private static final long serialVersionUID = 1L;

    /**
     * $column.columnComment
     */
    private Long id;

    /**
     * 药品id
     */
    @Excel(name = "药品id")
    private Long drugId;

    /**
     * 状态
     */
    @Excel(name = "状态")
    private Long status;

    /**
     * 处方id
     */
    @Excel(name = "处方id")
    private Long prescriptionId;

    /**
     * 用法
     */
    @Excel(name = "用法")
    private Long medicineUsage;

    /**
     * 频率
     */
    @Excel(name = "频率")
    private Long frequency;

    /**
     * 天数
     */
    @Excel(name = "天数")
    private Long days;

    /**
     * 数量
     */
    @Excel(name = "数量")
    private Long num;

    /**
     * 医嘱
     */
    @Excel(name = "医嘱")
    private String medicalAdvice;

    /**
     * 退药数量
     */
    @Excel(name = "退药数量")
    private Long refundNum;

    /**
     * 用量
     */
    @Excel(name = "用量")
    private Long usageNum;

    /**
     * 用法
     */
    @Excel(name = "用法")
    private Long usageMeans;

    /**
     * 单位
     */
    @Excel(name = "单位")
    private Long usageNumUnit;

    /**
     * 当前量
     */
    @Excel(name = "当前量")
    private Long currentNum;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getDrugId() {
        return drugId;
    }

    public void setDrugId(Long drugId) {
        this.drugId = drugId;
    }

    public Long getStatus() {
        return status;
    }

    public void setStatus(Long status) {
        this.status = status;
    }

    public Long getPrescriptionId() {
        return prescriptionId;
    }

    public void setPrescriptionId(Long prescriptionId) {
        this.prescriptionId = prescriptionId;
    }

    public Long getMedicineUsage() {
        return medicineUsage;
    }

    public void setMedicineUsage(Long medicineUsage) {
        this.medicineUsage = medicineUsage;
    }

    public Long getFrequency() {
        return frequency;
    }

    public void setFrequency(Long frequency) {
        this.frequency = frequency;
    }

    public Long getDays() {
        return days;
    }

    public void setDays(Long days) {
        this.days = days;
    }

    public Long getNum() {
        return num;
    }

    public void setNum(Long num) {
        this.num = num;
    }

    public String getMedicalAdvice() {
        return medicalAdvice;
    }

    public void setMedicalAdvice(String medicalAdvice) {
        this.medicalAdvice = medicalAdvice;
    }

    public Long getRefundNum() {
        return refundNum;
    }

    public void setRefundNum(Long refundNum) {
        this.refundNum = refundNum;
    }

    public Long getUsageNum() {
        return usageNum;
    }

    public void setUsageNum(Long usageNum) {
        this.usageNum = usageNum;
    }

    public Long getUsageMeans() {
        return usageMeans;
    }

    public void setUsageMeans(Long usageMeans) {
        this.usageMeans = usageMeans;
    }

    public Long getUsageNumUnit() {
        return usageNumUnit;
    }

    public void setUsageNumUnit(Long usageNumUnit) {
        this.usageNumUnit = usageNumUnit;
    }

    public Long getCurrentNum() {
        return currentNum;
    }

    public void setCurrentNum(Long currentNum) {
        this.currentNum = currentNum;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
                .append("id", getId())
                .append("drugId", getDrugId())
                .append("status", getStatus())
                .append("prescriptionId", getPrescriptionId())
                .append("medicineUsage", getMedicineUsage())
                .append("frequency", getFrequency())
                .append("days", getDays())
                .append("num", getNum())
                .append("medicalAdvice", getMedicalAdvice())
                .append("refundNum", getRefundNum())
                .append("usageNum", getUsageNum())
                .append("usageMeans", getUsageMeans())
                .append("usageNumUnit", getUsageNumUnit())
                .append("currentNum", getCurrentNum())
                .toString();
    }
}
