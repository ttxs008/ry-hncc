package com.yyaccp.hncc.dms.controller;

import java.util.List;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.yyaccp.hncc.dms.domain.DmsCaseHistory;
import com.yyaccp.hncc.dms.service.IDmsCaseHistoryService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import io.swagger.annotations.*;
//com.yyaccp.hncc.common模块的包引入
import com.yyaccp.hncc.common.vo.dms.DmsCaseHistoryVO;
import com.yyaccp.hncc.common.util.BeanCopierUtil;

import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 病历Controller
 *
 * @author ruoyi
 * @date 2020-08-28
 */
@Api(tags = "病历")
@RestController
@RequestMapping("/dms/case_history")
public class DmsCaseHistoryController extends BaseController {
    @Autowired
    private IDmsCaseHistoryService dmsCaseHistoryService;

/**
 * 查询病历列表
 *
 */
@PreAuthorize("@ss.hasPermi('dms:case_history:list')")
@GetMapping("/list")
@ApiOperation(value = "查询病历" , notes = "查询所有病历" ,
        code = 200, produces = "application/json" , protocols = "Http" ,
        response = TableDataInfo.class, httpMethod = "GET")
@ApiResponses({
        @ApiResponse(code = 204, message = "操作已经执行成功，但是没有返回数据"),
        @ApiResponse(code = 303, message = "重定向"),
        @ApiResponse(code = 400, message = "参数列表错误（缺少，格式不匹配）"),
        @ApiResponse(code = 500, message = "系统内部错误"),
        @ApiResponse(code = 404, message = "资源，服务未找到"),
        @ApiResponse(code = 200, message = "操作成功"),
        @ApiResponse(code = 401, message = "未授权"),
        @ApiResponse(code = 403, message = "访问受限，授权过期")
})
@ApiParam(name = "DmsCaseHistoryVO对象" , type = "DmsCaseHistoryVO")
        public TableDataInfo list(DmsCaseHistoryVO dmsCaseHistoryVO) {
        //将Vo转化为实体
        DmsCaseHistory dmsCaseHistory=BeanCopierUtil.copy(dmsCaseHistoryVO,DmsCaseHistory. class);
        startPage();
        List<DmsCaseHistory> list = dmsCaseHistoryService.selectDmsCaseHistoryList(dmsCaseHistory);
        TableDataInfo tableDataInfo = getDataTable(list);
        //替换集合
        tableDataInfo.setRows(BeanCopierUtil.copy(tableDataInfo.getRows(), DmsCaseHistoryVO.class));
        return tableDataInfo;
    }
    
    /**
     * 导出病历列表
     */
    @PreAuthorize("@ss.hasPermi('dms:case_history:export')")
    @Log(title = "病历" , businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    @ApiOperation(value = "导出病历表" , notes = "导出所有病历" ,
            code = 200, produces = "application/json" , protocols = "Http" ,
            response = TableDataInfo.class, httpMethod = "GET")
    @ApiResponses({
            @ApiResponse(code = 204, message = "操作已经执行成功，但是没有返回数据"),
            @ApiResponse(code = 303, message = "重定向"),
            @ApiResponse(code = 400, message = "参数列表错误（缺少，格式不匹配）"),
            @ApiResponse(code = 500, message = "系统内部错误"),
            @ApiResponse(code = 404, message = "资源，服务未找到"),
            @ApiResponse(code = 200, message = "操作成功"),
            @ApiResponse(code = 401, message = "未授权"),
            @ApiResponse(code = 403, message = "访问受限，授权过期")
    })
    @ApiParam(name = "DmsCaseHistoryVO对象" , type = "DmsCaseHistoryVO")
    public AjaxResult export(DmsCaseHistoryVO dmsCaseHistoryVO) {
        //将VO转化为实体
        DmsCaseHistory dmsCaseHistory=BeanCopierUtil.copy(dmsCaseHistoryVO,DmsCaseHistory. class);
        List<DmsCaseHistoryVO> list = BeanCopierUtil.copy(dmsCaseHistoryService.selectDmsCaseHistoryList(dmsCaseHistory),DmsCaseHistoryVO.class);
        ExcelUtil<DmsCaseHistoryVO> util = new ExcelUtil<DmsCaseHistoryVO>(DmsCaseHistoryVO.class);
        return util.exportExcel(list, "case_history");
    }

    /**
     * 获取病历详细信息
     */
    @PreAuthorize("@ss.hasPermi('dms:case_history:query')")
    @GetMapping(value = "/{id}")
    @ApiOperation(value = "获取病历详细信息" ,
            notes = "根据病历id获取科室信息" , code = 200,
            produces = "application/json" , protocols = "Http" , response = AjaxResult.class,
            httpMethod = "GET")
    @ApiResponses({
            @ApiResponse(code = 204, message = "操作已经执行成功，但是没有返回数据"),
            @ApiResponse(code = 303, message = "重定向"),
            @ApiResponse(code = 400, message = "参数列表错误（缺少，格式不匹配）"),
            @ApiResponse(code = 500, message = "系统内部错误"),
            @ApiResponse(code = 404, message = "资源，服务未找到"),
            @ApiResponse(code = 200, message = "操作成功"),
            @ApiResponse(code = 401, message = "未授权"),
            @ApiResponse(code = 403, message = "访问受限，授权过期")
    })
    @ApiParam(name = "dmsCaseHistory.id" , type = "Long")
    public AjaxResult getInfo(@PathVariable("id") Long id) {
        return AjaxResult.success(BeanCopierUtil.copy(dmsCaseHistoryService.selectDmsCaseHistoryById(id),DmsCaseHistoryVO.class));
    }

    /**
     * 新增病历
     */
    @PreAuthorize("@ss.hasPermi('dms:case_history:add')")
    @Log(title = "新增病历" , businessType = BusinessType.INSERT)
    @PostMapping
    @ApiOperation(value = "新增病历信息" , notes = "新增病历信息" , code = 200,
            produces = "application/json" , protocols = "Http" , response = AjaxResult.class,
            httpMethod = "POST")
    @ApiResponses({
            @ApiResponse(code = 204, message = "操作已经执行成功，但是没有返回数据"),
            @ApiResponse(code = 303, message = "重定向"),
            @ApiResponse(code = 400, message = "参数列表错误（缺少，格式不匹配）"),
            @ApiResponse(code = 500, message = "系统内部错误"),
            @ApiResponse(code = 404, message = "资源，服务未找到"),
            @ApiResponse(code = 200, message = "操作成功"),
            @ApiResponse(code = 401, message = "未授权"),
            @ApiResponse(code = 403, message = "访问受限，授权过期")
    })
    @ApiParam(name = "DmsCaseHistoryVO对象" , type = "DmsCaseHistoryVO")
    public AjaxResult add(@RequestBody DmsCaseHistoryVO dmsCaseHistoryVO) {
        return toAjax(dmsCaseHistoryService.insertDmsCaseHistory(BeanCopierUtil.copy(dmsCaseHistoryVO,DmsCaseHistory. class)));
    }

    /**
     * 修改病历
     */
    @PreAuthorize("@ss.hasPermi('dms:case_history:edit')")
    @Log(title = "病历" , businessType = BusinessType.UPDATE)
    @PutMapping
    @ApiOperation(value = "修改病历信息" , notes = "修改病历信息" , code = 200,
            produces = "application/json" , protocols = "Http" , response = AjaxResult.class,
            httpMethod = "PUT")
    @ApiResponses({
            @ApiResponse(code = 204, message = "操作已经执行成功，但是没有返回数据"),
            @ApiResponse(code = 303, message = "重定向"),
            @ApiResponse(code = 400, message = "参数列表错误（缺少，格式不匹配）"),
            @ApiResponse(code = 500, message = "系统内部错误"),
            @ApiResponse(code = 404, message = "资源，服务未找到"),
            @ApiResponse(code = 200, message = "操作成功"),
            @ApiResponse(code = 401, message = "未授权"),
            @ApiResponse(code = 403, message = "访问受限，授权过期")
    })
    @ApiParam(name = "DmsCaseHistoryVO对象" , type = "DmsCaseHistoryVO")
    public AjaxResult edit(@RequestBody DmsCaseHistoryVO dmsCaseHistoryVO) {
        return toAjax(dmsCaseHistoryService.updateDmsCaseHistory(BeanCopierUtil.copy(dmsCaseHistoryVO,DmsCaseHistory. class)));
    }

    /**
     * 删除病历
     */
    @PreAuthorize("@ss.hasPermi('dms:case_history:remove')")
    @Log(title = "病历" , businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    @ApiOperation(value = "删除病历信息" , notes = "删除病历信息" , code = 200,
            produces = "application/json" , protocols = "Http" , response = AjaxResult.class,
            httpMethod = "DELETE")
    @ApiResponses({
            @ApiResponse(code = 204, message = "操作已经执行成功，但是没有返回数据"),
            @ApiResponse(code = 303, message = "重定向"),
            @ApiResponse(code = 400, message = "参数列表错误（缺少，格式不匹配）"),
            @ApiResponse(code = 500, message = "系统内部错误"),
            @ApiResponse(code = 404, message = "资源，服务未找到"),
            @ApiResponse(code = 200, message = "操作成功"),
            @ApiResponse(code = 401, message = "未授权"),
            @ApiResponse(code = 403, message = "访问受限，授权过期")
    })
    @ApiParam(name = "dmsCaseHistory.id" , type = "Long[]")
    public AjaxResult remove(@PathVariable Long[] ids) {
        return toAjax(dmsCaseHistoryService.deleteDmsCaseHistoryByIds(ids));
    }

    /**
     * 获取病历详细信息
     */
    @PreAuthorize("@ss.hasPermi('dms:case_history:patQuery')")
    @GetMapping(value = "/patHis/{patientId}")
    @ApiOperation(value = "通过挂号号获取病历详细信息" ,
            notes = "根据挂号号获取科室信息" , code = 200,
            produces = "application/json" , protocols = "Http" , response = AjaxResult.class,
            httpMethod = "GET")
    @ApiResponses({
            @ApiResponse(code = 204, message = "操作已经执行成功，但是没有返回数据"),
            @ApiResponse(code = 303, message = "重定向"),
            @ApiResponse(code = 400, message = "参数列表错误（缺少，格式不匹配）"),
            @ApiResponse(code = 500, message = "系统内部错误"),
            @ApiResponse(code = 404, message = "资源，服务未找到"),
            @ApiResponse(code = 200, message = "操作成功"),
            @ApiResponse(code = 401, message = "未授权"),
            @ApiResponse(code = 403, message = "访问受限，授权过期")
    })
    @ApiParam(name = "dmsCaseHistory.patient_id" , type = "Long")
    public TableDataInfo getRegHis(@PathVariable("patientId") Long patientId) {
            startPage();
            List<DmsCaseHistory> list = dmsCaseHistoryService.selectPatIdHis(patientId);
            TableDataInfo tableDataInfo = getDataTable(list);
            //替换集合
            tableDataInfo.setRows(BeanCopierUtil.copy(tableDataInfo.getRows(), DmsCaseHistoryVO.class));
            return tableDataInfo;
        }
}
