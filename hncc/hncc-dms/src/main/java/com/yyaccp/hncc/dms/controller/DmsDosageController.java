package com.yyaccp.hncc.dms.controller;

import java.util.List;

import com.yyaccp.hncc.common.util.BeanCopierUtil;
import com.yyaccp.hncc.common.vo.dms.DmsDosageVO;
import io.swagger.annotations.*;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.yyaccp.hncc.dms.domain.DmsDosage;
import com.yyaccp.hncc.dms.service.IDmsDosageService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 药品剂型Controller
 *
 * @author 周某
 * @date 2020-08-12
 */
@Api(tags = "药品剂型管理")
@RestController
@RequestMapping("/dms/dosage")
public class DmsDosageController extends BaseController
{
    @Autowired
    private IDmsDosageService dmsDosageService;

    /**
     * 查询药品剂型列表
     */
    @PreAuthorize("@ss.hasPermi('dms:dosage:list')")
    @GetMapping("/list")
    @ApiResponses({
            @ApiResponse(code = 204,message = "操作已经执行成功，但是没有返回数据"),
            @ApiResponse(code = 303,message = "重定向"),
            @ApiResponse(code = 400,message = "参数列表错误（缺少，格式不匹配）"),
            @ApiResponse(code = 500,message = "系统内部错误"),
            @ApiResponse(code = 404,message = "资源，服务未找到"),
            @ApiResponse(code = 200,message = "操作成功"),
            @ApiResponse(code = 401,message = "未授权"),
            @ApiResponse(code = 403,message = "访问受限，授权过期")
    })
    @ApiOperation(value = "查询药品剂型列表", notes = "查询药品剂型列表",
            code = 200, produces = "application/json", protocols = "Http",
            response = TableDataInfo.class, httpMethod = "GET")
    @ApiParam(name = "剂型DmsDosageVO对象", type = "DmsDosageVO")
    public TableDataInfo list(DmsDosageVO dmsDosagevo)
    {
        //将VO转化为实体
        DmsDosage dosage= BeanCopierUtil.copy(dmsDosagevo,DmsDosage.class);
        startPage();
        List<DmsDosage> list = dmsDosageService.selectDmsDosageList(dosage);
        TableDataInfo tableDataInfo=getDataTable(list);
        //替换集合
        tableDataInfo.setRows(BeanCopierUtil.copy(tableDataInfo.getRows(),DmsDosageVO.class));
        return tableDataInfo;

    }

    /**
     * 导出药品剂型列表
     */
    @PreAuthorize("@ss.hasPermi('dms:dosage:export')")
    @Log(title = "药品剂型", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    @ApiResponses({
            @ApiResponse(code = 204,message = "操作已经执行成功，但是没有返回数据"),
            @ApiResponse(code = 303,message = "重定向"),
            @ApiResponse(code = 400,message = "参数列表错误（缺少，格式不匹配）"),
            @ApiResponse(code = 500,message = "系统内部错误"),
            @ApiResponse(code = 404,message = "资源，服务未找到"),
            @ApiResponse(code = 200,message = "操作成功"),
            @ApiResponse(code = 401,message = "未授权"),
            @ApiResponse(code = 403,message = "访问受限，授权过期")
    })
    @ApiOperation(value = "导出药品剂型列表", notes = "导出药品剂型列表",
            code = 200, produces = "application/json", protocols = "Http",
            response = TableDataInfo.class, httpMethod = "GET")
    @ApiParam(name = "剂型DmsDosageVO对象", type = "DmsDosageVO")
    public AjaxResult export(DmsDosageVO dmsDosageVO)
    {
        //将VO转化为实体
        DmsDosage dosage= BeanCopierUtil.copy(dmsDosageVO,DmsDosage.class);
        List<DmsDosageVO> list = BeanCopierUtil.copy(dmsDosageService.selectDmsDosageList(dosage),DmsDosageVO.class);
        ExcelUtil<DmsDosageVO> util = new ExcelUtil<DmsDosageVO>(DmsDosageVO.class);
        return util.exportExcel(list, "drug");
    }

    /**
     * 获取药品剂型详细信息
     */
    @PreAuthorize("@ss.hasPermi('dms:dosage:query')")
    @GetMapping(value = "/{id}")
    @ApiResponses({
            @ApiResponse(code = 204,message = "操作已经执行成功，但是没有返回数据"),
            @ApiResponse(code = 303,message = "重定向"),
            @ApiResponse(code = 400,message = "参数列表错误（缺少，格式不匹配）"),
            @ApiResponse(code = 500,message = "系统内部错误"),
            @ApiResponse(code = 404,message = "资源，服务未找到"),
            @ApiResponse(code = 200,message = "操作成功"),
            @ApiResponse(code = 401,message = "未授权"),
            @ApiResponse(code = 403,message = "访问受限，授权过期")
    })
    @ApiOperation(value = "获取药品剂型详细信息", notes = "获取药品剂型详细信息",
            code = 200, produces = "application/json", protocols = "Http",
            response = AjaxResult.class, httpMethod = "GET")
    @ApiParam(name = "剂型ID", type = "Long")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(BeanCopierUtil.copy(dmsDosageService.selectDmsDosageById(id),DmsDosageVO.class));
    }

    /**
     * 新增药品剂型
     */
    @PreAuthorize("@ss.hasPermi('dms:dosage:add')")
    @Log(title = "药品剂型", businessType = BusinessType.INSERT)
    @PostMapping
    @ApiResponses({
            @ApiResponse(code = 204,message = "操作已经执行成功，但是没有返回数据"),
            @ApiResponse(code = 303,message = "重定向"),
            @ApiResponse(code = 400,message = "参数列表错误（缺少，格式不匹配）"),
            @ApiResponse(code = 500,message = "系统内部错误"),
            @ApiResponse(code = 404,message = "资源，服务未找到"),
            @ApiResponse(code = 200,message = "操作成功"),
            @ApiResponse(code = 401,message = "未授权"),
            @ApiResponse(code = 403,message = "访问受限，授权过期")
    })
    @ApiOperation(value = "新增药品剂型", notes = "新增药品剂型",
            code = 200, produces = "application/json", protocols = "Http",
            response = AjaxResult.class, httpMethod = "POST")
    @ApiParam(name = "剂型DmsDosageVO对象", type = "DmsDosageVO")
    public AjaxResult add(@RequestBody DmsDosageVO dmsDosageVO)
    {
        //将VO转化为实体
        DmsDosage dosage=BeanCopierUtil.copy(dmsDosageVO,DmsDosage.class);
        return toAjax(dmsDosageService.insertDmsDosage(dosage));
    }

    /**
     * 修改药品剂型
     */
    @PreAuthorize("@ss.hasPermi('dms:dosage:edit')")
    @Log(title = "药品剂型", businessType = BusinessType.UPDATE)
    @PutMapping
    @ApiResponses({
            @ApiResponse(code = 204,message = "操作已经执行成功，但是没有返回数据"),
            @ApiResponse(code = 303,message = "重定向"),
            @ApiResponse(code = 400,message = "参数列表错误（缺少，格式不匹配）"),
            @ApiResponse(code = 500,message = "系统内部错误"),
            @ApiResponse(code = 404,message = "资源，服务未找到"),
            @ApiResponse(code = 200,message = "操作成功"),
            @ApiResponse(code = 401,message = "未授权"),
            @ApiResponse(code = 403,message = "访问受限，授权过期")
    })
    @ApiOperation(value = "修改药品剂型", notes = "修改药品剂型",
            code = 200, produces = "application/json", protocols = "Http",
            response = AjaxResult.class, httpMethod = "PUT")
    @ApiParam(name = "剂型DmsDosageVO对象", type = "DmsDosageVO")
    public AjaxResult edit(@RequestBody DmsDosageVO dmsDosageVO)
    {
        //将VO转化为实体
        DmsDosage dosage=BeanCopierUtil.copy(dmsDosageVO,DmsDosage.class);
        return toAjax(dmsDosageService.updateDmsDosage(dosage));
    }

    /**
     * 删除药品剂型
     */
    @PreAuthorize("@ss.hasPermi('dms:dosage:remove')")
    @Log(title = "药品剂型", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    @ApiResponses({
            @ApiResponse(code = 204,message = "操作已经执行成功，但是没有返回数据"),
            @ApiResponse(code = 303,message = "重定向"),
            @ApiResponse(code = 400,message = "参数列表错误（缺少，格式不匹配）"),
            @ApiResponse(code = 500,message = "系统内部错误"),
            @ApiResponse(code = 404,message = "资源，服务未找到"),
            @ApiResponse(code = 200,message = "操作成功"),
            @ApiResponse(code = 401,message = "未授权"),
            @ApiResponse(code = 403,message = "访问受限，授权过期")
    })
    @ApiOperation(value = "删除药品剂型", notes = "删除药品剂型",
            code = 200, produces = "application/json", protocols = "Http",
            response = AjaxResult.class, httpMethod = "DELETE")
    @ApiParam(name = "剂型ID", type = "Long[]")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(dmsDosageService.deleteDmsDosageByIds(ids));
    }
}
