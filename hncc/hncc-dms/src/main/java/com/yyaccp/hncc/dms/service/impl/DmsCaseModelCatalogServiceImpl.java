package com.yyaccp.hncc.dms.service.impl;

import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.SecurityUtils;
import com.yyaccp.hncc.dms.domain.DmsCaseModel;
import com.yyaccp.hncc.dms.domain.DmsCaseModelCatalog;
import com.yyaccp.hncc.dms.mapper.DmsCaseModelCatalogMapper;
import com.yyaccp.hncc.dms.mapper.DmsCaseModelMapper;
import com.yyaccp.hncc.dms.service.IDmsCaseModelCatalogService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static com.yyaccp.hncc.common.DmsConstants.*;

/**
 * 病例模版目录
 * Service业务层处理
 *
 * @author 天天向上
 * @date 2020-08-15
 */
@Service
@Slf4j
@RequiredArgsConstructor
public class DmsCaseModelCatalogServiceImpl implements IDmsCaseModelCatalogService {

    private final DmsCaseModelCatalogMapper dmsCaseModelCatalogMapper;

    private final DmsCaseModelMapper dmsCaseModelMapper;

    /**
     * 查询病例模版目录
     *
     * @param id 病例模版目录ID
     * @return 病例模版目录
     */
    @Override
    public DmsCaseModelCatalog selectDmsCaseModelCatalogById(Long id) {
        return dmsCaseModelCatalogMapper.selectDmsCaseModelCatalogById(id);
    }

    /**
     * 查询病例模版目录列表
     *
     * @param dmsCaseModelCatalog 病例模版目录
     * @return 病例模版目录
     */
    @Override
    public List<DmsCaseModelCatalog> selectDmsCaseModelCatalogList(DmsCaseModelCatalog dmsCaseModelCatalog) {
        return dmsCaseModelCatalogMapper.selectDmsCaseModelCatalogList(dmsCaseModelCatalog);
    }

    /**
     * 新增病例模版目录
     * 如果类型是模板，添加一个模板
     * 同时添加一个关联的目录，并设置目录的model_id字段为插入模板的id
     * 否则只添加目录
     *
     * @param dmsCaseModelCatalog 病例模版目录
     * @return 结果
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int insertDmsCaseModelCatalog(DmsCaseModelCatalog dmsCaseModelCatalog) {
        if (dmsCaseModelCatalog.getType().equals(CASE_MODEL_CATALOG_TYPE_MODEL)) {
            DmsCaseModel caseModel = new DmsCaseModel();
            caseModel.setStatus(CASE_MODEL_STATUS_ENABLED);
            caseModel.setName(dmsCaseModelCatalog.getName());
            this.dmsCaseModelMapper.insertDmsCaseModel(caseModel);
            dmsCaseModelCatalog.setModelId(caseModel.getId());
        }
        dmsCaseModelCatalog.setOwnId(SecurityUtils.getLoginUser().getUser().getUserId());
        dmsCaseModelCatalog.setStatus(CASE_MODEL_CATALOG_STATUS_ENABLED);
        dmsCaseModelCatalog.setCreateTime(DateUtils.getNowDate());
        // 设置level等于父级的level+1
        if (dmsCaseModelCatalog.getParentId() != 0) {
            DmsCaseModelCatalog parentModel = this.dmsCaseModelCatalogMapper.selectDmsCaseModelCatalogById(dmsCaseModelCatalog.getParentId());
            dmsCaseModelCatalog.setLevel(parentModel.getLevel() + 1);
        } else {
            dmsCaseModelCatalog.setLevel(1);
        }
        return dmsCaseModelCatalogMapper.insertDmsCaseModelCatalog(dmsCaseModelCatalog);
    }

    /**
     * 修改病例模版目录
     *
     * @param dmsCaseModelCatalog 病例模版目录
     * @return 结果
     */
    @Override
    public int updateDmsCaseModelCatalog(DmsCaseModelCatalog dmsCaseModelCatalog) {
        return dmsCaseModelCatalogMapper.updateDmsCaseModelCatalog(dmsCaseModelCatalog);
    }

    /**
     * 批量删除病例模版目录
     *
     * @param ids 需要删除的病例模版目录ID
     * @return 结果
     */
    @Override
    public int deleteDmsCaseModelCatalogByIds(Long[] ids) {
        int result = 0;
        for (Long id : ids) {
            result += this.deleteDmsCaseModelCatalogById(id);
        }
        return result;
    }

    /**
     * 删除病例模版目录信息
     *
     * @param id 病例模版目录ID
     * @return 结果
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int deleteDmsCaseModelCatalogById(Long id) {
        // 先删除病例模板详情
        DmsCaseModelCatalog caseModelCatalog = dmsCaseModelCatalogMapper.selectDmsCaseModelCatalogById(id);
        if (caseModelCatalog.getModelId() != null) {
            dmsCaseModelMapper.deleteDmsCaseModelById(caseModelCatalog.getModelId());
        }
        return dmsCaseModelCatalogMapper.deleteDmsCaseModelCatalogById(id);
    }
}
