package com.yyaccp.hncc.dms.service.impl;


import com.yyaccp.hncc.dms.domain.DmsDiseCatalog;
import com.yyaccp.hncc.dms.mapper.DmsDiseCatalogMapper;
import com.yyaccp.hncc.dms.mapper.DmsDiseMapper;
import com.yyaccp.hncc.dms.service.IDmsDiseCatalogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.sql.SQLException;
import java.util.List;

/**
 * 诊断类型（疾病）目录Service业务层处理
 * 
 * @author ruoyi
 * @date 2020-08-12
 */
@Service
public class DmsDiseCatalogServiceImpl implements IDmsDiseCatalogService
{
    @Autowired
    private DmsDiseCatalogMapper dmsDiseCatalogMapper;
    @Autowired
    private DmsDiseMapper dmsDiseMapper;

    /**
     * 查询诊断类型（疾病）目录
     * 
     * @param id 诊断类型（疾病）目录ID
     * @return 诊断类型（疾病）目录
     */
    @Override
    public DmsDiseCatalog selectDmsDiseCatalogById(Long id)
    {
        return dmsDiseCatalogMapper.selectDmsDiseCatalogById(id);
    }

    /**
     * 查询诊断类型（疾病）目录列表
     * 
     * @param dmsDiseCatalog 诊断类型（疾病）目录
     * @return 诊断类型（疾病）目录
     */
    @Override
    public List<DmsDiseCatalog> selectDmsDiseCatalogList(DmsDiseCatalog dmsDiseCatalog)
    {
        return dmsDiseCatalogMapper.selectDmsDiseCatalogList(dmsDiseCatalog);
    }

    /**
     * 新增诊断类型（疾病）目录
     * 
     * @param dmsDiseCatalog 诊断类型（疾病）目录
     * @return 结果
     */
    @Override
    public int insertDmsDiseCatalog(DmsDiseCatalog dmsDiseCatalog)
    {
        return dmsDiseCatalogMapper.insertDmsDiseCatalog(dmsDiseCatalog);
    }

    /**
     * 修改诊断类型（疾病）目录
     * 
     * @param dmsDiseCatalog 诊断类型（疾病）目录
     * @return 结果
     */
    @Override
    public int updateDmsDiseCatalog(DmsDiseCatalog dmsDiseCatalog)
    {
        return dmsDiseCatalogMapper.updateDmsDiseCatalog(dmsDiseCatalog);
    }

    /**
     * 批量删除诊断类型（疾病）目录
     * 发生sql 异常时 回滚
     * @param ids 需要删除的诊断类型（疾病）目录ID
     * @return 结果
     */
    @Override
    @Transactional(rollbackFor = SQLException.class)
    public int deleteDmsDiseCatalogByIds(Long[] ids)
    {
        // 删除目录下的疾病
        dmsDiseMapper.deleteDmsDiseByCatIds(ids);
        return dmsDiseCatalogMapper.deleteDmsDiseCatalogByIds(ids);
    }

    /**
     * 删除诊断类型（疾病）目录信息
     * 
     * @param id 诊断类型（疾病）目录ID
     * @return 结果
     */
    @Override
    public int deleteDmsDiseCatalogById(Long id)
    {
        return dmsDiseCatalogMapper.deleteDmsDiseCatalogById(id);
    }
}
