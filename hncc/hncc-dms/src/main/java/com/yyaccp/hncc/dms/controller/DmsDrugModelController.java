package com.yyaccp.hncc.dms.controller;

import java.util.List;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.yyaccp.hncc.dms.domain.DmsDrugModel;
import com.yyaccp.hncc.dms.service.IDmsDrugModelService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import io.swagger.annotations.*;
//com.yyaccp.hncc.common模块的包引入
import com.yyaccp.hncc.common.vo.dms.DmsDrugModelVO;
import com.yyaccp.hncc.common.util.BeanCopierUtil;

import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 药品模版Controller
 *
 * @author 周某
 * @date 2020-08-18
 */
@Api(tags = "药品模版")
@RestController
@RequestMapping("/dms/drug_model")
public class DmsDrugModelController extends BaseController
{
    @Autowired
    private IDmsDrugModelService dmsDrugModelService;

/**
 * 查询药品模版列表
 *
 */
@PreAuthorize("@ss.hasPermi('dms:drug_model:list')")
@GetMapping("/list")
@ApiOperation(value = "查询药品模版",notes = "查询所有药品模版",
        code = 200, produces = "application/json",protocols = "Http",
        response = TableDataInfo.class, httpMethod = "GET")
@ApiResponses({
        @ApiResponse(code = 204,message = "操作已经执行成功，但是没有返回数据"),
        @ApiResponse(code = 303,message = "重定向"),
        @ApiResponse(code = 400,message = "参数列表错误（缺少，格式不匹配）"),
        @ApiResponse(code = 500,message = "系统内部错误"),
        @ApiResponse(code = 404,message = "资源，服务未找到"),
        @ApiResponse(code = 200,message = "操作成功"),
        @ApiResponse(code = 401,message = "未授权"),
        @ApiResponse(code = 403,message = "访问受限，授权过期")
})
@ApiParam(name = "DmsDrugModelVO对象", type = "DmsDrugModelVO")
        public TableDataInfo list(DmsDrugModelVO dmsDrugModelVO)
    {
        //将Vo转化为实体
        DmsDrugModel dmsDrugModel=BeanCopierUtil.copy(dmsDrugModelVO,DmsDrugModel.class);
        startPage();
        List<DmsDrugModel> list = dmsDrugModelService.selectDmsDrugModelList(dmsDrugModel);
        TableDataInfo tableDataInfo=getDataTable(list);
        //替换集合
        tableDataInfo.setRows(BeanCopierUtil.copy(tableDataInfo.getRows(), DmsDrugModelVO.class));
        return tableDataInfo;
    }
    
    /**
     * 导出药品模版列表
     */
    @PreAuthorize("@ss.hasPermi('dms:drug_model:export')")
    @Log(title = "药品模版", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    @ApiOperation(value = "导出药品模版表",notes = "导出所有药品模版",
            code = 200, produces = "application/json",protocols = "Http",
            response = TableDataInfo.class, httpMethod = "GET")
    @ApiResponses({
            @ApiResponse(code = 204,message = "操作已经执行成功，但是没有返回数据"),
            @ApiResponse(code = 303,message = "重定向"),
            @ApiResponse(code = 400,message = "参数列表错误（缺少，格式不匹配）"),
            @ApiResponse(code = 500,message = "系统内部错误"),
            @ApiResponse(code = 404,message = "资源，服务未找到"),
            @ApiResponse(code = 200,message = "操作成功"),
            @ApiResponse(code = 401,message = "未授权"),
            @ApiResponse(code = 403,message = "访问受限，授权过期")
    })
    @ApiParam(name = "DmsDrugModelVO对象", type = "DmsDrugModelVO")
    public AjaxResult export(DmsDrugModelVO dmsDrugModelVO)
    {
        //将VO转化为实体
        DmsDrugModel dmsDrugModel=BeanCopierUtil.copy(dmsDrugModelVO,DmsDrugModel.class);
        List<DmsDrugModelVO> list =BeanCopierUtil.copy(dmsDrugModelService.selectDmsDrugModelList(dmsDrugModel),DmsDrugModelVO.class);
        ExcelUtil<DmsDrugModelVO> util = new ExcelUtil<DmsDrugModelVO>(DmsDrugModelVO.class);
        return util.exportExcel(list, "drug_model");
    }

    /**
     * 获取药品模版详细信息
     */
    @PreAuthorize("@ss.hasPermi('dms:drug_model:query')")
    @GetMapping(value = "/{id}")
    @ApiOperation(value = "获取药品模版详细信息",
            notes = "根据药品模版id获取科室信息", code = 200,
            produces = "application/json",protocols = "Http",response = AjaxResult.class,
            httpMethod = "GET")
    @ApiResponses({
            @ApiResponse(code = 204,message = "操作已经执行成功，但是没有返回数据"),
            @ApiResponse(code = 303,message = "重定向"),
            @ApiResponse(code = 400,message = "参数列表错误（缺少，格式不匹配）"),
            @ApiResponse(code = 500,message = "系统内部错误"),
            @ApiResponse(code = 404,message = "资源，服务未找到"),
            @ApiResponse(code = 200,message = "操作成功"),
            @ApiResponse(code = 401,message = "未授权"),
            @ApiResponse(code = 403,message = "访问受限，授权过期")
    })
    @ApiParam(name = "dmsDrugModel.id", type = "Long")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(BeanCopierUtil.copy(dmsDrugModelService.selectDmsDrugModelById(id),DmsDrugModelVO.class));
    }

    /**
     * 新增药品模版
     */
    @PreAuthorize("@ss.hasPermi('dms:drug_model:add')")
    @Log(title = "新增药品模版", businessType = BusinessType.INSERT)
    @PostMapping
    @ApiOperation(value = "新增药品模版信息",notes = "新增药品模版信息",code = 200,
            produces = "application/json",protocols = "Http",response = AjaxResult.class,
            httpMethod = "POST")
    @ApiResponses({
            @ApiResponse(code = 204,message = "操作已经执行成功，但是没有返回数据"),
            @ApiResponse(code = 303,message = "重定向"),
            @ApiResponse(code = 400,message = "参数列表错误（缺少，格式不匹配）"),
            @ApiResponse(code = 500,message = "系统内部错误"),
            @ApiResponse(code = 404,message = "资源，服务未找到"),
            @ApiResponse(code = 200,message = "操作成功"),
            @ApiResponse(code = 401,message = "未授权"),
            @ApiResponse(code = 403,message = "访问受限，授权过期")
    })
    @ApiParam(name = "DmsDrugModelVO对象", type = "DmsDrugModelVO")
    public AjaxResult add(@RequestBody DmsDrugModelVO dmsDrugModelVO)
    {
        DmsDrugModel drugModel=BeanCopierUtil.copy(dmsDrugModelVO,DmsDrugModel.class);
        dmsDrugModelService.insertDmsDrugModel(drugModel);
        return new AjaxResult(200,"success",drugModel.getId());
    }

    /**
     * 修改药品模版
     */
    @PreAuthorize("@ss.hasPermi('dms:drug_model:edit')")
    @Log(title = "药品模版", businessType = BusinessType.UPDATE)
    @PutMapping
    @ApiOperation(value = "修改药品模版信息",notes = "修改药品模版信息",code = 200,
            produces = "application/json",protocols = "Http",response = AjaxResult.class,
            httpMethod = "PUT")
    @ApiResponses({
            @ApiResponse(code = 204,message = "操作已经执行成功，但是没有返回数据"),
            @ApiResponse(code = 303,message = "重定向"),
            @ApiResponse(code = 400,message = "参数列表错误（缺少，格式不匹配）"),
            @ApiResponse(code = 500,message = "系统内部错误"),
            @ApiResponse(code = 404,message = "资源，服务未找到"),
            @ApiResponse(code = 200,message = "操作成功"),
            @ApiResponse(code = 401,message = "未授权"),
            @ApiResponse(code = 403,message = "访问受限，授权过期")
    })
    @ApiParam(name = "DmsDrugModelVO对象", type = "DmsDrugModelVO")
    public AjaxResult edit(@RequestBody DmsDrugModelVO dmsDrugModelVO)
    {
        return toAjax(dmsDrugModelService.updateDmsDrugModel(BeanCopierUtil.copy(dmsDrugModelVO,DmsDrugModel.class)));
    }

    /**
     * 删除药品模版
     */
    @PreAuthorize("@ss.hasPermi('dms:drug_model:remove')")
    @Log(title = "药品模版", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    @ApiOperation(value = "删除药品模版信息",notes = "删除药品模版信息",code = 200,
            produces = "application/json",protocols = "Http",response = AjaxResult.class,
            httpMethod = "DELETE")
    @ApiResponses({
            @ApiResponse(code = 204,message = "操作已经执行成功，但是没有返回数据"),
            @ApiResponse(code = 303,message = "重定向"),
            @ApiResponse(code = 400,message = "参数列表错误（缺少，格式不匹配）"),
            @ApiResponse(code = 500,message = "系统内部错误"),
            @ApiResponse(code = 404,message = "资源，服务未找到"),
            @ApiResponse(code = 200,message = "操作成功"),
            @ApiResponse(code = 401,message = "未授权"),
            @ApiResponse(code = 403,message = "访问受限，授权过期")
    })
    @ApiParam(name = "dmsDrugModel.id", type = "Long[]")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(dmsDrugModelService.deleteDmsDrugModelByIds(ids));
    }
}
