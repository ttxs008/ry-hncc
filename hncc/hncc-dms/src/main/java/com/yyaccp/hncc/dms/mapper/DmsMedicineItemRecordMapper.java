package com.yyaccp.hncc.dms.mapper;

import com.yyaccp.hncc.dms.domain.DmsMedicineItemRecord;

import java.util.List;

/**
 * 成药项记录Mapper接口
 *
 * @author 余归
 * @date 2020-09-02
 */
public interface DmsMedicineItemRecordMapper {
    /**
     * 查询成药项记录
     *
     * @param id 成药项记录ID
     * @return 成药项记录
     */
    public DmsMedicineItemRecord selectDmsMedicineItemRecordById(Long id);

    /**
     * 查询成药项记录列表
     *
     * @param dmsMedicineItemRecord 成药项记录
     * @return 成药项记录集合
     */
    public List<DmsMedicineItemRecord> selectDmsMedicineItemRecordList(DmsMedicineItemRecord dmsMedicineItemRecord);

    /**
     * 新增成药项记录
     *
     * @param dmsMedicineItemRecord 成药项记录
     * @return 结果
     */
    public int insertDmsMedicineItemRecord(DmsMedicineItemRecord dmsMedicineItemRecord);

    /**
     * 修改成药项记录
     *
     * @param dmsMedicineItemRecord 成药项记录
     * @return 结果
     */
    public int updateDmsMedicineItemRecord(DmsMedicineItemRecord dmsMedicineItemRecord);

    /**
     * 删除成药项记录
     *
     * @param id 成药项记录ID
     * @return 结果
     */
    public int deleteDmsMedicineItemRecordById(Long id);

    /**
     * 批量删除成药项记录
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteDmsMedicineItemRecordByIds(Long[] ids);
}
