package com.yyaccp.hncc.dms.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.TreeEntity;

/**
 * 病例模版目录对象 dms_case_model_catalog
 * 
 * @author ruoyi
 * @date 2020-08-15
 */
public class DmsCaseModelCatalog extends TreeEntity<DmsCaseModelCatalog>
{
    private static final long serialVersionUID = 1L;

    /** id */
    private Long id;

    /** 层级 */
    private Integer level;

    /** 类型  */
    @Excel(name = "类型 ")
    private Integer type;

    /** 状态 */
    @Excel(name = "状态")
    private Integer status;

    /** 模板id */
    private Long modelId;

    /** 范围 */
    @Excel(name = "范围")
    private Long scope;

    /** 所属人Id */
    private Long ownId;

    /** 名称 */
    @Excel(name = "名称")
    private String name;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setLevel(Integer level) 
    {
        this.level = level;
    }

    public Integer getLevel() 
    {
        return level;
    }
    public void setType(Integer type) 
    {
        this.type = type;
    }

    public Integer getType() 
    {
        return type;
    }
    public void setStatus(Integer status) 
    {
        this.status = status;
    }

    public Integer getStatus() 
    {
        return status;
    }
    public void setModelId(Long modelId) 
    {
        this.modelId = modelId;
    }

    public Long getModelId() 
    {
        return modelId;
    }
    public void setScope(Long scope) 
    {
        this.scope = scope;
    }

    public Long getScope() 
    {
        return scope;
    }
    public void setOwnId(Long ownId) 
    {
        this.ownId = ownId;
    }

    public Long getOwnId() 
    {
        return ownId;
    }
    public void setName(String name) 
    {
        this.name = name;
    }

    public String getName() 
    {
        return name;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("parentId", getParentId())
            .append("level", getLevel())
            .append("type", getType())
            .append("status", getStatus())
            .append("modelId", getModelId())
            .append("scope", getScope())
            .append("ownId", getOwnId())
            .append("createTime", getCreateTime())
            .append("name", getName())
            .toString();
    }
}
