package com.yyaccp.hncc.dms.service.impl;

import java.util.List;

import com.yyaccp.hncc.dms.domain.DmsDrug;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.yyaccp.hncc.dms.mapper.DmsHerbalModelItemMapper;
import com.yyaccp.hncc.dms.domain.DmsHerbalModelItem;
import com.yyaccp.hncc.dms.service.IDmsHerbalModelItemService;

/**
 * 草药模版项Service业务层处理
 *
 * @author 周某
 * @date 2020-08-17
 */
@Service
public class DmsHerbalModelItemServiceImpl implements IDmsHerbalModelItemService 
{
    @Autowired
    private DmsHerbalModelItemMapper dmsHerbalModelItemMapper;

    /**
     * 查询草药模版项
     * 
     * @param id 草药模版项ID
     * @return 草药模版项
     */
    @Override
    public DmsHerbalModelItem selectDmsHerbalModelItemById(Long id)
    {
        return dmsHerbalModelItemMapper.selectDmsHerbalModelItemById(id);
    }

    /**
     * 查询草药模版项列表
     * 
     * @param dmsHerbalModelItem 草药模版项
     * @return 草药模版项
     */
    @Override
    public List<DmsHerbalModelItem> selectDmsHerbalModelItemList(DmsHerbalModelItem dmsHerbalModelItem)
    {
        return dmsHerbalModelItemMapper.selectDmsHerbalModelItemList(dmsHerbalModelItem);
    }

    /**
     * 新增草药模版项
     * 
     * @param dmsHerbalModelItem 草药模版项
     * @return 结果
     */
    @Override
    public int insertDmsHerbalModelItem(DmsHerbalModelItem dmsHerbalModelItem)
    {
        return dmsHerbalModelItemMapper.insertDmsHerbalModelItem(dmsHerbalModelItem);
    }

    /**
     * 修改草药模版项
     * 
     * @param dmsHerbalModelItem 草药模版项
     * @return 结果
     */
    @Override
    public int updateDmsHerbalModelItem(DmsHerbalModelItem dmsHerbalModelItem)
    {
        return dmsHerbalModelItemMapper.updateDmsHerbalModelItem(dmsHerbalModelItem);
    }

    /**
     * 批量删除草药模版项
     * 
     * @param ids 需要删除的草药模版项ID
     * @return 结果
     */
    @Override
    public int deleteDmsHerbalModelItemByIds(Long[] ids)
    {
        return dmsHerbalModelItemMapper.deleteDmsHerbalModelItemByIds(ids);
    }

    /**
     * 删除草药模版项信息
     * 
     * @param id 草药模版项ID
     * @return 结果
     */
    @Override
    public int deleteDmsHerbalModelItemById(Long id)
    {
        return dmsHerbalModelItemMapper.deleteDmsHerbalModelItemById(id);
    }

    @Override
    public List<DmsHerbalModelItem> getDrugInfoByModelId(Long id) {
        return dmsHerbalModelItemMapper.selectDrugInfoByModelId(id);
    }
}
