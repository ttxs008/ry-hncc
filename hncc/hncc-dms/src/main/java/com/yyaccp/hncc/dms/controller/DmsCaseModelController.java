package com.yyaccp.hncc.dms.controller;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.yyaccp.hncc.common.util.BeanCopierUtil;
import com.yyaccp.hncc.common.vo.dms.DmsCaseModelVO;
import com.yyaccp.hncc.dms.domain.DmsCaseModel;
import com.yyaccp.hncc.dms.service.IDmsCaseModelService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 病例模版Controller
 *
 * @author 天天向上
 * @date 2020-08-15
 */
@RestController
@RequestMapping("/dms/dms_case_model")
public class DmsCaseModelController extends BaseController {
    @Autowired
    private IDmsCaseModelService dmsCaseModelService;

    /**
     * 查询病例模版列表
     */
    @PreAuthorize("@ss.hasPermi('dms:dms_case_model:list')")
    @GetMapping("/list")
    public TableDataInfo list(DmsCaseModelVO dmsCaseModel) {
        startPage();
        List<DmsCaseModel> list = dmsCaseModelService.selectDmsCaseModelList(BeanCopierUtil.copy(dmsCaseModel, DmsCaseModel.class));
        return getDataTable(BeanCopierUtil.copy(list, DmsCaseModelVO.class));
    }

    /**
     * 导出病例模版列表
     */
    @PreAuthorize("@ss.hasPermi('dms:dms_case_model:export')")
    @Log(title = "病例模版", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(DmsCaseModelVO dmsCaseModel) {
        List<DmsCaseModel> list = dmsCaseModelService.selectDmsCaseModelList(BeanCopierUtil.copy(dmsCaseModel, DmsCaseModel.class));
        ExcelUtil<DmsCaseModel> util = new ExcelUtil<DmsCaseModel>(DmsCaseModel.class);
        return util.exportExcel(list, "dms_case_model");
    }

    /**
     * 获取病例模版详细信息
     */
    @PreAuthorize("@ss.hasPermi('dms:dms_case_model:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id) {
        return AjaxResult.success(BeanCopierUtil.copy(dmsCaseModelService.selectDmsCaseModelById(id), DmsCaseModelVO.class));
    }

    /**
     * 新增病例模版
     */
    @PreAuthorize("@ss.hasPermi('dms:dms_case_model:add')")
    @Log(title = "病例模版", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody DmsCaseModelVO dmsCaseModel) {
        return toAjax(dmsCaseModelService.insertDmsCaseModel(BeanCopierUtil.copy(dmsCaseModel, DmsCaseModel.class)));
    }

    /**
     * 修改病例模版
     */
    @PreAuthorize("@ss.hasPermi('dms:dms_case_model:edit')")
    @Log(title = "病例模版", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody DmsCaseModelVO dmsCaseModel) {
        return toAjax(dmsCaseModelService.updateDmsCaseModel(BeanCopierUtil.copy(dmsCaseModel, DmsCaseModel.class)));
    }

    /**
     * 删除病例模版
     */
    @PreAuthorize("@ss.hasPermi('dms:dms_case_model:remove')")
    @Log(title = "病例模版", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids) {
        return toAjax(dmsCaseModelService.deleteDmsCaseModelByIds(ids));
    }
}
