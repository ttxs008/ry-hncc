package com.yyaccp.hncc.dms.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.yyaccp.hncc.dms.mapper.DmsDosageMapper;
import com.yyaccp.hncc.dms.domain.DmsDosage;
import com.yyaccp.hncc.dms.service.IDmsDosageService;

/**
 * 药品剂型Service业务层处理
 *
 * @author 周某
 * @date 2020-08-12
 */
@Service
public class DmsDosageServiceImpl implements IDmsDosageService 
{
    @Autowired
    private DmsDosageMapper dmsDosageMapper;

    /**
     * 查询药品剂型
     * 
     * @param id 药品剂型ID
     * @return 药品剂型
     */
    @Override
    public DmsDosage selectDmsDosageById(Long id)
    {
        return dmsDosageMapper.selectDmsDosageById(id);
    }

    /**
     * 查询药品剂型列表
     * 
     * @param dmsDosage 药品剂型
     * @return 药品剂型
     */
    @Override
    public List<DmsDosage> selectDmsDosageList(DmsDosage dmsDosage)
    {
        return dmsDosageMapper.selectDmsDosageList(dmsDosage);
    }

    /**
     * 新增药品剂型
     * 
     * @param dmsDosage 药品剂型
     * @return 结果
     */
    @Override
    public int insertDmsDosage(DmsDosage dmsDosage)
    {
        return dmsDosageMapper.insertDmsDosage(dmsDosage);
    }

    /**
     * 修改药品剂型
     * 
     * @param dmsDosage 药品剂型
     * @return 结果
     */
    @Override
    public int updateDmsDosage(DmsDosage dmsDosage)
    {
        return dmsDosageMapper.updateDmsDosage(dmsDosage);
    }

    /**
     * 批量删除药品剂型
     * 
     * @param ids 需要删除的药品剂型ID
     * @return 结果
     */
    @Override
    public int deleteDmsDosageByIds(Long[] ids)
    {
        return dmsDosageMapper.deleteDmsDosageByIds(ids);
    }

    /**
     * 删除药品剂型信息
     * 
     * @param id 药品剂型ID
     * @return 结果
     */
    @Override
    public int deleteDmsDosageById(Long id)
    {
        return dmsDosageMapper.deleteDmsDosageById(id);
    }
}
