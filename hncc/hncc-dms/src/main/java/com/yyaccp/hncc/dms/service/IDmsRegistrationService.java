package com.yyaccp.hncc.dms.service;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;

import com.yyaccp.hncc.common.vo.dms.DmsHandleRefundVo;
import com.yyaccp.hncc.common.vo.dms.DmsPayVo;
import com.yyaccp.hncc.common.vo.dms.DmsPaymentVo;
import com.yyaccp.hncc.dms.domain.DmsRegistration;

/**
 * 就诊(门诊)信息Service接口
 * 
 * @author ruoyi
 * @date 2020-08-20
 */
public interface IDmsRegistrationService 
{
    /**
     * 查询就诊(门诊)信息
     * 
     * @param id 就诊(门诊)信息ID
     * @return 就诊(门诊)信息
     */
    public DmsRegistration selectDmsRegistrationById(Long id);

    /**
     * 查询就诊(门诊)信息列表
     * 
     * @param dmsRegistration 就诊(门诊)信息
     * @return 就诊(门诊)信息集合
     */
    public List<DmsRegistration> selectDmsRegistrationList(DmsRegistration dmsRegistration);

    /**
     * 新增就诊(门诊)信息
     * 
     * @param dmsRegistration 就诊(门诊)信息
     * @return 结果
     */
    public int insertDmsRegistration(DmsRegistration dmsRegistration);

    /**
     * 修改就诊(门诊)信息
     * 
     * @param dmsRegistration 就诊(门诊)信息
     * @return 结果
     */
    public int updateDmsRegistration(DmsRegistration dmsRegistration);

    /**
     * 批量删除就诊(门诊)信息
     * 
     * @param ids 需要删除的就诊(门诊)信息ID
     * @return 结果
     */
    public int deleteDmsRegistrationByIds(Long[] ids);

    /**
     * 删除就诊(门诊)信息信息
     * 
     * @param id 就诊(门诊)信息ID
     * @return 结果
     */
    public int deleteDmsRegistrationById(Long id);

    /***
     * 查询就诊(门诊)信息(一个病人一个病历号，多次就诊，多次就诊信息)列表
     * 根据用户的姓名或病例号
     * @param name  用户名
     * @param medicalRecordNo  病历号
     * @return 对象集合
     */
    public List<DmsRegistration> selectListByNameAndMedicalRecordNo(String name, String medicalRecordNo);


    /**
     * 挂号操作
     * 新增患者（非读卡）  将患者新增与 其他业务隔离 防止事务的一起回滚
     * @param dmsRegistration 就诊(门诊)信息(一个病人一个病历号，多次就诊，多次就诊信息)
     * @param invoiceNo  发票号
     * @param price   金额
     * @param  catId  挂号级别id
     * @param staffId  看诊医生id
     * @return 结果
     */
    public int handleRegistration(DmsRegistration dmsRegistration, BigDecimal price, Long invoiceNo, Long catId, Long staffId);


    /**
     * 挂号操作
     * 新增发票信息
     * 新增记账流水
     * 新增挂号
     * 对应日期医生挂号限额减一
     * @param dmsRegistration 就诊(门诊)信息(一个病人一个病历号，多次就诊，多次就诊信息)
     * @param invoiceNo  发票号
     * @param price   金额
     * @param  catId  挂号级别id
     * @param staffId  看诊医生id
     * @return 结果
     */
    public int handleRegistrationDetails(DmsRegistration dmsRegistration,BigDecimal price,Long invoiceNo,Long catId,Long staffId);


    /**
     *  退号处理
     * @param id  发票id
     * @param registrationId 挂号Id
     * @param skdId    排班Id
     * @return 结果
     */
    public int handleBounce(Long id,Long registrationId,Long skdId);


    /**
     *  查询缴费列表
     *
     * @param registerId 挂号Id
     * @return 缴费列表集合
     */
    public List<HashMap<String, Object>> payList(Long registerId);

    /**
     * 处理缴费
     * @param  dmsPaymentVo 缴费对象
     * @return  结果
     */
    public int handlePayment(DmsPaymentVo dmsPaymentVo);

    /**
     *  查询退费列表
     *
     * @param registerId 挂号Id
     * @return 退费列表集合
     */
    public List<HashMap<String, Object>> refundList(Long registerId);


    /**
     * 处理退费
     * @param  handleRefundVo 退费对象
     * @return  结果
     */
    public int handleRefund(DmsHandleRefundVo handleRefundVo);

}
