package com.yyaccp.hncc.dms.service.impl;

import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.SecurityUtils;
import com.yyaccp.hncc.common.HnccStatus;
import com.yyaccp.hncc.dms.domain.DmsMedicineItemRecord;
import com.yyaccp.hncc.dms.domain.DmsMedicinePrescriptionRecord;
import com.yyaccp.hncc.dms.mapper.DmsMedicineItemRecordMapper;
import com.yyaccp.hncc.dms.mapper.DmsMedicinePrescriptionRecordMapper;
import com.yyaccp.hncc.dms.mq.IExpireOrderService;
import com.yyaccp.hncc.dms.service.IDmsMedicinePrescriptionRecordService;
import com.yyaccp.hncc.mq.FeeType;
import com.yyaccp.hncc.mq.OrderMessage;
import com.yyaccp.hncc.mq.rabbit.OrderMessageSender;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 成药处方Service业务层处理
 *
 * @author ruoyi
 * @date 2020-08-26
 */
@Service
@Slf4j
@RequiredArgsConstructor
public class DmsMedicinePrescriptionRecordServiceImpl implements IDmsMedicinePrescriptionRecordService, IExpireOrderService {

    private final DmsMedicinePrescriptionRecordMapper dmsMedicinePrescriptionRecordMapper;
    private final DmsMedicineItemRecordMapper dmsMedicineItemRecordMapper;
    private final OrderMessageSender orderMessageSender;
    @Value("${hncc.mq.order.delayTimes}")
    private int delayTimes;

    /**
     * 查询成药处方
     *
     * @param id 成药处方ID
     * @return 成药处方
     */
    @Override
    public DmsMedicinePrescriptionRecord selectDmsMedicinePrescriptionRecordById(Long id) {
        return dmsMedicinePrescriptionRecordMapper.selectDmsMedicinePrescriptionRecordById(id);
    }

    /**
     * 查询成药处方列表
     *
     * @param dmsMedicinePrescriptionRecord 成药处方
     * @return 成药处方
     */
    @Override
    public List<DmsMedicinePrescriptionRecord> selectDmsMedicinePrescriptionRecordList(DmsMedicinePrescriptionRecord dmsMedicinePrescriptionRecord) {
        return dmsMedicinePrescriptionRecordMapper.selectDmsMedicinePrescriptionRecordList(dmsMedicinePrescriptionRecord);
    }

    /**
     * 新增成药处方
     *
     * @param dmsMedicinePrescriptionRecord 成药处方
     * @return 结果
     */
    @Override
    public int insertDmsMedicinePrescriptionRecord(DmsMedicinePrescriptionRecord dmsMedicinePrescriptionRecord) {
        dmsMedicinePrescriptionRecord.setCreateStaffId(SecurityUtils.getLoginUser().getUser().getUserId());
        dmsMedicinePrescriptionRecord.setCreateTime(DateUtils.getNowDate());
        if (dmsMedicinePrescriptionRecordMapper.insertDmsMedicinePrescriptionRecord(dmsMedicinePrescriptionRecord) > 0) {
            List<DmsMedicineItemRecord> list = dmsMedicinePrescriptionRecord.getMedicineItemRecords();
            for (DmsMedicineItemRecord dmsMedicineItemRecord : list) {
                dmsMedicineItemRecord.setPrescriptionId(dmsMedicinePrescriptionRecord.getId());
                dmsMedicineItemRecordMapper.insertDmsMedicineItemRecord(dmsMedicineItemRecord);
            }
            // 发送订单延迟取消消息
            orderMessageSender.sendMessage(new OrderMessage(dmsMedicinePrescriptionRecord.getId(),
                    FeeType.MEDICINE,
                    delayTimes * 1000L));
            return 1;
        }
        return 0;
    }

    /**
     * 修改成药处方
     *
     * @param dmsMedicinePrescriptionRecord 成药处方
     * @return 结果
     */
    @Override
    public int updateDmsMedicinePrescriptionRecord(DmsMedicinePrescriptionRecord dmsMedicinePrescriptionRecord) {
        return dmsMedicinePrescriptionRecordMapper.updateDmsMedicinePrescriptionRecord(dmsMedicinePrescriptionRecord);
    }

    /**
     * 批量删除成药处方
     *
     * @param ids 需要删除的成药处方ID
     * @return 结果
     */
    @Override
    public int deleteDmsMedicinePrescriptionRecordByIds(Long[] ids) {
        return dmsMedicinePrescriptionRecordMapper.deleteDmsMedicinePrescriptionRecordByIds(ids);
    }

    /**
     * 删除成药处方信息
     *
     * @param id 成药处方ID
     * @return 结果
     */
    @Override
    public int deleteDmsMedicinePrescriptionRecordById(Long id) {
        return dmsMedicinePrescriptionRecordMapper.deleteDmsMedicinePrescriptionRecordById(id);
    }

    /**
     * 批量修改成药处方(发药)
     *
     * @param ids 需要批量修改的成药处方ID
     * @return 结果
     */
    @Override
    public int batchModificationOfPrescriptions(List<Long> ids, Integer status) {
        return dmsMedicinePrescriptionRecordMapper.updateDmsMedicinePrescriptionRecordStatusByIds(ids, status);
    }

    @Override
    public int expire(Long orderId) {
        int result = 0;
        DmsMedicinePrescriptionRecord order = dmsMedicinePrescriptionRecordMapper.selectDmsMedicinePrescriptionRecordById(orderId);
        if (order == null) {
            log.error("没有找到对应的成药处方收费记录：{}，无法过期订单", orderId);
            // 保存异常记录到数据库，待实现
        } else {
            order.setStatus((long) HnccStatus.Prescription.EXPIRED.getValue());
            result = dmsMedicinePrescriptionRecordMapper.updateDmsMedicinePrescriptionRecord(order);
        }
        return result;
    }
}
