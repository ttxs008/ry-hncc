package com.yyaccp.hncc.dms.mapper;

import java.util.List;
import com.yyaccp.hncc.dms.domain.DmsCaseModel;

/**
 * 病例模版Mapper接口
 * 
 * @author 天天向上
 * @date 2020-08-15
 */
public interface DmsCaseModelMapper 
{
    /**
     * 查询病例模版
     * 
     * @param id 病例模版ID
     * @return 病例模版
     */
    public DmsCaseModel selectDmsCaseModelById(Long id);

    /**
     * 查询病例模版列表
     * 
     * @param dmsCaseModel 病例模版
     * @return 病例模版集合
     */
    public List<DmsCaseModel> selectDmsCaseModelList(DmsCaseModel dmsCaseModel);

    /**
     * 新增病例模版
     * 
     * @param dmsCaseModel 病例模版
     * @return 结果
     */
    public int insertDmsCaseModel(DmsCaseModel dmsCaseModel);

    /**
     * 修改病例模版
     * 
     * @param dmsCaseModel 病例模版
     * @return 结果
     */
    public int updateDmsCaseModel(DmsCaseModel dmsCaseModel);

    /**
     * 删除病例模版
     * 
     * @param id 病例模版ID
     * @return 结果
     */
    public int deleteDmsCaseModelById(Long id);

    /**
     * 批量删除病例模版
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteDmsCaseModelByIds(Long[] ids);
}
