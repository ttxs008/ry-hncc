package com.yyaccp.hncc.dms.controller;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.yyaccp.hncc.common.util.BeanCopierUtil;
import com.yyaccp.hncc.common.vo.dms.DmsNonDrugVO;
import com.yyaccp.hncc.dms.domain.DmsNonDrug;
import com.yyaccp.hncc.dms.service.IDmsNonDrugService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 非药品收费项目Controller
 */
@Api(tags = "非药品收费项目管理")
@RestController
@RequestMapping("/dms/non_drug")
public class DmsNonDrugController extends BaseController {
    @Autowired
    private IDmsNonDrugService dmsNonDrugService;

    /**
     * 查询非药品收费项目列表
     */

    @PreAuthorize("@ss.hasPermi('dms:non_drug:list')")
    @ApiOperation(value = "查询非药品收费项目列表", notes = "查询非药品收费项目列表",
            code = 200, produces = "application/json", protocols = "Http",
            response = TableDataInfo.class, httpMethod = "GET")
    @ApiParam(name = "非药品收费项目VO对象", type = "DmsNonDrugVO")
    @GetMapping("/list")
    public TableDataInfo list(DmsNonDrugVO dmsNonDrugVO) {
        startPage();
        //VO转换成实体类
        DmsNonDrug dmsNonDrug = BeanCopierUtil.copy(dmsNonDrugVO, DmsNonDrug.class);
        //查询出数据
        List<DmsNonDrug> list = dmsNonDrugService.selectDmsNonDrugList(dmsNonDrug);
        //分页出数据
        TableDataInfo tableDataInfo = getDataTable(list);
        //转换成VO集合
        List<DmsNonDrugVO> vos = BeanCopierUtil.copy(tableDataInfo.getRows(), DmsNonDrugVO.class);
        //加载转换后的数据
        tableDataInfo.setRows(vos);
        return getDataTable(list);
    }

    /**
     * 导出非药品收费项目列表
     */
    @PreAuthorize("@ss.hasPermi('dms:non_drug:export')")
    @Log(title = "非药品收费项目", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(DmsNonDrug dmsNonDrug) {
        List<DmsNonDrug> list = dmsNonDrugService.selectDmsNonDrugList(dmsNonDrug);
        ExcelUtil<DmsNonDrug> util = new ExcelUtil<DmsNonDrug>(DmsNonDrug.class);
        return util.exportExcel(list, "non_drug");
    }

    /**
     * 获取非药品收费项目详细信息
     */
    @PreAuthorize("@ss.hasPermi('dms:non_drug:query')")
    @ApiOperation(value = "获取非药品收费项目详细信息", notes = "获取非药品收费项目详细信息",
            code = 200, produces = "application/json", protocols = "Http",
            response = AjaxResult.class, httpMethod = "GET")
    @ApiParam(name = "非药品收费项目ID", type = "Long")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id) {
        return AjaxResult.success(dmsNonDrugService.selectDmsNonDrugById(id));
    }

    /**
     * 新增非药品收费项目
     */
    @PreAuthorize("@ss.hasPermi('dms:non_drug:add')")
    @ApiOperation(value = "新增非药品收费项目", notes = "新增非药品收费项目",
            code = 200, produces = "application/json", protocols = "Http",
            response = AjaxResult.class, httpMethod = "POST")
    @ApiParam(name = "非药品收费项目VO对象", type = "DmsNonDrugVO")
    @Log(title = "非药品收费项目", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody DmsNonDrugVO dmsNonDrugVO) {
        return toAjax(dmsNonDrugService.insertDmsNonDrug(BeanCopierUtil.copy(dmsNonDrugVO, DmsNonDrug.class)));
    }

    /**
     * 修改非药品收费项目
     */
    @PreAuthorize("@ss.hasPermi('dms:non_drug:edit')")
    @ApiOperation(value = "修改非药品收费项目", notes = "修改非药品收费项目",
            code = 200, produces = "application/json", protocols = "Http",
            response = AjaxResult.class, httpMethod = "PUT")
    @ApiParam(name = "非药品收费项目VO对象", type = "DmsNonDrugVO")
    @Log(title = "非药品收费项目", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody DmsNonDrugVO dmsNonDrugVO) {
        return toAjax(dmsNonDrugService.updateDmsNonDrug(BeanCopierUtil.copy(dmsNonDrugVO, DmsNonDrug.class)));
    }

    /**
     * 删除非药品收费项目
     */
    @PreAuthorize("@ss.hasPermi('dms:non_drug:remove')")
    @ApiOperation(value = "删除非药品收费项目", notes = "删除非药品收费项目",
            code = 200, produces = "application/json", protocols = "Http",
            response = AjaxResult.class, httpMethod = "DELETE")
    @ApiParam(name = "非药品收费项目IDs", type = "Long[]")
    @Log(title = "非药品收费项目", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids) {
        return toAjax(dmsNonDrugService.deleteDmsNonDrugByIds(ids));
    }
}
