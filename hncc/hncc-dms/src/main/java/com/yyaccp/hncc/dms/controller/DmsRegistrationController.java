package com.yyaccp.hncc.dms.controller;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.yyaccp.hncc.common.util.BeanCopierUtil;
import com.yyaccp.hncc.common.vo.dms.*;
import com.yyaccp.hncc.dms.domain.DmsRegistration;
import com.yyaccp.hncc.dms.service.IDmsRegistrationService;
import io.swagger.annotations.*;
import org.omg.PortableInterceptor.INACTIVE;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.util.*;
import java.util.stream.Collectors;

//com.yyaccp.hncc.common模块的包引入

/**
 * 就诊(门诊)信息Controller
 *
 * @author ruoyi
 * @date 2020-08-20
 */
@Api(tags = "就诊(门诊)信息")
@RestController
@RequestMapping("/dms/registration")
public class DmsRegistrationController extends BaseController {
    @Autowired
    private IDmsRegistrationService dmsRegistrationService;

/**
 * 查询就诊(门诊)信息列表
 *
 */
@PreAuthorize("@ss.hasPermi('dms:registration:list')")
@GetMapping("/list")
@ApiOperation(value = "查询就诊(门诊)信息" , notes = "查询所有就诊(门诊)信息" ,
        code = 200, produces = "application/json" , protocols = "Http" ,
        response = TableDataInfo.class, httpMethod = "GET")
@ApiResponses({
        @ApiResponse(code = 204, message = "操作已经执行成功，但是没有返回数据"),
        @ApiResponse(code = 303, message = "重定向"),
        @ApiResponse(code = 400, message = "参数列表错误（缺少，格式不匹配）"),
        @ApiResponse(code = 500, message = "系统内部错误"),
        @ApiResponse(code = 404, message = "资源，服务未找到"),
        @ApiResponse(code = 200, message = "操作成功"),
        @ApiResponse(code = 401, message = "未授权"),
        @ApiResponse(code = 403, message = "访问受限，授权过期")
})
@ApiParam(name = "DmsRegistrationVO对象" , type = "DmsRegistrationVO")
        public TableDataInfo list(DmsRegistrationVO dmsRegistrationVO) {
        //将Vo转化为实体
        DmsRegistration dmsRegistration= BeanCopierUtil.copy(dmsRegistrationVO, DmsRegistration. class);
        startPage();
        List<DmsRegistration> list = dmsRegistrationService.selectDmsRegistrationList(dmsRegistration);
        TableDataInfo tableDataInfo = getDataTable(list);
        //替换集合
        tableDataInfo.setRows(BeanCopierUtil.copy(tableDataInfo.getRows(), DmsRegistrationVO.class));
        return tableDataInfo;
    }
    
    /**
     * 导出就诊(门诊)信息列表
     */
    @PreAuthorize("@ss.hasPermi('dms:registration:export')")
    @Log(title = "就诊(门诊)信息" , businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    @ApiOperation(value = "导出就诊(门诊)信息表" , notes = "导出所有就诊(门诊)信息" ,
            code = 200, produces = "application/json" , protocols = "Http" ,
            response = TableDataInfo.class, httpMethod = "GET")
    @ApiResponses({
            @ApiResponse(code = 204, message = "操作已经执行成功，但是没有返回数据"),
            @ApiResponse(code = 303, message = "重定向"),
            @ApiResponse(code = 400, message = "参数列表错误（缺少，格式不匹配）"),
            @ApiResponse(code = 500, message = "系统内部错误"),
            @ApiResponse(code = 404, message = "资源，服务未找到"),
            @ApiResponse(code = 200, message = "操作成功"),
            @ApiResponse(code = 401, message = "未授权"),
            @ApiResponse(code = 403, message = "访问受限，授权过期")
    })
    @ApiParam(name = "DmsRegistrationVO对象" , type = "DmsRegistrationVO")
    public AjaxResult export(DmsRegistrationVO dmsRegistrationVO) {
        //将VO转化为实体
        DmsRegistration dmsRegistration= BeanCopierUtil.copy(dmsRegistrationVO, DmsRegistration. class);
        List<DmsRegistrationVO> list = BeanCopierUtil.copy(dmsRegistrationService.selectDmsRegistrationList(dmsRegistration), DmsRegistrationVO.class);
        ExcelUtil<DmsRegistrationVO> util = new ExcelUtil<DmsRegistrationVO>(DmsRegistrationVO.class);
        return util.exportExcel(list, "registration");
    }

    /**
     * 获取就诊(门诊)信息详细信息
     */
    @PreAuthorize("@ss.hasPermi('dms:registration:query')")
    @GetMapping(value = "/{id}")
    @ApiOperation(value = "获取就诊(门诊)信息详细信息" ,
            notes = "根据就诊(门诊)信息id获取科室信息" , code = 200,
            produces = "application/json" , protocols = "Http" , response = AjaxResult.class,
            httpMethod = "GET")
    @ApiResponses({
            @ApiResponse(code = 204, message = "操作已经执行成功，但是没有返回数据"),
            @ApiResponse(code = 303, message = "重定向"),
            @ApiResponse(code = 400, message = "参数列表错误（缺少，格式不匹配）"),
            @ApiResponse(code = 500, message = "系统内部错误"),
            @ApiResponse(code = 404, message = "资源，服务未找到"),
            @ApiResponse(code = 200, message = "操作成功"),
            @ApiResponse(code = 401, message = "未授权"),
            @ApiResponse(code = 403, message = "访问受限，授权过期")
    })
    @ApiParam(name = "dmsRegistration.id" , type = "Long")
    public AjaxResult getInfo(@PathVariable("id") Long id) {
        return AjaxResult.success(BeanCopierUtil.copy(dmsRegistrationService.selectDmsRegistrationById(id), DmsRegistrationVO.class));
    }

    /**
     * 修改就诊(门诊)信息
     */
    @PreAuthorize("@ss.hasPermi('dms:registration:edit')")
    @Log(title = "就诊(门诊)信息" , businessType = BusinessType.UPDATE)
    @PutMapping
    @ApiOperation(value = "修改就诊(门诊)信息信息" , notes = "修改就诊(门诊)信息信息" , code = 200,
            produces = "application/json" , protocols = "Http" , response = AjaxResult.class,
            httpMethod = "PUT")
    @ApiResponses({
            @ApiResponse(code = 204, message = "操作已经执行成功，但是没有返回数据"),
            @ApiResponse(code = 303, message = "重定向"),
            @ApiResponse(code = 400, message = "参数列表错误（缺少，格式不匹配）"),
            @ApiResponse(code = 500, message = "系统内部错误"),
            @ApiResponse(code = 404, message = "资源，服务未找到"),
            @ApiResponse(code = 200, message = "操作成功"),
            @ApiResponse(code = 401, message = "未授权"),
            @ApiResponse(code = 403, message = "访问受限，授权过期")
    })
    @ApiParam(name = "DmsRegistrationVO对象" , type = "DmsRegistrationVO")
    public AjaxResult edit(@RequestBody DmsRegistrationVO dmsRegistrationVO) {
        return toAjax(dmsRegistrationService.updateDmsRegistration(BeanCopierUtil.copy(dmsRegistrationVO, DmsRegistration. class)));
    }




    /**
     * 查询就诊(门诊)信息列表  根据病人名称 与  病患号
     */
    @PreAuthorize("@ss.hasPermi('dms:registration:listByNameAndNo')")
    @GetMapping("/listByNameAndNo")
    @ApiOperation(value = "查询就诊(门诊)信息)", notes = "查询所有就诊(门诊)信息 根据病人名称 与  病患号",
            code = 200, produces = "application/json", protocols = "Http",
            response = TableDataInfo.class, httpMethod = "GET")
    @ApiResponses({
            @ApiResponse(code = 204, message = "操作已经执行成功，但是没有返回数据"),
            @ApiResponse(code = 303, message = "重定向"),
            @ApiResponse(code = 400, message = "参数列表错误（缺少，格式不匹配）"),
            @ApiResponse(code = 500, message = "系统内部错误"),
            @ApiResponse(code = 404, message = "资源，服务未找到"),
            @ApiResponse(code = 200, message = "操作成功"),
            @ApiResponse(code = 401, message = "未授权"),
            @ApiResponse(code = 403, message = "访问受限，授权过期")
    })
    @ApiParam(name = "DmsRegistrationVO对象", type = "DmsRegistrationVO")
    public TableDataInfo registrationPatientList(DmsRegistrationVO dmsRegistrationVO) {
        startPage();
        List<DmsRegistration> list = dmsRegistrationService.selectListByNameAndMedicalRecordNo(dmsRegistrationVO.getName(), dmsRegistrationVO.getMedicalRecordNo());
        TableDataInfo tableDataInfo = getDataTable(list);
        //替换集合
        tableDataInfo.setRows(BeanCopierUtil.copy(tableDataInfo.getRows(), DmsRegistrationPatientVO.class));

        return tableDataInfo;
    }


    /**
     * 新增就诊(门诊)信息(一个病人一个病历号，多次就诊，多次就诊信息)
     */
    @PreAuthorize("@ss.hasPermi('dms:registration:handleRegistration')")
    @Log(title = "挂号处理", businessType = BusinessType.INSERT)
    @PostMapping(value = "/handleRegistration")
    @ApiOperation(value = "新增挂号", notes = "新增挂号", code = 200,
            produces = "application/json", protocols = "Http", response = AjaxResult.class,
            httpMethod = "POST")
    @ApiResponses({
            @ApiResponse(code = 204, message = "操作已经执行成功，但是没有返回数据"),
            @ApiResponse(code = 303, message = "重定向"),
            @ApiResponse(code = 400, message = "参数列表错误（缺少，格式不匹配）"),
            @ApiResponse(code = 500, message = "系统内部错误"),
            @ApiResponse(code = 404, message = "资源，服务未找到"),
            @ApiResponse(code = 200, message = "操作成功"),
            @ApiResponse(code = 401, message = "未授权"),
            @ApiResponse(code = 403, message = "访问受限，授权过期")
    })
    @ApiParam(name = "DmsRegistrationVO对象", type = "DmsRegistrationVO")
    public AjaxResult handleRegistration(@RequestBody DmsRegistrationPatientVO dmsRegistrationPatientVO) {
        DmsRegistration dmsRegistration = BeanCopierUtil.copy(dmsRegistrationPatientVO, DmsRegistration.class);
        // 金额
        BigDecimal price = dmsRegistrationPatientVO.getPrice();
        // 发票号
        Long invoiceNo = dmsRegistrationPatientVO.getInvoiceNo();
        // 挂号级别Id
        Long catId = dmsRegistrationPatientVO.getCatId();
        // 看诊医生id
        Long staffId = dmsRegistrationPatientVO.getStaffId();
        return toAjax(dmsRegistrationService.handleRegistration(dmsRegistration, price, invoiceNo, catId, staffId));
    }


    /**
     *  退号
     */
    @PreAuthorize("@ss.hasPermi('dms:registration:handleBounce')")
    @Log(title = "退号处理", businessType = BusinessType.INSERT)
    @PostMapping(value = "/handleBounce")
    @ApiOperation(value = "退号", notes = "退号", code = 200,
            produces = "application/json", protocols = "Http", response = AjaxResult.class,
            httpMethod = "POST")
    @ApiResponses({
            @ApiResponse(code = 204, message = "操作已经执行成功，但是没有返回数据"),
            @ApiResponse(code = 303, message = "重定向"),
            @ApiResponse(code = 400, message = "参数列表错误（缺少，格式不匹配）"),
            @ApiResponse(code = 500, message = "系统内部错误"),
            @ApiResponse(code = 404, message = "资源，服务未找到"),
            @ApiResponse(code = 200, message = "操作成功"),
            @ApiResponse(code = 401, message = "未授权"),
            @ApiResponse(code = 403, message = "访问受限，授权过期")
    })
    @ApiImplicitParams({
            @ApiImplicitParam(name="id",value="发票Id",required=true,dataType="Long"),
            @ApiImplicitParam(name="registrationId",value="挂号Id",dataType="Long"),
            @ApiImplicitParam(name="skdId",value="排班Id",required=true,dataType="Long")
    })
    public AjaxResult handleBounce(@RequestBody Map<String,Long> param){
        return toAjax(dmsRegistrationService.handleBounce(param.get("id"),param.get("registrationId"),param.get("skdId")));
    }

    /**
     * 查询缴费列表
     *
     */
    @PreAuthorize("@ss.hasPermi('dms:registration:payList')")
    @GetMapping("/payList")
    @ApiOperation(value = "查询缴费列表" , notes = "查询缴费列表 根据挂号ID" ,
            code = 200, produces = "application/json" , protocols = "Http" ,
            response = TableDataInfo.class, httpMethod = "GET")
    @ApiResponses({
            @ApiResponse(code = 204, message = "操作已经执行成功，但是没有返回数据"),
            @ApiResponse(code = 303, message = "重定向"),
            @ApiResponse(code = 400, message = "参数列表错误（缺少，格式不匹配）"),
            @ApiResponse(code = 500, message = "系统内部错误"),
            @ApiResponse(code = 404, message = "资源，服务未找到"),
            @ApiResponse(code = 200, message = "操作成功"),
            @ApiResponse(code = 401, message = "未授权"),
            @ApiResponse(code = 403, message = "访问受限，授权过期")
    })
    @ApiParam(name = "registerId 挂号Id" , type = "Long")
    public TableDataInfo paymentList(Long registerId) {
        startPage();
        List<HashMap<String, Object>> objects= dmsRegistrationService.payList(registerId);
        TableDataInfo tableDataInfo = getDataTable(objects);
        //替换集合
        tableDataInfo.setRows(BeanCopierUtil.copy(tableDataInfo.getRows(), DmsPayVo.class));
        return tableDataInfo;
    }

    /**
     * 缴费处理
     */
    @PreAuthorize("@ss.hasPermi('dms:registration:handlePayment')")
    @PostMapping("/handlePayment")
    @ApiOperation(value = "缴费处理", notes = "缴费处理",
            code = 200, produces = "application/json", protocols = "Http",
            response = TableDataInfo.class, httpMethod = "post")
    @ApiResponses({
            @ApiResponse(code = 204, message = "操作已经执行成功，但是没有返回数据"),
            @ApiResponse(code = 303, message = "重定向"),
            @ApiResponse(code = 400, message = "参数列表错误（缺少，格式不匹配）"),
            @ApiResponse(code = 500, message = "系统内部错误"),
            @ApiResponse(code = 404, message = "资源，服务未找到"),
            @ApiResponse(code = 200, message = "操作成功"),
            @ApiResponse(code = 401, message = "未授权"),
            @ApiResponse(code = 403, message = "访问受限，授权过期")
    })
    @ApiParam(name = "DmsPaymentVo 对象", type = "DmsPaymentVo")
    public AjaxResult handlePayment(@RequestBody DmsPaymentVo dmsPaymentVo) {

        return toAjax(dmsRegistrationService.handlePayment(dmsPaymentVo));
    }


    /**
     * 查询退费列表
     *
     */
    @PreAuthorize("@ss.hasPermi('dms:registration:refundList')")
    @GetMapping("/refundList")
    @ApiOperation(value = "查询退费列表" , notes = "查询退费列表 根据挂号ID" ,
            code = 200, produces = "application/json" , protocols = "Http" ,
            response = TableDataInfo.class, httpMethod = "GET")
    @ApiResponses({
            @ApiResponse(code = 204, message = "操作已经执行成功，但是没有返回数据"),
            @ApiResponse(code = 303, message = "重定向"),
            @ApiResponse(code = 400, message = "参数列表错误（缺少，格式不匹配）"),
            @ApiResponse(code = 500, message = "系统内部错误"),
            @ApiResponse(code = 404, message = "资源，服务未找到"),
            @ApiResponse(code = 200, message = "操作成功"),
            @ApiResponse(code = 401, message = "未授权"),
            @ApiResponse(code = 403, message = "访问受限，授权过期")
    })
    @ApiParam(name = "registerId 挂号Id" , type = "Long")
    public TableDataInfo refundList(Long registerId) {
        startPage();
        List<HashMap<String, Object>> objects= dmsRegistrationService.refundList(registerId);
        TableDataInfo tableDataInfo = getDataTable(objects);
        //替换集合
        tableDataInfo.setRows(BeanCopierUtil.copy(tableDataInfo.getRows(), DmsRefundVo.class));
        return tableDataInfo;
    }

    /**
     * 退费处理
     */
    @PreAuthorize("@ss.hasPermi('dms:registration:handleRefund')")
    @PostMapping("/handleRefund")
    @ApiOperation(value = "缴费处理", notes = "缴费处理",
            code = 200, produces = "application/json", protocols = "Http",
            response = TableDataInfo.class, httpMethod = "post")
    @ApiResponses({
            @ApiResponse(code = 204, message = "操作已经执行成功，但是没有返回数据"),
            @ApiResponse(code = 303, message = "重定向"),
            @ApiResponse(code = 400, message = "参数列表错误（缺少，格式不匹配）"),
            @ApiResponse(code = 500, message = "系统内部错误"),
            @ApiResponse(code = 404, message = "资源，服务未找到"),
            @ApiResponse(code = 200, message = "操作成功"),
            @ApiResponse(code = 401, message = "未授权"),
            @ApiResponse(code = 403, message = "访问受限，授权过期")
    })
    @ApiParam(name = "DmsHandleRefundVo 对象", type = "DmsHandleRefundVo")
    public AjaxResult handleRefund(@RequestBody DmsHandleRefundVo dmsHandleRefundVo) {

        return toAjax(dmsRegistrationService.handleRefund(dmsHandleRefundVo));
    }
}
