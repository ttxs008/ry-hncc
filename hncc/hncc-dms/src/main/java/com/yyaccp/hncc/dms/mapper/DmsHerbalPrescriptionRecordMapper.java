package com.yyaccp.hncc.dms.mapper;

import java.util.List;
import com.yyaccp.hncc.dms.domain.DmsHerbalPrescriptionRecord;
import org.apache.ibatis.annotations.Param;

/**
 * 草处方Mapper接口
 * 
 * @author ruoyi
 * @date 2020-09-07
 */
public interface DmsHerbalPrescriptionRecordMapper 
{
    /**
     * 查询草处方
     * 
     * @param id 草处方ID
     * @return 草处方
     */
    public DmsHerbalPrescriptionRecord selectDmsHerbalPrescriptionRecordById(Long id);

    /**
     * 查询草处方列表
     * 
     * @param dmsHerbalPrescriptionRecord 草处方
     * @return 草处方集合
     */
    public List<DmsHerbalPrescriptionRecord> selectDmsHerbalPrescriptionRecordList(DmsHerbalPrescriptionRecord dmsHerbalPrescriptionRecord);

    /**
     * 新增草处方
     * 
     * @param dmsHerbalPrescriptionRecord 草处方
     * @return 结果
     */
    public int insertDmsHerbalPrescriptionRecord(DmsHerbalPrescriptionRecord dmsHerbalPrescriptionRecord);

    /**
     * 修改草处方
     * 
     * @param dmsHerbalPrescriptionRecord 草处方
     * @return 结果
     */
    public int updateDmsHerbalPrescriptionRecord(DmsHerbalPrescriptionRecord dmsHerbalPrescriptionRecord);

    /**
     * 删除草处方
     * 
     * @param id 草处方ID
     * @return 结果
     */
    public int deleteDmsHerbalPrescriptionRecordById(Long id);

    /**
     * 批量删除草处方
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteDmsHerbalPrescriptionRecordByIds(Long[] ids);

    /**
     *  批量修改草药处方状态
     * @param ids ID数组
     * @param status  状态
     * @return 结果
     */
    public int updateDmsHerbalPrescriptionRecordStatusByIds(@Param("ids") List<Long> ids, @Param("status")Integer status);
}
