package com.yyaccp.hncc.dms.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.yyaccp.hncc.dms.mapper.DmsDictDataManageMapper;
import com.yyaccp.hncc.dms.domain.DmsDictDataManage;
import com.yyaccp.hncc.dms.service.IDmsDictDataManageService;

/**
 * 字典数据Service业务层处理
 * 
 * @author M
 * @date 2020-08-19
 */
@Service
public class DmsDictDataManageServiceImpl implements IDmsDictDataManageService 
{
    @Autowired
    private DmsDictDataManageMapper dmsDictDataManageMapper;


    /**
     * 查询字典数据列表
     * 
     * @param dmsDictDataManage 字典数据
     * @return 字典数据
     */
    @Override
    public List<DmsDictDataManage> selectDmsDictDataManageList(DmsDictDataManage dmsDictDataManage)
    {
        return dmsDictDataManageMapper.selectDmsDictDataManageList(dmsDictDataManage);
    }

}
