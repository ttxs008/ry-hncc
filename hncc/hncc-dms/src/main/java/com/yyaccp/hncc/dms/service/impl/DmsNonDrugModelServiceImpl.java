package com.yyaccp.hncc.dms.service.impl;

import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.SecurityUtils;
import com.yyaccp.hncc.dms.domain.DmsNonDrugModel;
import com.yyaccp.hncc.dms.mapper.DmsNonDrugModelMapper;
import com.yyaccp.hncc.dms.service.IDmsNonDrugModelService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 非药品模版Service业务层处理
 *
 * @author yugui
 * @date 2020-08-17
 */
@Service
public class DmsNonDrugModelServiceImpl implements IDmsNonDrugModelService {
    @Autowired
    private DmsNonDrugModelMapper dmsNonDrugModelMapper;

    /**
     * 查询非药品模版
     *
     * @param id 非药品模版ID
     * @return 非药品模版
     */
    @Override
    public DmsNonDrugModel selectDmsNonDrugModelById(Long id) {
        return dmsNonDrugModelMapper.selectDmsNonDrugModelById(id);
    }

    /**
     * 查询非药品模版列表
     *
     * @param dmsNonDrugModel 非药品模版
     * @return 非药品模版
     */
    @Override
    public List<DmsNonDrugModel> selectDmsNonDrugModelList(DmsNonDrugModel dmsNonDrugModel) {
        return dmsNonDrugModelMapper.selectDmsNonDrugModelList(dmsNonDrugModel);
    }

    /**
     * 新增非药品模版
     *
     * @param dmsNonDrugModel 非药品模版
     * @return 结果
     */
    @Override
    public int insertDmsNonDrugModel(DmsNonDrugModel dmsNonDrugModel) {
        dmsNonDrugModel.setStatus((long) 1);
        dmsNonDrugModel.setScope((long) 0);
        dmsNonDrugModel.setOwnId(SecurityUtils.getLoginUser().getUser().getUserId());
        dmsNonDrugModel.setCode(DateUtils.dateTimeNow());
        dmsNonDrugModel.setCreateTime(DateUtils.getNowDate());
        return dmsNonDrugModelMapper.insertDmsNonDrugModel(dmsNonDrugModel);
    }

    /**
     * 修改非药品模版
     *
     * @param dmsNonDrugModel 非药品模版
     * @return 结果
     */
    @Override
    public int updateDmsNonDrugModel(DmsNonDrugModel dmsNonDrugModel) {
        return dmsNonDrugModelMapper.updateDmsNonDrugModel(dmsNonDrugModel);
    }

    /**
     * 批量删除非药品模版
     *
     * @param ids 需要删除的非药品模版ID
     * @return 结果
     */
    @Override
    public int deleteDmsNonDrugModelByIds(Long[] ids) {
        return dmsNonDrugModelMapper.deleteDmsNonDrugModelByIds(ids);
    }

    /**
     * 删除非药品模版信息
     *
     * @param id 非药品模版ID
     * @return 结果
     */
    @Override
    public int deleteDmsNonDrugModelById(Long id) {
        return dmsNonDrugModelMapper.deleteDmsNonDrugModelById(id);
    }
}
