package com.yyaccp.hncc.dms.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 药品模版对象 dms_drug_model
 *
 * @author 周某
 * @date 2020-08-18
 */
public class DmsDrugModel extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 药品模版id */
    private Long id;

    /** 药品模版状态 */
    @Excel(name = "药品模版状态")
    private Integer status;

    /** 药品模版名字 */
    @Excel(name = "药品模版名字")
    private String name;

    /** 药品模版范围类型 scope 0个人 1科室  2全院 */
    @Excel(name = "药品模版范围类型 scope 0个人 1科室  2全院")
    private Integer scope;

    /** 药品模版所属ID */
    @Excel(name = "药品模版所属ID")
    private Long ownId;

    /** 药品模版目的 */
    @Excel(name = "药品模版目的")
    private String aim;

    /** 药品模版编码 */
    @Excel(name = "药品模版编码")
    private String code;

    /** 药品类型 */
    @Excel(name = "药品类型")
    private Integer type;

    /** 草药：付数 */
    @Excel(name = "草药：付数")
    private Long pairNum;

    /** 草药：频次 */
    @Excel(name = "草药：频次")
    private Integer frequency;

    /** 草药：治法 */
    @Excel(name = "草药：治法")
    private String therapy;

    /** 草药：治法细节 */
    @Excel(name = "草药：治法细节")
    private String therapyDetails;

    /** 草药：医嘱 */
    @Excel(name = "草药：医嘱")
    private String medicalAdvice;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setStatus(Integer status) 
    {
        this.status = status;
    }

    public Integer getStatus() 
    {
        return status;
    }
    public void setName(String name) 
    {
        this.name = name;
    }

    public String getName() 
    {
        return name;
    }
    public void setScope(Integer scope) 
    {
        this.scope = scope;
    }

    public Integer getScope() 
    {
        return scope;
    }
    public void setOwnId(Long ownId) 
    {
        this.ownId = ownId;
    }

    public Long getOwnId() 
    {
        return ownId;
    }
    public void setAim(String aim) 
    {
        this.aim = aim;
    }

    public String getAim() 
    {
        return aim;
    }
    public void setCode(String code) 
    {
        this.code = code;
    }

    public String getCode() 
    {
        return code;
    }
    public void setType(Integer type) 
    {
        this.type = type;
    }

    public Integer getType() 
    {
        return type;
    }
    public void setPairNum(Long pairNum) 
    {
        this.pairNum = pairNum;
    }

    public Long getPairNum() 
    {
        return pairNum;
    }
    public void setFrequency(Integer frequency) 
    {
        this.frequency = frequency;
    }

    public Integer getFrequency() 
    {
        return frequency;
    }
    public void setTherapy(String therapy) 
    {
        this.therapy = therapy;
    }

    public String getTherapy() 
    {
        return therapy;
    }
    public void setTherapyDetails(String therapyDetails) 
    {
        this.therapyDetails = therapyDetails;
    }

    public String getTherapyDetails() 
    {
        return therapyDetails;
    }
    public void setMedicalAdvice(String medicalAdvice) 
    {
        this.medicalAdvice = medicalAdvice;
    }

    public String getMedicalAdvice() 
    {
        return medicalAdvice;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("status", getStatus())
            .append("name", getName())
            .append("scope", getScope())
            .append("ownId", getOwnId())
            .append("aim", getAim())
            .append("createTime", getCreateTime())
            .append("code", getCode())
            .append("type", getType())
            .append("pairNum", getPairNum())
            .append("frequency", getFrequency())
            .append("therapy", getTherapy())
            .append("therapyDetails", getTherapyDetails())
            .append("medicalAdvice", getMedicalAdvice())
            .toString();
    }
}
