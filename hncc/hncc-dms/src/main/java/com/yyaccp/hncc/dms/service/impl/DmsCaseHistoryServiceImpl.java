package com.yyaccp.hncc.dms.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.yyaccp.hncc.dms.mapper.DmsCaseHistoryMapper;
import com.yyaccp.hncc.dms.domain.DmsCaseHistory;
import com.yyaccp.hncc.dms.service.IDmsCaseHistoryService;

import javax.annotation.Resource;

/**
 * 病历Service业务层处理
 * 
 * @author ruoyi
 * @date 2020-08-28
 */
@Service
public class DmsCaseHistoryServiceImpl implements IDmsCaseHistoryService 
{
    @Autowired
    private DmsCaseHistoryMapper dmsCaseHistoryMapper;

    /**
     * 查询病历
     * 
     * @param id 病历ID
     * @return 病历
     */
    @Override
    public DmsCaseHistory selectDmsCaseHistoryById(Long id)
    {
        return dmsCaseHistoryMapper.selectDmsCaseHistoryById(id);
    }

    /**
     * 查询病历列表
     * 
     * @param dmsCaseHistory 病历
     * @return 病历
     */
    @Override
    public List<DmsCaseHistory> selectDmsCaseHistoryList(DmsCaseHistory dmsCaseHistory)
    {
        return dmsCaseHistoryMapper.selectDmsCaseHistoryList(dmsCaseHistory);
    }

    /**
     * 新增病历
     * 
     * @param dmsCaseHistory 病历
     * @return 结果
     */
    @Override
    public int insertDmsCaseHistory(DmsCaseHistory dmsCaseHistory)
    {
        dmsCaseHistory.setCreateTime(DateUtils.getNowDate());
        return dmsCaseHistoryMapper.insertDmsCaseHistory(dmsCaseHistory);
    }

    /**
     * 修改病历
     * 
     * @param dmsCaseHistory 病历
     * @return 结果
     */
    @Override
    public int updateDmsCaseHistory(DmsCaseHistory dmsCaseHistory)
    {
        return dmsCaseHistoryMapper.updateDmsCaseHistory(dmsCaseHistory);
    }

    /**
     * 批量删除病历
     * 
     * @param ids 需要删除的病历ID
     * @return 结果
     */
    @Override
    public int deleteDmsCaseHistoryByIds(Long[] ids)
    {
        return dmsCaseHistoryMapper.deleteDmsCaseHistoryByIds(ids);
    }

    /**
     * 删除病历信息
     * 
     * @param id 病历ID
     * @return 结果
     */
    @Override
    public int deleteDmsCaseHistoryById(Long id)
    {
        return dmsCaseHistoryMapper.deleteDmsCaseHistoryById(id);
    }

    /**
     * 通过挂号号查询单条病历信息
     *
     * @param patientId
     * @return
     */
    @Override
    public List selectPatIdHis(Long patientId) {
        return dmsCaseHistoryMapper.selectPatIdHis(patientId);
    }
}
