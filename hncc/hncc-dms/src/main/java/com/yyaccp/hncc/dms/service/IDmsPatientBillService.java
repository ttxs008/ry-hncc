package com.yyaccp.hncc.dms.service;

import com.yyaccp.hncc.dms.domain.DmsPatientBill;

import java.util.List;

/**
 * Created by 余归 on 2020/8/24
 *
 * @author 余归
 */
public interface IDmsPatientBillService {

    /**
     * 查询患者账单
     */
    List<DmsPatientBill> selectPatientBills(Integer registrationId);

}
