package com.yyaccp.hncc.dms.service.impl;

import com.yyaccp.hncc.dms.domain.DmsMedicineItemRecord;
import com.yyaccp.hncc.dms.mapper.DmsMedicineItemRecordMapper;
import com.yyaccp.hncc.dms.service.IDmsMedicineItemRecordService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 成药项记录Service业务层处理
 *
 * @author 余归
 * @date 2020-09-02
 */
@Service
public class DmsMedicineItemRecordServiceImpl implements IDmsMedicineItemRecordService {
    @Autowired
    private DmsMedicineItemRecordMapper dmsMedicineItemRecordMapper;

    /**
     * 查询成药项记录
     *
     * @param id 成药项记录ID
     * @return 成药项记录
     */
    @Override
    public DmsMedicineItemRecord selectDmsMedicineItemRecordById(Long id) {
        return dmsMedicineItemRecordMapper.selectDmsMedicineItemRecordById(id);
    }

    /**
     * 查询成药项记录列表
     *
     * @param dmsMedicineItemRecord 成药项记录
     * @return 成药项记录
     */
    @Override
    public List<DmsMedicineItemRecord> selectDmsMedicineItemRecordList(DmsMedicineItemRecord dmsMedicineItemRecord) {
        return dmsMedicineItemRecordMapper.selectDmsMedicineItemRecordList(dmsMedicineItemRecord);
    }

    /**
     * 新增成药项记录
     *
     * @param dmsMedicineItemRecord 成药项记录
     * @return 结果
     */
    @Override
    public int insertDmsMedicineItemRecord(DmsMedicineItemRecord dmsMedicineItemRecord) {
        return dmsMedicineItemRecordMapper.insertDmsMedicineItemRecord(dmsMedicineItemRecord);
    }

    /**
     * 修改成药项记录
     *
     * @param dmsMedicineItemRecord 成药项记录
     * @return 结果
     */
    @Override
    public int updateDmsMedicineItemRecord(DmsMedicineItemRecord dmsMedicineItemRecord) {
        return dmsMedicineItemRecordMapper.updateDmsMedicineItemRecord(dmsMedicineItemRecord);
    }

    /**
     * 批量删除成药项记录
     *
     * @param ids 需要删除的成药项记录ID
     * @return 结果
     */
    @Override
    public int deleteDmsMedicineItemRecordByIds(Long[] ids) {
        return dmsMedicineItemRecordMapper.deleteDmsMedicineItemRecordByIds(ids);
    }

    /**
     * 删除成药项记录信息
     *
     * @param id 成药项记录ID
     * @return 结果
     */
    @Override
    public int deleteDmsMedicineItemRecordById(Long id) {
        return dmsMedicineItemRecordMapper.deleteDmsMedicineItemRecordById(id);
    }
}
