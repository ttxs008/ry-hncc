package com.yyaccp.hncc.dms.mapper;

import java.util.List;
import com.yyaccp.hncc.dms.domain.DmsCaseHistory;

/**
 * 病历Mapper接口
 * 
 * @author ruoyi
 * @date 2020-08-28
 */
public interface DmsCaseHistoryMapper 
{
    /**
     * 查询病历
     * 
     * @param id 病历ID
     * @return 病历
     */
    public DmsCaseHistory selectDmsCaseHistoryById(Long id);

    /**
     * 查询病历列表
     * 
     * @param dmsCaseHistory 病历
     * @return 病历集合
     */
    public List<DmsCaseHistory> selectDmsCaseHistoryList(DmsCaseHistory dmsCaseHistory);

    /**
     * 新增病历
     * 
     * @param dmsCaseHistory 病历
     * @return 结果
     */
    public int insertDmsCaseHistory(DmsCaseHistory dmsCaseHistory);

    /**
     * 修改病历
     * 
     * @param dmsCaseHistory 病历
     * @return 结果
     */
    public int updateDmsCaseHistory(DmsCaseHistory dmsCaseHistory);

    /**
     * 删除病历
     * 
     * @param id 病历ID
     * @return 结果
     */
    public int deleteDmsCaseHistoryById(Long id);

    /**
     * 批量删除病历
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteDmsCaseHistoryByIds(Long[] ids);

    /**
     * 按照病人id查询病历
     *
     */

    public List selectPatIdHis(Long patientId);

}
