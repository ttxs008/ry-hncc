package com.yyaccp.hncc.dms.service;

import com.yyaccp.hncc.dms.domain.DmsCaseModelCatalog;

import java.util.List;

/**
 * 病例模版目录
 * Service接口
 *
 * @author ruoyi
 * @date 2020-08-15
 */
public interface IDmsCaseModelCatalogService {
    /**
     * 查询病例模版目录
     *
     * @param id 病例模版目录ID
     * @return 病例模版目录
     */
    public DmsCaseModelCatalog selectDmsCaseModelCatalogById(Long id);

    /**
     * 查询病例模版目录列表
     *
     * @param dmsCaseModelCatalog 病例模版目录
     * @return 病例模版目录集合
     */
    public List<DmsCaseModelCatalog> selectDmsCaseModelCatalogList(DmsCaseModelCatalog dmsCaseModelCatalog);

    /**
     * 新增病例模版目录
     *
     * @param dmsCaseModelCatalog 病例模版目录
     * @return 结果
     */
    public int insertDmsCaseModelCatalog(DmsCaseModelCatalog dmsCaseModelCatalog);

    /**
     * 修改病例模版目录
     *
     * @param dmsCaseModelCatalog 病例模版目录
     * @return 结果
     */
    public int updateDmsCaseModelCatalog(DmsCaseModelCatalog dmsCaseModelCatalog);

    /**
     * 批量删除病例模版目录
     *
     * @param ids 需要删除的病例模版目录ID
     * @return 结果
     */
    public int deleteDmsCaseModelCatalogByIds(Long[] ids);

    /**
     * 删除病例模版目录信息
     *
     * @param id 病例模版目录ID
     * @return 结果
     */
    public int deleteDmsCaseModelCatalogById(Long id);
}
