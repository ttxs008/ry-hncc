package com.yyaccp.hncc.dms.controller;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.yyaccp.hncc.common.util.BeanCopierUtil;
import com.yyaccp.hncc.common.vo.dms.DmsHerbalPrescriptionRecordVO;
import com.yyaccp.hncc.dms.domain.DmsHerbalPrescriptionRecord;
import com.yyaccp.hncc.dms.service.IDmsHerbalPrescriptionRecordService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

//com.yyaccp.hncc.common模块的包引入

/**
 * 草处方Controller
 *
 * @author ruoyi
 * @date 2020-09-07
 */
@Api(tags = "草处方")
@RestController
@RequestMapping("/dms/herbal_prescription_record")
public class DmsHerbalPrescriptionRecordController extends BaseController {
    @Autowired
    private IDmsHerbalPrescriptionRecordService dmsHerbalPrescriptionRecordService;

/**
 * 查询草处方列表
 *
 */
@PreAuthorize("@ss.hasPermi('dms:herbal_prescription_record:list')")
@GetMapping("/list")
@ApiOperation(value = "查询草处方" , notes = "查询所有草处方" ,
        code = 200, produces = "application/json" , protocols = "Http" ,
        response = TableDataInfo.class, httpMethod = "GET")
@ApiResponses({
        @ApiResponse(code = 204, message = "操作已经执行成功，但是没有返回数据"),
        @ApiResponse(code = 303, message = "重定向"),
        @ApiResponse(code = 400, message = "参数列表错误（缺少，格式不匹配）"),
        @ApiResponse(code = 500, message = "系统内部错误"),
        @ApiResponse(code = 404, message = "资源，服务未找到"),
        @ApiResponse(code = 200, message = "操作成功"),
        @ApiResponse(code = 401, message = "未授权"),
        @ApiResponse(code = 403, message = "访问受限，授权过期")
})
@ApiParam(name = "DmsHerbalPrescriptionRecordVO对象" , type = "DmsHerbalPrescriptionRecordVO")
        public TableDataInfo list(DmsHerbalPrescriptionRecordVO dmsHerbalPrescriptionRecordVO) {
        //将Vo转化为实体
        DmsHerbalPrescriptionRecord dmsHerbalPrescriptionRecord=BeanCopierUtil.copy(dmsHerbalPrescriptionRecordVO,DmsHerbalPrescriptionRecord. class);
        startPage();
        List<DmsHerbalPrescriptionRecord> list = dmsHerbalPrescriptionRecordService.selectDmsHerbalPrescriptionRecordList(dmsHerbalPrescriptionRecord);
        TableDataInfo tableDataInfo = getDataTable(list);
        //替换集合
        tableDataInfo.setRows(BeanCopierUtil.copy(tableDataInfo.getRows(), DmsHerbalPrescriptionRecordVO.class));
        return tableDataInfo;
    }
    
    /**
     * 导出草处方列表
     */
    @PreAuthorize("@ss.hasPermi('dms:herbal_prescription_record:export')")
    @Log(title = "草处方" , businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    @ApiOperation(value = "导出草处方表" , notes = "导出所有草处方" ,
            code = 200, produces = "application/json" , protocols = "Http" ,
            response = TableDataInfo.class, httpMethod = "GET")
    @ApiResponses({
            @ApiResponse(code = 204, message = "操作已经执行成功，但是没有返回数据"),
            @ApiResponse(code = 303, message = "重定向"),
            @ApiResponse(code = 400, message = "参数列表错误（缺少，格式不匹配）"),
            @ApiResponse(code = 500, message = "系统内部错误"),
            @ApiResponse(code = 404, message = "资源，服务未找到"),
            @ApiResponse(code = 200, message = "操作成功"),
            @ApiResponse(code = 401, message = "未授权"),
            @ApiResponse(code = 403, message = "访问受限，授权过期")
    })
    @ApiParam(name = "DmsHerbalPrescriptionRecordVO对象" , type = "DmsHerbalPrescriptionRecordVO")
    public AjaxResult export(DmsHerbalPrescriptionRecordVO dmsHerbalPrescriptionRecordVO) {
        //将VO转化为实体
        DmsHerbalPrescriptionRecord dmsHerbalPrescriptionRecord=BeanCopierUtil.copy(dmsHerbalPrescriptionRecordVO,DmsHerbalPrescriptionRecord. class);
        List<DmsHerbalPrescriptionRecordVO> list = BeanCopierUtil.copy(dmsHerbalPrescriptionRecordService.selectDmsHerbalPrescriptionRecordList(dmsHerbalPrescriptionRecord),DmsHerbalPrescriptionRecordVO.class);
        ExcelUtil<DmsHerbalPrescriptionRecordVO> util = new ExcelUtil<DmsHerbalPrescriptionRecordVO>(DmsHerbalPrescriptionRecordVO.class);
        return util.exportExcel(list, "herbal_prescription_record");
    }

    /**
     * 获取草处方详细信息
     */
    @PreAuthorize("@ss.hasPermi('dms:herbal_prescription_record:query')")
    @GetMapping(value = "/{id}")
    @ApiOperation(value = "获取草处方详细信息" ,
            notes = "根据草处方id获取科室信息" , code = 200,
            produces = "application/json" , protocols = "Http" , response = AjaxResult.class,
            httpMethod = "GET")
    @ApiResponses({
            @ApiResponse(code = 204, message = "操作已经执行成功，但是没有返回数据"),
            @ApiResponse(code = 303, message = "重定向"),
            @ApiResponse(code = 400, message = "参数列表错误（缺少，格式不匹配）"),
            @ApiResponse(code = 500, message = "系统内部错误"),
            @ApiResponse(code = 404, message = "资源，服务未找到"),
            @ApiResponse(code = 200, message = "操作成功"),
            @ApiResponse(code = 401, message = "未授权"),
            @ApiResponse(code = 403, message = "访问受限，授权过期")
    })
    @ApiParam(name = "dmsHerbalPrescriptionRecord.id" , type = "Long")
    public AjaxResult getInfo(@PathVariable("id") Long id) {
        return AjaxResult.success(BeanCopierUtil.copy(dmsHerbalPrescriptionRecordService.selectDmsHerbalPrescriptionRecordById(id),DmsHerbalPrescriptionRecordVO.class));
    }

    /**
     * 新增草处方
     */
    @PreAuthorize("@ss.hasPermi('dms:herbal_prescription_record:add')")
    @Log(title = "新增草处方" , businessType = BusinessType.INSERT)
    @PostMapping
    @ApiOperation(value = "新增草处方信息" , notes = "新增草处方信息" , code = 200,
            produces = "application/json" , protocols = "Http" , response = AjaxResult.class,
            httpMethod = "POST")
    @ApiResponses({
            @ApiResponse(code = 204, message = "操作已经执行成功，但是没有返回数据"),
            @ApiResponse(code = 303, message = "重定向"),
            @ApiResponse(code = 400, message = "参数列表错误（缺少，格式不匹配）"),
            @ApiResponse(code = 500, message = "系统内部错误"),
            @ApiResponse(code = 404, message = "资源，服务未找到"),
            @ApiResponse(code = 200, message = "操作成功"),
            @ApiResponse(code = 401, message = "未授权"),
            @ApiResponse(code = 403, message = "访问受限，授权过期")
    })
    @ApiParam(name = "DmsHerbalPrescriptionRecordVO对象" , type = "DmsHerbalPrescriptionRecordVO")
    public AjaxResult add(@RequestBody DmsHerbalPrescriptionRecordVO dmsHerbalPrescriptionRecordVO) {
        return toAjax(dmsHerbalPrescriptionRecordService.insertDmsHerbalPrescriptionRecord(BeanCopierUtil.copy(dmsHerbalPrescriptionRecordVO,DmsHerbalPrescriptionRecord. class)));
    }

    /**
     * 修改草处方
     */
    @PreAuthorize("@ss.hasPermi('dms:herbal_prescription_record:edit')")
    @Log(title = "草处方" , businessType = BusinessType.UPDATE)
    @PutMapping
    @ApiOperation(value = "修改草处方信息" , notes = "修改草处方信息" , code = 200,
            produces = "application/json" , protocols = "Http" , response = AjaxResult.class,
            httpMethod = "PUT")
    @ApiResponses({
            @ApiResponse(code = 204, message = "操作已经执行成功，但是没有返回数据"),
            @ApiResponse(code = 303, message = "重定向"),
            @ApiResponse(code = 400, message = "参数列表错误（缺少，格式不匹配）"),
            @ApiResponse(code = 500, message = "系统内部错误"),
            @ApiResponse(code = 404, message = "资源，服务未找到"),
            @ApiResponse(code = 200, message = "操作成功"),
            @ApiResponse(code = 401, message = "未授权"),
            @ApiResponse(code = 403, message = "访问受限，授权过期")
    })
    @ApiParam(name = "DmsHerbalPrescriptionRecordVO对象" , type = "DmsHerbalPrescriptionRecordVO")
    public AjaxResult edit(@RequestBody DmsHerbalPrescriptionRecordVO dmsHerbalPrescriptionRecordVO) {
        return toAjax(dmsHerbalPrescriptionRecordService.updateDmsHerbalPrescriptionRecord(BeanCopierUtil.copy(dmsHerbalPrescriptionRecordVO,DmsHerbalPrescriptionRecord. class)));
    }

    /**
     * 删除草处方
     */
    @PreAuthorize("@ss.hasPermi('dms:herbal_prescription_record:remove')")
    @Log(title = "草处方" , businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    @ApiOperation(value = "删除草处方信息" , notes = "删除草处方信息" , code = 200,
            produces = "application/json" , protocols = "Http" , response = AjaxResult.class,
            httpMethod = "DELETE")
    @ApiResponses({
            @ApiResponse(code = 204, message = "操作已经执行成功，但是没有返回数据"),
            @ApiResponse(code = 303, message = "重定向"),
            @ApiResponse(code = 400, message = "参数列表错误（缺少，格式不匹配）"),
            @ApiResponse(code = 500, message = "系统内部错误"),
            @ApiResponse(code = 404, message = "资源，服务未找到"),
            @ApiResponse(code = 200, message = "操作成功"),
            @ApiResponse(code = 401, message = "未授权"),
            @ApiResponse(code = 403, message = "访问受限，授权过期")
    })
    @ApiParam(name = "dmsHerbalPrescriptionRecord.id", type = "Long[]")
    public AjaxResult remove(@PathVariable Long[] ids) {
        return toAjax(dmsHerbalPrescriptionRecordService.deleteDmsHerbalPrescriptionRecordByIds(ids));
    }

    /**
     * 修改草处方
     */
    @PreAuthorize("@ss.hasPermi('dms:herbal_prescription_record:edits')")
    @Log(title = "草处方", businessType = BusinessType.UPDATE)
    @PutMapping("/edits")
    @ApiOperation(value = "修改草处方信息(发药)", notes = "修改草处方信息(发药)", code = 200,
            produces = "application/json", protocols = "Http", response = AjaxResult.class,
            httpMethod = "PUT")
    @ApiResponses({
            @ApiResponse(code = 204, message = "操作已经执行成功，但是没有返回数据"),
            @ApiResponse(code = 303, message = "重定向"),
            @ApiResponse(code = 400, message = "参数列表错误（缺少，格式不匹配）"),
            @ApiResponse(code = 500, message = "系统内部错误"),
            @ApiResponse(code = 404, message = "资源，服务未找到"),
            @ApiResponse(code = 200, message = "操作成功"),
            @ApiResponse(code = 401, message = "未授权"),
            @ApiResponse(code = 403, message = "访问受限，授权过期")
    })
    public AjaxResult edit(@RequestBody Map<String, Object> data) {
        List<Long> ids = (List<Long>) data.get("ids");
        //List去重
        List<Long> newids = ids.stream().distinct().collect(Collectors.toList());
        Integer status = (Integer) data.get("status");
        return toAjax(dmsHerbalPrescriptionRecordService.batchRevisionOfDraftPrescriptions(newids, status));
    }
}
