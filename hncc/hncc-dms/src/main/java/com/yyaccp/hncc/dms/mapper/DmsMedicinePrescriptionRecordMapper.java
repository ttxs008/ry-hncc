package com.yyaccp.hncc.dms.mapper;

import com.yyaccp.hncc.dms.domain.DmsMedicinePrescriptionRecord;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 成药处方Mapper接口
 *
 * @author ruoyi
 * @date 2020-08-26
 */
public interface DmsMedicinePrescriptionRecordMapper {
    /**
     * 查询成药处方
     *
     * @param id 成药处方ID
     * @return 成药处方
     */
    public DmsMedicinePrescriptionRecord selectDmsMedicinePrescriptionRecordById(Long id);

    /**
     * 查询成药处方列表
     *
     * @param dmsMedicinePrescriptionRecord 成药处方
     * @return 成药处方集合
     */
    public List<DmsMedicinePrescriptionRecord> selectDmsMedicinePrescriptionRecordList(DmsMedicinePrescriptionRecord dmsMedicinePrescriptionRecord);

    /**
     * 新增成药处方
     *
     * @param dmsMedicinePrescriptionRecord 成药处方
     * @return 结果
     */
    public int insertDmsMedicinePrescriptionRecord(DmsMedicinePrescriptionRecord dmsMedicinePrescriptionRecord);

    /**
     * 修改成药处方
     *
     * @param dmsMedicinePrescriptionRecord 成药处方
     * @return 结果
     */
    public int updateDmsMedicinePrescriptionRecord(DmsMedicinePrescriptionRecord dmsMedicinePrescriptionRecord);

    /**
     * 删除成药处方
     *
     * @param id 成药处方ID
     * @return 结果
     */
    public int deleteDmsMedicinePrescriptionRecordById(Long id);

    /**
     * 批量删除成药处方
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteDmsMedicinePrescriptionRecordByIds(Long[] ids);

    /**
     *  批量修改成药处方的状态
     * @param ids ID数组
     * @param status  状态
     * @return 结果
     */
    public int updateDmsMedicinePrescriptionRecordStatusByIds(@Param("ids") List<Long> ids, @Param("status")Integer status);
}
