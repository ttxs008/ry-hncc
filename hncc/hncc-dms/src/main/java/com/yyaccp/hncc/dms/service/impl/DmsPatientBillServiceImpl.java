package com.yyaccp.hncc.dms.service.impl;

import com.yyaccp.hncc.dms.domain.DmsPatientBill;
import com.yyaccp.hncc.dms.mapper.DmsPatientBillMapper;
import com.yyaccp.hncc.dms.service.IDmsPatientBillService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by 余归 on 2020/8/24
 *
 * @author 余归
 */
@Service
public class DmsPatientBillServiceImpl implements IDmsPatientBillService {

    @Resource
    private DmsPatientBillMapper dmsPatientBillMapper;

    @Override
    public List<DmsPatientBill> selectPatientBills(Integer registrationId) {
        List<DmsPatientBill> dmsPatientBills = new ArrayList<>();
        dmsPatientBills.addAll(dmsPatientBillMapper.selectNonDrugs(registrationId));
        dmsPatientBills.addAll(dmsPatientBillMapper.selectHerbalDrugs(registrationId));
        dmsPatientBills.addAll(dmsPatientBillMapper.selectMedicineDrugs(registrationId));
        return dmsPatientBills;
    }
}
