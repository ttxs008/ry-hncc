package com.yyaccp.hncc.dms.controller;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.yyaccp.hncc.common.util.BeanCopierUtil;
import com.yyaccp.hncc.common.vo.dms.DmsDiseCatalogVO;
import com.yyaccp.hncc.dms.domain.DmsDiseCatalog;
import com.yyaccp.hncc.dms.service.IDmsDiseCatalogService;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 诊断类型（疾病）目录Controller
 * 
 * @author ruoyi
 * @date 2020-08-12
 */
@RestController
@RequestMapping("/dms/diseCatalog")
@Api(tags = "诊断类型（疾病）目录")
public class DmsDiseCatalogController extends BaseController
{
    @Autowired
    private IDmsDiseCatalogService dmsDiseCatalogService;

    /**
     * 查询诊断类型（疾病）目录列表
     */
    @ApiOperation(value = "查询",notes = "查询目录列表",response = TableDataInfo.class)
    @ApiResponses({
            @ApiResponse(code = 204,message = "操作已经执行成功，但是没有返回数据"),
            @ApiResponse(code = 303,message = "重定向"),
            @ApiResponse(code = 400,message = "参数列表错误（缺少，格式不匹配）"),
            @ApiResponse(code = 500,message = "系统内部错误"),
            @ApiResponse(code = 404,message = "资源，服务未找到"),
            @ApiResponse(code = 200,message = "操作成功"),
            @ApiResponse(code = 401,message = "未授权"),
            @ApiResponse(code = 403,message = "访问受限，授权过期")
    })
    @PreAuthorize("@ss.hasPermi('dms:diseCatalog:list')")
    @GetMapping("/list")
    public TableDataInfo list(DmsDiseCatalogVO dmsDiseCatalogVO)
    {
        startPage();
        List<DmsDiseCatalog> list = dmsDiseCatalogService
                                                        //将实体vo转换成 对应的实体
                .selectDmsDiseCatalogList(BeanCopierUtil.copy(dmsDiseCatalogVO,DmsDiseCatalog.class));
        // 先获取总行数
        TableDataInfo tableDataInfo = getDataTable(list);
        // 替换 vo 集合
        tableDataInfo.setRows(BeanCopierUtil.copy(list,DmsDiseCatalogVO.class));
        return tableDataInfo;
    }

    /**
     * 导出诊断类型（疾病）目录列表
     */
    @ApiOperation(value = "导出",notes = "导出目录列表",response = AjaxResult.class)
    @ApiResponses({
            @ApiResponse(code = 204,message = "操作已经执行成功，但是没有返回数据"),
            @ApiResponse(code = 303,message = "重定向"),
            @ApiResponse(code = 400,message = "参数列表错误（缺少，格式不匹配）"),
            @ApiResponse(code = 500,message = "系统内部错误"),
            @ApiResponse(code = 404,message = "资源，服务未找到"),
            @ApiResponse(code = 200,message = "操作成功"),
            @ApiResponse(code = 401,message = "未授权"),
            @ApiResponse(code = 403,message = "访问受限，授权过期")
    })
    @PreAuthorize("@ss.hasPermi('dms:diseCatalog:export')")
    @Log(title = "诊断类型（疾病）目录", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(DmsDiseCatalogVO dmsDiseCatalogVO)
    {
        List<DmsDiseCatalog> list = dmsDiseCatalogService.selectDmsDiseCatalogList(BeanCopierUtil.copy(dmsDiseCatalogVO,DmsDiseCatalog.class));
        ExcelUtil<DmsDiseCatalogVO> util = new ExcelUtil<DmsDiseCatalogVO>(DmsDiseCatalogVO.class);
        List<DmsDiseCatalogVO> vos = BeanCopierUtil.copy(list,DmsDiseCatalogVO.class);
        return util.exportExcel(vos, "diseCatalog");
    }

    /**
     * 获取诊断类型（疾病）目录详细信息
     */
    @ApiOperation(value = "详细信息",notes = "根据目录Id获取目录详情",response = AjaxResult.class)
    @ApiResponses({
            @ApiResponse(code = 204,message = "操作已经执行成功，但是没有返回数据"),
            @ApiResponse(code = 303,message = "重定向"),
            @ApiResponse(code = 400,message = "参数列表错误（缺少，格式不匹配）"),
            @ApiResponse(code = 500,message = "系统内部错误"),
            @ApiResponse(code = 404,message = "资源，服务未找到"),
            @ApiResponse(code = 200,message = "操作成功"),
            @ApiResponse(code = 401,message = "未授权"),
            @ApiResponse(code = 403,message = "访问受限，授权过期")
    })
    @PreAuthorize("@ss.hasPermi('dms:diseCatalog:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") @ApiParam(value = "目录id",name ="id" ,defaultValue = "1",required = true) Long id)
    {
        return AjaxResult.success(BeanCopierUtil.copy(dmsDiseCatalogService.selectDmsDiseCatalogById(id),DmsDiseCatalog.class));
    }

    /**
     * 新增诊断类型（疾病）目录
     */
    @ApiOperation(value = "新增",notes = "新增一个目录,id可以不用输入",response = AjaxResult.class)
    @ApiResponses({
            @ApiResponse(code = 204,message = "操作已经执行成功，但是没有返回数据"),
            @ApiResponse(code = 303,message = "重定向"),
            @ApiResponse(code = 400,message = "参数列表错误（缺少，格式不匹配）"),
            @ApiResponse(code = 500,message = " 系统内部错误"),
            @ApiResponse(code = 404,message = " 资源，服务未找到"),
            @ApiResponse(code = 200,message = " 操作成功")
    })
    @PreAuthorize("@ss.hasPermi('dms:diseCatalog:add')")
    @Log(title = "诊断类型（疾病）目录", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody @ApiParam(value = "DmsDiseCatalogVO", name ="诊断目录对象VO",type = "DmsDiseCatalogVO") DmsDiseCatalogVO dmsDiseCatalogVO)
    {
        return toAjax(dmsDiseCatalogService.insertDmsDiseCatalog(BeanCopierUtil.copy(dmsDiseCatalogVO,DmsDiseCatalog.class)));
    }

    /**
     * 修改诊断类型（疾病）目录
     */
    @ApiOperation(value = "修改",notes = "修改目录,id必须输入",response = AjaxResult.class)
    @ApiResponses({
            @ApiResponse(code = 204,message = "操作已经执行成功，但是没有返回数据"),
            @ApiResponse(code = 303,message = "重定向"),
            @ApiResponse(code = 400,message = "参数列表错误（缺少，格式不匹配）"),
            @ApiResponse(code = 500,message = " 系统内部错误"),
            @ApiResponse(code = 404,message = " 资源，服务未找到"),
            @ApiResponse(code = 200,message = " 操作成功")
    })
    @PreAuthorize("@ss.hasPermi('dms:diseCatalog:edit')")
    @Log(title = "诊断类型（疾病）目录", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody @ApiParam(value = "DmsDiseCatalogVO", name ="诊断目录对象VO",type = "DmsDiseCatalogVO") DmsDiseCatalogVO dmsDiseCatalogVO)
    {
        return toAjax(dmsDiseCatalogService.updateDmsDiseCatalog(BeanCopierUtil.copy(dmsDiseCatalogVO,DmsDiseCatalog.class)));
    }

    /**
     * 删除诊断类型（疾病）目录
     */
    @ApiOperation(value = "删除",notes = "根据目录id删除目录",response = AjaxResult.class)
    @ApiResponses({
            @ApiResponse(code = 204,message = "操作已经执行成功，但是没有返回数据"),
            @ApiResponse(code = 303,message = "重定向"),
            @ApiResponse(code = 400,message = "参数列表错误（缺少，格式不匹配）"),
            @ApiResponse(code = 500,message = " 系统内部错误"),
            @ApiResponse(code = 404,message = " 资源，服务未找到"),
            @ApiResponse(code = 200,message = " 操作成功")
    })
    @PreAuthorize("@ss.hasPermi('dms:diseCatalog:remove')")
    @Log(title = "诊断类型（疾病）目录", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable @ApiParam(value = "ids,目录id可以是数组。删除时目录内的疾病都会被删除。",name ="ids" ,defaultValue = "1",required = true) Long[] ids)

    {
        return toAjax(dmsDiseCatalogService.deleteDmsDiseCatalogByIds(ids));
    }
}
