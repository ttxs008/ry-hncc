package com.yyaccp.hncc.dms.domain;

import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModel;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * 诊断类型(疾病)管理对象 dms_dise
 * 
 * @author ruoyi
 * @date 2020-08-12
 */
public class DmsDise extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** id */
    private Long id;

    /** 目录id */
    @Excel(name = "目录id")
    private Long catId;

    /** 疾病编码 */
    @Excel(name = "疾病编码")
    private String code;

    /** 疾病名称 */
    @Excel(name = "疾病名称")
    private String name;

    /** ICD编码 */
    @Excel(name = "ICD编码")
    private String icd;

    /** 状态 */
    @Excel(name = "状态")
    private Integer status;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setCatId(Long catId) 
    {
        this.catId = catId;
    }

    public Long getCatId() 
    {
        return catId;
    }
    public void setCode(String code) 
    {
        this.code = code;
    }

    public String getCode() 
    {
        return code;
    }
    public void setName(String name) 
    {
        this.name = name;
    }

    public String getName() 
    {
        return name;
    }
    public void setIcd(String icd) 
    {
        this.icd = icd;
    }

    public String getIcd() 
    {
        return icd;
    }
    public void setStatus(Integer status) 
    {
        this.status = status;
    }

    public Integer getStatus() 
    {
        return status;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("catId", getCatId())
            .append("code", getCode())
            .append("name", getName())
            .append("icd", getIcd())
            .append("status", getStatus())
            .toString();
    }
}
