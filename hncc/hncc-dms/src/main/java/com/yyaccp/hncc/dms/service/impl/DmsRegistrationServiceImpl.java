package com.yyaccp.hncc.dms.service.impl;

import java.math.BigDecimal;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.SecurityUtils;
import com.yyaccp.hncc.bms.domain.BmsBillsRecord;
import com.yyaccp.hncc.bms.domain.BmsInvoiceRecord;
import com.yyaccp.hncc.bms.mapper.BmsBillsRecordMapper;
import com.yyaccp.hncc.bms.mapper.BmsInvoiceRecordMapper;
import com.yyaccp.hncc.common.HnccStatus;
import com.yyaccp.hncc.common.HnccType;
import com.yyaccp.hncc.common.util.AgeUtil;
import com.yyaccp.hncc.common.util.DateToNoUtil;
import com.yyaccp.hncc.common.vo.dms.DmsHandleRefundVo;
import com.yyaccp.hncc.common.vo.dms.DmsPayVo;
import com.yyaccp.hncc.common.vo.dms.DmsPaymentVo;
import com.yyaccp.hncc.common.vo.dms.DmsRefundVo;
import com.yyaccp.hncc.dms.mapper.DmsHerbalPrescriptionRecordMapper;
import com.yyaccp.hncc.dms.mapper.DmsMedicinePrescriptionRecordMapper;
import com.yyaccp.hncc.dms.mapper.DmsNonDrugItemRecordMapper;
import com.yyaccp.hncc.pms.domain.PmsPatient;
import com.yyaccp.hncc.pms.mapper.PmsPatientMapper;
import com.yyaccp.hncc.sms.domain.SmsSkd;
import com.yyaccp.hncc.sms.mapper.SmsSkdMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.yyaccp.hncc.dms.mapper.DmsRegistrationMapper;
import com.yyaccp.hncc.dms.domain.DmsRegistration;
import com.yyaccp.hncc.dms.service.IDmsRegistrationService;
import org.springframework.transaction.annotation.Transactional;

import static java.util.stream.Collectors.*;

/**
 * 就诊(门诊)信息Service业务层处理
 * 
 * @author ruoyi
 * @date 2020-08-20
 */
@Service
public class DmsRegistrationServiceImpl  implements IDmsRegistrationService
{
    @Autowired
    private DmsRegistrationMapper dmsRegistrationMapper;

    @Autowired
    private SmsSkdMapper skdMapper;

    @Autowired
    private PmsPatientMapper patientMapper;

    @Autowired
    private BmsBillsRecordMapper billsRecordMapper;

    @Autowired
    private BmsInvoiceRecordMapper invoiceRecordMapper;

    @Autowired
    private DmsNonDrugItemRecordMapper nonDrugItemRecordMapper;

    @Autowired
    private DmsHerbalPrescriptionRecordMapper herbalPrescriptionRecordMapper;

    @Autowired
    private DmsMedicinePrescriptionRecordMapper medicinePrescriptionRecordMapper;

    /**
     * 查询就诊(门诊)信息
     * 
     * @param id 就诊(门诊)信息ID
     * @return 就诊(门诊)信息
     */
    @Override
    public DmsRegistration selectDmsRegistrationById(Long id)
    {
        return dmsRegistrationMapper.selectDmsRegistrationById(id);
    }

    /**
     * 查询就诊(门诊)信息列表
     * 
     * @param dmsRegistration 就诊(门诊)信息
     * @return 就诊(门诊)信息
     */
    @Override
    public List<DmsRegistration> selectDmsRegistrationList(DmsRegistration dmsRegistration)
    {
        return dmsRegistrationMapper.selectDmsRegistrationList(dmsRegistration);
    }

    /**
     * 新增就诊(门诊)信息
     * 
     * @param dmsRegistration 就诊(门诊)信息
     * @return 结果
     */
    @Override
    public int insertDmsRegistration(DmsRegistration dmsRegistration)
    {
        dmsRegistration.setCreateTime(DateUtils.getNowDate());
        return dmsRegistrationMapper.insertDmsRegistration(dmsRegistration);
    }

    /**
     * 修改就诊(门诊)信息
     * 
     * @param dmsRegistration 就诊(门诊)信息
     * @return 结果
     */
    @Override
    public int updateDmsRegistration(DmsRegistration dmsRegistration)
    {
        return dmsRegistrationMapper.updateDmsRegistration(dmsRegistration);
    }

    /**
     * 批量删除就诊(门诊)信息
     * 
     * @param ids 需要删除的就诊(门诊)信息ID
     * @return 结果
     */
    @Override
    public int deleteDmsRegistrationByIds(Long[] ids)
    {
        return dmsRegistrationMapper.deleteDmsRegistrationByIds(ids);
    }

    /**
     * 删除就诊(门诊)信息信息
     * 
     * @param id 就诊(门诊)信息ID
     * @return 结果
     */
    @Override
    public int deleteDmsRegistrationById(Long id)
    {
        return dmsRegistrationMapper.deleteDmsRegistrationById(id);
    }



    /***
     * 查询就诊(门诊)信息(一个病人一个病历号，多次就诊，多次就诊信息)列表
     * 根据用户的姓名或病例号
     * @param name  用户名
     * @param medicalRecordNo  病历号
     * @return
     */
    @Override
    public List<DmsRegistration> selectListByNameAndMedicalRecordNo(String name, String medicalRecordNo) {
        return dmsRegistrationMapper.selectListByNameAndMedicalRecordNo(name, medicalRecordNo);
    }

    /**
     * 1.对应日期医生挂号限额减一      sms_skd  排班时间表  优先
     * 1.新增患者（非读卡）  pms_patient   病人基本信息表
     * 1.新增挂号     dms_registration  就诊(门诊)信息表
     * 2.新增发票信息   bms_invoice_record 发票表
     * 3.新增记账流水  bms_bills_record   医院账单流水表
     *
     * @param dmsRegistration 就诊(门诊)信息(一个病人一个病历号，多次就诊，多次就诊信息)
     * @return 结果
     */
    @Override
    public int handleRegistration(DmsRegistration dmsRegistration, BigDecimal price, Long invoiceNo, Long catId, Long staffId) {
        PmsPatient pmsPatient = dmsRegistration.getPmsPatient();
        // 判断用户是否已入库
        if (pmsPatient.getId() == null) {
            //  获取病人表病历号
            String recordNo = patientMapper.selectMaxMedicalRecordNo(DateUtils.getDate());
            //  生成病人病历号
            String no = DateToNoUtil.DateToNoAndIncrease(DateToNoUtil.YYYYMMDDHHMMSS,2,recordNo);
            pmsPatient.setMedicalRecordNo(no);
            patientMapper.insertPmsPatient(pmsPatient);
        }
        return this.handleRegistrationDetails(dmsRegistration, price, invoiceNo, catId, staffId);
    }

    /**
     * 1.对应日期医生挂号限额减一      sms_skd  排班时间表  优先
     * 1.新增挂号     dms_registration  就诊(门诊)信息表
     * 2.新增发票信息   bms_invoice_record 发票表
     * 3.新增记账流水  bms_bills_record   医院账单流水表
     * 隔开 患者的插入操作  避免数据回退时 患者表也一起回退
     *
     * @param dmsRegistration 就诊(门诊)信息(一个病人一个病历号，多次就诊，多次就诊信息)
     * @param invoiceNo       发票号
     * @param price           金额
     * @param catId           挂号级别id
     * @param staffId         看诊医生id
     * @return 结果
     */

    @Transactional(rollbackFor = Exception.class)
    @Override
    public int handleRegistrationDetails(DmsRegistration dmsRegistration, BigDecimal price, Long invoiceNo, Long catId, Long staffId) {
        SmsSkd smsSkd = skdMapper.selectSmsSkdById(dmsRegistration.getSkdId());
        int skdFlag = 0;
        int registrationFlag = 0;
        int billsRecordFlag = 0;
        int invoiceRecordFlag = 0;
        if (smsSkd.getSkLimit() >= 1) {
            // 修改限额
            smsSkd.setSkLimit(smsSkd.getSkLimit() - 1);
            skdFlag = skdMapper.updateSmsSkd(smsSkd);
            // 新增挂号
            PmsPatient pmsPatient = dmsRegistration.getPmsPatient();
            // 患者id
            dmsRegistration.setPatientId(pmsPatient.getId());
            // 状态
            dmsRegistration.setStatus(HnccStatus.Registration.NOT_VISIT.getValue());
            // 创建时间
            dmsRegistration.setCreateTime(DateUtils.getNowDate());
            // 绑定状态
            dmsRegistration.setBindStatus(1);
            // 就诊年龄
            dmsRegistration.setPatientAgeStr(AgeUtil.getAgeByBirth(pmsPatient.getDateOfBirth()) + "岁");
            registrationFlag = dmsRegistrationMapper.insertDmsRegistration(dmsRegistration);


            // 新增医院账单流水
            BmsBillsRecord bmsBillsRecord = new BmsBillsRecord();
            // 流水号处理
            // 获取最大流水号
            String maxBillNo = billsRecordMapper.selectMaxBillNo(DateUtils.getDate());
            bmsBillsRecord.setBillNo(DateToNoUtil.DateToNoAndIncrease(DateToNoUtil.YYYYMMDDHHMM,4,maxBillNo));
            // 创建时间
            bmsBillsRecord.setCreateTime(DateUtils.getNowDate());
            // 状态
            bmsBillsRecord.setStatus(1);
            //发票数量
            bmsBillsRecord.setInvoiceNum(1);
            // 挂号id
            bmsBillsRecord.setRegistrationId(dmsRegistration.getId());
            // 记录列表 格式 5,0><  表的id,表的类型><
            bmsBillsRecord.setRecordList(dmsRegistration.getId() + ",0><");
            billsRecordFlag = billsRecordMapper.insertBmsBillsRecord(bmsBillsRecord);


            // 新增发票信息
            BmsInvoiceRecord bmsInvoiceRecord = new BmsInvoiceRecord();
            // 创建时间
            bmsInvoiceRecord.setCreateTime(DateUtils.getNowDate());
            // 发票号
            bmsInvoiceRecord.setInvoiceNo(invoiceNo);
            // 账单id
            bmsInvoiceRecord.setBillId(bmsBillsRecord.getId());
            //  总金额
            bmsInvoiceRecord.setAmount(price);
            // 对账人id 对应员工id
            bmsInvoiceRecord.setOperatorId(SecurityUtils.getLoginUser().getUser().getUserId());
            //  挂号级别id
            bmsInvoiceRecord.setSettlementCatId(catId);
            // 记录列表   5,0,16><  表数据id,表的类型,金钱
            bmsInvoiceRecord.setItemList(dmsRegistration.getId() + ",0," + price + "><");
            bmsInvoiceRecord.setType(HnccType.Invoice.NORMAL.getValue());
            invoiceRecordFlag = invoiceRecordMapper.insertBmsInvoiceRecord(bmsInvoiceRecord);
        }
        // 全部操作成功则返回
        if (skdFlag > 0 && registrationFlag > 0 && billsRecordFlag > 0 && invoiceRecordFlag > 0) {
            return 1;
        }
        return 0;
    }


    /**
     *  退号处理
     * @param id  发票id
     * @param registrationId 挂号Id
     * @param skdId    排班Id
     * @return 结果
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public int handleBounce(Long id, Long registrationId, Long skdId) {
        // 修改 原发票状态
       BmsInvoiceRecord invoiceRecord = new BmsInvoiceRecord();
       invoiceRecord.setId(id);
       invoiceRecord.setType(HnccType.Invoice.REFUND.getValue());
       int invoiceUpdate=invoiceRecordMapper.updateBmsInvoiceRecord(invoiceRecord);

        // 修改 原挂号状态
       DmsRegistration registration = new DmsRegistration();
       registration.setId(registrationId);
       registration.setStatus(HnccStatus.Registration.YES_BOUNCE.getValue());
       int registrationUpdate = dmsRegistrationMapper.updateDmsRegistration(registration);

        // 修改 原排班限额
       SmsSkd smsSkd = skdMapper.selectSmsSkdById(skdId);
       smsSkd.setSkLimit(smsSkd.getSkLimit()+1);
       int skdUpdate =skdMapper.updateSmsSkd(smsSkd);

       // 增加冲红发票
       BmsInvoiceRecord redInvoiceRecord= invoiceRecordMapper.selectBmsInvoiceRecordById(id);
       redInvoiceRecord.setId(null);
       redInvoiceRecord.setOperatorId(SecurityUtils.getLoginUser().getUser().getUserId());
       redInvoiceRecord.setAmount(redInvoiceRecord.getAmount().negate());
       redInvoiceRecord.setAssociateId(id);
       redInvoiceRecord.setSettlementCatId(null);
       redInvoiceRecord.setItemList(null);
       redInvoiceRecord.setType(HnccType.Invoice.RED.getValue());
       redInvoiceRecord.setCreateTime(DateUtils.getNowDate());
       int invoiceInsert= invoiceRecordMapper.insertBmsInvoiceRecord(redInvoiceRecord);
       if(invoiceUpdate>0 && registrationUpdate>0&& skdUpdate>0&& invoiceInsert>0){
           return 1;
       }
       return 0;
    }

    /**
     *  查询缴费列表
     *
     * @param registerId 挂号Id
     * @return 缴费列表集合
     */
    @Override
    public List<HashMap<String, Object>> payList(Long registerId) {
        return dmsRegistrationMapper.selectPayList(registerId);
    }


    /**
     *  处理缴费
     * @param dmsPaymentVo 缴费对象
     * @return  结果
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public int handlePayment(DmsPaymentVo dmsPaymentVo) {

        //  判断操作是否成功
        AtomicInteger handleFlag= new AtomicInteger(1);
        /*---------------------------------------------------------------参数处理---------------------------------------*/
         // 发票收费项 需要金额 格式 id,表类型,金额><
        StringBuilder invoiceItemList= new StringBuilder();
        //  账单流水收费项 格式 id,表类型><
        StringBuilder billsRecordList= new StringBuilder();

        // 数据分组 按类型分类获得缴费项ID
        Map<Integer,List<Long>> itemGroups = dmsPaymentVo.getPayList().stream().collect(groupingBy(DmsPayVo::getType,mapping(DmsPayVo::getId,toList())));
        // 获得item_list
        dmsPaymentVo.getPayList().forEach(payVo -> {
            invoiceItemList.append(payVo.getId()).append(",").append(payVo.getType()).append(",").append(payVo.getAmount()).append("><");
            billsRecordList.append(payVo.getId()).append(",").append(payVo.getType()).append("><");
        });

        /*---------------------------------------------------------------更新xx项记录表的状态---------------------------------------*/
        // 遍历 idsGroups过 滤空的ids,更新xx项记录表的状态
        itemGroups.forEach((type,ids)->{
            // 修改 检查检验处置(非药品)  状态
            if(type== HnccType.NonDrug.CHECK.getValue() || type == HnccType.NonDrug.TEST.getValue() || type== HnccType.NonDrug.DISPOSITION.getValue()){
                handleFlag.set(nonDrugItemRecordMapper.updateDmsNonDrugItemRecordStatusByIds(ids, HnccStatus.Prescription.PAID.getValue()));
            } else if(type == HnccType.Prescription.MEDICINE_PRESCRIPTION.getValue()){
                // 修改  成药 状态
                handleFlag.set(medicinePrescriptionRecordMapper.updateDmsMedicinePrescriptionRecordStatusByIds(ids, HnccStatus.DrugItem.PAID.getValue()));
            }else if(type == HnccType.Prescription.HERBAL_PRESCRIPTION.getValue()){
                // 修改 草药  状态
                handleFlag.set(herbalPrescriptionRecordMapper.updateDmsHerbalPrescriptionRecordStatusByIds(ids, HnccStatus.DrugItem.PAID.getValue()));
            }
        });
        /*------------------------------------------------------------插入流水表的信息----------------------------------*/

        // 新增医院账单流水
        BmsBillsRecord bmsBillsRecord = new BmsBillsRecord();
        // 流水号处理
        // 获取最大流水号
        String maxBillNo = billsRecordMapper.selectMaxBillNo(DateUtils.getDate());
        bmsBillsRecord.setBillNo(DateToNoUtil.DateToNoAndIncrease(DateToNoUtil.YYYYMMDDHHMM,4,maxBillNo));
        // 创建时间
        bmsBillsRecord.setCreateTime(DateUtils.getNowDate());
        // 状态
        bmsBillsRecord.setStatus(1);
        //发票数量
        bmsBillsRecord.setInvoiceNum(1);
        // 挂号id
        bmsBillsRecord.setRegistrationId(dmsPaymentVo.getRegisterId());
        // 记录列表 格式 5,0><  表的id,表的类型><
        bmsBillsRecord.setRecordList(billsRecordList.toString());
        handleFlag.set(billsRecordMapper.insertBmsBillsRecord(bmsBillsRecord));
        /*------------------------------------------------------------插入发票表---------------------------------------*/
        // 新增发票信息
        BmsInvoiceRecord bmsInvoiceRecord = new BmsInvoiceRecord();
        // 创建时间
        bmsInvoiceRecord.setCreateTime(DateUtils.getNowDate());
        // 发票号
        bmsInvoiceRecord.setInvoiceNo(dmsPaymentVo.getInvoiceNo());
        // 账单id
        bmsInvoiceRecord.setBillId(bmsBillsRecord.getId());
        //  总金额
        bmsInvoiceRecord.setAmount(dmsPaymentVo.getAmount());
        // 对账人id 对应当前登录的用户ID
        bmsInvoiceRecord.setOperatorId(SecurityUtils.getLoginUser().getUser().getUserId());
        //  挂号级别id
        bmsInvoiceRecord.setSettlementCatId(dmsPaymentVo.getCatId());
        // 记录列表   5,0,16><  表数据id,表的类型,金钱
        bmsInvoiceRecord.setItemList(invoiceItemList.toString());
        bmsInvoiceRecord.setType(HnccType.Invoice.NORMAL.getValue());
        handleFlag.set(invoiceRecordMapper.insertBmsInvoiceRecord(bmsInvoiceRecord));

        return handleFlag.get();
    }


    /**
     *  查询退费列表
     *
     * @param registerId 挂号Id
     * @return 退费列表集合
     */
    @Override
    public List<HashMap<String, Object>> refundList(Long registerId) {
        return dmsRegistrationMapper.selectRefundList(registerId);
    }

    /**
     * 处理退费
     * @param  handleRefundVo 退费对象
     * @return  结果
     */
    @Override
    public int handleRefund(DmsHandleRefundVo handleRefundVo) {
        //  判断操作是否成功
        AtomicInteger handleFlag= new AtomicInteger(1);
        // 按发票Id将数据分组
        Map<Long,List<DmsRefundVo>> invoiceGroups = handleRefundVo.getRefundList().stream().collect(groupingBy(DmsRefundVo::getInvoiceId));
        invoiceGroups.forEach((id,items)->{
            // 获得 发票对象
            BmsInvoiceRecord invoiceRecord =invoiceRecordMapper.selectBmsInvoiceRecordById(id);
            // 原发票 退费项
            List<DmsRefundVo>  originalItemList =  Arrays.stream(invoiceRecord.getItemList().split("><")).map(s -> {
                 String[] strings= s.split(",");
                DmsRefundVo dmsRefundVo  = new DmsRefundVo();
                dmsRefundVo.setRefundId(Long.valueOf(strings[0]));
                dmsRefundVo.setType(Integer.valueOf(strings[1]));
                dmsRefundVo.setAmount(new BigDecimal(strings[2]));
                    return  dmsRefundVo;
            }).collect(Collectors.toList());

            // 修改发票状态
            invoiceRecord.setType(HnccType.Invoice.REFUND.getValue());
            handleFlag.set(invoiceRecordMapper.updateBmsInvoiceRecord(invoiceRecord));
            /*----------------------------------------- 修改对应的XX 项------------------------------------------*/
            // 发票收费项 需要金额 格式 id,表类型,金额><
            StringBuilder invoiceItemList= new StringBuilder();
            // 数据分组 按类型分类获得缴费项ID
            Map<Integer,List<Long>> itemGroups = items.stream().collect(groupingBy(DmsRefundVo::getType,mapping(DmsRefundVo::getRefundId,toList())));
            items.forEach(refundVo -> {
                invoiceItemList.append(refundVo.getRefundId()).append(",").append(refundVo.getType()).append(",").append(refundVo.getAmount()).append("><");
            });
            // 遍历 idsGroups过 滤空的ids,更新xx项记录表的状态
            itemGroups.forEach((type,ids)->{
                // 修改 检查检验处置(非药品)  状态
                if(type== HnccType.NonDrug.CHECK.getValue() || type == HnccType.NonDrug.TEST.getValue() || type== HnccType.NonDrug.DISPOSITION.getValue()){
                    handleFlag.set(nonDrugItemRecordMapper.updateDmsNonDrugItemRecordStatusByIds(ids, HnccStatus.Prescription.REFUNDED.getValue()));
                } else if(type == HnccType.Prescription.MEDICINE_PRESCRIPTION.getValue()){
                    // 修改  成药 状态
                    handleFlag.set(medicinePrescriptionRecordMapper.updateDmsMedicinePrescriptionRecordStatusByIds(ids, HnccStatus.NonDrugItem.REFUNDED.getValue()));
                }else if(type == HnccType.Prescription.HERBAL_PRESCRIPTION.getValue()){
                    // 修改 草药  状态
                    handleFlag.set(herbalPrescriptionRecordMapper.updateDmsHerbalPrescriptionRecordStatusByIds(ids, HnccStatus.NonDrugItem.REFUNDED.getValue()));
                }
            });
            /*----------------------------------------- 插入冲红发票------------------------------------------*/
            // 获取每一个发票下退费项的总金额
            BigDecimal totalAmount=  items.stream().map(DmsRefundVo::getAmount).reduce(BigDecimal.ZERO,BigDecimal::add);
            // 生成冲红发票
            BmsInvoiceRecord redInvoiceRecord= new BmsInvoiceRecord();
            redInvoiceRecord.setInvoiceNo(invoiceRecord.getInvoiceNo());
            redInvoiceRecord.setBillId(invoiceRecord.getBillId());
            redInvoiceRecord.setAmount(totalAmount.negate());
            redInvoiceRecord.setAssociateId(id);
            redInvoiceRecord.setType(HnccType.Invoice.RED.getValue());
            redInvoiceRecord.setOperatorId(SecurityUtils.getLoginUser().getUser().getUserId());
            redInvoiceRecord.setCreateTime(DateUtils.getNowDate());
            redInvoiceRecord.setItemList(invoiceItemList.toString());
            handleFlag.set(invoiceRecordMapper.insertBmsInvoiceRecord(redInvoiceRecord));
            /*-----------------------------------------------通过原发票的item_list  判断是否需要 新增发票-------------------------------------------*/
            // 过滤当前发票 已经还未退费的数据
            List<DmsRefundVo> noRefundItemList =
                    originalItemList.stream().filter(item -> !items.stream().map(im -> im.getRefundId() + "|" + im.getType() + "|" + im.getAmount())
                            .collect(Collectors.toList()).contains(item.getRefundId() + "|" + item.getType() + "|" + item.getAmount()))
                            .collect(Collectors.toList());

            if(noRefundItemList.size()>0){
                // 发票收费项 需要金额 格式 id,表类型,金额><
                StringBuilder newInvoiceItemList= new StringBuilder();
                noRefundItemList.forEach(refundVo -> {
                    newInvoiceItemList.append(refundVo.getRefundId()).append(",").append(refundVo.getType()).append(",").append(refundVo.getAmount()).append("><");
                });
                // 总金额
                BigDecimal newTotalAmount=  noRefundItemList.stream().map(DmsRefundVo::getAmount).reduce(BigDecimal.ZERO,BigDecimal::add);
                // 发票号
                String newInvoiceNo=DateToNoUtil.DateToNoAndIncrease(DateToNoUtil.YYYYMMDD,6,invoiceRecordMapper.selectMaxInvoiceNo());
                // 新增发票信息
                BmsInvoiceRecord newInvoiceRecord = new BmsInvoiceRecord();
                // 创建时间
                newInvoiceRecord.setCreateTime(DateUtils.getNowDate());
                // 发票号
                newInvoiceRecord.setInvoiceNo(Long.valueOf(newInvoiceNo));
                // 账单id
                newInvoiceRecord.setBillId(invoiceRecord.getBillId());
                //  总金额
                newInvoiceRecord.setAmount(newTotalAmount);
                // 对账人id 对应当前登录的用户ID
                newInvoiceRecord.setOperatorId(SecurityUtils.getLoginUser().getUser().getUserId());
                // 记录列表   5,0,16><  表数据id,表的类型,金钱
                newInvoiceRecord.setItemList(newInvoiceItemList.toString());
                newInvoiceRecord.setType(HnccType.Invoice.NORMAL.getValue());
                handleFlag.set(invoiceRecordMapper.insertBmsInvoiceRecord(newInvoiceRecord));
            }
        });
        return handleFlag.get();
    }
}
