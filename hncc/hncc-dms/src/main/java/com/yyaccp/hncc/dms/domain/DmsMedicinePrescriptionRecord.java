package com.yyaccp.hncc.dms.domain;

import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.math.BigDecimal;
import java.util.List;

/**
 * @Author 余归
 * @Date 2020/9/1 0001 20:30
 * @Version 1.0
 */
public class DmsMedicinePrescriptionRecord extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     * id
     */
    private Long id;

    /**
     * 成药处方状态
     */
    @Excel(name = "成药处方状态")
    private Long status;

    /**
     * 总金额
     */
    @Excel(name = "总金额")
    private BigDecimal amount;

    /**
     * 处方名
     */
    @Excel(name = "处方名")
    private String name;

    /**
     * 挂号id
     */
    @Excel(name = "挂号id")
    private Long registrationId;

    /**
     * 退款状态
     */
    @Excel(name = "退款状态")
    private Long refundStatus;

    /**
     * 类型（普诊）
     */
    @Excel(name = "类型", readConverterExp = "普=诊")
    private Long type;

    /**
     * 开立人
     */
    @Excel(name = "开立人")
    private Long createStaffId;

    /**
     * 成药处方下的成药记录集合
     */
    private List<DmsMedicineItemRecord> medicineItemRecords;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getStatus() {
        return status;
    }

    public void setStatus(Long status) {
        this.status = status;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getRegistrationId() {
        return registrationId;
    }

    public void setRegistrationId(Long registrationId) {
        this.registrationId = registrationId;
    }

    public Long getRefundStatus() {
        return refundStatus;
    }

    public void setRefundStatus(Long refundStatus) {
        this.refundStatus = refundStatus;
    }

    public Long getType() {
        return type;
    }

    public void setType(Long type) {
        this.type = type;
    }

    public Long getCreateStaffId() {
        return createStaffId;
    }

    public void setCreateStaffId(Long createStaffId) {
        this.createStaffId = createStaffId;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
                .append("id", getId())
                .append("status", getStatus())
                .append("createTime", getCreateTime())
                .append("amount", getAmount())
                .append("name", getName())
                .append("registrationId", getRegistrationId())
                .append("refundStatus", getRefundStatus())
                .append("type", getType())
                .append("createStaffId", getCreateStaffId())
                .toString();
    }

    public List<DmsMedicineItemRecord> getMedicineItemRecords() {
        return medicineItemRecords;
    }

    public void setMedicineItemRecords(List<DmsMedicineItemRecord> medicineItemRecords) {
        this.medicineItemRecords = medicineItemRecords;
    }
}
