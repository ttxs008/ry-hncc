package com.yyaccp.hncc.dms.mapper;

import java.util.List;
import com.yyaccp.hncc.dms.domain.DmsMedicineModelItem;

/**
 * 成药模版Mapper接口
 * 
 * @author 周某
 * @date 2020-08-30
 */
public interface DmsMedicineModelItemMapper 
{
    /**
     * 查询成药模版
     * 
     * @param id 成药模版ID
     * @return 成药模版
     */
    public DmsMedicineModelItem selectDmsMedicineModelItemById(Long id);

    /**
     * 查询成药模版列表
     * 
     * @param dmsMedicineModelItem 成药模版
     * @return 成药模版集合
     */
    public List<DmsMedicineModelItem> selectDmsMedicineModelItemList(DmsMedicineModelItem dmsMedicineModelItem);

    /**
     * 新增成药模版
     * 
     * @param dmsMedicineModelItem 成药模版
     * @return 结果
     */
    public int insertDmsMedicineModelItem(DmsMedicineModelItem dmsMedicineModelItem);

    /**
     * 修改成药模版
     * 
     * @param dmsMedicineModelItem 成药模版
     * @return 结果
     */
    public int updateDmsMedicineModelItem(DmsMedicineModelItem dmsMedicineModelItem);

    /**
     * 删除成药模版
     * 
     * @param id 成药模版ID
     * @return 结果
     */
    public int deleteDmsMedicineModelItemById(Long id);

    /**
     * 批量删除成药模版
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteDmsMedicineModelItemByIds(Long[] ids);


    /**
     * 根据模板Id查询模板明细的药品信息集合
     * @param id 草药模版项ID
     * @return 药品信息集合
     */
    List<DmsMedicineModelItem> selectDrugInfoByModelId(Long id);
}
