package com.yyaccp.hncc.dms.service;

import java.util.List;
import com.yyaccp.hncc.dms.domain.DmsFrequentUsed;

/**
 * 医生常用诊断（疾病）、检查检验(搁置)Service接口
 * 
 * @author ruoyi
 * @date 2020-08-20
 */
public interface IDmsFrequentUsedService 
{
    /**
     * 查询医生常用诊断（疾病）、检查检验(搁置)
     * 
     * @param id 医生常用诊断（疾病）、检查检验(搁置)ID
     * @return 医生常用诊断（疾病）、检查检验(搁置)
     */
    public DmsFrequentUsed selectDmsFrequentUsedById(Long id);

    /**
     * 查询医生常用诊断（疾病）、检查检验(搁置)列表
     * 
     * @param dmsFrequentUsed 医生常用诊断（疾病）、检查检验(搁置)
     * @return 医生常用诊断（疾病）、检查检验(搁置)集合
     */
    public List<DmsFrequentUsed> selectDmsFrequentUsedList(DmsFrequentUsed dmsFrequentUsed);

    /**
     * 新增医生常用诊断（疾病）、检查检验(搁置)
     * 
     * @param dmsFrequentUsed 医生常用诊断（疾病）、检查检验(搁置)
     * @return 结果
     */
    public int insertDmsFrequentUsed(DmsFrequentUsed dmsFrequentUsed);

    /**
     * 修改医生常用诊断（疾病）、检查检验(搁置)
     * 
     * @param dmsFrequentUsed 医生常用诊断（疾病）、检查检验(搁置)
     * @return 结果
     */
    public int updateDmsFrequentUsed(DmsFrequentUsed dmsFrequentUsed);

    /**
     * 批量删除医生常用诊断（疾病）、检查检验(搁置)
     * 
     * @param ids 需要删除的医生常用诊断（疾病）、检查检验(搁置)ID
     * @return 结果
     */
    public int deleteDmsFrequentUsedByIds(Long[] ids);

    /**
     * 删除医生常用诊断（疾病）、检查检验(搁置)信息
     * 
     * @param id 医生常用诊断（疾病）、检查检验(搁置)ID
     * @return 结果
     */
    public int deleteDmsFrequentUsedById(Long id);
}
