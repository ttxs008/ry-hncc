package com.yyaccp.hncc.dms.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 非药品模版对象 dms_non_drug_model
 *
 * @author yugui
 * @date 2020-08-17
 */
public class DmsNonDrugModel extends BaseEntity {
    private static final long serialVersionUID = 1L;

    /**
     * 模版编号
     */
    private Long id;

    /**
     * 模版状态
     */
    private Long status;

    /**
     * 模版名称
     */
    @Excel(name = "模版名称")
    private String name;

    /**
     * 模版所包括非药品的id
     */
    @Excel(name = "模版所包括非药品的id")
    private String nonDrugIdList;

    /**
     * 模版范围
     */
    @Excel(name = "模版范围")
    private Long scope;

    /**
     * 所属人Id
     */
    private Long ownId;

    /**
     * 模版简介
     */
    @Excel(name = "模版简介")
    private String aim;

    /**
     * 模版编码
     */
    @Excel(name = "模版编码")
    private String code;

    /**
     * 模版类型
     */
    @Excel(name = "模版类型")
    private Long type;

    public void setId(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setStatus(Long status) {
        this.status = status;
    }

    public Long getStatus() {
        return status;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setNonDrugIdList(String nonDrugIdList) {
        this.nonDrugIdList = nonDrugIdList;
    }

    public String getNonDrugIdList() {
        return nonDrugIdList;
    }

    public void setScope(Long scope) {
        this.scope = scope;
    }

    public Long getScope() {
        return scope;
    }

    public void setOwnId(Long ownId) {
        this.ownId = ownId;
    }

    public Long getOwnId() {
        return ownId;
    }

    public void setAim(String aim) {
        this.aim = aim;
    }

    public String getAim() {
        return aim;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }

    public void setType(Long type) {
        this.type = type;
    }

    public Long getType() {
        return type;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
                .append("id", getId())
                .append("status", getStatus())
                .append("name", getName())
                .append("nonDrugIdList", getNonDrugIdList())
                .append("scope", getScope())
                .append("ownId", getOwnId())
                .append("aim", getAim())
                .append("createTime", getCreateTime())
                .append("code", getCode())
                .append("type", getType())
                .toString();
    }
}
