package com.yyaccp.hncc.dms.service;

import com.yyaccp.hncc.dms.domain.DmsMedicinePrescriptionRecord;

import java.util.List;

/**
 * 成药处方Service接口
 *
 * @author ruoyi
 * @date 2020-08-26
 */
public interface IDmsMedicinePrescriptionRecordService {
    /**
     * 查询成药处方
     *
     * @param id 成药处方ID
     * @return 成药处方
     */
    public DmsMedicinePrescriptionRecord selectDmsMedicinePrescriptionRecordById(Long id);

    /**
     * 查询成药处方列表
     *
     * @param dmsMedicinePrescriptionRecord 成药处方
     * @return 成药处方集合
     */
    public List<DmsMedicinePrescriptionRecord> selectDmsMedicinePrescriptionRecordList(DmsMedicinePrescriptionRecord dmsMedicinePrescriptionRecord);

    /**
     * 新增成药处方
     *
     * @param dmsMedicinePrescriptionRecord 成药处方
     * @return 结果
     */
    public int insertDmsMedicinePrescriptionRecord(DmsMedicinePrescriptionRecord dmsMedicinePrescriptionRecord);

    /**
     * 修改成药处方
     *
     * @param dmsMedicinePrescriptionRecord 成药处方
     * @return 结果
     */
    public int updateDmsMedicinePrescriptionRecord(DmsMedicinePrescriptionRecord dmsMedicinePrescriptionRecord);

    /**
     * 批量删除成药处方
     *
     * @param ids 需要删除的成药处方ID
     * @return 结果
     */
    public int deleteDmsMedicinePrescriptionRecordByIds(Long[] ids);

    /**
     * 删除成药处方信息
     *
     * @param id 成药处方ID
     * @return 结果
     */
    public int deleteDmsMedicinePrescriptionRecordById(Long id);

    /**
     * 批量修改成药处方(发药)
     *
     * @param ids 需要批量修改的成药处方ID
     * @return 结果
     */
    public int batchModificationOfPrescriptions(List<Long> ids, Integer status);
}
