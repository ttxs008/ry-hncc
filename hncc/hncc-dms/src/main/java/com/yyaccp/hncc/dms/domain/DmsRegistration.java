package com.yyaccp.hncc.dms.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.common.core.domain.entity.SysDept;
import com.yyaccp.hncc.pms.domain.PmsPatient;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 就诊(门诊)信息对象 dms_registration
 * 
 * @author ruoyi
 * @date 2020-08-20
 */
public class DmsRegistration extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 姓名 */
    @Excel(name = "姓名")
    private String name;

    /** 病历号 */
    @Excel(name = "病历号")
    private String medicalRecordNo;

    /** 性别 */
    @Excel(name = "性别")
    private Long gender;

    /** id */
    private Long id;

    /** 患者id */
    @Excel(name = "患者id")
    private Long patientId;

    /** 诊断状态 */
    @Excel(name = "诊断状态")
    private Integer endAttendance;

    /** 状态 */
    @Excel(name = "状态")
    private Integer status;

    /** 排班id */
    @Excel(name = "排班id")
    private Long skdId;

    /** 是否需要病历本 */
    @Excel(name = "是否需要病历本")
    private Integer needBook;

    /** 绑定状态 */
    @Excel(name = "绑定状态")
    private Integer bindStatus;

    /** 科室id */
    @Excel(name = "科室id")
    private Long deptId;

    /** 就诊日期 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "就诊日期", width = 30, dateFormat = "yyyy-MM-dd")
    private Date attendanceDate;

    /** 就诊年龄 */
    @Excel(name = "就诊年龄")
    private String patientAgeStr;

    /** 病人 一对一 */
    private PmsPatient pmsPatient;

    /** 科室 一对一 */
    private SysDept sysDept;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setPatientId(Long patientId) 
    {
        this.patientId = patientId;
    }

    public Long getPatientId() 
    {
        return patientId;
    }
    public void setEndAttendance(Integer endAttendance) 
    {
        this.endAttendance = endAttendance;
    }

    public Integer getEndAttendance() 
    {
        return endAttendance;
    }
    public void setStatus(Integer status) 
    {
        this.status = status;
    }

    public Integer getStatus() 
    {
        return status;
    }
    public void setSkdId(Long skdId) 
    {
        this.skdId = skdId;
    }

    public Long getSkdId() 
    {
        return skdId;
    }
    public void setNeedBook(Integer needBook) 
    {
        this.needBook = needBook;
    }

    public Integer getNeedBook() 
    {
        return needBook;
    }
    public void setBindStatus(Integer bindStatus) 
    {
        this.bindStatus = bindStatus;
    }

    public Integer getBindStatus() 
    {
        return bindStatus;
    }
    public void setDeptId(Long deptId) 
    {
        this.deptId = deptId;
    }

    public Long getDeptId() 
    {
        return deptId;
    }
    public void setAttendanceDate(Date attendanceDate) 
    {
        this.attendanceDate = attendanceDate;
    }

    public Date getAttendanceDate() 
    {
        return attendanceDate;
    }
    public void setPatientAgeStr(String patientAgeStr) 
    {
        this.patientAgeStr = patientAgeStr;
    }

    public String getPatientAgeStr() 
    {
        return patientAgeStr;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMedicalRecordNo() {
        return medicalRecordNo;
    }

    public void setMedicalRecordNo(String medicalRecordNo) {
        this.medicalRecordNo = medicalRecordNo;
    }

    public Long getGender() {
        return gender;
    }

    public void setGender(Long gender) {
        this.gender = gender;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("patientId", getPatientId())
            .append("createTime", getCreateTime())
            .append("endAttendance", getEndAttendance())
            .append("status", getStatus())
            .append("skdId", getSkdId())
            .append("needBook", getNeedBook())
            .append("bindStatus", getBindStatus())
            .append("deptId", getDeptId())
            .append("attendanceDate", getAttendanceDate())
            .append("patientAgeStr", getPatientAgeStr())
            .append("name", getName())
            .append("medicalRecordNo", getMedicalRecordNo())
            .append("gender", getGender())
            .toString();
    }

    public PmsPatient getPmsPatient() {
        return pmsPatient;
    }

    public void setPmsPatient(PmsPatient pmsPatient) {
        this.pmsPatient = pmsPatient;
    }

    public SysDept getSysDept() {
        return sysDept;
    }

    public void setSysDept(SysDept sysDept) {
        this.sysDept = sysDept;
    }
}
