package com.yyaccp.hncc.dms.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 成药模版对象 dms_medicine_model_item
 * 
 * @author 周某
 * @date 2020-08-30
 */
public class DmsMedicineModelItem extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    public DmsDrug getDrug() {
        return drug;
    }

    public void setDrug(DmsDrug drug) {
        this.drug = drug;
    }

    /** id */
    private Long id;

    /** 模板id */
    @Excel(name = "模板id")
    private Long modelId;

    /** 状态 */
    @Excel(name = "状态")
    private Integer status;

    /** 药品id */
    @Excel(name = "药品id")
    private Long drugId;

    /** 用法 */
    @Excel(name = "用法")
    private Integer medicineUsage;

    /** 频率 */
    @Excel(name = "频率")
    private Integer frequency;

    /** 天数 */
    @Excel(name = "天数")
    private Long days;

    /** 数量 */
    @Excel(name = "数量")
    private Long num;

    /** 医嘱 */
    @Excel(name = "医嘱")
    private String medicalAdvice;

    /** 用量 */
    @Excel(name = "用量")
    private Long usageNum;

    /** 用法 */
    @Excel(name = "用法")
    private Integer usageMeans;

    /** 单位 */
    @Excel(name = "单位")
    private Integer usageNumUnit;

    /**药品*/
    private DmsDrug drug;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setModelId(Long modelId) 
    {
        this.modelId = modelId;
    }

    public Long getModelId() 
    {
        return modelId;
    }
    public void setStatus(Integer status) 
    {
        this.status = status;
    }

    public Integer getStatus() 
    {
        return status;
    }
    public void setDrugId(Long drugId) 
    {
        this.drugId = drugId;
    }

    public Long getDrugId() 
    {
        return drugId;
    }
    public void setMedicineUsage(Integer medicineUsage) 
    {
        this.medicineUsage = medicineUsage;
    }

    public Integer getMedicineUsage() 
    {
        return medicineUsage;
    }
    public void setFrequency(Integer frequency) 
    {
        this.frequency = frequency;
    }

    public Integer getFrequency() 
    {
        return frequency;
    }
    public void setDays(Long days) 
    {
        this.days = days;
    }

    public Long getDays() 
    {
        return days;
    }
    public void setNum(Long num) 
    {
        this.num = num;
    }

    public Long getNum() 
    {
        return num;
    }
    public void setMedicalAdvice(String medicalAdvice) 
    {
        this.medicalAdvice = medicalAdvice;
    }

    public String getMedicalAdvice() 
    {
        return medicalAdvice;
    }
    public void setUsageNum(Long usageNum) 
    {
        this.usageNum = usageNum;
    }

    public Long getUsageNum() 
    {
        return usageNum;
    }
    public void setUsageMeans(Integer usageMeans) 
    {
        this.usageMeans = usageMeans;
    }

    public Integer getUsageMeans() 
    {
        return usageMeans;
    }
    public void setUsageNumUnit(Integer usageNumUnit) 
    {
        this.usageNumUnit = usageNumUnit;
    }

    public Integer getUsageNumUnit() 
    {
        return usageNumUnit;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("modelId", getModelId())
            .append("status", getStatus())
            .append("drugId", getDrugId())
            .append("medicineUsage", getMedicineUsage())
            .append("frequency", getFrequency())
            .append("days", getDays())
            .append("num", getNum())
            .append("medicalAdvice", getMedicalAdvice())
            .append("usageNum", getUsageNum())
            .append("usageMeans", getUsageMeans())
            .append("usageNumUnit", getUsageNumUnit())
            .toString();
    }
}
