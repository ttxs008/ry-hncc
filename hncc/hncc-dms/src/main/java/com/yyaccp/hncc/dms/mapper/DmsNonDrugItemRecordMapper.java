package com.yyaccp.hncc.dms.mapper;

import java.util.List;
import com.yyaccp.hncc.dms.domain.DmsNonDrugItemRecord;
import org.apache.ibatis.annotations.Param;

/**
 * 检查项检验项处置项记录(开立的)Mapper接口
 *
 * @author ruoyi
 * @date 2020-09-03
 */
public interface DmsNonDrugItemRecordMapper
{
    /**
     * 查询检查项检验项处置项记录(开立的)
     *
     * @param id 检查项检验项处置项记录(开立的)ID
     * @return 检查项检验项处置项记录(开立的)
     */
    public DmsNonDrugItemRecord selectDmsNonDrugItemRecordById(Long id);

    /**
     * 查询检查项检验项处置项记录(开立的)列表
     *
     * @param dmsNonDrugItemRecord 检查项检验项处置项记录(开立的)
     * @return 检查项检验项处置项记录(开立的)集合
     */
    public List<DmsNonDrugItemRecord> selectDmsNonDrugItemRecordList(DmsNonDrugItemRecord dmsNonDrugItemRecord);

    /**
     * 新增检查项检验项处置项记录(开立的)
     *
     * @param dmsNonDrugItemRecord 检查项检验项处置项记录(开立的)
     * @return 结果
     */
    public int insertDmsNonDrugItemRecord(DmsNonDrugItemRecord dmsNonDrugItemRecord);

    /**
     * 修改检查项检验项处置项记录(开立的)
     *
     * @param dmsNonDrugItemRecord 检查项检验项处置项记录(开立的)
     * @return 结果
     */
    public int updateDmsNonDrugItemRecord(DmsNonDrugItemRecord dmsNonDrugItemRecord);

    /**
     * 删除检查项检验项处置项记录(开立的)
     *
     * @param id 检查项检验项处置项记录(开立的)ID
     * @return 结果
     */
    public int deleteDmsNonDrugItemRecordById(Long id);

    /**
     * 批量删除检查项检验项处置项记录(开立的)
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteDmsNonDrugItemRecordByIds(Long[] ids);

    List<DmsNonDrugItemRecord> selectDmsNonDrugItemRecordDealWithList(DmsNonDrugItemRecord dmsNonDrugItemRecord);

    /**
     * 添加检查结果
     *
     * @param dmsNonDrugItemRecord 添加检查结果
     * @return 结果
     */
    public int updateDmsNonDrugItemRecordCheckResult(DmsNonDrugItemRecord dmsNonDrugItemRecord);


    /**
     *  批量修改检查项检验项处置项记录的状态
     * @param ids ID数组
     * @param status  状态
     * @return 结果
     */
    public int updateDmsNonDrugItemRecordStatusByIds(@Param("ids") List<Long> ids, @Param("status")Integer status);


}
