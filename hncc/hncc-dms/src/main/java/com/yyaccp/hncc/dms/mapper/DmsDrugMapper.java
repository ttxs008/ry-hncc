package com.yyaccp.hncc.dms.mapper;

import java.util.List;
import com.yyaccp.hncc.dms.domain.DmsDrug;
import org.apache.ibatis.annotations.Param;


/**
 * 药品项目（包括了重要、中成药、草药）Mapper接口
 *
 * @author 周某
 * @date 2020-08-12
 */
public interface DmsDrugMapper
{
    /**
     * 查询药品项目（包括了重要、中成药、草药）
     *
     * @param id 药品项目（包括了重要、中成药、草药）ID
     * @return 药品项目（包括了重要、中成药、草药）
     */
    public DmsDrug selectDmsDrugById(Long id);

    /**
     * 查询药品项目（包括了重要、中成药、草药）列表
     *
     * @param dmsDrug 药品项目（包括了重要、中成药、草药）
     * @return 药品项目（包括了重要、中成药、草药）集合
     */
    public List<DmsDrug> selectDmsDrugList(DmsDrug dmsDrug);

    /**
     * 新增药品项目（包括了重要、中成药、草药）
     *
     * @param dmsDrug 药品项目（包括了重要、中成药、草药）
     * @return 结果
     */
    public int insertDmsDrug(DmsDrug dmsDrug);

    /**
     * 修改药品项目（包括了重要、中成药、草药）
     *
     * @param dmsDrug 药品项目（包括了重要、中成药、草药）
     * @return 结果
     */
    public int updateDmsDrug(DmsDrug dmsDrug);

    /**
     * 删除药品项目（包括了重要、中成药、草药）
     *
     * @param id 药品项目（包括了重要、中成药、草药）ID
     * @return 结果
     */
    public int deleteDmsDrugById(Long id);

    /**
     * 批量删除药品项目（包括了重要、中成药、草药）
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteDmsDrugByIds(Long[] ids);

    /**
     * 根据ids查询药品项目集合
     * @param name 需要查询的数据name
     * @param dmsDrugIds 需要查询的数据ID
     * @param type 需要查询的数据ID
     * @return 结果
     */
    public List<DmsDrug> selectDmsDrugListByIdsOrTypeOrName(@Param("dmsDrugIds") String[] dmsDrugIds, @Param("type")Integer type, @Param("name")String name);


}