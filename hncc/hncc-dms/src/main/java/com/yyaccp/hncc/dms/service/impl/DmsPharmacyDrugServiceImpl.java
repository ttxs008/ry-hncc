package com.yyaccp.hncc.dms.service.impl;

import com.yyaccp.hncc.dms.domain.DmsPharmacyDrug;
import com.yyaccp.hncc.dms.mapper.DmsPharmacyDrugMapper;
import com.yyaccp.hncc.dms.service.IDmsPharmacyDrugService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @Author 余归
 * @Date 2020/9/15 0015 15:05
 * @Version 1.0
 */
@Service
@Slf4j
@RequiredArgsConstructor
public class DmsPharmacyDrugServiceImpl implements IDmsPharmacyDrugService {

    @Autowired
    private DmsPharmacyDrugMapper dmsPharmacyDrugMapper;

    @Override
    public List<DmsPharmacyDrug> queryOverTheCounterPharmacyDrugs(Long registrationId, Long status) {
        return dmsPharmacyDrugMapper.queryOverTheCounterPharmacyDrugs(registrationId, status);
    }

    @Override
    public List<DmsPharmacyDrug> inquireAboutHerbalPharmacyDrugs(Long registrationId, Long status) {
        return dmsPharmacyDrugMapper.inquireAboutHerbalPharmacyDrugs(registrationId, status);
    }
}
