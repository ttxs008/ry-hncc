package com.yyaccp.hncc.dms.service;

import java.util.List;
import com.yyaccp.hncc.dms.domain.DmsDosage;

/**
 * 药品剂型Service接口
 *
 * @author 周某
 * @date 2020-08-12
 */
public interface IDmsDosageService 
{
    /**
     * 查询药品剂型
     * 
     * @param id 药品剂型ID
     * @return 药品剂型
     */
    public DmsDosage selectDmsDosageById(Long id);

    /**
     * 查询药品剂型列表
     * 
     * @param dmsDosage 药品剂型
     * @return 药品剂型集合
     */
    public List<DmsDosage> selectDmsDosageList(DmsDosage dmsDosage);

    /**
     * 新增药品剂型
     * 
     * @param dmsDosage 药品剂型
     * @return 结果
     */
    public int insertDmsDosage(DmsDosage dmsDosage);

    /**
     * 修改药品剂型
     * 
     * @param dmsDosage 药品剂型
     * @return 结果
     */
    public int updateDmsDosage(DmsDosage dmsDosage);

    /**
     * 批量删除药品剂型
     * 
     * @param ids 需要删除的药品剂型ID
     * @return 结果
     */
    public int deleteDmsDosageByIds(Long[] ids);

    /**
     * 删除药品剂型信息
     * 
     * @param id 药品剂型ID
     * @return 结果
     */
    public int deleteDmsDosageById(Long id);
}
