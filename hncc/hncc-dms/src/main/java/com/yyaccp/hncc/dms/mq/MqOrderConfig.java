package com.yyaccp.hncc.dms.mq;

import lombok.Data;

/**
 * @author 天天向上 （john.yi@qq.com）
 * @date 2020/9/6.
 */
@Data
public class MqOrderConfig {
    private String feeTypeName;
    private String feeTypeNameCn;
    private String serviceBeanId;
}
