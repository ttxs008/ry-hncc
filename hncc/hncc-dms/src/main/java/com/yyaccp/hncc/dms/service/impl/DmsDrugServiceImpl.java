package com.yyaccp.hncc.dms.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.yyaccp.hncc.dms.mapper.DmsDrugMapper;
import com.yyaccp.hncc.dms.domain.DmsDrug;
import com.yyaccp.hncc.dms.service.IDmsDrugService;
import javax.annotation.Resource;

/**
 * 药品项目（包括了重要、中成药、草药）Service业务层处理
 * @author 周某
 * @date 2020-08-12
 */
@Service
public class DmsDrugServiceImpl implements IDmsDrugService
{
    @Autowired

    @Resource

    private DmsDrugMapper dmsDrugMapper;

    /**
     * 查询药品项目（包括了重要、中成药、草药）
     *
     * @param id 药品项目（包括了重要、中成药、草药）ID
     * @return 药品项目（包括了重要、中成药、草药）
     */
    @Override
    public DmsDrug selectDmsDrugById(Long id)
    {
        return dmsDrugMapper.selectDmsDrugById(id);
    }

    /**
     * 查询药品项目（包括了重要、中成药、草药）列表
     *
     * @param dmsDrug 药品项目（包括了重要、中成药、草药）
     * @return 药品项目（包括了重要、中成药、草药）
     */
    @Override
    public List<DmsDrug> selectDmsDrugList(DmsDrug dmsDrug)
    {
        return dmsDrugMapper.selectDmsDrugList(dmsDrug);
    }

    /**
     * 新增药品项目（包括了重要、中成药、草药）
     *
     * @param dmsDrug 药品项目（包括了重要、中成药、草药）
     * @return 结果
     */
    @Override
    public int insertDmsDrug(DmsDrug dmsDrug)
    {
        return dmsDrugMapper.insertDmsDrug(dmsDrug);
    }

    /**
     * 修改药品项目（包括了重要、中成药、草药）
     *
     * @param dmsDrug 药品项目（包括了重要、中成药、草药）
     * @return 结果
     */
    @Override
    public int updateDmsDrug(DmsDrug dmsDrug)
    {
        return dmsDrugMapper.updateDmsDrug(dmsDrug);
    }

    /**
     * 批量删除药品项目（包括了重要、中成药、草药）
     *
     * @param ids 需要删除的药品项目（包括了重要、中成药、草药）ID
     * @return 结果
     */
    @Override
    public int deleteDmsDrugByIds(Long[] ids)
    {
        return dmsDrugMapper.deleteDmsDrugByIds(ids);
    }

    /**
     * 删除药品项目（包括了重要、中成药、草药）信息
     *
     * @param id 药品项目（包括了重要、中成药、草药）ID
     * @return 结果
     */
    @Override
    public int deleteDmsDrugById(Long id)
    {
        return dmsDrugMapper.deleteDmsDrugById(id);
    }

    /**
     * 根据ids查询药品项目集合
     *
     * @param dmsDrugIds 需要查询的数据ID
     * @param type       需要查询的数据ID
     * @return 结果
     */
    @Override
    public List<DmsDrug> selectDmsDrugListByIdsOrTypeOrName(String dmsDrugIds, Integer type,String name) {

        return dmsDrugMapper.selectDmsDrugListByIdsOrTypeOrName(dmsDrugIds==null?null:dmsDrugIds.split(","),type,name);
    }
}
