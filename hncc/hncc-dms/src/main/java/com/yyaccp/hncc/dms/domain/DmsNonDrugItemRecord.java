package com.yyaccp.hncc.dms.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;
import com.ruoyi.common.core.domain.entity.SysDept;
import com.ruoyi.common.core.domain.entity.SysUser;
import com.yyaccp.hncc.pms.domain.PmsPatient;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.math.BigDecimal;
import java.util.Date;

/**
 * 检查项检验项处置项记录(开立的)对象 dms_non_drug_item_record
 *
 * @author 杨治宇（769430030@qq.com）
 * @date 2020/9/3
 */
public class DmsNonDrugItemRecord extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /** id */
    private Long id;

    /** 挂号id */
    @Excel(name = "挂号id")
    private Long registrationId;

    /** 状态：0 作废（删除）1未缴费 2未登记（已缴费） 3已登记 4已执行 5已退费 6已过期 */
    @Excel(name = "状态：0 作废", readConverterExp = "删=除")
    private Integer status;

    /** 目的 */
    @Excel(name = "目的")
    private String aim;

    /** 要求 */
    @Excel(name = "要求")
    private String demand;

    /** 记录状态 */
    @Excel(name = "记录状态")
    private Integer logStatus;

    /** 检查结果 */
    @Excel(name = "检查结果")
    private String checkResult;

    /** 结果图片url列表 */
    @Excel(name = "结果图片url列表")
    private String resultImgUrlList;

    /** 临床印象 */
    @Excel(name = "临床印象")
    private String clinicalImpression;

    /** 临床诊断 */
    @Excel(name = "临床诊断")
    private String clinicalDiagnosis;

    /** 执行医生id */
    @Excel(name = "执行医生id")
    private Long excuteStaffId;

    /** 登记时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "登记时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date logDatetime;

    /** 非药品Id */
    @Excel(name = "非药品Id")
    private Long noDrugId;

    /** 检查部位 */
    @Excel(name = "检查部位")
    private String checkParts;

    /** 类型： 0检查 1检验  2处置 */
    @Excel(name = "类型： 0检查 1检验  2处置")
    private Integer type;

    /** 执行科室id */
    @Excel(name = "执行科室id")
    private Long excuteDeptId;

    /** 开立医生id */
    @Excel(name = "开立医生id")
    private Long createStaffId;

    /** 登记医生id */
    @Excel(name = "登记医生id")
    private Long logStaffId;

    /** 执行时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "执行时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date excuteTime;

    /** 金额 */
    @Excel(name = "金额")
    private BigDecimal amount;

    /**外加对象列*/
    /** 挂号id */
    private DmsRegistration registration;

    /** 执行医生id */
    private SysUser carriedOutSysUser;

    /** 非药品Id */
    private DmsNonDrug nonDrug;

    /** 执行科室id */
    private SysDept sysDept;

    /** 开立医生id */
    private SysUser openSysUser;

    /** 登记医生id */
    private SysUser registerSysUser;

    /** 患者 */
    private PmsPatient patient;

    public PmsPatient getPatient() {
        return patient;
    }

    public void setPatient(PmsPatient patient) {
        this.patient = patient;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getRegistrationId() {
        return registrationId;
    }

    public void setRegistrationId(Long registrationId) {
        this.registrationId = registrationId;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getAim() {
        return aim;
    }

    public void setAim(String aim) {
        this.aim = aim;
    }

    public String getDemand() {
        return demand;
    }

    public void setDemand(String demand) {
        this.demand = demand;
    }

    public Integer getLogStatus() {
        return logStatus;
    }

    public void setLogStatus(Integer logStatus) {
        this.logStatus = logStatus;
    }

    public String getCheckResult() {
        return checkResult;
    }

    public void setCheckResult(String checkResult) {
        this.checkResult = checkResult;
    }

    public String getResultImgUrlList() {
        return resultImgUrlList;
    }

    public void setResultImgUrlList(String resultImgUrlList) {
        this.resultImgUrlList = resultImgUrlList;
    }

    public String getClinicalImpression() {
        return clinicalImpression;
    }

    public void setClinicalImpression(String clinicalImpression) {
        this.clinicalImpression = clinicalImpression;
    }

    public String getClinicalDiagnosis() {
        return clinicalDiagnosis;
    }

    public void setClinicalDiagnosis(String clinicalDiagnosis) {
        this.clinicalDiagnosis = clinicalDiagnosis;
    }

    public Long getExcuteStaffId() {
        return excuteStaffId;
    }

    public void setExcuteStaffId(Long excuteStaffId) {
        this.excuteStaffId = excuteStaffId;
    }

    public Date getLogDatetime() {
        return logDatetime;
    }

    public void setLogDatetime(Date logDatetime) {
        this.logDatetime = logDatetime;
    }

    public Long getNoDrugId() {
        return noDrugId;
    }

    public void setNoDrugId(Long noDrugId) {
        this.noDrugId = noDrugId;
    }

    public String getCheckParts() {
        return checkParts;
    }

    public void setCheckParts(String checkParts) {
        this.checkParts = checkParts;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Long getExcuteDeptId() {
        return excuteDeptId;
    }

    public void setExcuteDeptId(Long excuteDeptId) {
        this.excuteDeptId = excuteDeptId;
    }

    public Long getCreateStaffId() {
        return createStaffId;
    }

    public void setCreateStaffId(Long createStaffId) {
        this.createStaffId = createStaffId;
    }

    public Long getLogStaffId() {
        return logStaffId;
    }

    public void setLogStaffId(Long logStaffId) {
        this.logStaffId = logStaffId;
    }

    public Date getExcuteTime() {
        return excuteTime;
    }

    public void setExcuteTime(Date excuteTime) {
        this.excuteTime = excuteTime;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public DmsRegistration getRegistration() {
        return registration;
    }

    public void setRegistration(DmsRegistration registration) {
        this.registration = registration;
    }

    public SysUser getCarriedOutSysUser() {
        return carriedOutSysUser;
    }

    public void setCarriedOutSysUser(SysUser carriedOutSysUser) {
        this.carriedOutSysUser = carriedOutSysUser;
    }

    public DmsNonDrug getNonDrug() {
        return nonDrug;
    }

    public void setNonDrug(DmsNonDrug nonDrug) {
        this.nonDrug = nonDrug;
    }

    public SysDept getSysDept() {
        return sysDept;
    }

    public void setSysDept(SysDept sysDept) {
        this.sysDept = sysDept;
    }

    public SysUser getOpenSysUser() {
        return openSysUser;
    }

    public void setOpenSysUser(SysUser openSysUser) {
        this.openSysUser = openSysUser;
    }

    public SysUser getRegisterSysUser() {
        return registerSysUser;
    }

    public void setRegisterSysUser(SysUser registerSysUser) {
        this.registerSysUser = registerSysUser;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
                .append("id", getId())
                .append("registrationId", getRegistrationId())
                .append("status", getStatus())
                .append("aim", getAim())
                .append("demand", getDemand())
                .append("logStatus", getLogStatus())
                .append("checkResult", getCheckResult())
                .append("resultImgUrlList", getResultImgUrlList())
                .append("clinicalImpression", getClinicalImpression())
                .append("clinicalDiagnosis", getClinicalDiagnosis())
                .append("createTime", getCreateTime())
                .append("excuteStaffId", getExcuteStaffId())
                .append("logDatetime", getLogDatetime())
                .append("noDrugId", getNoDrugId())
                .append("checkParts", getCheckParts())
                .append("type", getType())
                .append("excuteDeptId", getExcuteDeptId())
                .append("createStaffId", getCreateStaffId())
                .append("logStaffId", getLogStaffId())
                .append("excuteTime", getExcuteTime())
                .append("amount", getAmount())
                .toString();
    }
}
