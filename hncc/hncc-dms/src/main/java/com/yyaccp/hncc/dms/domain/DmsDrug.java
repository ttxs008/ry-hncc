package com.yyaccp.hncc.dms.domain;

import java.math.BigDecimal;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 药品项目（包括了重要、中成药、草药）对象 dms_drug
 *
 * @author 周某
 * @date 2020-08-12
 */
public class DmsDrug extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** id */
    @ApiModelProperty(value = "ID",dataType ="Long",example = "1")
    private Long id;

    /** 药品编码 */
    @ApiModelProperty(value = "药品编码",dataType ="String",example = "86979474000208")
    @Excel(name = "药品编码")
    private String code;

    /** 药品名称 */
    @ApiModelProperty(value = "药品名称",dataType ="String",example = "注射用甲氨喋呤")
    @Excel(name = "药品名称")
    private String name;

    /** 药品规格 */
    @ApiModelProperty(value = "药品规格",dataType ="String",example = "1g×1支")
    @Excel(name = "药品规格")
    private String format;

    /** 药品单价 */
    @ApiModelProperty(value = "药品单价",dataType ="BigDecimal",example = "15.73")
    @Excel(name = "药品单价")
    private BigDecimal price;

    /** 包装单位 */
    @ApiModelProperty(value = "包装单位",dataType ="String",example = "支")
    @Excel(name = "包装单位")
    private String unit;

    /** 生产厂家 */
    @ApiModelProperty(value = "生产厂家",dataType ="String",example = "江苏恒瑞医药股份有限公司")
    @Excel(name = "生产厂家")
    private String manufacturer;

    /** 药品剂型 */
    @ApiModelProperty(value = "药品剂型",dataType ="Long",example = "110")
    @Excel(name = "药品剂型")
    private Long dosageId;

    /** 药品剂型名称 */
   // @Excel(name = "药品剂型")
    private String dosageName;


    /** 药品类型 */
    @ApiModelProperty(value = "药品类型",dataType ="Long",example = "101")
    @Excel(name = "药品类型")
    private Long typeId;

    /** 拼音助记码 */
    @ApiModelProperty(value = "拼音助记码",dataType ="String",example = "ZSYJAZZ")
    @Excel(name = "拼音助记码")
    private String mnemonicCode;

    /** 创建时间 */
    @ApiModelProperty(value = "创建时间",dataType ="Date",example = "2019-03-01 00:00:00")
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "创建时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date createDate;

    /** 库存 */
    @ApiModelProperty(value = "库存",dataType ="Long",example = "10")
    @Excel(name = "库存")
    private Long stock;

    /** 通用名 */
    @ApiModelProperty(value = "通用名",dataType ="String",example = "10")
    @Excel(name = "通用名")
    private String genericName;

    /** 状态 */
    @ApiModelProperty(value = "状态",dataType ="Integer",example = "1")
    @Excel(name = "状态")
    private Integer status;

    public String getDosageName() {
        return dosageName;
    }

    public void setDosageName(String dosageName) {
        this.dosageName = dosageName;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setCode(String code) 
    {
        this.code = code;
    }

    public String getCode() 
    {
        return code;
    }
    public void setName(String name) 
    {
        this.name = name;
    }

    public String getName() 
    {
        return name;
    }
    public void setFormat(String format) 
    {
        this.format = format;
    }

    public String getFormat() 
    {
        return format;
    }
    public void setPrice(BigDecimal price) 
    {
        this.price = price;
    }

    public BigDecimal getPrice() 
    {
        return price;
    }
    public void setUnit(String unit) 
    {
        this.unit = unit;
    }

    public String getUnit() 
    {
        return unit;
    }
    public void setManufacturer(String manufacturer) 
    {
        this.manufacturer = manufacturer;
    }

    public String getManufacturer() 
    {
        return manufacturer;
    }
    public void setDosageId(Long dosageId) 
    {
        this.dosageId = dosageId;
    }

    public Long getDosageId() 
    {
        return dosageId;
    }
    public void setTypeId(Long typeId) 
    {
        this.typeId = typeId;
    }

    public Long getTypeId() 
    {
        return typeId;
    }
    public void setMnemonicCode(String mnemonicCode) 
    {
        this.mnemonicCode = mnemonicCode;
    }

    public String getMnemonicCode() 
    {
        return mnemonicCode;
    }
    public void setCreateDate(Date createDate) 
    {
        this.createDate = createDate;
    }

    public Date getCreateDate() 
    {
        return createDate;
    }
    public void setStock(Long stock) 
    {
        this.stock = stock;
    }

    public Long getStock() 
    {
        return stock;
    }
    public void setGenericName(String genericName) 
    {
        this.genericName = genericName;
    }

    public String getGenericName() 
    {
        return genericName;
    }
    public void setStatus(Integer status) 
    {
        this.status = status;
    }

    public Integer getStatus() 
    {
        return status;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("code", getCode())
            .append("name", getName())
            .append("format", getFormat())
            .append("price", getPrice())
            .append("unit", getUnit())
            .append("manufacturer", getManufacturer())
            .append("dosageId", getDosageId())
            .append("typeId", getTypeId())
            .append("mnemonicCode", getMnemonicCode())
            .append("createDate", getCreateDate())
            .append("stock", getStock())
            .append("genericName", getGenericName())
            .append("status", getStatus())
            .toString();
    }
}
