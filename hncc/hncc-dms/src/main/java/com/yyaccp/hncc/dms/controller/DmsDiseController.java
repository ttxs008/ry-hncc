package com.yyaccp.hncc.dms.controller;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.yyaccp.hncc.common.util.BeanCopierUtil;
import com.yyaccp.hncc.common.vo.dms.DmsDiseVO;
import com.yyaccp.hncc.dms.domain.DmsDise;
import com.yyaccp.hncc.dms.service.IDmsDiseService;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 诊断类型(疾病)管理Controller
 * 
 * @author ruoyi
 * @date 2020-08-12
 */
@RestController
@RequestMapping("/dms/dise")
@Api(tags = "诊断类型（疾病)")
public class DmsDiseController extends BaseController
{
    @Autowired
    private IDmsDiseService dmsDiseService;

    /**
     * 查询诊断类型(疾病)管理列表
     */
    @ApiOperation(value = "查询",notes = "查询疾病列表,根据疾病id查询未实现",response = TableDataInfo.class)
    @ApiResponses({
            @ApiResponse(code = 204,message = "操作已经执行成功，但是没有返回数据"),
            @ApiResponse(code = 303,message = "重定向"),
            @ApiResponse(code = 400,message = "参数列表错误（缺少，格式不匹配）"),
            @ApiResponse(code = 500,message = "系统内部错误"),
            @ApiResponse(code = 404,message = "资源，服务未找到"),
            @ApiResponse(code = 200,message = "操作成功"),
            @ApiResponse(code = 401,message = "未授权"),
            @ApiResponse(code = 403,message = "访问受限，授权过期")
    })
    @PreAuthorize("@ss.hasPermi('dms:dise:list')")
    @GetMapping("/list")
    public TableDataInfo list(DmsDiseVO dmsDiseVO)
    {
        startPage();
        List<DmsDise> list = dmsDiseService
                .selectDmsDiseList(BeanCopierUtil.copy(dmsDiseVO,DmsDise.class));
        // 先获取总行数
        TableDataInfo tableDataInfo = getDataTable(list);
        // 替换 vo 集合
        tableDataInfo.setRows(BeanCopierUtil.copy(list, DmsDiseVO.class));
        return tableDataInfo;
    }

    /**
     * 导出诊断类型(疾病)管理列表
     */
    @ApiOperation(value = "导出",notes = "导出疾病列表",response = AjaxResult.class)
    @ApiResponses({
            @ApiResponse(code = 204,message = "操作已经执行成功，但是没有返回数据"),
            @ApiResponse(code = 303,message = "重定向"),
            @ApiResponse(code = 400,message = "参数列表错误（缺少，格式不匹配）"),
            @ApiResponse(code = 500,message = "系统内部错误"),
            @ApiResponse(code = 404,message = "资源，服务未找到"),
            @ApiResponse(code = 200,message = "操作成功"),
            @ApiResponse(code = 401,message = "未授权"),
            @ApiResponse(code = 403,message = "访问受限，授权过期")
    })
    @PreAuthorize("@ss.hasPermi('dms:dise:export')")
    @Log(title = "诊断类型(疾病)管理", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(DmsDiseVO dmsDiseVO)
    {
        List<DmsDise> list = dmsDiseService.selectDmsDiseList(BeanCopierUtil.copy(dmsDiseVO,DmsDise.class));
        ExcelUtil<DmsDiseVO> util = new ExcelUtil<DmsDiseVO>(DmsDiseVO.class);
        List<DmsDiseVO> vos = BeanCopierUtil.copy(list,DmsDiseVO.class);
        return util.exportExcel(vos, "dise");
    }

    /**
     * 获取诊断类型(疾病)管理详细信息
     */
    @ApiOperation(value = "详细信息",notes = "根据疾病Id获取疾病详情",response = AjaxResult.class)
    @ApiResponses({
            @ApiResponse(code = 204,message = "操作已经执行成功，但是没有返回数据"),
            @ApiResponse(code = 303,message = "重定向"),
            @ApiResponse(code = 400,message = "参数列表错误（缺少，格式不匹配）"),
            @ApiResponse(code = 500,message = "系统内部错误"),
            @ApiResponse(code = 404,message = "资源，服务未找到"),
            @ApiResponse(code = 200,message = "操作成功"),
            @ApiResponse(code = 401,message = "未授权"),
            @ApiResponse(code = 403,message = "访问受限，授权过期")
    })
    @PreAuthorize("@ss.hasPermi('dms:dise:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@ApiParam(value = "疾病Id",name ="id" ,defaultValue = "1",required = true) @PathVariable("id") Long id)
    {
        return AjaxResult.success(BeanCopierUtil.copy(dmsDiseService.selectDmsDiseById(id),DmsDiseVO.class ));
    }

    /**
     * 新增诊断类型(疾病)管理
     */
    @ApiOperation(value = "新增",notes = "新增一个疾病,id可以不用输入",response = AjaxResult.class)
    @ApiResponses({
            @ApiResponse(code = 204,message = "操作已经执行成功，但是没有返回数据"),
            @ApiResponse(code = 303,message = "重定向"),
            @ApiResponse(code = 400,message = "参数列表错误（缺少，格式不匹配）"),
            @ApiResponse(code = 500,message = " 系统内部错误"),
            @ApiResponse(code = 404,message = " 资源，服务未找到"),
            @ApiResponse(code = 200,message = " 操作成功")
    })
    @PreAuthorize("@ss.hasPermi('dms:dise:add')")
    @Log(title = "诊断类型(疾病)管理", businessType = BusinessType.INSERT)
    @PostMapping

    public AjaxResult add(@RequestBody @ApiParam(value = "DmsDiseVO", name ="诊断分类(疾病)对象VO",type = "DmsDiseVO") DmsDiseVO dmsDiseVO)
    {
        return toAjax(dmsDiseService.insertDmsDise(BeanCopierUtil.copy(dmsDiseVO,DmsDise.class)));
    }

    /**
     * 修改诊断类型(疾病)管理
     */
    @ApiOperation(value = "修改",notes = "修改疾病,id必须输入",response = AjaxResult.class)
    @ApiResponses({
            @ApiResponse(code = 204,message = "操作已经执行成功，但是没有返回数据"),
            @ApiResponse(code = 303,message = "重定向"),
            @ApiResponse(code = 400,message = "参数列表错误（缺少，格式不匹配）"),
            @ApiResponse(code = 500,message = " 系统内部错误"),
            @ApiResponse(code = 404,message = " 资源，服务未找到"),
            @ApiResponse(code = 200,message = " 操作成功")
    })
    @PreAuthorize("@ss.hasPermi('dms:dise:edit')")
    @Log(title = "诊断类型(疾病)管理", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody @ApiParam(value = "DmsDiseVO", name ="诊断分类(疾病)对象VO") DmsDiseVO dmsDiseVO)
    {
        return toAjax(dmsDiseService.updateDmsDise(BeanCopierUtil.copy(dmsDiseVO,DmsDise.class)));
    }

    /**
     * 删除诊断类型(疾病)管理
     */
    @ApiOperation(value = "删除",notes = "根据疾病id删除疾病",response = AjaxResult.class)
    @ApiResponses({
            @ApiResponse(code = 204,message = "操作已经执行成功，但是没有返回数据"),
            @ApiResponse(code = 303,message = "重定向"),
            @ApiResponse(code = 400,message = "参数列表错误（缺少，格式不匹配）"),
            @ApiResponse(code = 500,message = " 系统内部错误"),
            @ApiResponse(code = 404,message = " 资源，服务未找到"),
            @ApiResponse(code = 200,message = " 操作成功")
    })
    @PreAuthorize("@ss.hasPermi('dms:dise:remove')")
    @Log(title = "诊断类型(疾病)管理", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    @ApiImplicitParam(name = "ids", value = "ids,疾病id可以是数组" ,defaultValue = "1" ,required = true )
    public AjaxResult remove( @PathVariable Long[] ids)
    {
        return toAjax(dmsDiseService.deleteDmsDiseByIds(ids));
    }
}
