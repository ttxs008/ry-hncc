package com.yyaccp.hncc.dms.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 草药模版项对象 dms_herbal_model_item
 *
 * @author 周某
 * @date 2020-08-17
 */
public class DmsHerbalModelItem extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** id */
    private Long id;

    /** 状态 */
    @Excel(name = "状态")
    private Integer status;

    /** 说明 */
    @Excel(name = "说明")
    private String footnote;

    /** 药品id */
    @Excel(name = "药品id")
    private Long drugId;

    /** 用量 */
    @Excel(name = "用量")
    private Long usageNum;

    /** 用量单位 */
    @Excel(name = "用量单位")
    private Integer usageNumUnit;

    /** 药品模板id */
    @Excel(name = "药品模板id")
    private Long modelId;
    /**药品*/
    private DmsDrug drug;
    /**模板ID*/
    private DmsDrugModel drugModel;

    public DmsDrug getDrug() {
        return drug;
    }

    public void setDrug(DmsDrug drug) {
        this.drug = drug;
    }

    public DmsDrugModel getDrugModel() {
        return drugModel;
    }

    public void setDrugModel(DmsDrugModel drugModel) {
        this.drugModel = drugModel;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setStatus(Integer status) 
    {
        this.status = status;
    }

    public Integer getStatus() 
    {
        return status;
    }
    public void setFootnote(String footnote) 
    {
        this.footnote = footnote;
    }

    public String getFootnote() 
    {
        return footnote;
    }
    public void setDrugId(Long drugId) 
    {
        this.drugId = drugId;
    }

    public Long getDrugId() 
    {
        return drugId;
    }
    public void setUsageNum(Long usageNum) 
    {
        this.usageNum = usageNum;
    }

    public Long getUsageNum() 
    {
        return usageNum;
    }
    public void setUsageNumUnit(Integer usageNumUnit) 
    {
        this.usageNumUnit = usageNumUnit;
    }

    public Integer getUsageNumUnit() 
    {
        return usageNumUnit;
    }
    public void setModelId(Long modelId) 
    {
        this.modelId = modelId;
    }

    public Long getModelId() 
    {
        return modelId;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("status", getStatus())
            .append("footnote", getFootnote())
            .append("drugId", getDrugId())
            .append("usageNum", getUsageNum())
            .append("usageNumUnit", getUsageNumUnit())
            .append("modelId", getModelId())
            .toString();
    }
}
