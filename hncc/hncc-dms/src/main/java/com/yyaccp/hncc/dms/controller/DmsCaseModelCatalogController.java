package com.yyaccp.hncc.dms.controller;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.yyaccp.hncc.common.util.BeanCopierUtil;
import com.yyaccp.hncc.common.vo.dms.DmsCaseModelCatalogVO;
import com.yyaccp.hncc.dms.domain.DmsCaseModelCatalog;
import com.yyaccp.hncc.dms.service.IDmsCaseModelCatalogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 病例模版目录Controller
 *
 * @author ruoyi
 * @date 2020-08-15
 */
@RestController
@RequestMapping("/dms/dms_case_model_catalog")
public class DmsCaseModelCatalogController extends BaseController {
    @Autowired
    private IDmsCaseModelCatalogService dmsCaseModelCatalogService;

    /**
     * 查询病例模版目录列表
     */
    @PreAuthorize("@ss.hasPermi('dms:dms_case_model_catalog:list')")
    @GetMapping("/list")
    public AjaxResult list(DmsCaseModelCatalogVO dmsCaseModelCatalog) {
        List<DmsCaseModelCatalog> list = dmsCaseModelCatalogService.selectDmsCaseModelCatalogList(BeanCopierUtil.copy(dmsCaseModelCatalog, DmsCaseModelCatalog.class));
        return AjaxResult.success(BeanCopierUtil.copy(list, DmsCaseModelCatalogVO.class));
    }

    /**
     * 导出病例模版目录列表
     */
    @PreAuthorize("@ss.hasPermi('dms:dms_case_model_catalog:export')")
    @Log(title = "病例模版目录", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(DmsCaseModelCatalogVO dmsCaseModelCatalog) {
        List<DmsCaseModelCatalog> list = dmsCaseModelCatalogService.selectDmsCaseModelCatalogList(BeanCopierUtil.copy(dmsCaseModelCatalog, DmsCaseModelCatalog.class));
        ExcelUtil<DmsCaseModelCatalog> util = new ExcelUtil<>(DmsCaseModelCatalog.class);
        return util.exportExcel(list, "dms_case_model_catalog");
    }

    /**
     * 获取病例模版目录详细信息
     */
    @PreAuthorize("@ss.hasPermi('dms:dms_case_model_catalog:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id) {
        return AjaxResult.success(BeanCopierUtil.copy(dmsCaseModelCatalogService.selectDmsCaseModelCatalogById(id), DmsCaseModelCatalogVO.class));
    }

    /**
     * 新增病例模版目录
     */
    @PreAuthorize("@ss.hasPermi('dms:dms_case_model_catalog:add')")
    @Log(title = "病例模版目录", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody DmsCaseModelCatalogVO dmsCaseModelCatalog) {
        return toAjax(dmsCaseModelCatalogService.insertDmsCaseModelCatalog(BeanCopierUtil.copy(dmsCaseModelCatalog, DmsCaseModelCatalog.class)));
    }

    /**
     * 修改病例模版目录
     */
    @PreAuthorize("@ss.hasPermi('dms:dms_case_model_catalog:edit')")
    @Log(title = "病例模版目录", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody DmsCaseModelCatalogVO dmsCaseModelCatalog) {
        return toAjax(dmsCaseModelCatalogService.updateDmsCaseModelCatalog(BeanCopierUtil.copy(dmsCaseModelCatalog, DmsCaseModelCatalog.class)));
    }

    /**
     * 删除病例模版目录
     */
    @PreAuthorize("@ss.hasPermi('dms:dms_case_model_catalog:remove')")
    @Log(title = "病例模版目录", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids) {
        return toAjax(dmsCaseModelCatalogService.deleteDmsCaseModelCatalogByIds(ids));
    }
}
