package com.yyaccp.hncc.dms.service;

import com.yyaccp.hncc.dms.domain.DmsPharmacyDrug;

import java.util.List;

/**
 * @Author 余归
 * @Date 2020/9/15 0015 15:03
 * @Version 1.0
 */
public interface IDmsPharmacyDrugService {

    /**
     * 查询成药药房药品  (患者,已发药/未发药)
     */
    List<DmsPharmacyDrug> queryOverTheCounterPharmacyDrugs(Long registrationId, Long status);

    /**
     * 查询草药药房药品  (患者,已发药/未发药)
     */
    List<DmsPharmacyDrug> inquireAboutHerbalPharmacyDrugs(Long registrationId, Long status);


}
