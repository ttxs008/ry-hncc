package com.yyaccp.hncc.dms.service;

import com.yyaccp.hncc.dms.domain.DmsDise;

import java.util.List;

/**
 * 诊断类型(疾病)管理Service接口
 * 
 * @author ruoyi
 * @date 2020-08-12
 */
public interface IDmsDiseService 
{
    /**
     * 查询诊断类型(疾病)管理
     * 
     * @param id 诊断类型(疾病)管理ID
     * @return 诊断类型(疾病)管理
     */
    public DmsDise selectDmsDiseById(Long id);

    /**
     * 查询诊断类型(疾病)管理列表
     * 
     * @param dmsDise 诊断类型(疾病)管理
     * @return 诊断类型(疾病)管理集合
     */
    public List<DmsDise> selectDmsDiseList(DmsDise dmsDise);

    /**
     * 新增诊断类型(疾病)管理
     * 
     * @param dmsDise 诊断类型(疾病)管理
     * @return 结果
     */
    public int insertDmsDise(DmsDise dmsDise);

    /**
     * 修改诊断类型(疾病)管理
     * 
     * @param dmsDise 诊断类型(疾病)管理
     * @return 结果
     */
    public int updateDmsDise(DmsDise dmsDise);

    /**
     * 批量删除诊断类型(疾病)管理
     * 
     * @param ids 需要删除的诊断类型(疾病)管理ID
     * @return 结果
     */
    public int deleteDmsDiseByIds(Long[] ids);

    /**
     * 删除诊断类型(疾病)管理信息
     * 
     * @param id 诊断类型(疾病)管理ID
     * @return 结果
     */
    public int deleteDmsDiseById(Long id);

    /**
     * 根据ids查询诊断类型管理集合
     * @param ids 需要查询的数据ID
     * @param name 需要查询的数据name
     * @return 结果
     */
    public List<DmsDise> selectDmsDiseListByIdsOrName( String ids,String name);
}
