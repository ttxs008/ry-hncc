package com.yyaccp.hncc.dms.mapper;

import com.yyaccp.hncc.dms.domain.DmsPharmacyDrug;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @Author 余归
 * @Date 2020/9/15 0015 14:52
 * @Version 1.0
 */
public interface DmsPharmacyDrugMapper {

    /**
     * 查询成药药房药品  (患者,已发药/未发药)
     */
    List<DmsPharmacyDrug> queryOverTheCounterPharmacyDrugs(@Param("registrationId") Long registrationId, @Param("status") Long status);

    /**
     * 查询草药药房药品  (患者,已发药/未发药)
     */
    List<DmsPharmacyDrug> inquireAboutHerbalPharmacyDrugs(@Param("registrationId") Long registrationId, @Param("status") Long status);
}
