package com.yyaccp.hncc.dms.service.impl;

import com.yyaccp.hncc.dms.domain.DmsDise;
import com.yyaccp.hncc.dms.mapper.DmsDiseMapper;
import com.yyaccp.hncc.dms.service.IDmsDiseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 诊断类型(疾病)管理Service业务层处理
 * 
 * @author ruoyi
 * @date 2020-08-12
 */
@Service
public class DmsDiseServiceImpl implements IDmsDiseService
{
    @Autowired
    private DmsDiseMapper dmsDiseMapper;

    /**
     * 查询诊断类型(疾病)管理
     * 
     * @param id 诊断类型(疾病)管理ID
     * @return 诊断类型(疾病)管理
     */
    @Override
    public DmsDise selectDmsDiseById(Long id)
    {
        return dmsDiseMapper.selectDmsDiseById(id);
    }

    /**
     * 查询诊断类型(疾病)管理列表
     * 
     * @param dmsDise 诊断类型(疾病)管理
     * @return 诊断类型(疾病)管理
     */
    @Override
    public List<DmsDise> selectDmsDiseList(DmsDise dmsDise)
    {
        return dmsDiseMapper.selectDmsDiseList(dmsDise);
    }

    /**
     * 新增诊断类型(疾病)管理
     * 
     * @param dmsDise 诊断类型(疾病)管理
     * @return 结果
     */
    @Override
    public int insertDmsDise(DmsDise dmsDise)
    {
        return dmsDiseMapper.insertDmsDise(dmsDise);
    }

    /**
     * 修改诊断类型(疾病)管理
     * 
     * @param dmsDise 诊断类型(疾病)管理
     * @return 结果
     */
    @Override
    public int updateDmsDise(DmsDise dmsDise)
    {
        return dmsDiseMapper.updateDmsDise(dmsDise);
    }

    /**
     * 批量删除诊断类型(疾病)管理
     * 
     * @param ids 需要删除的诊断类型(疾病)管理ID
     * @return 结果
     */
    @Override
    public int deleteDmsDiseByIds(Long[] ids)
    {
        return dmsDiseMapper.deleteDmsDiseByIds(ids);
    }

    /**
     * 删除诊断类型(疾病)管理信息
     * 
     * @param id 诊断类型(疾病)管理ID
     * @return 结果
     */
    @Override
    public int deleteDmsDiseById(Long id)
    {
        return dmsDiseMapper.deleteDmsDiseById(id);
    }

    /**
     * 根据ids查询诊断类型管理集合
     *
     * @param ids 需要查询的数据ID
     * @return 结果
     */
    @Override
    public List<DmsDise> selectDmsDiseListByIdsOrName(String ids,String name) {
        return dmsDiseMapper.selectDmsDiseListByIdsOrName(ids==null?null:ids.split(","),name);
    }
}
