package com.yyaccp.hncc.dms.controller;

import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.yyaccp.hncc.common.util.BeanCopierUtil;
import com.yyaccp.hncc.common.vo.dms.DmsPatientBillVO;
import com.yyaccp.hncc.dms.service.IDmsPatientBillService;
import io.swagger.annotations.*;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * Created by 余归 on 2020/8/24
 *
 * @author 余归
 */
@Api(tags = "患者账单")
@RestController
@RequestMapping("/dms/patient_bill")
public class DmsPatientBillController extends BaseController {

    @Resource
    private IDmsPatientBillService dmsPatientBillService;

    /**
     * 查询患者账单
     */
    //@PreAuthorize("@ss.hasPermi('dms:patient_bill:list')")
    @GetMapping("/list")
    @ApiOperation(value = "查询患者账单", notes = "查询患者账单",
            code = 200, produces = "application/json", protocols = "Http",
            response = TableDataInfo.class, httpMethod = "GET")
    @ApiResponses({
            @ApiResponse(code = 204, message = "操作已经执行成功，但是没有返回数据"),
            @ApiResponse(code = 303, message = "重定向"),
            @ApiResponse(code = 400, message = "参数列表错误（缺少，格式不匹配）"),
            @ApiResponse(code = 500, message = "系统内部错误"),
            @ApiResponse(code = 404, message = "资源，服务未找到"),
            @ApiResponse(code = 200, message = "操作成功"),
            @ApiResponse(code = 401, message = "未授权"),
            @ApiResponse(code = 403, message = "访问受限，授权过期")
    })
    @ApiParam(name = "registrationId(患者挂号id)", type = "Integer")
    public AjaxResult list(Integer registrationId) {
        return AjaxResult.success(BeanCopierUtil.copy(dmsPatientBillService.selectPatientBills(registrationId), DmsPatientBillVO.class));
    }

}
