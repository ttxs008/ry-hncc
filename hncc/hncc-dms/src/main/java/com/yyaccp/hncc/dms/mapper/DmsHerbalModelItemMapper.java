package com.yyaccp.hncc.dms.mapper;

import java.util.List;

import com.yyaccp.hncc.dms.domain.DmsDrug;
import com.yyaccp.hncc.dms.domain.DmsHerbalModelItem;

/**
 * 草药模版项Mapper接口
 *
 * @author 周某
 * @date 2020-08-17
 */
public interface DmsHerbalModelItemMapper 
{
    /**
     * 查询草药模版项
     * 
     * @param id 草药模版项ID
     * @return 草药模版项
     */
    public DmsHerbalModelItem selectDmsHerbalModelItemById(Long id);

    /**
     * 查询草药模版项列表
     * 
     * @param dmsHerbalModelItem 草药模版项
     * @return 草药模版项集合
     */
    public List<DmsHerbalModelItem> selectDmsHerbalModelItemList(DmsHerbalModelItem dmsHerbalModelItem);

    /**
     * 新增草药模版项
     * 
     * @param dmsHerbalModelItem 草药模版项
     * @return 结果
     */
    public int insertDmsHerbalModelItem(DmsHerbalModelItem dmsHerbalModelItem);

    /**
     * 修改草药模版项
     * 
     * @param dmsHerbalModelItem 草药模版项
     * @return 结果
     */
    public int updateDmsHerbalModelItem(DmsHerbalModelItem dmsHerbalModelItem);

    /**
     * 删除草药模版项
     * 
     * @param id 草药模版项ID
     * @return 结果
     */
    public int deleteDmsHerbalModelItemById(Long id);

    /**
     * 批量删除草药模版项
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteDmsHerbalModelItemByIds(Long[] ids);

    /**
     * 根据模板Id查询模板明细的药品信息集合
     * @param id 草药模版项ID
     * @return 药品信息集合
     */
    List<DmsHerbalModelItem> selectDrugInfoByModelId(Long id);
}
