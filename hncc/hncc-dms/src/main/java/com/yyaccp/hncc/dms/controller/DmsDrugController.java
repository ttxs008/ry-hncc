package com.yyaccp.hncc.dms.controller;

import java.util.ArrayList;
import java.util.List;


import com.yyaccp.hncc.common.util.BeanCopierUtil;


import com.yyaccp.hncc.common.vo.dms.DmsDrugVO;
import io.swagger.annotations.*;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.yyaccp.hncc.dms.domain.DmsDrug;
import com.yyaccp.hncc.dms.service.IDmsDrugService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 药品项目（包括了重要、中成药、草药）Controller
 *
 * @author 周某
 * @date 2020-08-12
 */
@Api(tags = "药品项目管理（包括了重要、中成药、草药）")
@RestController
@RequestMapping("/dms/drug")
public class DmsDrugController extends BaseController
{
    @Autowired
    private IDmsDrugService dmsDrugService;

    /**
     * 查询药品项目（包括了重要、中成药、草药）列表
     */
    @PreAuthorize("@ss.hasPermi('dms:drug:list')")
    @GetMapping("/list")
    @ApiResponses({
            @ApiResponse(code = 204,message = "操作已经执行成功，但是没有返回数据"),
            @ApiResponse(code = 303,message = "重定向"),
            @ApiResponse(code = 400,message = "参数列表错误（缺少，格式不匹配）"),
            @ApiResponse(code = 500,message = "系统内部错误"),
            @ApiResponse(code = 404,message = "资源，服务未找到"),
            @ApiResponse(code = 200,message = "操作成功"),
            @ApiResponse(code = 401,message = "未授权"),
            @ApiResponse(code = 403,message = "访问受限，授权过期")
    })
    @ApiOperation(value = "查询药品项目（包括了重要、中成药、草药）", notes = "查询药品项目（包括了重要、中成药、草药）",
            code = 200, produces = "application/json", protocols = "Http",
            response = TableDataInfo.class, httpMethod = "GET")
    @ApiParam(name = "药品DmsDrugVO对象", type = "DmsDrugVO")
    public TableDataInfo list(DmsDrugVO dmsDrugVO)
    {
        //将Vo转化为实体
        DmsDrug dmsDrug=BeanCopierUtil.copy(dmsDrugVO,DmsDrug.class);
        startPage();
        List<DmsDrug> dmsDrugList= dmsDrugService.selectDmsDrugList(dmsDrug);
        TableDataInfo tableDataInfo=getDataTable(dmsDrugList);
        //替换集合
        tableDataInfo.setRows(BeanCopierUtil.copy(tableDataInfo.getRows(), DmsDrugVO.class));
        return tableDataInfo;
    }

    /**
     * 导出药品项目（包括了重要、中成药、草药）列表
     */
    @PreAuthorize("@ss.hasPermi('dms:drug:export')")
    @Log(title = "药品项目（包括了重要、中成药、草药）", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    @ApiResponses({
            @ApiResponse(code = 204,message = "操作已经执行成功，但是没有返回数据"),
            @ApiResponse(code = 303,message = "重定向"),
            @ApiResponse(code = 400,message = "参数列表错误（缺少，格式不匹配）"),
            @ApiResponse(code = 500,message = "系统内部错误"),
            @ApiResponse(code = 404,message = "资源，服务未找到"),
            @ApiResponse(code = 200,message = "操作成功"),
            @ApiResponse(code = 401,message = "未授权"),
            @ApiResponse(code = 403,message = "访问受限，授权过期")
    })
    @ApiOperation(value = "导出药品项目（包括了重要、中成药、草药）列表", notes = "导出药品项目（包括了重要、中成药、草药）列表",
            code = 200, produces = "application/json", protocols = "Http",
            response = AjaxResult.class, httpMethod = "GET")
    @ApiParam(name = "药品DmsDrugVO对象", type = "DmsDrugVO")
    public AjaxResult export(DmsDrugVO dmsDrugVO)
    {
        //将VO转化为实体
        DmsDrug dmsDrug=BeanCopierUtil.copy(dmsDrugVO,DmsDrug.class);
        List<DmsDrugVO> list =BeanCopierUtil.copy(dmsDrugService.selectDmsDrugList(dmsDrug),DmsDrugVO.class);
        ExcelUtil<DmsDrugVO> util = new ExcelUtil<DmsDrugVO>(DmsDrugVO.class);
        return util.exportExcel(list, "drug");
    }

    /**
     * 获取药品项目（包括了重要、中成药、草药）详细信息
     */
    @PreAuthorize("@ss.hasPermi('dms:drug:query')")
    @GetMapping(value = "/{id}")
    @ApiResponses({
            @ApiResponse(code = 204,message = "操作已经执行成功，但是没有返回数据"),
            @ApiResponse(code = 303,message = "重定向"),
            @ApiResponse(code = 400,message = "参数列表错误（缺少，格式不匹配）"),
            @ApiResponse(code = 500,message = "系统内部错误"),
            @ApiResponse(code = 404,message = "资源，服务未找到"),
            @ApiResponse(code = 200,message = "操作成功"),
            @ApiResponse(code = 401,message = "未授权"),
            @ApiResponse(code = 403,message = "访问受限，授权过期")
    })
    @ApiOperation(value = "获取药品项目（包括了重要、中成药、草药）详细信息", notes = "获取药品项目（包括了重要、中成药、草药）详细信息",
            code = 200, produces = "application/json", protocols = "Http",
            response = AjaxResult.class, httpMethod = "GET")
    @ApiParam(name = "药品ID", type = "Long")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(BeanCopierUtil.copy(dmsDrugService.selectDmsDrugById(id),DmsDrugVO.class));
    }

    /**
     * 新增药品项目（包括了重要、中成药、草药）
     */
    @PreAuthorize("@ss.hasPermi('dms:drug:add')")
    @Log(title = "药品项目（包括了重要、中成药、草药）", businessType = BusinessType.INSERT)
    @PostMapping
    @ApiResponses({
            @ApiResponse(code = 204,message = "操作已经执行成功，但是没有返回数据"),
            @ApiResponse(code = 303,message = "重定向"),
            @ApiResponse(code = 400,message = "参数列表错误（缺少，格式不匹配）"),
            @ApiResponse(code = 500,message = "系统内部错误"),
            @ApiResponse(code = 404,message = "资源，服务未找到"),
            @ApiResponse(code = 200,message = "操作成功"),
            @ApiResponse(code = 401,message = "未授权"),
            @ApiResponse(code = 403,message = "访问受限，授权过期")
    })
    @ApiOperation(value = "新增药品项目（包括了重要、中成药、草药）", notes = "新增药品项目（包括了重要、中成药、草药）",
            code = 200, produces = "application/json", protocols = "Http",
            response = AjaxResult.class, httpMethod = "POST")
    @ApiParam(name = "药品DmsDrugVO对象", type = "DmsDrugVO")
    public AjaxResult add(@RequestBody DmsDrugVO dmsDrugVO)
    {
        return toAjax(dmsDrugService.insertDmsDrug(BeanCopierUtil.copy(dmsDrugVO,DmsDrug.class)));
    }



        /**
         * 修改药品项目（包括了重要、中成药、草药）
         */
    @PreAuthorize("@ss.hasPermi('dms:drug:edit')")
    @Log(title = "药品项目（包括了重要、中成药、草药）", businessType = BusinessType.UPDATE)
    @PutMapping
    @ApiResponses({
            @ApiResponse(code = 204,message = "操作已经执行成功，但是没有返回数据"),
            @ApiResponse(code = 303,message = "重定向"),
            @ApiResponse(code = 400,message = "参数列表错误（缺少，格式不匹配）"),
            @ApiResponse(code = 500,message = "系统内部错误"),
            @ApiResponse(code = 404,message = "资源，服务未找到"),
            @ApiResponse(code = 200,message = "操作成功"),
            @ApiResponse(code = 401,message = "未授权"),
            @ApiResponse(code = 403,message = "访问受限，授权过期")
    })
    @ApiOperation(value = "修改药品项目（包括了重要、中成药、草药）", notes = "修改药品项目（包括了重要、中成药、草药）",
            code = 200, produces = "application/json", protocols = "Http",
            response = AjaxResult.class, httpMethod = "PUT")
    @ApiParam(name = "药品DmsDrugVO对象", type = "DmsDrugVO")
    public AjaxResult edit(@RequestBody DmsDrugVO dmsDrugVO)
    {
        return toAjax(dmsDrugService.updateDmsDrug(BeanCopierUtil.copy(dmsDrugVO,DmsDrug.class)));
    }

    /**
     * 删除药品项目（包括了重要、中成药、草药）
     */
    @PreAuthorize("@ss.hasPermi('dms:drug:remove')")
    @Log(title = "药品项目（包括了重要、中成药、草药）", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    @ApiResponses({
            @ApiResponse(code = 204,message = "操作已经执行成功，但是没有返回数据"),
            @ApiResponse(code = 303,message = "重定向"),
            @ApiResponse(code = 400,message = "参数列表错误（缺少，格式不匹配）"),
            @ApiResponse(code = 500,message = "系统内部错误"),
            @ApiResponse(code = 404,message = "资源，服务未找到"),
            @ApiResponse(code = 200,message = "操作成功"),
            @ApiResponse(code = 401,message = "未授权"),
            @ApiResponse(code = 403,message = "访问受限，授权过期")
    })
    @ApiOperation(value = "删除药品项目（包括了重要、中成药、草药）", notes = "删除药品项目（包括了重要、中成药、草药）",
            code = 200, produces = "application/json", protocols = "Http",
            response = AjaxResult.class, httpMethod = "DELETE")
    @ApiParam(name = "药品ID", type = "Long[]")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(dmsDrugService.deleteDmsDrugByIds(ids));
    }
}
