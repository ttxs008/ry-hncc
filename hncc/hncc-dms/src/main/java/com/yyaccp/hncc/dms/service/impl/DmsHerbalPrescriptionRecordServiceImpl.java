package com.yyaccp.hncc.dms.service.impl;

import com.ruoyi.common.utils.DateUtils;
import com.yyaccp.hncc.dms.domain.DmsHerbalPrescriptionRecord;
import com.yyaccp.hncc.dms.mapper.DmsHerbalPrescriptionRecordMapper;
import com.yyaccp.hncc.dms.service.IDmsHerbalPrescriptionRecordService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 草处方Service业务层处理
 * 
 * @author ruoyi
 * @date 2020-09-07
 */
@Service
public class DmsHerbalPrescriptionRecordServiceImpl implements IDmsHerbalPrescriptionRecordService 
{
    @Autowired
    private DmsHerbalPrescriptionRecordMapper dmsHerbalPrescriptionRecordMapper;

    /**
     * 查询草处方
     * 
     * @param id 草处方ID
     * @return 草处方
     */
    @Override
    public DmsHerbalPrescriptionRecord selectDmsHerbalPrescriptionRecordById(Long id)
    {
        return dmsHerbalPrescriptionRecordMapper.selectDmsHerbalPrescriptionRecordById(id);
    }

    /**
     * 查询草处方列表
     * 
     * @param dmsHerbalPrescriptionRecord 草处方
     * @return 草处方
     */
    @Override
    public List<DmsHerbalPrescriptionRecord> selectDmsHerbalPrescriptionRecordList(DmsHerbalPrescriptionRecord dmsHerbalPrescriptionRecord)
    {
        return dmsHerbalPrescriptionRecordMapper.selectDmsHerbalPrescriptionRecordList(dmsHerbalPrescriptionRecord);
    }

    /**
     * 新增草处方
     * 
     * @param dmsHerbalPrescriptionRecord 草处方
     * @return 结果
     */
    @Override
    public int insertDmsHerbalPrescriptionRecord(DmsHerbalPrescriptionRecord dmsHerbalPrescriptionRecord)
    {
        dmsHerbalPrescriptionRecord.setCreateTime(DateUtils.getNowDate());
        return dmsHerbalPrescriptionRecordMapper.insertDmsHerbalPrescriptionRecord(dmsHerbalPrescriptionRecord);
    }

    /**
     * 修改草处方
     * 
     * @param dmsHerbalPrescriptionRecord 草处方
     * @return 结果
     */
    @Override
    public int updateDmsHerbalPrescriptionRecord(DmsHerbalPrescriptionRecord dmsHerbalPrescriptionRecord)
    {
        return dmsHerbalPrescriptionRecordMapper.updateDmsHerbalPrescriptionRecord(dmsHerbalPrescriptionRecord);
    }

    /**
     * 批量删除草处方
     * 
     * @param ids 需要删除的草处方ID
     * @return 结果
     */
    @Override
    public int deleteDmsHerbalPrescriptionRecordByIds(Long[] ids)
    {
        return dmsHerbalPrescriptionRecordMapper.deleteDmsHerbalPrescriptionRecordByIds(ids);
    }

    /**
     * 删除草处方信息
     *
     * @param id 草处方ID
     * @return 结果
     */
    @Override
    public int deleteDmsHerbalPrescriptionRecordById(Long id) {
        return dmsHerbalPrescriptionRecordMapper.deleteDmsHerbalPrescriptionRecordById(id);
    }

    @Override
    public int batchRevisionOfDraftPrescriptions(List<Long> ids, Integer status) {
        return dmsHerbalPrescriptionRecordMapper.updateDmsHerbalPrescriptionRecordStatusByIds(ids, status);
    }
}
