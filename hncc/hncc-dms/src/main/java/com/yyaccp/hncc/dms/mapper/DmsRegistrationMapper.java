package com.yyaccp.hncc.dms.mapper;

import java.util.HashMap;
import java.util.List;
import com.yyaccp.hncc.dms.domain.DmsRegistration;
import org.apache.ibatis.annotations.Param;

/**
 * 就诊(门诊)信息Mapper接口
 * 
 * @author ruoyi
 * @date 2020-08-20
 */
public interface DmsRegistrationMapper 
{
    /**
     * 查询就诊(门诊)信息
     * 
     * @param id 就诊(门诊)信息ID
     * @return 就诊(门诊)信息
     */
    public DmsRegistration selectDmsRegistrationById(Long id);

    /**
     * 查询就诊(门诊)信息列表
     * 
     * @param dmsRegistration 就诊(门诊)信息
     * @return 就诊(门诊)信息集合
     */
    public List<DmsRegistration> selectDmsRegistrationList(DmsRegistration dmsRegistration);

    /**
     * 新增就诊(门诊)信息
     * 
     * @param dmsRegistration 就诊(门诊)信息
     * @return 结果
     */
    public int insertDmsRegistration(DmsRegistration dmsRegistration);

    /**
     * 修改就诊(门诊)信息
     * 
     * @param dmsRegistration 就诊(门诊)信息
     * @return 结果
     */
    public int updateDmsRegistration(DmsRegistration dmsRegistration);

    /**
     * 删除就诊(门诊)信息
     * 
     * @param id 就诊(门诊)信息ID
     * @return 结果
     */
    public int deleteDmsRegistrationById(Long id);

    /**
     * 批量删除就诊(门诊)信息
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteDmsRegistrationByIds(Long[] ids);

    /**
     * 查询就诊(门诊)信息(一个病人一个病历号，多次就诊，多次就诊信息)列表
     * 根据用户的姓名或病例号
     * @param name 用户姓名
     * @param medicalRecordNo 病历号
     * @return 就诊(门诊)信息(一个病人一个病历号，多次就诊，多次就诊信息)集合
     */
    public List<DmsRegistration> selectListByNameAndMedicalRecordNo(@Param("name") String name, @Param("medicalRecordNo")String medicalRecordNo);

    /**
     *  查询缴费列表
     *
     * @param registerId 挂号Id
     * @return 缴费列表集合
     */
    public List<HashMap<String, Object>> selectPayList(Long registerId);


    /**
     *  查询退费列表
     *
     * @param registerId 挂号Id
     * @return 退费列表集合
     */
    public List<HashMap<String, Object>> selectRefundList(Long registerId);

}
