package com.yyaccp.hncc.dms.mq;

/**
 * 取消订单服务接口，需要定时过期或取消订单的业务类需要实现该接口
 * @author 天天向上 （john.yi@qq.com）
 * @date 2020/9/6.
 */
public interface IExpireOrderService {
    /**
     * 修改指定id的订单状态为已过期
     * @param orderId
     * @return 修改的记录数
     */
    int expire(Long orderId);
}
