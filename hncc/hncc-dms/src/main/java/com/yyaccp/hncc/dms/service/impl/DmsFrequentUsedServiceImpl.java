package com.yyaccp.hncc.dms.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.yyaccp.hncc.dms.mapper.DmsFrequentUsedMapper;
import com.yyaccp.hncc.dms.domain.DmsFrequentUsed;
import com.yyaccp.hncc.dms.service.IDmsFrequentUsedService;

/**
 * 医生常用诊断（疾病）、检查检验(搁置)Service业务层处理
 * 
 * @author ruoyi
 * @date 2020-08-20
 */
@Service
public class DmsFrequentUsedServiceImpl implements IDmsFrequentUsedService 
{
    @Autowired
    private DmsFrequentUsedMapper dmsFrequentUsedMapper;

    /**
     * 查询医生常用诊断（疾病）、检查检验(搁置)
     * 
     * @param id 医生常用诊断（疾病）、检查检验(搁置)ID
     * @return 医生常用诊断（疾病）、检查检验(搁置)
     */
    @Override
    public DmsFrequentUsed selectDmsFrequentUsedById(Long id)
    {
        return dmsFrequentUsedMapper.selectDmsFrequentUsedById(id);
    }

    /**
     * 查询医生常用诊断（疾病）、检查检验(搁置)列表
     * 
     * @param dmsFrequentUsed 医生常用诊断（疾病）、检查检验(搁置)
     * @return 医生常用诊断（疾病）、检查检验(搁置)
     */
    @Override
    public List<DmsFrequentUsed> selectDmsFrequentUsedList(DmsFrequentUsed dmsFrequentUsed)
    {
        return dmsFrequentUsedMapper.selectDmsFrequentUsedList(dmsFrequentUsed);
    }

    /**
     * 新增医生常用诊断（疾病）、检查检验(搁置)
     * 
     * @param dmsFrequentUsed 医生常用诊断（疾病）、检查检验(搁置)
     * @return 结果
     */
    @Override
    public int insertDmsFrequentUsed(DmsFrequentUsed dmsFrequentUsed)
    {
        return dmsFrequentUsedMapper.insertDmsFrequentUsed(dmsFrequentUsed);
    }

    /**
     * 修改医生常用诊断（疾病）、检查检验(搁置)
     * 
     * @param dmsFrequentUsed 医生常用诊断（疾病）、检查检验(搁置)
     * @return 结果
     */
    @Override
    public int updateDmsFrequentUsed(DmsFrequentUsed dmsFrequentUsed)
    {
        return dmsFrequentUsedMapper.updateDmsFrequentUsed(dmsFrequentUsed);
    }

    /**
     * 批量删除医生常用诊断（疾病）、检查检验(搁置)
     * 
     * @param ids 需要删除的医生常用诊断（疾病）、检查检验(搁置)ID
     * @return 结果
     */
    @Override
    public int deleteDmsFrequentUsedByIds(Long[] ids)
    {
        return dmsFrequentUsedMapper.deleteDmsFrequentUsedByIds(ids);
    }

    /**
     * 删除医生常用诊断（疾病）、检查检验(搁置)信息
     * 
     * @param id 医生常用诊断（疾病）、检查检验(搁置)ID
     * @return 结果
     */
    @Override
    public int deleteDmsFrequentUsedById(Long id)
    {
        return dmsFrequentUsedMapper.deleteDmsFrequentUsedById(id);
    }
}
