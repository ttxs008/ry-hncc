package com.yyaccp.hncc.dms.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 医生常用诊断（疾病）、检查检验(搁置)对象 sms_frequent_used
 * 
 * @author ruoyi
 * @date 2020-08-20
 */
public class DmsFrequentUsed extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 常用项id */
    private Long id;

    /** 西医诊断列表 */
    @Excel(name = "西医诊断列表")
    private String medicineDiseIdList;

    /** 中医诊断列表 */
    @Excel(name = "中医诊断列表")
    private String herbalDiseIdList;

    /** 处置项id列表 */
    @Excel(name = "处置项id列表")
    private String dispositionIdList;

    /** 检验项列表 */
    @Excel(name = "检验项列表")
    private String testIdList;

    /** 检查项列表 */
    @Excel(name = "检查项列表")
    private String checkIdList;

    /** 检查模板id列表 */
    @Excel(name = "检查模板id列表")
    private String checkModelIdList;

    /** 处置模板id列表 */
    @Excel(name = "处置模板id列表")
    private String dispositionModelIdList;

    /** 检验模板id列表 */
    @Excel(name = "检验模板id列表")
    private String testModelIdList;

    /** 药品模板id列表 */
    @Excel(name = "药品模板id列表")
    private String drugModelIdList;

    /** 所属用户id */
    @Excel(name = "所属用户id")
    private Long staffId;

    /** 药品列表 */
    @Excel(name = "药品列表")
    private String drugIdList;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setMedicineDiseIdList(String medicineDiseIdList) 
    {
        this.medicineDiseIdList = medicineDiseIdList;
    }

    public String getMedicineDiseIdList() 
    {
        return medicineDiseIdList;
    }
    public void setHerbalDiseIdList(String herbalDiseIdList) 
    {
        this.herbalDiseIdList = herbalDiseIdList;
    }

    public String getHerbalDiseIdList() 
    {
        return herbalDiseIdList;
    }
    public void setDispositionIdList(String dispositionIdList) 
    {
        this.dispositionIdList = dispositionIdList;
    }

    public String getDispositionIdList() 
    {
        return dispositionIdList;
    }
    public void setTestIdList(String testIdList) 
    {
        this.testIdList = testIdList;
    }

    public String getTestIdList() 
    {
        return testIdList;
    }
    public void setCheckIdList(String checkIdList) 
    {
        this.checkIdList = checkIdList;
    }

    public String getCheckIdList() 
    {
        return checkIdList;
    }
    public void setCheckModelIdList(String checkModelIdList) 
    {
        this.checkModelIdList = checkModelIdList;
    }

    public String getCheckModelIdList() 
    {
        return checkModelIdList;
    }
    public void setDispositionModelIdList(String dispositionModelIdList) 
    {
        this.dispositionModelIdList = dispositionModelIdList;
    }

    public String getDispositionModelIdList() 
    {
        return dispositionModelIdList;
    }
    public void setTestModelIdList(String testModelIdList) 
    {
        this.testModelIdList = testModelIdList;
    }

    public String getTestModelIdList() 
    {
        return testModelIdList;
    }
    public void setDrugModelIdList(String drugModelIdList) 
    {
        this.drugModelIdList = drugModelIdList;
    }

    public String getDrugModelIdList() 
    {
        return drugModelIdList;
    }
    public void setStaffId(Long staffId) 
    {
        this.staffId = staffId;
    }

    public Long getStaffId() 
    {
        return staffId;
    }
    public void setDrugIdList(String drugIdList) 
    {
        this.drugIdList = drugIdList;
    }

    public String getDrugIdList() 
    {
        return drugIdList;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("medicineDiseIdList", getMedicineDiseIdList())
            .append("herbalDiseIdList", getHerbalDiseIdList())
            .append("dispositionIdList", getDispositionIdList())
            .append("testIdList", getTestIdList())
            .append("checkIdList", getCheckIdList())
            .append("checkModelIdList", getCheckModelIdList())
            .append("dispositionModelIdList", getDispositionModelIdList())
            .append("testModelIdList", getTestModelIdList())
            .append("drugModelIdList", getDrugModelIdList())
            .append("staffId", getStaffId())
            .append("drugIdList", getDrugIdList())
            .toString();
    }
}
