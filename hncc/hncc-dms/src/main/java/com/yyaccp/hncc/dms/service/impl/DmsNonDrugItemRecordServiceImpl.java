package com.yyaccp.hncc.dms.service.impl;

import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.SecurityUtils;
import com.yyaccp.hncc.common.HnccStatus;
import com.yyaccp.hncc.dms.domain.DmsNonDrugItemRecord;
import com.yyaccp.hncc.dms.mapper.DmsNonDrugItemRecordMapper;
import com.yyaccp.hncc.dms.mq.IExpireOrderService;
import com.yyaccp.hncc.dms.service.IDmsNonDrugItemRecordService;
import com.yyaccp.hncc.mq.FeeType;
import com.yyaccp.hncc.mq.OrderMessage;
import com.yyaccp.hncc.mq.rabbit.OrderMessageSender;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

/**
 * 检查项检验项处置项记录(开立的)Service业务层处理
 *
 * @author ruoyi
 * @date 2020-09-03
 */
@Service
@Slf4j
@RequiredArgsConstructor
public class DmsNonDrugItemRecordServiceImpl implements IDmsNonDrugItemRecordService, IExpireOrderService
{
    private final DmsNonDrugItemRecordMapper dmsNonDrugItemRecordMapper;
    private final OrderMessageSender orderMessageSender;
    @Value("${hncc.mq.order.delayTimes}")
    private int delayTimes;
    /**
     * 查询检查项检验项处置项记录(开立的)
     *
     * @param id 检查项检验项处置项记录(开立的)ID
     * @return 检查项检验项处置项记录(开立的)
     */
    @Override
    public DmsNonDrugItemRecord selectDmsNonDrugItemRecordById(Long id)
    {
        return dmsNonDrugItemRecordMapper.selectDmsNonDrugItemRecordById(id);
    }

    /**
     * 查询检查项检验项处置项记录(开立的)列表
     *
     * @param dmsNonDrugItemRecord 检查项检验项处置项记录(开立的)
     * @return 检查项检验项处置项记录(开立的)
     */
    @Override
    public List<DmsNonDrugItemRecord> selectDmsNonDrugItemRecordList(DmsNonDrugItemRecord dmsNonDrugItemRecord)
    {
        return dmsNonDrugItemRecordMapper.selectDmsNonDrugItemRecordList(dmsNonDrugItemRecord);
    }

    /**
     * 新增检查项检验项处置项记录(开立的)
     *
     * @param dmsNonDrugItemRecord 检查项检验项处置项记录(开立的)
     * @return 结果
     */
    @Override
    public int insertDmsNonDrugItemRecord(DmsNonDrugItemRecord dmsNonDrugItemRecord)
    {
        dmsNonDrugItemRecord.setCreateTime(DateUtils.getNowDate());
        dmsNonDrugItemRecordMapper.insertDmsNonDrugItemRecord(dmsNonDrugItemRecord);
        // 发送订单延迟取消消息
        orderMessageSender.sendMessage(new OrderMessage(dmsNonDrugItemRecord.getId(),
                FeeType.fromIndex(dmsNonDrugItemRecord.getType()),
                delayTimes * 1000L));
        return 1;
    }

    /**
     * 修改检查项检验项处置项记录(开立的)
     *
     * @param dmsNonDrugItemRecord 检查项检验项处置项记录(开立的)
     * @return 结果
     */
    @Override
    public int updateDmsNonDrugItemRecord(DmsNonDrugItemRecord dmsNonDrugItemRecord)
    {
        dmsNonDrugItemRecord.setLogStaffId(SecurityUtils.getLoginUser().getUser().getUserId());
        dmsNonDrugItemRecord.setLogDatetime(new Date());
        return dmsNonDrugItemRecordMapper.updateDmsNonDrugItemRecord(dmsNonDrugItemRecord);
    }

    /**
     * 批量删除检查项检验项处置项记录(开立的)
     *
     * @param ids 需要删除的检查项检验项处置项记录(开立的)ID
     * @return 结果
     */
    @Override
    public int deleteDmsNonDrugItemRecordByIds(Long[] ids)
    {
        return dmsNonDrugItemRecordMapper.deleteDmsNonDrugItemRecordByIds(ids);
    }

    /**
     * 删除检查项检验项处置项记录(开立的)信息
     *
     * @param id 检查项检验项处置项记录(开立的)ID
     * @return 结果
     */
    @Override
    public int deleteDmsNonDrugItemRecordById(Long id)
    {
        return dmsNonDrugItemRecordMapper.deleteDmsNonDrugItemRecordById(id);
    }

    @Override
    public List<DmsNonDrugItemRecord> selectDmsNonDrugItemRecordDealWithList(DmsNonDrugItemRecord dmsNonDrugItemRecord) {
        return dmsNonDrugItemRecordMapper.selectDmsNonDrugItemRecordDealWithList(dmsNonDrugItemRecord);
    }

    @Override
    public int updateDmsNonDrugItemRecordCheckResult(DmsNonDrugItemRecord dmsNonDrugItemRecord) {
        return dmsNonDrugItemRecordMapper.updateDmsNonDrugItemRecordCheckResult(dmsNonDrugItemRecord);
    }

    @Override
    public int expire(Long orderId) {
        int result = 0;
        DmsNonDrugItemRecord order = dmsNonDrugItemRecordMapper.selectDmsNonDrugItemRecordById(orderId);
        if (order == null) {
            log.error("没有找到对应的非药品收费记录：{}，无法过期订单", orderId);
            // 保存异常记录到数据库，待实现
        } else {
            order.setStatus(HnccStatus.NonDrugItem.EXPIRED.getValue());
            result = dmsNonDrugItemRecordMapper.updateDmsNonDrugItemRecord(order);
        }
        return result;
    }
}
