package com.yyaccp.hncc.dms.mq;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import java.util.Map;

/**
 * @author 天天向上 （john.yi@qq.com）
 * @date 2020/9/6.
 */
@Data
@Configuration
@ConfigurationProperties(prefix = "hncc.mq.order")
public class MqOrderConfigProperties {
    private Map<String, MqOrderConfig> properties;
}
