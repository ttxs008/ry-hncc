package com.yyaccp.hncc.dms.controller;

import java.util.List;

import com.yyaccp.hncc.common.vo.dms.DmsDrugVO;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.yyaccp.hncc.dms.domain.DmsHerbalModelItem;
import com.yyaccp.hncc.dms.service.IDmsHerbalModelItemService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import io.swagger.annotations.*;
//com.yyaccp.hncc.common模块的包引入
import com.yyaccp.hncc.common.vo.dms.DmsHerbalModelItemVO;
import com.yyaccp.hncc.common.util.BeanCopierUtil;

import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 草药模版项Controller
 *
 * @author 周某
 * @date 2020-08-17
 */
@Api(tags = "草药模版项")
@RestController
@RequestMapping("/dms/herbal_model_item")
public class DmsHerbalModelItemController extends BaseController
{
    @Autowired
    private IDmsHerbalModelItemService dmsHerbalModelItemService;

/**
 * 查询草药模版项列表
 *
 */
@PreAuthorize("@ss.hasPermi('dms:herbal_model_item:list')")
@GetMapping("/list")
@ApiOperation(value = "查询草药模版项",notes = "查询所有草药模版项",
        code = 200, produces = "application/json",protocols = "Http",
        response = TableDataInfo.class, httpMethod = "GET")
@ApiResponses({
        @ApiResponse(code = 204,message = "操作已经执行成功，但是没有返回数据"),
        @ApiResponse(code = 303,message = "重定向"),
        @ApiResponse(code = 400,message = "参数列表错误（缺少，格式不匹配）"),
        @ApiResponse(code = 500,message = "系统内部错误"),
        @ApiResponse(code = 404,message = "资源，服务未找到"),
        @ApiResponse(code = 200,message = "操作成功"),
        @ApiResponse(code = 401,message = "未授权"),
        @ApiResponse(code = 403,message = "访问受限，授权过期")
})
@ApiParam(name = "DmsHerbalModelItemVO对象", type = "DmsHerbalModelItemVO")
        public TableDataInfo list(DmsHerbalModelItemVO dmsHerbalModelItemVO)
    {
        //将Vo转化为实体
        DmsHerbalModelItem dmsHerbalModelItem=BeanCopierUtil.copy(dmsHerbalModelItemVO,DmsHerbalModelItem.class);
        startPage();
        List<DmsHerbalModelItem> list = dmsHerbalModelItemService.selectDmsHerbalModelItemList(dmsHerbalModelItem);
        TableDataInfo tableDataInfo=getDataTable(list);
        //替换集合
        tableDataInfo.setRows(BeanCopierUtil.copy(tableDataInfo.getRows(), DmsHerbalModelItemVO.class));
        return tableDataInfo;
    }
    
    /**
     * 导出草药模版项列表
     */
    @PreAuthorize("@ss.hasPermi('dms:herbal_model_item:export')")
    @Log(title = "草药模版项", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    @ApiOperation(value = "导出草药模版项表",notes = "导出所有草药模版项",
            code = 200, produces = "application/json",protocols = "Http",
            response = TableDataInfo.class, httpMethod = "GET")
    @ApiResponses({
            @ApiResponse(code = 204,message = "操作已经执行成功，但是没有返回数据"),
            @ApiResponse(code = 303,message = "重定向"),
            @ApiResponse(code = 400,message = "参数列表错误（缺少，格式不匹配）"),
            @ApiResponse(code = 500,message = "系统内部错误"),
            @ApiResponse(code = 404,message = "资源，服务未找到"),
            @ApiResponse(code = 200,message = "操作成功"),
            @ApiResponse(code = 401,message = "未授权"),
            @ApiResponse(code = 403,message = "访问受限，授权过期")
    })
    @ApiParam(name = "DmsHerbalModelItemVO对象", type = "DmsHerbalModelItemVO")
    public AjaxResult export(DmsHerbalModelItemVO dmsHerbalModelItemVO)
    {
        //将VO转化为实体
        DmsHerbalModelItem dmsHerbalModelItem=BeanCopierUtil.copy(dmsHerbalModelItemVO,DmsHerbalModelItem.class);
        List<DmsHerbalModelItemVO> list =BeanCopierUtil.copy(dmsHerbalModelItemService.selectDmsHerbalModelItemList(dmsHerbalModelItem),DmsHerbalModelItemVO.class);
        ExcelUtil<DmsHerbalModelItemVO> util = new ExcelUtil<DmsHerbalModelItemVO>(DmsHerbalModelItemVO.class);
        return util.exportExcel(list, "herbal_model_item");
    }

    /**
     * 获取草药模版项详细信息
     */
    @PreAuthorize("@ss.hasPermi('dms:herbal_model_item:query')")
    @GetMapping(value = "/{id}")
    @ApiOperation(value = "获取草药模版项详细信息",
            notes = "根据草药模版项id获取科室信息", code = 200,
            produces = "application/json",protocols = "Http",response = AjaxResult.class,
            httpMethod = "GET")
    @ApiResponses({
            @ApiResponse(code = 204,message = "操作已经执行成功，但是没有返回数据"),
            @ApiResponse(code = 303,message = "重定向"),
            @ApiResponse(code = 400,message = "参数列表错误（缺少，格式不匹配）"),
            @ApiResponse(code = 500,message = "系统内部错误"),
            @ApiResponse(code = 404,message = "资源，服务未找到"),
            @ApiResponse(code = 200,message = "操作成功"),
            @ApiResponse(code = 401,message = "未授权"),
            @ApiResponse(code = 403,message = "访问受限，授权过期")
    })
    @ApiParam(name = "dmsHerbalModelItem.id", type = "Long")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(BeanCopierUtil.copy(dmsHerbalModelItemService.selectDmsHerbalModelItemById(id),DmsHerbalModelItemVO.class));
    }


    /**
     * 通过模板Id查询药品模板明细
     */
    @PreAuthorize("@ss.hasPermi('dms:herbal_model_item:queryDrugByModelId')")
    @GetMapping(value = "/queryDrugByModelId/{id}")
    @ApiOperation(value = "通过模板Id查询药品信息",
            notes = "通过模板Id查询药品信息", code = 200,
            produces = "application/json",protocols = "Http",response = AjaxResult.class,
            httpMethod = "GET")
    @ApiResponses({
            @ApiResponse(code = 204,message = "操作已经执行成功，但是没有返回数据"),
            @ApiResponse(code = 303,message = "重定向"),
            @ApiResponse(code = 400,message = "参数列表错误（缺少，格式不匹配）"),
            @ApiResponse(code = 500,message = "系统内部错误"),
            @ApiResponse(code = 404,message = "资源，服务未找到"),
            @ApiResponse(code = 200,message = "操作成功"),
            @ApiResponse(code = 401,message = "未授权"),
            @ApiResponse(code = 403,message = "访问受限，授权过期")
    })
    @ApiParam(name = "ID", type = "Long")
    public TableDataInfo getDrugInfoByModelId(@PathVariable("id") Long id)
    {
        startPage();
        List<DmsHerbalModelItem> list = dmsHerbalModelItemService.getDrugInfoByModelId(id);
        TableDataInfo tableDataInfo=getDataTable(list);
        //替换集合
        tableDataInfo.setRows(BeanCopierUtil.copy(tableDataInfo.getRows(), DmsHerbalModelItemVO.class));
        return tableDataInfo;
    }

    /**
     * 新增草药模版项
     */
    @PreAuthorize("@ss.hasPermi('dms:herbal_model_item:add')")
    @Log(title = "新增草药模版项", businessType = BusinessType.INSERT)
    @PostMapping
    @ApiOperation(value = "新增草药模版项信息",notes = "新增草药模版项信息",code = 200,
            produces = "application/json",protocols = "Http",response = AjaxResult.class,
            httpMethod = "POST")
    @ApiResponses({
            @ApiResponse(code = 204,message = "操作已经执行成功，但是没有返回数据"),
            @ApiResponse(code = 303,message = "重定向"),
            @ApiResponse(code = 400,message = "参数列表错误（缺少，格式不匹配）"),
            @ApiResponse(code = 500,message = "系统内部错误"),
            @ApiResponse(code = 404,message = "资源，服务未找到"),
            @ApiResponse(code = 200,message = "操作成功"),
            @ApiResponse(code = 401,message = "未授权"),
            @ApiResponse(code = 403,message = "访问受限，授权过期")
    })
    @ApiParam(name = "DmsHerbalModelItemVO对象", type = "DmsHerbalModelItemVO")
    public AjaxResult add(@RequestBody DmsHerbalModelItemVO dmsHerbalModelItemVO)
    {
        return toAjax(dmsHerbalModelItemService.insertDmsHerbalModelItem(BeanCopierUtil.copy(dmsHerbalModelItemVO,DmsHerbalModelItem.class)));
    }

    /**
     * 修改草药模版项
     */
    @PreAuthorize("@ss.hasPermi('dms:herbal_model_item:edit')")
    @Log(title = "草药模版项", businessType = BusinessType.UPDATE)
    @PutMapping
    @ApiOperation(value = "修改草药模版项信息",notes = "修改草药模版项信息",code = 200,
            produces = "application/json",protocols = "Http",response = AjaxResult.class,
            httpMethod = "PUT")
    @ApiResponses({
            @ApiResponse(code = 204,message = "操作已经执行成功，但是没有返回数据"),
            @ApiResponse(code = 303,message = "重定向"),
            @ApiResponse(code = 400,message = "参数列表错误（缺少，格式不匹配）"),
            @ApiResponse(code = 500,message = "系统内部错误"),
            @ApiResponse(code = 404,message = "资源，服务未找到"),
            @ApiResponse(code = 200,message = "操作成功"),
            @ApiResponse(code = 401,message = "未授权"),
            @ApiResponse(code = 403,message = "访问受限，授权过期")
    })
    @ApiParam(name = "DmsHerbalModelItemVO对象", type = "DmsHerbalModelItemVO")
    public AjaxResult edit(@RequestBody DmsHerbalModelItemVO dmsHerbalModelItemVO)
    {
        return toAjax(dmsHerbalModelItemService.updateDmsHerbalModelItem(BeanCopierUtil.copy(dmsHerbalModelItemVO,DmsHerbalModelItem.class)));
    }

    /**
     * 删除草药模版项
     */
    @PreAuthorize("@ss.hasPermi('dms:herbal_model_item:remove')")
    @Log(title = "草药模版项", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    @ApiOperation(value = "删除草药模版项信息",notes = "删除草药模版项信息",code = 200,
            produces = "application/json",protocols = "Http",response = AjaxResult.class,
            httpMethod = "DELETE")
    @ApiResponses({
            @ApiResponse(code = 204,message = "操作已经执行成功，但是没有返回数据"),
            @ApiResponse(code = 303,message = "重定向"),
            @ApiResponse(code = 400,message = "参数列表错误（缺少，格式不匹配）"),
            @ApiResponse(code = 500,message = "系统内部错误"),
            @ApiResponse(code = 404,message = "资源，服务未找到"),
            @ApiResponse(code = 200,message = "操作成功"),
            @ApiResponse(code = 401,message = "未授权"),
            @ApiResponse(code = 403,message = "访问受限，授权过期")
    })
    @ApiParam(name = "dmsHerbalModelItem.id", type = "Long[]")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(dmsHerbalModelItemService.deleteDmsHerbalModelItemByIds(ids));
    }
}
