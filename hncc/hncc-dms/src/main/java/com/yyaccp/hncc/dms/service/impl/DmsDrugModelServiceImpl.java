package com.yyaccp.hncc.dms.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.yyaccp.hncc.dms.mapper.DmsDrugModelMapper;
import com.yyaccp.hncc.dms.domain.DmsDrugModel;
import com.yyaccp.hncc.dms.service.IDmsDrugModelService;

/**
 * 药品模版Service业务层处理
 *
 * @author 周某
 * @date 2020-08-18
 */
@Service
public class DmsDrugModelServiceImpl implements IDmsDrugModelService 
{
    @Autowired
    private DmsDrugModelMapper dmsDrugModelMapper;

    /**
     * 查询药品模版
     * 
     * @param id 药品模版ID
     * @return 药品模版
     */
    @Override
    public DmsDrugModel selectDmsDrugModelById(Long id)
    {
        return dmsDrugModelMapper.selectDmsDrugModelById(id);
    }

    /**
     * 查询药品模版列表
     * 
     * @param dmsDrugModel 药品模版
     * @return 药品模版
     */
    @Override
    public List<DmsDrugModel> selectDmsDrugModelList(DmsDrugModel dmsDrugModel)
    {
        return dmsDrugModelMapper.selectDmsDrugModelList(dmsDrugModel);
    }

    /**
     * 新增药品模版
     * 
     * @param dmsDrugModel 药品模版
     * @return 结果
     */
    @Override
    public int insertDmsDrugModel(DmsDrugModel dmsDrugModel)
    {
        dmsDrugModel.setCreateTime(DateUtils.getNowDate());
        return dmsDrugModelMapper.insertDmsDrugModel(dmsDrugModel);
    }

    /**
     * 修改药品模版
     * 
     * @param dmsDrugModel 药品模版
     * @return 结果
     */
    @Override
    public int updateDmsDrugModel(DmsDrugModel dmsDrugModel)
    {
        return dmsDrugModelMapper.updateDmsDrugModel(dmsDrugModel);
    }

    /**
     * 批量删除药品模版
     * 
     * @param ids 需要删除的药品模版ID
     * @return 结果
     */
    @Override
    public int deleteDmsDrugModelByIds(Long[] ids)
    {
        return dmsDrugModelMapper.deleteDmsDrugModelByIds(ids);
    }

    /**
     * 删除药品模版信息
     * 
     * @param id 药品模版ID
     * @return 结果
     */
    @Override
    public int deleteDmsDrugModelById(Long id)
    {
        return dmsDrugModelMapper.deleteDmsDrugModelById(id);
    }
}
