package com.yyaccp.hncc.dms.service;

import com.yyaccp.hncc.dms.domain.DmsNonDrug;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 非药品收费项目Service接口
 *
 * @author yugui
 * @date 2020-08-11
 */
public interface IDmsNonDrugService {

    /**
     * 根据项目类型 查询项目编号
     */
    public Integer selectItemNumber(Integer recordType);

    /**
     * 查询非药品收费项目
     *
     * @param id 非药品收费项目ID
     * @return 非药品收费项目
     */
    public DmsNonDrug selectDmsNonDrugById(Long id);

    /**
     * 查询非药品收费项目列表
     *
     * @param dmsNonDrug 非药品收费项目
     * @return 非药品收费项目集合
     */
    public List<DmsNonDrug> selectDmsNonDrugList(DmsNonDrug dmsNonDrug);

    /**
     * 新增非药品收费项目
     *
     * @param dmsNonDrug 非药品收费项目
     * @return 结果
     */
    public int insertDmsNonDrug(DmsNonDrug dmsNonDrug);

    /**
     * 修改非药品收费项目
     *
     * @param dmsNonDrug 非药品收费项目
     * @return 结果
     */
    public int updateDmsNonDrug(DmsNonDrug dmsNonDrug);

    /**
     * 批量删除非药品收费项目
     *
     * @param ids 需要删除的非药品收费项目ID
     * @return 结果
     */
    public int deleteDmsNonDrugByIds(Long[] ids);

    /**
     * 删除非药品收费项目信息
     *
     * @param id 非药品收费项目ID
     * @return 结果
     */
    public int deleteDmsNonDrugById(Long id);

    /**
     * 根据ids查询非药品收费项目集合
     *
     * @param dmsNonDrugIds 需要查询的数据ID
     * @param recordType 需要查询的数据ID
     * @param name 需要查询的数据name
     * @return 结果
     */
    public List<DmsNonDrug> selectDmsNonDrugListByIdsOrTypeOrName(String dmsNonDrugIds,Integer recordType,String name);


}
