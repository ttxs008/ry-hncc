package com.yyaccp.hncc.dms.controller;

import java.util.List;
import java.util.concurrent.TimeUnit;
import com.ruoyi.common.core.redis.RedisCache;
import com.yyaccp.hncc.common.DmsConstants;
import com.yyaccp.hncc.common.HnccType;
import com.yyaccp.hncc.common.vo.dms.DmsNonDrugVO;
import com.yyaccp.hncc.dms.domain.DmsNonDrug;
import com.yyaccp.hncc.pms.domain.PmsPatient;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.yyaccp.hncc.dms.domain.DmsNonDrugItemRecord;
import com.yyaccp.hncc.dms.service.IDmsNonDrugItemRecordService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import io.swagger.annotations.*;
//com.yyaccp.hncc.common模块的包引入
import com.yyaccp.hncc.common.vo.dms.DmsNonDrugItemRecordVO;
import com.yyaccp.hncc.common.util.BeanCopierUtil;

import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 检查项检验项处置项记录(开立的)Controller
 *
 * @author ruoyi
 * @date 2020-09-03
 */
@Api(tags = "检查项检验项处置项记录(开立的)")
@RestController
@RequestMapping("/dms/non_drug_item_record")
public class DmsNonDrugItemRecordController extends BaseController {
    @Autowired
    private RedisCache redisCache;

    @Autowired
    private IDmsNonDrugItemRecordService dmsNonDrugItemRecordService;

    @Autowired
    public RedisTemplate redisTemplate;

/**
 * 查询检查项检验项处置项记录(开立的)列表
 *
 */
@PreAuthorize("@ss.hasPermi('dms:non_drug_item_record:list')")
@GetMapping("/list")
@ApiOperation(value = "查询检查项检验项处置项记录(开立的)" , notes = "查询所有检查项检验项处置项记录(开立的)" ,
        code = 200, produces = "application/json" , protocols = "Http" ,
        response = TableDataInfo.class, httpMethod = "GET")
@ApiResponses({
        @ApiResponse(code = 204, message = "操作已经执行成功，但是没有返回数据"),
        @ApiResponse(code = 303, message = "重定向"),
        @ApiResponse(code = 400, message = "参数列表错误（缺少，格式不匹配）"),
        @ApiResponse(code = 500, message = "系统内部错误"),
        @ApiResponse(code = 404, message = "资源，服务未找到"),
        @ApiResponse(code = 200, message = "操作成功"),
        @ApiResponse(code = 401, message = "未授权"),
        @ApiResponse(code = 403, message = "访问受限，授权过期")
})
@ApiParam(name = "DmsNonDrugItemRecordVO对象" , type = "DmsNonDrugItemRecordVO")
        public TableDataInfo list(String keyword) {
        //将Vo转化为实体
        DmsNonDrugItemRecord dmsNonDrugItemRecord= new DmsNonDrugItemRecord();
        if (keyword!=null) {
            PmsPatient pmsPatient = new PmsPatient();
            pmsPatient.setName(keyword);
            pmsPatient.setMedicalRecordNo(keyword);
            dmsNonDrugItemRecord.setPatient(pmsPatient);
        }
        startPage();
        List<DmsNonDrugItemRecord> list = dmsNonDrugItemRecordService.selectDmsNonDrugItemRecordList(dmsNonDrugItemRecord);
        TableDataInfo tableDataInfo = getDataTable(list);
        //替换集合
        tableDataInfo.setRows(BeanCopierUtil.copy(tableDataInfo.getRows(), DmsNonDrugItemRecordVO.class));
        return tableDataInfo;
    }
    
    /**
     * 导出检查项检验项处置项记录(开立的)列表
     */
    @PreAuthorize("@ss.hasPermi('dms:non_drug_item_record:export')")
    @Log(title = "检查项检验项处置项记录(开立的)" , businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    @ApiOperation(value = "导出检查项检验项处置项记录(开立的)表" , notes = "导出所有检查项检验项处置项记录(开立的)" ,
            code = 200, produces = "application/json" , protocols = "Http" ,
            response = TableDataInfo.class, httpMethod = "GET")
    @ApiResponses({
            @ApiResponse(code = 204, message = "操作已经执行成功，但是没有返回数据"),
            @ApiResponse(code = 303, message = "重定向"),
            @ApiResponse(code = 400, message = "参数列表错误（缺少，格式不匹配）"),
            @ApiResponse(code = 500, message = "系统内部错误"),
            @ApiResponse(code = 404, message = "资源，服务未找到"),
            @ApiResponse(code = 200, message = "操作成功"),
            @ApiResponse(code = 401, message = "未授权"),
            @ApiResponse(code = 403, message = "访问受限，授权过期")
    })
    @ApiParam(name = "DmsNonDrugItemRecordVO对象" , type = "DmsNonDrugItemRecordVO")
    public AjaxResult export(DmsNonDrugItemRecordVO dmsNonDrugItemRecordVO) {
        //将VO转化为实体
        DmsNonDrugItemRecord dmsNonDrugItemRecord=BeanCopierUtil.copy(dmsNonDrugItemRecordVO,DmsNonDrugItemRecord. class);
        List<DmsNonDrugItemRecordVO> list = BeanCopierUtil.copy(dmsNonDrugItemRecordService.selectDmsNonDrugItemRecordList(dmsNonDrugItemRecord),DmsNonDrugItemRecordVO.class);
        ExcelUtil<DmsNonDrugItemRecordVO> util = new ExcelUtil<DmsNonDrugItemRecordVO>(DmsNonDrugItemRecordVO.class);
        return util.exportExcel(list, "non_drug_item_record");
    }

    /**
     * 获取检查项检验项处置项记录(开立的)详细信息
     */
    @PreAuthorize("@ss.hasPermi('dms:non_drug_item_record:query')")
    @GetMapping(value = "/{id}")
    @ApiOperation(value = "获取检查项检验项处置项记录(开立的)详细信息" ,
            notes = "根据检查项检验项处置项记录(开立的)id获取科室信息" , code = 200,
            produces = "application/json" , protocols = "Http" , response = AjaxResult.class,
            httpMethod = "GET")
    @ApiResponses({
            @ApiResponse(code = 204, message = "操作已经执行成功，但是没有返回数据"),
            @ApiResponse(code = 303, message = "重定向"),
            @ApiResponse(code = 400, message = "参数列表错误（缺少，格式不匹配）"),
            @ApiResponse(code = 500, message = "系统内部错误"),
            @ApiResponse(code = 404, message = "资源，服务未找到"),
            @ApiResponse(code = 200, message = "操作成功"),
            @ApiResponse(code = 401, message = "未授权"),
            @ApiResponse(code = 403, message = "访问受限，授权过期")
    })
    @ApiParam(name = "dmsNonDrugItemRecord.id" , type = "Long")
    public AjaxResult getInfo(@PathVariable("id") Long id) {
        return AjaxResult.success(BeanCopierUtil.copy(dmsNonDrugItemRecordService.selectDmsNonDrugItemRecordById(id),DmsNonDrugItemRecordVO.class));
    }

    /**
     * 新增检查项检验项处置项记录(开立的)
     */
    @PreAuthorize("@ss.hasPermi('dms:non_drug_item_record:add')")
    @Log(title = "新增检查项检验项处置项记录(开立的)" , businessType = BusinessType.INSERT)
    @PostMapping
    @ApiOperation(value = "新增检查项检验项处置项记录(开立的)信息" , notes = "新增检查项检验项处置项记录(开立的)信息" , code = 200,
            produces = "application/json" , protocols = "Http" , response = AjaxResult.class,
            httpMethod = "POST")
    @ApiResponses({
            @ApiResponse(code = 204, message = "操作已经执行成功，但是没有返回数据"),
            @ApiResponse(code = 303, message = "重定向"),
            @ApiResponse(code = 400, message = "参数列表错误（缺少，格式不匹配）"),
            @ApiResponse(code = 500, message = "系统内部错误"),
            @ApiResponse(code = 404, message = "资源，服务未找到"),
            @ApiResponse(code = 200, message = "操作成功"),
            @ApiResponse(code = 401, message = "未授权"),
            @ApiResponse(code = 403, message = "访问受限，授权过期")
    })
    @ApiParam(name = "DmsNonDrugItemRecordVO对象" , type = "DmsNonDrugItemRecordVO")
    public AjaxResult add(@RequestBody DmsNonDrugItemRecordVO dmsNonDrugItemRecordVO) {
        return toAjax(dmsNonDrugItemRecordService.insertDmsNonDrugItemRecord(BeanCopierUtil.copy(dmsNonDrugItemRecordVO,DmsNonDrugItemRecord. class)));
    }

    /**
     * 修改检查项检验项处置项记录(开立的)
     */
    @PreAuthorize("@ss.hasPermi('dms:non_drug_item_record:edit')")
    @Log(title = "检查项检验项处置项记录(开立的)" , businessType = BusinessType.UPDATE)
    @PutMapping("/edit")
    @ApiOperation(value = "修改检查项检验项处置项记录(开立的)信息" , notes = "修改检查项检验项处置项记录(开立的)信息" , code = 200,
            produces = "application/json" , protocols = "Http" , response = AjaxResult.class,
            httpMethod = "PUT")
    @ApiResponses({
            @ApiResponse(code = 204, message = "操作已经执行成功，但是没有返回数据"),
            @ApiResponse(code = 303, message = "重定向"),
            @ApiResponse(code = 400, message = "参数列表错误（缺少，格式不匹配）"),
            @ApiResponse(code = 500, message = "系统内部错误"),
            @ApiResponse(code = 404, message = "资源，服务未找到"),
            @ApiResponse(code = 200, message = "操作成功"),
            @ApiResponse(code = 401, message = "未授权"),
            @ApiResponse(code = 403, message = "访问受限，授权过期")
    })
    @ApiParam(name = "DmsNonDrugItemRecordVO对象" , type = "DmsNonDrugItemRecordVO")
    public AjaxResult edit(@RequestBody DmsNonDrugItemRecordVO dmsNonDrugItemRecordVO) {
        return toAjax(dmsNonDrugItemRecordService.updateDmsNonDrugItemRecord(BeanCopierUtil.copy(dmsNonDrugItemRecordVO,DmsNonDrugItemRecord. class)));
    }

    /**
     * 删除检查项检验项处置项记录(开立的)
     */
    @PreAuthorize("@ss.hasPermi('dms:non_drug_item_record:remove')")
    @Log(title = "检查项检验项处置项记录(开立的)" , businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    @ApiOperation(value = "删除检查项检验项处置项记录(开立的)信息" , notes = "删除检查项检验项处置项记录(开立的)信息" , code = 200,
            produces = "application/json" , protocols = "Http" , response = AjaxResult.class,
            httpMethod = "DELETE")
    @ApiResponses({
            @ApiResponse(code = 204, message = "操作已经执行成功，但是没有返回数据"),
            @ApiResponse(code = 303, message = "重定向"),
            @ApiResponse(code = 400, message = "参数列表错误（缺少，格式不匹配）"),
            @ApiResponse(code = 500, message = "系统内部错误"),
            @ApiResponse(code = 404, message = "资源，服务未找到"),
            @ApiResponse(code = 200, message = "操作成功"),
            @ApiResponse(code = 401, message = "未授权"),
            @ApiResponse(code = 403, message = "访问受限，授权过期")
    })
    @ApiParam(name = "dmsNonDrugItemRecord.id" , type = "Long[]")
    public AjaxResult remove(@PathVariable Long[] ids) {
        return toAjax(dmsNonDrugItemRecordService.deleteDmsNonDrugItemRecordByIds(ids));
    }

    /**
     * 查询检查项检验项处置项记录(开立的)列表
     *
     */
    @PreAuthorize("@ss.hasPermi('dms:non_drug_item_record:dealWithList')")
    @GetMapping("/dealWithList")
    @ApiOperation(value = "查询检查项检验项处置项记录(开立的)" , notes = "查询所有检查项检验项处置项记录(开立的)" ,
            code = 200, produces = "application/json" , protocols = "Http" ,
            response = TableDataInfo.class, httpMethod = "GET")
    @ApiResponses({
            @ApiResponse(code = 204, message = "操作已经执行成功，但是没有返回数据"),
            @ApiResponse(code = 303, message = "重定向"),
            @ApiResponse(code = 400, message = "参数列表错误（缺少，格式不匹配）"),
            @ApiResponse(code = 500, message = "系统内部错误"),
            @ApiResponse(code = 404, message = "资源，服务未找到"),
            @ApiResponse(code = 200, message = "操作成功"),
            @ApiResponse(code = 401, message = "未授权"),
            @ApiResponse(code = 403, message = "访问受限，授权过期")
    })
    @ApiParam(name = "DmsNonDrugItemRecordVO对象" , type = "DmsNonDrugItemRecordVO")
    public TableDataInfo dealWithListList(DmsNonDrugItemRecordVO dmsNonDrugItemRecordVO) {
        //将Vo转化为实体
        DmsNonDrugItemRecord dmsNonDrugItemRecord=BeanCopierUtil.copy(dmsNonDrugItemRecordVO,DmsNonDrugItemRecord. class);
        startPage();
        List<DmsNonDrugItemRecord> list = dmsNonDrugItemRecordService.selectDmsNonDrugItemRecordDealWithList(dmsNonDrugItemRecord);
        TableDataInfo tableDataInfo = getDataTable(list);
        //替换集合
        tableDataInfo.setRows(BeanCopierUtil.copy(tableDataInfo.getRows(), DmsNonDrugItemRecordVO.class));
        return tableDataInfo;
    }

    /**
     * 修改检查项检验项处置项记录(开立的)
     */
    @PreAuthorize("@ss.hasPermi('dms:non_drug_item_record:edit')")
    @Log(title = "检查项检验项处置项记录(开立的)" , businessType = BusinessType.UPDATE)
    @PutMapping("/editCheckResult")
    @ApiOperation(value = "修改检查项检验项处置项记录(开立的)信息" , notes = "修改检查项检验项处置项记录(开立的)信息" , code = 200,
            produces = "application/json" , protocols = "Http" , response = AjaxResult.class,
            httpMethod = "PUT")
    @ApiResponses({
            @ApiResponse(code = 204, message = "操作已经执行成功，但是没有返回数据"),
            @ApiResponse(code = 303, message = "重定向"),
            @ApiResponse(code = 400, message = "参数列表错误（缺少，格式不匹配）"),
            @ApiResponse(code = 500, message = "系统内部错误"),
            @ApiResponse(code = 404, message = "资源，服务未找到"),
            @ApiResponse(code = 200, message = "操作成功"),
            @ApiResponse(code = 401, message = "未授权"),
            @ApiResponse(code = 403, message = "访问受限，授权过期")
    })
    @ApiParam(name = "DmsNonDrugItemRecordVO对象" , type = "DmsNonDrugItemRecordVO")
    public AjaxResult editCheckResult(@RequestBody DmsNonDrugItemRecordVO dmsNonDrugItemRecordVO) {
        return toAjax(dmsNonDrugItemRecordService.updateDmsNonDrugItemRecord(BeanCopierUtil.copy(dmsNonDrugItemRecordVO,DmsNonDrugItemRecord. class)));
    }

    /**
     * redis存入操作
     * 1.先判断业务状态（键名为：status）：是处置，检验，检查,来写Redis的Key值
     * 2.根据Key在Redis中获取数据Value，如果有Value不为NULL(有数据)，那么就进行累加。如果不存在，就在Value中添加新的数据
     * 3.把Value存入Redis中
     * status:状态
     * key:键名
     * */
    @PreAuthorize("@ss.hasPermi('dms:non_drug_item_record:redisSave')")
    @PostMapping("/redisSave/{registrationId}/{status}")
    public AjaxResult addInfoToRedis(@RequestBody DmsNonDrugVO nonDrug,@PathVariable String registrationId,@PathVariable Integer status){
        String key=choseKey(registrationId,status);
        Long result=redisTemplate.opsForList().rightPush(choseKey(registrationId,status),nonDrug);
        //设置超时时间
        redisCache.expire(key ,60*10, TimeUnit.SECONDS);
        return toAjax(result.intValue());
    }

    /**redis取出操作*/
    @PreAuthorize("@ss.hasPermi('dms:non_drug_item_record:redisGet')")
    @PostMapping("/redisGet/{registrationId}/{status}")
    public AjaxResult getInfoToRedis(@PathVariable String registrationId,@PathVariable Integer status){
        String key=choseKey(registrationId,status);
        return AjaxResult.success(redisCache.getCacheList(key));
    }

    /**redis删除操作*/
    @PreAuthorize("@ss.hasPermi('dms:non_drug_item_record:redisGet')")
    @PostMapping("/redisDel/{registrationId}/{status}")
    public AjaxResult delInfoToRedis(@RequestBody DmsNonDrugVO nonDrug,@PathVariable String registrationId,@PathVariable Integer status){
        String key=choseKey(registrationId,status);
        return toAjax(redisTemplate.opsForList().remove(key,0,nonDrug).intValue());
    }

    /***
     * @param registrationId:挂号ID
     * @param status:处置检查检验类型
     * @return:Redis的Key值
     */
    private String choseKey(final String registrationId,final Integer status){
        String key=null;
        //判断业务类型（检查检验处置）,生成相应Key
        if(status==HnccType.NonDrug.CHECK.getValue()){
            //检查
            key=DmsConstants.INSPECTION+":"+registrationId+":"+ DmsConstants.CORRESPONDING_REGISTRATION;
        }else if(status==HnccType.NonDrug.TEST.getValue()){
            //检验
            key=DmsConstants.TEST+":"+registrationId+":"+ DmsConstants.CORRESPONDING_REGISTRATION;
        }else if(status==HnccType.NonDrug.DISPOSITION.getValue()){
            //处置
            key=DmsConstants.DISPOSAL+":"+registrationId+":"+ DmsConstants.CORRESPONDING_REGISTRATION;
        }
        return key;
    }
}
