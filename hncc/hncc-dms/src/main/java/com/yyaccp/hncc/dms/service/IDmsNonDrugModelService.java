package com.yyaccp.hncc.dms.service;

import java.util.List;

import com.yyaccp.hncc.dms.domain.DmsNonDrugModel;

/**
 * 非药品模版Service接口
 *
 * @author yugui
 * @date 2020-08-17
 */
public interface IDmsNonDrugModelService {
    /**
     * 查询非药品模版
     *
     * @param id 非药品模版ID
     * @return 非药品模版
     */
    public DmsNonDrugModel selectDmsNonDrugModelById(Long id);

    /**
     * 查询非药品模版列表
     *
     * @param dmsNonDrugModel 非药品模版
     * @return 非药品模版集合
     */
    public List<DmsNonDrugModel> selectDmsNonDrugModelList(DmsNonDrugModel dmsNonDrugModel);

    /**
     * 新增非药品模版
     *
     * @param dmsNonDrugModel 非药品模版
     * @return 结果
     */
    public int insertDmsNonDrugModel(DmsNonDrugModel dmsNonDrugModel);

    /**
     * 修改非药品模版
     *
     * @param dmsNonDrugModel 非药品模版
     * @return 结果
     */
    public int updateDmsNonDrugModel(DmsNonDrugModel dmsNonDrugModel);

    /**
     * 批量删除非药品模版
     *
     * @param ids 需要删除的非药品模版ID
     * @return 结果
     */
    public int deleteDmsNonDrugModelByIds(Long[] ids);

    /**
     * 删除非药品模版信息
     *
     * @param id 非药品模版ID
     * @return 结果
     */
    public int deleteDmsNonDrugModelById(Long id);
}
