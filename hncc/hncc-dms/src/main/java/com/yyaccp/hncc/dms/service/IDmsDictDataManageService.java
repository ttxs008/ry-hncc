package com.yyaccp.hncc.dms.service;

import java.util.List;
import com.yyaccp.hncc.dms.domain.DmsDictDataManage;

/**
 * 字典数据Service接口
 * 
 * @author M
 * @date 2020-08-19
 */
public interface IDmsDictDataManageService 
{
    /**
     * 查询字典数据列表
     * 
     * @param dmsDictDataManage 字典数据
     * @return 字典数据集合
     */
    public List<DmsDictDataManage> selectDmsDictDataManageList(DmsDictDataManage dmsDictDataManage);


}
