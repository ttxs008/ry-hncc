package com.yyaccp.hncc.dms.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 病例模版对象 dms_case_model
 * 
 * @author 天天向上
 * @date 2020-08-15
 */
public class DmsCaseModel extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 模板id */
    private Long id;

    /** 主诉 */
    @Excel(name = "主诉")
    private String chiefComplaint;

    /** 症状 */
    @Excel(name = "症状")
    private String historyOfPresentIllness;

    /** 历史治疗 */
    @Excel(name = "历史治疗")
    private String historyOfTreatment;

    /** 既往史 */
    @Excel(name = "既往史")
    private String pastHistory;

    /** 过敏史 */
    @Excel(name = "过敏史")
    private String allergies;

    /** 健康检查 */
    @Excel(name = "健康检查")
    private String healthCheckup;

    /** 初步诊断Id串 */
    @Excel(name = "初步诊断Id串")
    private String priliminaryDiseIdList;

    /** 初诊诊断id串对应诊断名字串 */
    @Excel(name = "初诊诊断id串对应诊断名字串")
    private String priliminaryDiseStrList;

    /** 病例模板名 */
    @Excel(name = "病例模板名")
    private String name;

    /** 状态 */
    @Excel(name = "状态")
    private Integer status;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setChiefComplaint(String chiefComplaint) 
    {
        this.chiefComplaint = chiefComplaint;
    }

    public String getChiefComplaint() 
    {
        return chiefComplaint;
    }
    public void setHistoryOfPresentIllness(String historyOfPresentIllness) 
    {
        this.historyOfPresentIllness = historyOfPresentIllness;
    }

    public String getHistoryOfPresentIllness() 
    {
        return historyOfPresentIllness;
    }
    public void setHistoryOfTreatment(String historyOfTreatment) 
    {
        this.historyOfTreatment = historyOfTreatment;
    }

    public String getHistoryOfTreatment() 
    {
        return historyOfTreatment;
    }
    public void setPastHistory(String pastHistory) 
    {
        this.pastHistory = pastHistory;
    }

    public String getPastHistory() 
    {
        return pastHistory;
    }
    public void setAllergies(String allergies) 
    {
        this.allergies = allergies;
    }

    public String getAllergies() 
    {
        return allergies;
    }
    public void setHealthCheckup(String healthCheckup) 
    {
        this.healthCheckup = healthCheckup;
    }

    public String getHealthCheckup() 
    {
        return healthCheckup;
    }
    public void setPriliminaryDiseIdList(String priliminaryDiseIdList) 
    {
        this.priliminaryDiseIdList = priliminaryDiseIdList;
    }

    public String getPriliminaryDiseIdList() 
    {
        return priliminaryDiseIdList;
    }
    public void setPriliminaryDiseStrList(String priliminaryDiseStrList) 
    {
        this.priliminaryDiseStrList = priliminaryDiseStrList;
    }

    public String getPriliminaryDiseStrList() 
    {
        return priliminaryDiseStrList;
    }
    public void setName(String name) 
    {
        this.name = name;
    }

    public String getName() 
    {
        return name;
    }
    public void setStatus(Integer status) 
    {
        this.status = status;
    }

    public Integer getStatus() 
    {
        return status;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("chiefComplaint", getChiefComplaint())
            .append("historyOfPresentIllness", getHistoryOfPresentIllness())
            .append("historyOfTreatment", getHistoryOfTreatment())
            .append("pastHistory", getPastHistory())
            .append("allergies", getAllergies())
            .append("healthCheckup", getHealthCheckup())
            .append("priliminaryDiseIdList", getPriliminaryDiseIdList())
            .append("priliminaryDiseStrList", getPriliminaryDiseStrList())
            .append("name", getName())
            .append("status", getStatus())
            .toString();
    }
}
