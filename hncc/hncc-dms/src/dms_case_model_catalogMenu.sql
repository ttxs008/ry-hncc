-- 菜单 SQL
insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('病例模版目录', '2000', '1', 'dms_case_model_catalog', 'dms/dms_case_model_catalog/index', 1, 'C', '0', '0', 'dms:dms_case_model_catalog:list', '#', 'admin', '2018-03-01', 'ry', '2018-03-01', '病例模版目录菜单');

-- 按钮父菜单ID
SELECT @parentId := LAST_INSERT_ID();

-- 按钮 SQL
insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('病例模版目录查询', @parentId, '1',  '#', '', 1,  'F', '0',  '0', 'dms:dms_case_model_catalog:query',        '#', 'admin', '2018-03-01', 'ry', '2018-03-01', '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('病例模版目录新增', @parentId, '2',  '#', '', 1,  'F', '0',  '0', 'dms:dms_case_model_catalog:add',          '#', 'admin', '2018-03-01', 'ry', '2018-03-01', '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('病例模版目录修改', @parentId, '3',  '#', '', 1,  'F', '0',  '0', 'dms:dms_case_model_catalog:edit',         '#', 'admin', '2018-03-01', 'ry', '2018-03-01', '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('病例模版目录删除', @parentId, '4',  '#', '', 1,  'F', '0',  '0', 'dms:dms_case_model_catalog:remove',       '#', 'admin', '2018-03-01', 'ry', '2018-03-01', '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('病例模版目录导出', @parentId, '5',  '#', '', 1,  'F', '0',  '0', 'dms:dms_case_model_catalog:export',       '#', 'admin', '2018-03-01', 'ry', '2018-03-01', '');