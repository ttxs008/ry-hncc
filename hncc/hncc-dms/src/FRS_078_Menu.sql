INSERT INTO `hncc`.`sys_menu`(`menu_id`, `menu_name`, `parent_id`, `order_num`, `path`, `component`, `is_frame`, `menu_type`, `visible`, `status`, `perms`, `icon`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`)
VALUES (0, '药房医生工作台', 0, 3, 'pharmacy', NULL, 1, 'M', '0', '0', NULL, 'post', 'admin', '2020-09-09 13:58:17', '', NULL, '');

SELECT @parentId := LAST_INSERT_ID();

INSERT INTO `hncc`.`sys_menu`(`menu_id`, `menu_name`, `parent_id`, `order_num`, `path`, `component`, `is_frame`, `menu_type`, `visible`, `status`, `perms`, `icon`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`)
VALUES (0, '成药药房', @parentId, 1, 'pharmacy', 'dms/pharmacy/index', 1, 'C', '0', '0', '', '#', 'admin', '2018-03-01 00:00:00', 'admin', '2020-09-09 14:38:18', '');

INSERT INTO `hncc`.`sys_menu`(`menu_id`, `menu_name`, `parent_id`, `order_num`, `path`, `component`, `is_frame`, `menu_type`, `visible`, `status`, `perms`, `icon`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`)
VALUES (0, '草药药房', @parentId, 1, 'herbal_harmacy', 'dms/herbal_pharmacy/index', 1, 'C', '0', '0', '', '#', 'admin', '2018-03-01 00:00:00', 'admin', '2020-09-09 14:38:18', '');