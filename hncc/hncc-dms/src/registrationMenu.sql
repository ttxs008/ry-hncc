-- 菜单 SQL
insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('就诊(门诊)信息', '2143', '1', 'registration', 'dms/registration/index', 1, 'C', '0', '0', 'dms:registration:list', '#', 'admin', '2018-03-01', 'ry', '2018-03-01', '就诊(门诊)信息菜单');

-- 按钮父菜单ID
SELECT @parentId := LAST_INSERT_ID();

-- 按钮 SQL
insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('就诊(门诊)信息查询', @parentId, '1',  '#', '', 1,  'F', '0',  '0', 'dms:registration:query',        '#', 'admin', '2018-03-01', 'ry', '2018-03-01', '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('就诊(门诊)信息新增', @parentId, '2',  '#', '', 1,  'F', '0',  '0', 'dms:registration:add',          '#', 'admin', '2018-03-01', 'ry', '2018-03-01', '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('就诊(门诊)信息修改', @parentId, '3',  '#', '', 1,  'F', '0',  '0', 'dms:registration:edit',         '#', 'admin', '2018-03-01', 'ry', '2018-03-01', '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('就诊(门诊)信息删除', @parentId, '4',  '#', '', 1,  'F', '0',  '0', 'dms:registration:remove',       '#', 'admin', '2018-03-01', 'ry', '2018-03-01', '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('就诊(门诊)信息导出', @parentId, '5',  '#', '', 1,  'F', '0',  '0', 'dms:registration:export',       '#', 'admin', '2018-03-01', 'ry', '2018-03-01', '');

-- 门诊挂号收费页面
INSERT INTO `hncc`.`sys_menu`(`menu_id`, `menu_name`, `parent_id`, `order_num`, `path`, `component`, `is_frame`, `menu_type`, `visible`, `status`, `perms`, `icon`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`) VALUES (0, '门诊挂号收费', 0, 1, 'registrationPatient', 'dms/registration/indexRegistrationFees', 1, 'C', '0', '0', NULL, '#', 'admin', '2020-08-31 19:06:59', '何磊', '2020-08-31 19:06:57', '门诊挂号收费');

--就诊(门诊)信息查询根据患者名与病历号
INSERT INTO `hncc`.`sys_menu`(`menu_id`, `menu_name`, `parent_id`, `order_num`, `path`, `component`, `is_frame`, `menu_type`, `visible`, `status`, `perms`, `icon`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`) VALUES (2184, '就诊(门诊)信息查询根据患者名与病历号', 2178, 6, '#', '', 1, 'F', '0', '0', 'dms:registration:listByNameAndNo', '#', 'admin', '2020-08-27 17:14:51', '何磊', '2020-08-27 17:14:56', '');

-- 挂号处理
INSERT INTO `hncc`.`sys_menu`(`menu_id`, `menu_name`, `parent_id`, `order_num`, `path`, `component`, `is_frame`, `menu_type`, `visible`, `status`, `perms`, `icon`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`) VALUES (2176, '挂号处理', 2144, 6, '#', NULL, 1, 'F', '0', '0', 'dms:registration:handleRegistration', '#', 'admin', '2020-08-27 15:15:21', '何磊', '2020-08-27 15:15:42', '');
-- 退号处理
INSERT INTO `hncc`.`sys_menu`(`menu_id`, `menu_name`, `parent_id`, `order_num`, `path`, `component`, `is_frame`, `menu_type`, `visible`, `status`, `perms`, `icon`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`) VALUES (2195, '退号处理', 2144, 7, '#', NULL, 1, 'F', '0', '0', 'dms:registration:handleBounce', '#', 'admin', '2020-09-04 17:00:41', '何磊', '2020-09-04 17:00:49', '');
-- 缴费列表
INSERT INTO `hncc`.`sys_menu`(`menu_id`, `menu_name`, `parent_id`, `order_num`, `path`, `component`, `is_frame`, `menu_type`, `visible`, `status`, `perms`, `icon`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`) VALUES (2201, '缴费列表', 2185, 8, '#', NULL, 1, 'F', '0', '0', 'dms:registration:payList', '#', 'admin', '2020-09-04 17:00:41', '何磊', '2020-09-04 17:00:49', '');
-- 处理挂号
INSERT INTO `hncc`.`sys_menu`(`menu_id`, `menu_name`, `parent_id`, `order_num`, `path`, `component`, `is_frame`, `menu_type`, `visible`, `status`, `perms`, `icon`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`) VALUES (2202, '缴费处理', 2185, 9, '#', NULL, 1, 'F', '0', '0', 'dms:registration:handlePayment', '#', 'admin', '2020-09-04 17:00:41', '何磊', '2020-09-04 17:00:49', '');
-- 退费列表
INSERT INTO `hncc`.`sys_menu`(`menu_id`, `menu_name`, `parent_id`, `order_num`, `path`, `component`, `is_frame`, `menu_type`, `visible`, `status`, `perms`, `icon`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`) VALUES (2203, '退费列表', 2185, 10, '#', NULL, 1, 'F', '0', '0', 'dms:registration:refundList', '#', 'admin', '2020-09-04 17:00:41', '何磊', '2020-09-04 17:00:49', '');
-- 退费处理
INSERT INTO `hncc`.`sys_menu`(`menu_id`, `menu_name`, `parent_id`, `order_num`, `path`, `component`, `is_frame`, `menu_type`, `visible`, `status`, `perms`, `icon`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`) VALUES (2204, '退费处理', 2185, 11, '#', NULL, 1, 'F', '0', '0', 'dms:registration:handleRefund', '#', 'admin', '2020-09-04 17:00:41', '何磊', '2020-09-04 17:00:49', '');