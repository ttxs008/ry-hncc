/**
 字典类型表 sys_dict_type
 */
INSERT INTO `hncc`.`sys_dict_type`(`dict_id`, `dict_name`, `dict_type`, `status`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`) VALUES (100, '模版状态', 'model_status', '0', 'admin', '2020-08-11 15:28:02', '', NULL, NULL);
INSERT INTO `hncc`.`sys_dict_type`(`dict_id`, `dict_name`, `dict_type`, `status`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`) VALUES (101, '模版范围', 'model_scope', '0', 'admin', '2020-08-11 15:29:44', '', NULL, NULL);
INSERT INTO `hncc`.`sys_dict_type`(`dict_id`, `dict_name`, `dict_type`, `status`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`) VALUES (102, '模版类型', 'molde_type', '0', 'admin', '2020-08-11 15:31:00', '', NULL, NULL);
INSERT INTO `hncc`.`sys_dict_type`(`dict_id`, `dict_name`, `dict_type`, `status`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`) VALUES (103, '项目类型', 'no_drug_type', '0', 'admin', '2020-08-11 16:17:35', '', NULL, NULL);


INSERT INTO `sys_dict_type`(`dict_id`, `dict_name`, `dict_type`, `status`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`) VALUES (104, '启用状态', 'sms_registration_rank', '0', 'admin', '2020-08-14 10:28:38', '', NULL, '挂号级别状态');


INSERT INTO `sys_dict_type`(`dict_id`, `dict_name`, `dict_type`, `status`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`) VALUES (104, '启用状态', 'sms_registration_rank', '0', 'admin', '2020-08-14 10:28:38', '', NULL, '挂号级别状态');

INSERT INTO `sys_dict_type`(`dict_id`, `dict_name`, `dict_type`, `status`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`) VALUES (104, '启用状态', 'sms_registration_rank', '0', 'admin', '2020-08-14 10:28:38', '', NULL, '挂号级别状态');



INSERT INTO `sys_dict_type`(`dict_id`, `dict_name`, `dict_type`, `status`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`) VALUES (104, '启用状态', 'sms_registration_rank', '0', 'admin', '2020-08-14 10:28:38', '', NULL, '挂号级别状态');


/**
 字典数据表 sys_dict_data
 */
INSERT INTO `hncc`.`sys_dict_data`(`dict_code`, `dict_sort`, `dict_label`, `dict_value`, `dict_type`, `css_class`, `list_class`, `is_default`, `status`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`) VALUES (100, 0, '删除', '0', 'model_status', NULL, NULL, 'N', '0', 'admin', '2020-08-11 15:28:39', 'admin', '2020-08-11 15:28:45', NULL);
INSERT INTO `hncc`.`sys_dict_data`(`dict_code`, `dict_sort`, `dict_label`, `dict_value`, `dict_type`, `css_class`, `list_class`, `is_default`, `status`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`) VALUES (101, 1, '正常', '1', 'model_status', NULL, NULL, 'N', '0', 'admin', '2020-08-11 15:28:59', '', NULL, NULL);
INSERT INTO `hncc`.`sys_dict_data`(`dict_code`, `dict_sort`, `dict_label`, `dict_value`, `dict_type`, `css_class`, `list_class`, `is_default`, `status`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`) VALUES (102, 0, '个人', '0', 'model_scope', NULL, NULL, 'N', '0', 'admin', '2020-08-11 15:30:05', '', NULL, NULL);
INSERT INTO `hncc`.`sys_dict_data`(`dict_code`, `dict_sort`, `dict_label`, `dict_value`, `dict_type`, `css_class`, `list_class`, `is_default`, `status`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`) VALUES (103, 1, '科室', '1', 'model_scope', NULL, NULL, 'N', '0', 'admin', '2020-08-11 15:30:17', '', NULL, NULL);
INSERT INTO `hncc`.`sys_dict_data`(`dict_code`, `dict_sort`, `dict_label`, `dict_value`, `dict_type`, `css_class`, `list_class`, `is_default`, `status`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`) VALUES (104, 2, '全院', '2', 'model_scope', NULL, NULL, 'N', '0', 'admin', '2020-08-11 15:30:28', 'admin', '2020-08-11 15:30:40', NULL);

INSERT INTO `hncc`.`sys_dict_data`(`dict_code`, `dict_sort`, `dict_label`, `dict_value`, `dict_type`, `css_class`, `list_class`, `is_default`, `status`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`) VALUES (105, 0, '检查', '0', 'molde_type', NULL, NULL, 'N', '0', 'admin', '2020-08-11 15:31:10', '', NULL, NULL);
INSERT INTO `hncc`.`sys_dict_data`(`dict_code`, `dict_sort`, `dict_label`, `dict_value`, `dict_type`, `css_class`, `list_class`, `is_default`, `status`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`) VALUES (106, 1, '检验', '1', 'molde_type', NULL, NULL, 'N', '0', 'admin', '2020-08-11 15:31:18', '', NULL, NULL);
INSERT INTO `hncc`.`sys_dict_data`(`dict_code`, `dict_sort`, `dict_label`, `dict_value`, `dict_type`, `css_class`, `list_class`, `is_default`, `status`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`) VALUES (107, 2, '处置', '2', 'molde_type', NULL, NULL, 'N', '0', 'admin', '2020-08-11 15:31:26', '', NULL, NULL);
INSERT INTO `hncc`.`sys_dict_data`(`dict_code`, `dict_sort`, `dict_label`, `dict_value`, `dict_type`, `css_class`, `list_class`, `is_default`, `status`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`) VALUES (108, 1, '检查', '1', 'no_drug_type', NULL, NULL, 'N', '0', 'admin', '2020-08-11 16:18:10', '', NULL, NULL);
INSERT INTO `hncc`.`sys_dict_data`(`dict_code`, `dict_sort`, `dict_label`, `dict_value`, `dict_type`, `css_class`, `list_class`, `is_default`, `status`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`) VALUES (109, 2, '检验', '2', 'no_drug_type', NULL, NULL, 'N', '0', 'admin', '2020-08-11 16:18:23', '', NULL, NULL);
INSERT INTO `hncc`.`sys_dict_data`(`dict_code`, `dict_sort`, `dict_label`, `dict_value`, `dict_type`, `css_class`, `list_class`, `is_default`, `status`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`) VALUES (110, 3, '处置', '3', 'no_drug_type', NULL, NULL, 'N', '0', 'admin', '2020-08-11 16:18:31', '', NULL, NULL);

/*
这三行数据确定对么？？？？molde_type是个什么鬼？
INSERT INTO `hncc`.`sys_dict_data`(`dict_code`, `dict_sort`, `dict_label`, `dict_value`, `dict_type`, `css_class`, `list_class`, `is_default`, `status`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`) VALUES (105, 0, '检查', '0', 'molde_type', NULL, NULL, 'N', '0', 'admin', '2020-08-11 15:31:10', '', NULL, NULL);
INSERT INTO `hncc`.`sys_dict_data`(`dict_code`, `dict_sort`, `dict_label`, `dict_value`, `dict_type`, `css_class`, `list_class`, `is_default`, `status`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`) VALUES (106, 1, '检验', '1', 'molde_type', NULL, NULL, 'N', '0', 'admin', '2020-08-11 15:31:18', '', NULL, NULL);
INSERT INTO `hncc`.`sys_dict_data`(`dict_code`, `dict_sort`, `dict_label`, `dict_value`, `dict_type`, `css_class`, `list_class`, `is_default`, `status`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`) VALUES (107, 2, '处置', '2', 'molde_type', NULL, NULL, 'N', '0', 'admin', '2020-08-11 15:31:26', '', NULL, NULL);
*/

INSERT INTO `hncc`.`sys_dict_data`(`dict_code`, `dict_sort`, `dict_label`, `dict_value`, `dict_type`, `css_class`, `list_class`, `is_default`, `status`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`) VALUES (108, 1, '检查', '1', 'no_drug_type', NULL, NULL, 'N', '0', 'admin', '2020-08-11 16:18:10', '', NULL, NULL);
INSERT INTO `hncc`.`sys_dict_data`(`dict_code`, `dict_sort`, `dict_label`, `dict_value`, `dict_type`, `css_class`, `list_class`, `is_default`, `status`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`) VALUES (109, 2, '检验', '2', 'no_drug_type', NULL, NULL, 'N', '0', 'admin', '2020-08-11 16:18:23', '', NULL, NULL);
INSERT INTO `hncc`.`sys_dict_data`(`dict_code`, `dict_sort`, `dict_label`, `dict_value`, `dict_type`, `css_class`, `list_class`, `is_default`, `status`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`) VALUES (110, 3, '处置', '3', 'no_drug_type', NULL, NULL, 'N', '0', 'admin', '2020-08-11 16:18:31', '', NULL, NULL);
INSERT INTO `sys_dict_data`(`dict_code`, `dict_sort`, `dict_label`, `dict_value`, `dict_type`, `css_class`, `list_class`, `is_default`, `status`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`) VALUES (111, 1, '启用', '1', 'sms_registration_rank', NULL, NULL, 'N', '0', 'admin', '2020-08-14 11:19:21', '', NULL, '启用状态');
INSERT INTO `sys_dict_data`(`dict_code`, `dict_sort`, `dict_label`, `dict_value`, `dict_type`, `css_class`, `list_class`, `is_default`, `status`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`) VALUES (112, 2, '禁用', '0', 'sms_registration_rank', NULL, NULL, 'N', '0', 'admin', '2020-08-14 11:19:45', '', NULL, '禁用状态');

INSERT INTO `hncc`.`sys_dict_data`(`dict_code`, `dict_sort`, `dict_label`, `dict_value`, `dict_type`, `css_class`, `list_class`, `is_default`, `status`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`) VALUES (105, 0, '检查', '0', 'molde_type', NULL, NULL, 'N', '0', 'admin', '2020-08-11 15:31:10', '', NULL, NULL);
INSERT INTO `hncc`.`sys_dict_data`(`dict_code`, `dict_sort`, `dict_label`, `dict_value`, `dict_type`, `css_class`, `list_class`, `is_default`, `status`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`) VALUES (106, 1, '检验', '1', 'molde_type', NULL, NULL, 'N', '0', 'admin', '2020-08-11 15:31:18', '', NULL, NULL);
INSERT INTO `hncc`.`sys_dict_data`(`dict_code`, `dict_sort`, `dict_label`, `dict_value`, `dict_type`, `css_class`, `list_class`, `is_default`, `status`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`) VALUES (107, 2, '处置', '2', 'molde_type', NULL, NULL, 'N', '0', 'admin', '2020-08-11 15:31:26', '', NULL, NULL);
INSERT INTO `hncc`.`sys_dict_data`(`dict_code`, `dict_sort`, `dict_label`, `dict_value`, `dict_type`, `css_class`, `list_class`, `is_default`, `status`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`) VALUES (108, 1, '检查', '1', 'no_drug_type', NULL, NULL, 'N', '0', 'admin', '2020-08-11 16:18:10', '', NULL, NULL);
INSERT INTO `hncc`.`sys_dict_data`(`dict_code`, `dict_sort`, `dict_label`, `dict_value`, `dict_type`, `css_class`, `list_class`, `is_default`, `status`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`) VALUES (109, 2, '检验', '2', 'no_drug_type', NULL, NULL, 'N', '0', 'admin', '2020-08-11 16:18:23', '', NULL, NULL);
INSERT INTO `hncc`.`sys_dict_data`(`dict_code`, `dict_sort`, `dict_label`, `dict_value`, `dict_type`, `css_class`, `list_class`, `is_default`, `status`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`) VALUES (110, 3, '处置', '3', 'no_drug_type', NULL, NULL, 'N', '0', 'admin', '2020-08-11 16:18:31', '', NULL, NULL);



/*
    草药模板dms_drug_model的药瓶类型字典（101：西药,102:中成药,103草药）
 */
INSERT INTO `hncc`.`sys_dict_data`(`dict_code`, `dict_sort`, `dict_label`, `dict_value`, `dict_type`, `css_class`, `list_class`, `is_default`, `status`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`) VALUES (111, 1, '西药', '101', 'drug_type_id', NULL, NULL, 'N', '0', 'admin', '2020-08-11 16:18:10', '', NULL, NULL);
INSERT INTO `hncc`.`sys_dict_data`(`dict_code`, `dict_sort`, `dict_label`, `dict_value`, `dict_type`, `css_class`, `list_class`, `is_default`, `status`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`) VALUES (112, 2, '中成药', '102', 'drug_type_id', NULL, NULL, 'N', '0', 'admin', '2020-08-11 16:18:23', '', NULL, NULL);
INSERT INTO `hncc`.`sys_dict_data`(`dict_code`, `dict_sort`, `dict_label`, `dict_value`, `dict_type`, `css_class`, `list_class`, `is_default`, `status`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`) VALUES (113, 3, '草药', '103', 'drug_type_id', NULL, NULL, 'N', '0', 'admin', '2020-08-11 16:18:31', '', NULL, NULL);


#FRS_012
INSERT INTO `hncc`.`sys_dict_type` (`dict_id`, `dict_name`, `dict_type`, `status`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`) VALUES ('104', '病例模板类型', 'dms_case_model_catalog_type', '0', 'admin', '2020-08-15 20:28:30', '', NULL, '1目录2模板');
INSERT INTO `hncc`.`sys_dict_data` (`dict_code`, `dict_sort`, `dict_label`, `dict_value`, `dict_type`, `css_class`, `list_class`, `is_default`, `status`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`) VALUES ('111', '0', '目录', '1', 'dms_case_model_catalog_type', NULL, NULL, 'N', '0', 'admin', '2020-08-15 20:29:37', '', NULL, NULL);
INSERT INTO `hncc`.`sys_dict_data` (`dict_code`, `dict_sort`, `dict_label`, `dict_value`, `dict_type`, `css_class`, `list_class`, `is_default`, `status`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`) VALUES ('112', '0', '模板', '2', 'dms_case_model_catalog_type', NULL, NULL, 'N', '0', 'admin', '2020-08-15 20:29:54', '', NULL, NULL);

#FRS_017
INSERT INTO `ruoyi`.`sys_dict_type`(`dict_id`, `dict_name`, `dict_type`, `status`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`) VALUES (11, '常用项类别', 'sms_frequent_used', '0', 'admin', '2020-08-18 13:03:14', 'admin', '2020-08-18 13:50:11', '1检查,2检验,3处置,4诊断,101西药,102中成药,103中药');
INSERT INTO `ruoyi`.`sys_dict_data`(`dict_code`, `dict_sort`, `dict_label`, `dict_value`, `dict_type`, `css_class`, `list_class`, `is_default`, `status`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`) VALUES (29, 1, '检查', '1', 'sms_frequent_used', NULL, NULL, 'N', '0', 'admin', '2020-08-18 13:53:44', '', NULL, '显示检查常用项');
INSERT INTO `ruoyi`.`sys_dict_data`(`dict_code`, `dict_sort`, `dict_label`, `dict_value`, `dict_type`, `css_class`, `list_class`, `is_default`, `status`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`) VALUES (30, 2, '检验', '2', 'sms_frequent_used', NULL, NULL, 'N', '0', 'admin', '2020-08-18 13:54:15', '', NULL, '检验常用项');
INSERT INTO `ruoyi`.`sys_dict_data`(`dict_code`, `dict_sort`, `dict_label`, `dict_value`, `dict_type`, `css_class`, `list_class`, `is_default`, `status`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`) VALUES (31, 3, '处置', '3', 'sms_frequent_used', NULL, NULL, 'N', '0', 'admin', '2020-08-18 13:55:44', '', NULL, '处置常用项');
INSERT INTO `ruoyi`.`sys_dict_data`(`dict_code`, `dict_sort`, `dict_label`, `dict_value`, `dict_type`, `css_class`, `list_class`, `is_default`, `status`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`) VALUES (32, 4, '诊断', '4', 'sms_frequent_used', NULL, NULL, 'N', '0', 'admin', '2020-08-18 15:19:02', '', NULL, '诊断常用项\n');
INSERT INTO `ruoyi`.`sys_dict_data`(`dict_code`, `dict_sort`, `dict_label`, `dict_value`, `dict_type`, `css_class`, `list_class`, `is_default`, `status`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`) VALUES (33, 5, '西药', '101', 'sms_frequent_used', NULL, NULL, 'N', '0', 'admin', '2020-08-18 15:19:24', '', NULL, '西药常用项');
INSERT INTO `ruoyi`.`sys_dict_data`(`dict_code`, `dict_sort`, `dict_label`, `dict_value`, `dict_type`, `css_class`, `list_class`, `is_default`, `status`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`) VALUES (34, 6, '中成药', '102', 'sms_frequent_used', NULL, NULL, 'N', '0', 'admin', '2020-08-18 15:19:56', '', NULL, '中成药常用项');
INSERT INTO `ruoyi`.`sys_dict_data`(`dict_code`, `dict_sort`, `dict_label`, `dict_value`, `dict_type`, `css_class`, `list_class`, `is_default`, `status`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`) VALUES (35, 7, '中草药', '103', 'sms_frequent_used', NULL, NULL, 'N', '0', 'admin', '2020-08-18 15:20:53', '', NULL, '中草药常用项');

