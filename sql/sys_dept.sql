/*
 Navicat Premium Data Transfer

 Source Server         : MySQL
 Source Server Type    : MySQL
 Source Server Version : 50528
 Source Host           : localhost:3306
 Source Schema         : hncc

 Target Server Type    : MySQL
 Target Server Version : 50528
 File Encoding         : 65001

 Date: 25/08/2020 15:35:12
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for sys_dept
-- ----------------------------
DROP TABLE IF EXISTS `sys_dept`;
CREATE TABLE `sys_dept`  (
  `dept_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '部门id',
  `parent_id` bigint(20) NULL DEFAULT 0 COMMENT '父部门id',
  `ancestors` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '祖级列表',
  `dept_name` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '部门名称',
  `order_num` int(4) NULL DEFAULT 0 COMMENT '显示顺序',
  `leader` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '负责人',
  `phone` varchar(11) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '联系电话',
  `email` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '邮箱',
  `status` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '部门状态（0正常 1停用）',
  `del_flag` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '删除标志（0代表存在 2代表删除）',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  `cat_id` int(11) NULL DEFAULT NULL,
  `code` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`dept_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 261 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '部门表' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of sys_dept
-- ----------------------------
INSERT INTO `sys_dept` VALUES (1, 0, '', '海纳云医院', 0, '张三', '13310001000', 'john.yi@qq.com', '0', '0', '', '2018-03-16 11:33:00', 'admin', '2020-08-24 16:06:56', 0, NULL);
INSERT INTO `sys_dept` VALUES (110, 250, ',1,248,250', '心血管内科', 1, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2018-03-16 11:33:00', 'admin', '2020-08-25 15:10:22', 11, 'XXGNK');
INSERT INTO `sys_dept` VALUES (111, 256, ',1,248,256', '神经内科', 0, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2018-03-16 11:33:00', 'admin', '2020-08-25 15:28:51', 11, 'SJNK');
INSERT INTO `sys_dept` VALUES (112, 112, '0,112', '普通内科', 2, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', 11, 'PTNK');
INSERT INTO `sys_dept` VALUES (113, 250, ',1,248,250', '消化内科', 4, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2018-03-16 11:33:00', 'admin', '2020-08-25 15:10:33', 11, 'XHNK');
INSERT INTO `sys_dept` VALUES (114, 250, ',1,248,250', '呼吸内科', 5, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2018-03-16 11:33:00', 'admin', '2020-08-25 15:10:38', 11, 'HXNK');
INSERT INTO `sys_dept` VALUES (115, 250, ',1,248,250', '内分泌科', 6, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2018-03-16 11:33:00', 'admin', '2020-08-25 15:10:43', 11, 'NFMK');
INSERT INTO `sys_dept` VALUES (116, 250, ',1,248,250', '肾病内科', 7, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2018-03-16 11:33:00', 'admin', '2020-08-25 15:10:49', 11, 'SBNK');
INSERT INTO `sys_dept` VALUES (117, 250, ',1,248,250', '血液内科', 3, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2018-03-16 11:33:00', 'admin', '2020-08-25 15:10:29', 11, 'XYNK');
INSERT INTO `sys_dept` VALUES (118, 112, '0,112,118', '感染内科', 2, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', 11, 'GRNK');
INSERT INTO `sys_dept` VALUES (119, 250, ',1,248,250', '老年病内科', 8, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2018-03-16 11:33:00', 'admin', '2020-08-25 15:10:53', 11, 'LNBNK');
INSERT INTO `sys_dept` VALUES (120, 250, ',1,248,250', '风湿免疫内科', 9, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2018-03-16 11:33:00', 'admin', '2020-08-25 15:11:19', 11, 'FSMYNK');
INSERT INTO `sys_dept` VALUES (121, 121, '0,121', '透析科', 2, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', 11, 'TXK');
INSERT INTO `sys_dept` VALUES (122, 122, '0,122', '变态反应科', 2, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', 11, 'BTFYK');
INSERT INTO `sys_dept` VALUES (123, 251, ',1,248,251', '普通外科', 2, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2018-03-16 11:33:00', 'admin', '2020-08-25 14:23:13', 12, 'PTWK');
INSERT INTO `sys_dept` VALUES (124, 251, ',1,248,251', '泌尿外科', 20, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2018-03-16 11:33:00', 'admin', '2020-08-25 15:09:48', 12, 'MNWK');
INSERT INTO `sys_dept` VALUES (125, 251, ',1,248,251', '神经外科', 19, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2018-03-16 11:33:00', 'admin', '2020-08-25 15:09:42', 12, 'SJWK');
INSERT INTO `sys_dept` VALUES (126, 251, ',1,248,251', '胸外科', 18, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2018-03-16 11:33:00', 'admin', '2020-08-25 15:09:35', 12, 'XWK');
INSERT INTO `sys_dept` VALUES (127, 251, ',1,248,251', '整形外科', 9, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2018-03-16 11:33:00', 'admin', '2020-08-25 15:08:34', 12, 'ZXWK');
INSERT INTO `sys_dept` VALUES (128, 251, ',1,248,251', '肛肠外科', 17, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2018-03-16 11:33:00', 'admin', '2020-08-25 15:09:29', 12, 'GCWK');
INSERT INTO `sys_dept` VALUES (129, 251, ',1,248,251', '肝胆外科', 10, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2018-03-16 11:33:00', 'admin', '2020-08-25 15:08:45', 12, 'GDWK');
INSERT INTO `sys_dept` VALUES (130, 251, ',1,248,251', '乳腺外科', 16, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2018-03-16 11:33:00', 'admin', '2020-08-25 15:09:20', 12, 'RXWK');
INSERT INTO `sys_dept` VALUES (131, 251, ',1,248,251', '心血管外科', 15, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2018-03-16 11:33:00', 'admin', '2020-08-25 15:09:10', 12, 'XXGWK');
INSERT INTO `sys_dept` VALUES (132, 251, ',1,248,251', '心脏外科', 12, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2018-03-16 11:33:00', 'admin', '2020-08-25 15:08:58', 12, 'XZWK');
INSERT INTO `sys_dept` VALUES (133, 102, '0,100,102', '器官移植', 2, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', 12, 'QGYZ');
INSERT INTO `sys_dept` VALUES (134, 251, ',1,248,251', '微创外科', 13, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2018-03-16 11:33:00', 'admin', '2020-08-25 15:09:04', 12, 'WCWK');
INSERT INTO `sys_dept` VALUES (135, 251, ',1,248,251', '功能神经外科', 11, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2018-03-16 11:33:00', 'admin', '2020-08-25 15:08:50', 12, 'GNSJWK');
INSERT INTO `sys_dept` VALUES (136, 250, ',1,248,250', '腺体外科', 10, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2018-03-16 11:33:00', 'admin', '2020-08-25 15:11:05', 12, 'XTWK');
INSERT INTO `sys_dept` VALUES (137, 254, ',1,248,254', '儿科综合', 11, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2018-03-16 11:33:00', 'admin', '2020-08-25 15:26:06', 14, 'EKZH');
INSERT INTO `sys_dept` VALUES (138, 254, ',1,248,254', '小儿外科', 10, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2018-03-16 11:33:00', 'admin', '2020-08-25 15:25:54', 14, 'XEWK');
INSERT INTO `sys_dept` VALUES (139, 254, ',1,248,254', '儿童保健科', 9, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2018-03-16 11:33:00', 'admin', '2020-08-25 15:25:49', 14, 'ETBJK');
INSERT INTO `sys_dept` VALUES (140, 254, ',1,248,254', '新生儿科', 12, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2018-03-16 11:33:00', 'admin', '2020-08-25 15:26:25', 14, 'XSEK');
INSERT INTO `sys_dept` VALUES (141, 254, ',1,248,254', '小儿骨科', 13, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2018-03-16 11:33:00', 'admin', '2020-08-25 15:26:30', 14, 'XEGK');
INSERT INTO `sys_dept` VALUES (142, 254, ',1,248,254', '小儿神经内科', 14, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2018-03-16 11:33:00', 'admin', '2020-08-25 15:26:38', 14, 'XESJNK');
INSERT INTO `sys_dept` VALUES (143, 254, ',1,248,254', '小儿呼吸科', 15, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2018-03-16 11:33:00', 'admin', '2020-08-25 15:26:45', 14, 'XEHXK');
INSERT INTO `sys_dept` VALUES (144, 254, ',1,248,254', '小儿血液科', 23, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2018-03-16 11:33:00', 'admin', '2020-08-25 15:27:45', 14, 'XEXYK');
INSERT INTO `sys_dept` VALUES (145, 254, ',1,248,254', '小儿耳鼻喉科', 16, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2018-03-16 11:33:00', 'admin', '2020-08-25 15:27:00', 14, 'XEEBHK');
INSERT INTO `sys_dept` VALUES (146, 254, ',1,248,254', '小儿心内科', 8, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2018-03-16 11:33:00', 'admin', '2020-08-25 15:25:06', 14, 'XEXNK');
INSERT INTO `sys_dept` VALUES (147, 254, ',1,248,254', '小儿康复科', 7, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2018-03-16 11:33:00', 'admin', '2020-08-25 15:25:00', 14, 'XEKFK');
INSERT INTO `sys_dept` VALUES (148, 254, ',1,248,254', '小儿精神科', 0, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2018-03-16 11:33:00', 'admin', '2020-08-25 15:24:14', 14, 'XEJSK');
INSERT INTO `sys_dept` VALUES (149, 254, ',1,248,254', '小儿肾内科', 1, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2018-03-16 11:33:00', 'admin', '2020-08-25 15:24:20', 14, 'XESNK');
INSERT INTO `sys_dept` VALUES (150, 254, '0,254,150', '小儿消化科', 2, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', 14, 'XEXHK');
INSERT INTO `sys_dept` VALUES (151, 254, ',1,248,254', '小儿皮肤科', 3, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2018-03-16 11:33:00', 'admin', '2020-08-25 15:24:27', 14, 'XEPFK');
INSERT INTO `sys_dept` VALUES (152, 254, ',1,248,254', '小儿急诊科', 4, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2018-03-16 11:33:00', 'admin', '2020-08-25 15:24:34', 14, 'XEJZK');
INSERT INTO `sys_dept` VALUES (153, 254, ',1,248,254', '小儿内分泌科', 5, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2018-03-16 11:33:00', 'admin', '2020-08-25 15:24:40', 14, 'XENFMK');
INSERT INTO `sys_dept` VALUES (154, 254, ',1,248,254', '小儿泌尿外科', 6, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2018-03-16 11:33:00', 'admin', '2020-08-25 15:24:50', 14, 'XEMNWK');
INSERT INTO `sys_dept` VALUES (155, 254, ',1,248,254', '小儿感染科', 17, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2018-03-16 11:33:00', 'admin', '2020-08-25 15:27:10', 14, 'XEGRK');
INSERT INTO `sys_dept` VALUES (156, 254, ',1,248,254', '小儿心外科', 24, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2018-03-16 11:33:00', 'admin', '2020-08-25 15:27:49', 14, 'XEXWK01');
INSERT INTO `sys_dept` VALUES (157, 254, ',1,248,254', '小儿胸外科', 18, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2018-03-16 11:33:00', 'admin', '2020-08-25 15:27:17', 14, 'XEXWK02');
INSERT INTO `sys_dept` VALUES (158, 254, ',1,248,254', '小儿神经外科', 19, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2018-03-16 11:33:00', 'admin', '2020-08-25 15:27:23', 14, 'XESJWK');
INSERT INTO `sys_dept` VALUES (159, 254, ',1,248,254', '小儿整形科', 20, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2018-03-16 11:33:00', 'admin', '2020-08-25 15:27:28', 14, 'XEZXK');
INSERT INTO `sys_dept` VALUES (160, 254, ',1,248,254', '小儿风湿免疫科', 21, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2018-03-16 11:33:00', 'admin', '2020-08-25 15:27:34', 14, 'XEFSMYK');
INSERT INTO `sys_dept` VALUES (161, 254, ',1,248,254', '小儿妇科', 22, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2018-03-16 11:33:00', 'admin', '2020-08-25 15:27:41', 14, 'XEFK');
INSERT INTO `sys_dept` VALUES (162, 248, ',1,248', '传染科', 5, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2018-03-16 11:33:00', 'admin', '2020-08-25 15:04:10', 15, 'CRK');
INSERT INTO `sys_dept` VALUES (163, 250, ',1,248,250', '肝病科', 0, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2018-03-16 11:33:00', 'admin', '2020-08-25 15:10:16', 15, 'GBK');
INSERT INTO `sys_dept` VALUES (164, 162, ',1,248,162', '艾滋病科', 1, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2018-03-16 11:33:00', 'admin', '2020-08-25 15:24:08', 15, 'AZBK');
INSERT INTO `sys_dept` VALUES (165, 162, ',1,248,162', '传染危重室', 0, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2018-03-16 11:33:00', 'admin', '2020-08-25 15:24:03', 15, 'CRWZS');
INSERT INTO `sys_dept` VALUES (166, 258, ',1,248,258', '妇产科综合', 1, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2018-03-16 11:33:00', 'admin', '2020-08-25 15:20:15', 16, 'FCKZH');
INSERT INTO `sys_dept` VALUES (167, 258, ',1,248,258', '妇科', 4, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2018-03-16 11:33:00', 'admin', '2020-08-25 15:20:34', 16, 'FK');
INSERT INTO `sys_dept` VALUES (168, 258, ',1,248,258', '产科', 7, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2018-03-16 11:33:00', 'admin', '2020-08-25 15:21:07', 16, 'CK');
INSERT INTO `sys_dept` VALUES (169, 258, ',1,248,258', '计划生育科', 6, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2018-03-16 11:33:00', 'admin', '2020-08-25 15:21:02', 16, 'JHSYK');
INSERT INTO `sys_dept` VALUES (170, 258, ',1,248,258', '妇科内分泌', 3, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2018-03-16 11:33:00', 'admin', '2020-08-25 15:20:29', 16, 'FKNFM');
INSERT INTO `sys_dept` VALUES (171, 102, '0,100,102', '遗传咨询科', 2, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', 16, 'YCZYK');
INSERT INTO `sys_dept` VALUES (172, 258, ',1,248,258', '产前检查科', 5, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2018-03-16 11:33:00', 'admin', '2020-08-25 15:20:56', 16, 'CQJCK');
INSERT INTO `sys_dept` VALUES (173, 258, ',1,248,258', '妇泌尿科', 0, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2018-03-16 11:33:00', 'admin', '2020-08-25 15:20:08', 16, 'FMNK');
INSERT INTO `sys_dept` VALUES (174, 255, ',1,248,255', '前列腺', 1, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2018-03-16 11:33:00', 'admin', '2020-08-25 15:28:38', 17, 'QLX');
INSERT INTO `sys_dept` VALUES (175, 255, ',1,248,255', '性功能障碍', 0, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2018-03-16 11:33:00', 'admin', '2020-08-25 15:28:33', 17, 'XGNZA');
INSERT INTO `sys_dept` VALUES (176, 257, ',1,248,257', '生殖器感染', 2, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2018-03-16 11:33:00', 'admin', '2020-08-25 14:42:58', 17, 'SZQGR');
INSERT INTO `sys_dept` VALUES (177, 255, ',1,248,255', '男性不育', 2, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2018-03-16 11:33:00', 'admin', '2020-08-25 14:00:28', 17, 'NXBY');
INSERT INTO `sys_dept` VALUES (178, 255, ',1,248,255', '生殖整形', 3, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2018-03-16 11:33:00', 'admin', '2020-08-25 15:28:44', 17, 'SZZX');
INSERT INTO `sys_dept` VALUES (179, 256, ',1,248,256', '精神科', 3, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2018-03-16 11:33:00', 'admin', '2020-08-25 15:29:04', 18, 'JSK');
INSERT INTO `sys_dept` VALUES (180, 260, ',1,248,260', '司法鉴定科', 0, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2018-03-16 11:33:00', 'admin', '2020-08-25 15:29:09', 18, 'SFJDK');
INSERT INTO `sys_dept` VALUES (181, 256, ',1,248,256', '药物依赖科', 2, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2018-03-16 11:33:00', 'admin', '2020-08-25 14:39:34', 18, 'YWYLK');
INSERT INTO `sys_dept` VALUES (182, 252, ',1,248,252', '中医精神科', 17, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2018-03-16 11:33:00', 'admin', '2020-08-25 15:14:57', 18, 'ZYJSK');
INSERT INTO `sys_dept` VALUES (183, 256, ',1,248,256', '双相障碍科', 1, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2018-03-16 11:33:00', 'admin', '2020-08-25 15:28:55', 18, 'SXZAK');
INSERT INTO `sys_dept` VALUES (184, 257, ',1,248,257', '皮肤科', 0, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2018-03-16 11:33:00', 'admin', '2020-08-25 15:28:22', 19, 'PFK');
INSERT INTO `sys_dept` VALUES (185, 257, ',1,248,257', '性病科', 1, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2018-03-16 11:33:00', 'admin', '2020-08-25 15:28:27', 19, 'XBK');
INSERT INTO `sys_dept` VALUES (186, 252, ',1,248,252', '中医综合科', 18, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2018-03-16 11:33:00', 'admin', '2020-08-25 15:15:12', 20, 'ZYZHK');
INSERT INTO `sys_dept` VALUES (187, 252, ',1,248,252', '针灸科', 19, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2018-03-16 11:33:00', 'admin', '2020-08-25 15:15:28', 20, 'ZJK');
INSERT INTO `sys_dept` VALUES (188, 252, ',1,248,252', '中医骨科', 22, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2018-03-16 11:33:00', 'admin', '2020-08-25 15:18:51', 20, 'ZYGK');
INSERT INTO `sys_dept` VALUES (189, 252, ',1,248,252', '中医妇产科', 21, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2018-03-16 11:33:00', 'admin', '2020-08-25 15:18:42', 20, 'ZYFCK');
INSERT INTO `sys_dept` VALUES (190, 252, ',1,248,252', '中医外科', 20, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2018-03-16 11:33:00', 'admin', '2020-08-25 15:18:37', 20, 'ZYWK');
INSERT INTO `sys_dept` VALUES (191, 252, ',1,248,252', '中医儿科', 15, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2018-03-16 11:33:00', 'admin', '2020-08-25 15:14:39', 20, 'ZYEK');
INSERT INTO `sys_dept` VALUES (192, 252, ',1,248,252', '中医肛肠科', 14, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2018-03-16 11:33:00', 'admin', '2020-08-25 15:14:33', 20, 'ZYGCK');
INSERT INTO `sys_dept` VALUES (193, 252, ',1,248,252', '中医皮肤科', 13, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2018-03-16 11:33:00', 'admin', '2020-08-25 15:14:24', 20, 'ZYPFK');
INSERT INTO `sys_dept` VALUES (194, 252, ',1,248,252', '中医五官科', 16, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2018-03-16 11:33:00', 'admin', '2020-08-25 15:14:46', 20, 'ZYWGK');
INSERT INTO `sys_dept` VALUES (195, 252, ',1,248,252', '中医按摩科', 9, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2018-03-16 11:33:00', 'admin', '2020-08-25 15:13:35', 20, 'ZYAMK');
INSERT INTO `sys_dept` VALUES (196, 252, ',1,248,252', '中医消化科', 10, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2018-03-16 11:33:00', 'admin', '2020-08-25 15:13:44', 20, 'ZYXHK');
INSERT INTO `sys_dept` VALUES (197, 252, ',1,248,252', '中医肿瘤科', 0, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2018-03-16 11:33:00', 'admin', '2020-08-25 15:11:52', 20, 'ZYZLK');
INSERT INTO `sys_dept` VALUES (198, 252, ',1,248,252', '中医心内科', 1, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2018-03-16 11:33:00', 'admin', '2020-08-25 15:11:59', 20, 'ZYXNK');
INSERT INTO `sys_dept` VALUES (199, 252, ',1,248,252', '中医神经内科', 12, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2018-03-16 11:33:00', 'admin', '2020-08-25 15:14:11', 20, 'ZYSJNK');
INSERT INTO `sys_dept` VALUES (200, 252, ',1,248,252', '中医肾病内科', 3, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2018-03-16 11:33:00', 'admin', '2020-08-25 15:12:13', 20, 'ZYSBNK');
INSERT INTO `sys_dept` VALUES (201, 252, ',1,248,252', '中医内分泌', 4, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2018-03-16 11:33:00', 'admin', '2020-08-25 15:12:23', 20, 'ZYNFM');
INSERT INTO `sys_dept` VALUES (202, 252, ',1,248,252', '中医呼吸科', 24, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2018-03-16 11:33:00', 'admin', '2020-08-25 15:19:34', 20, 'ZYHXK');
INSERT INTO `sys_dept` VALUES (203, 252, ',1,248,252', '中医肝病科', 5, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2018-03-16 11:33:00', 'admin', '2020-08-25 15:12:37', 20, 'ZYGBK');
INSERT INTO `sys_dept` VALUES (204, 252, ',1,248,252', '中医男科', 6, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2018-03-16 11:33:00', 'admin', '2020-08-25 15:12:42', 20, 'ZYNK');
INSERT INTO `sys_dept` VALUES (205, 252, ',1,248,252', '中医风湿免疫内科', 7, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2018-03-16 11:33:00', 'admin', '2020-08-25 15:12:52', 20, 'ZYFSMYNK');
INSERT INTO `sys_dept` VALUES (206, 252, ',1,248,252', '中医血液科', 8, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2018-03-16 11:33:00', 'admin', '2020-08-25 15:13:28', 20, 'ZYXYK');
INSERT INTO `sys_dept` VALUES (207, 252, ',1,248,252', '中医乳腺外科', 23, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2018-03-16 11:33:00', 'admin', '2020-08-25 15:19:14', 20, 'ZYRXWK');
INSERT INTO `sys_dept` VALUES (208, 252, ',1,248,252', '中医老年病科', 11, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2018-03-16 11:33:00', 'admin', '2020-08-25 15:13:56', 20, 'ZYLNBK');
INSERT INTO `sys_dept` VALUES (209, 259, ',1,248,259', '肿瘤综合科', 5, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2018-03-16 11:33:00', 'admin', '2020-08-25 15:22:29', 21, 'ZLZHK');
INSERT INTO `sys_dept` VALUES (210, 259, ',1,248,259', '肿瘤内科', 6, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2018-03-16 11:33:00', 'admin', '2020-08-25 15:22:33', 21, 'ZLNK');
INSERT INTO `sys_dept` VALUES (211, 259, ',1,248,259', '放疗科', 4, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2018-03-16 11:33:00', 'admin', '2020-08-25 15:22:19', 21, 'FLK');
INSERT INTO `sys_dept` VALUES (212, 259, ',1,248,259', '肿瘤外科', 3, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2018-03-16 11:33:00', 'admin', '2020-08-25 15:22:15', 21, 'ZLWK');
INSERT INTO `sys_dept` VALUES (213, 259, ',1,248,259', '肿瘤妇科', 2, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2018-03-16 11:33:00', 'admin', '2020-08-25 14:29:58', 21, 'ZLFK');
INSERT INTO `sys_dept` VALUES (214, 259, ',1,248,259', '骨肿瘤科', 1, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2018-03-16 11:33:00', 'admin', '2020-08-25 15:22:01', 21, 'GZLK');
INSERT INTO `sys_dept` VALUES (215, 259, ',1,248,259', '肿瘤康复科', 0, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2018-03-16 11:33:00', 'admin', '2020-08-25 15:21:56', 21, 'ZLKFK');
INSERT INTO `sys_dept` VALUES (216, 251, ',1,248,251', '骨外科', 0, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2018-03-16 11:33:00', 'admin', '2020-08-25 15:07:26', 22, 'GWK');
INSERT INTO `sys_dept` VALUES (217, 251, ',1,248,251', '手外科', 1, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2018-03-16 11:33:00', 'admin', '2020-08-25 15:07:32', 22, 'SWK');
INSERT INTO `sys_dept` VALUES (218, 251, ',1,248,251', '创伤骨科', 3, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2018-03-16 11:33:00', 'admin', '2020-08-25 15:07:40', 22, 'CSGK');
INSERT INTO `sys_dept` VALUES (219, 251, ',1,248,251', '脊柱外科', 4, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2018-03-16 11:33:00', 'admin', '2020-08-25 15:07:46', 22, 'JZWK');
INSERT INTO `sys_dept` VALUES (220, 251, ',1,248,251', '骨关节科', 5, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2018-03-16 11:33:00', 'admin', '2020-08-25 15:07:51', 22, 'GGJK');
INSERT INTO `sys_dept` VALUES (221, 251, ',1,248,251', '骨质疏松科', 7, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2018-03-16 11:33:00', 'admin', '2020-08-25 15:08:09', 22, 'GZSSK');
INSERT INTO `sys_dept` VALUES (222, 251, ',1,248,251', '矫形骨科', 8, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2018-03-16 11:33:00', 'admin', '2020-08-25 15:08:23', 22, 'JXGK');
INSERT INTO `sys_dept` VALUES (223, 251, ',1,248,251', '耳鼻咽喉头颈科', 6, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2018-03-16 11:33:00', 'admin', '2020-08-25 15:08:04', 23, 'EBYHTJK');
INSERT INTO `sys_dept` VALUES (224, 224, '0,224', '口腔科', 2, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', 23, 'KQK');
INSERT INTO `sys_dept` VALUES (225, 225, '0,225', '眼科', 2, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', 23, 'YK');
INSERT INTO `sys_dept` VALUES (226, 226, '0,226', '康复科', 2, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', 24, 'KFK');
INSERT INTO `sys_dept` VALUES (227, 227, '0,227', '理疗科', 2, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', 24, 'LLK');
INSERT INTO `sys_dept` VALUES (228, 228, '0,228', '麻醉科', 2, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', 25, 'MZK');
INSERT INTO `sys_dept` VALUES (229, 229, '0,229', '疼痛科', 2, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', 25, 'TTK');
INSERT INTO `sys_dept` VALUES (230, 230, '0,230', '营养科', 2, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', 26, 'YYK');
INSERT INTO `sys_dept` VALUES (231, 231, '0,231', '高压氧科', 2, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', 27, 'GYYK');
INSERT INTO `sys_dept` VALUES (232, 232, '0,232', '功能检查科', 2, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', 27, 'GNJCK');
INSERT INTO `sys_dept` VALUES (233, 233, '0,233', '病理科', 2, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', 27, 'BLK');
INSERT INTO `sys_dept` VALUES (234, 234, '0,234', '检验科', 2, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', 27, 'JYK');
INSERT INTO `sys_dept` VALUES (235, 235, '0,235', '实验中心', 2, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', 27, 'SYZX');
INSERT INTO `sys_dept` VALUES (236, 236, '0,236', '心电图科', 2, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', 27, 'XDTK');
INSERT INTO `sys_dept` VALUES (237, 237, '0,237', '放射科', 2, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', 28, 'FSK');
INSERT INTO `sys_dept` VALUES (238, 238, '0,238', '超声诊断科', 2, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', 28, 'CSZDK');
INSERT INTO `sys_dept` VALUES (239, 239, '0,239', '医学影像科', 2, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', 28, 'YXYXK');
INSERT INTO `sys_dept` VALUES (240, 240, '0,240', '核医学科', 2, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', 28, 'HY学K');
INSERT INTO `sys_dept` VALUES (241, 241, '0,241', '药剂科', 2, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', 29, 'YJK');
INSERT INTO `sys_dept` VALUES (242, 242, '0,242', '护理科', 2, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', 29, 'HLK');
INSERT INTO `sys_dept` VALUES (243, 243, '0,243', '体检科', 2, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', 29, 'TJK');
INSERT INTO `sys_dept` VALUES (244, 244, '0,244', '急诊科', 2, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', 29, 'JZK');
INSERT INTO `sys_dept` VALUES (245, 245, '0,245', '公共卫生与预防科', 2, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', 29, 'GGWSYYFK');
INSERT INTO `sys_dept` VALUES (246, 260, ',1,248,260', '设备科', 1, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2018-03-16 11:33:00', 'admin', '2020-08-25 15:29:14', 29, 'SBK');
INSERT INTO `sys_dept` VALUES (247, 247, '0,247', '财务科', 2, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', 29, 'CWK');
INSERT INTO `sys_dept` VALUES (248, 1, ',1', ' 岳阳分院', 1, NULL, NULL, NULL, '0', '0', 'admin', '2020-08-24 16:07:58', '', NULL, NULL, NULL);
INSERT INTO `sys_dept` VALUES (249, 1, ',1', '深圳分院', 2, NULL, NULL, NULL, '0', '0', 'admin', '2020-08-24 16:08:13', '', NULL, NULL, NULL);
INSERT INTO `sys_dept` VALUES (250, 248, ',1,248', '内科', 1, NULL, NULL, NULL, '0', '0', 'admin', '2020-08-24 16:08:29', 'admin', '2020-08-25 15:03:24', NULL, NULL);
INSERT INTO `sys_dept` VALUES (251, 248, ',1,248', '外科', 0, NULL, NULL, NULL, '0', '0', 'admin', '2020-08-24 16:08:46', 'admin', '2020-08-25 15:03:16', NULL, NULL);
INSERT INTO `sys_dept` VALUES (252, 248, ',1,248', '中医科', 2, NULL, NULL, NULL, '0', '0', 'admin', '2020-08-25 13:48:08', 'admin', '2020-08-25 15:03:31', NULL, NULL);
INSERT INTO `sys_dept` VALUES (253, 248, ',1,248', '中医科', 1, NULL, NULL, NULL, '0', '2', 'admin', '2020-08-25 13:48:08', '', NULL, NULL, NULL);
INSERT INTO `sys_dept` VALUES (254, 248, ',1,248', '儿科', 6, NULL, NULL, NULL, '0', '0', 'admin', '2020-08-25 13:52:47', 'admin', '2020-08-25 15:04:37', NULL, NULL);
INSERT INTO `sys_dept` VALUES (255, 248, ',1,248', '男科', 8, NULL, NULL, NULL, '0', '0', 'admin', '2020-08-25 14:00:05', 'admin', '2020-08-25 15:05:06', NULL, NULL);
INSERT INTO `sys_dept` VALUES (256, 248, ',1,248', '神经心理科', 9, NULL, NULL, NULL, '0', '0', 'admin', '2020-08-25 14:04:24', 'admin', '2020-08-25 15:05:12', NULL, NULL);
INSERT INTO `sys_dept` VALUES (257, 248, ',1,248', '皮肤性病科', 7, NULL, NULL, NULL, '0', '0', 'admin', '2020-08-25 14:08:16', 'admin', '2020-08-25 15:05:01', NULL, NULL);
INSERT INTO `sys_dept` VALUES (258, 248, ',1,248', '妇产科', 3, NULL, NULL, NULL, '0', '0', 'admin', '2020-08-25 14:18:00', 'admin', '2020-08-25 15:04:00', NULL, NULL);
INSERT INTO `sys_dept` VALUES (259, 248, ',1,248', '肿瘤科', 4, NULL, NULL, NULL, '0', '0', 'admin', '2020-08-25 14:29:22', 'admin', '2020-08-25 15:04:05', NULL, NULL);
INSERT INTO `sys_dept` VALUES (260, 248, ',1,248', '其他科', 10, NULL, NULL, NULL, '0', '0', 'admin', '2020-08-25 14:35:22', 'admin', '2020-08-25 15:05:23', NULL, NULL);

SET FOREIGN_KEY_CHECKS = 1;
