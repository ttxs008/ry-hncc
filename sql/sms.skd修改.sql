/*
 Navicat Premium Data Transfer

 Source Server         : 111
 Source Server Type    : MySQL
 Source Server Version : 50528
 Source Host           : localhost:3306
 Source Schema         : hncc

 Target Server Type    : MySQL
 Target Server Version : 50528
 File Encoding         : 65001

 Date: 02/09/2020 13:52:01
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for sms_skd
-- ----------------------------
DROP TABLE IF EXISTS `sms_skd`;
CREATE TABLE `sms_skd`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `date` datetime NULL DEFAULT NULL COMMENT '上班日期',
  `status` int(1) NULL DEFAULT NULL COMMENT '状态',
  `remain` bigint(20) NULL DEFAULT NULL COMMENT '剩余号数',
  `noon` int(1) NULL DEFAULT NULL COMMENT '午别（0上午1下午）',
  `staff_id` bigint(20) NULL DEFAULT NULL COMMENT '员工id',
  `dept_id` bigint(20) NULL DEFAULT NULL COMMENT '科室id',
  `sk_limit` bigint(20) NULL DEFAULT NULL COMMENT '挂号限额',
  `sms_skd_rule_id` bigint(20) NULL DEFAULT NULL COMMENT '规则id',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 891 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '排班时间表' ROW_FORMAT = Compact;

SET FOREIGN_KEY_CHECKS = 1;
