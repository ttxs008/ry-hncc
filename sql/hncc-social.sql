/*
Navicat MySQL Data Transfer

Source Server         : hncc
Source Server Version : 50716
Source Host           : localhost:3306
Source Database       : hncc

Target Server Type    : MYSQL
Target Server Version : 50716
File Encoding         : 65001

Date: 2020-08-22 15:39:45
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for sys_social_platform
-- ----------------------------
DROP TABLE IF EXISTS `sys_social_platform`;
CREATE TABLE `sys_social_platform` (
  `platform_name` varchar(255) NOT NULL COMMENT '平台名',
  `server_url` varchar(255) DEFAULT NULL COMMENT '服务器url',
  `client_id` varchar(255) DEFAULT NULL COMMENT '授权应用id',
  `client_secret` varchar(255) DEFAULT NULL COMMENT '授权应用秘钥',
  `callback_url` varchar(255) DEFAULT NULL COMMENT '回调地址',
  `authorize_path` varchar(255) DEFAULT NULL COMMENT '授权码路径',
  `token_path` varchar(255) DEFAULT NULL COMMENT 'accessToken路径',
  `user_info_url` varchar(255) DEFAULT NULL COMMENT '用户信息url',
  `open_id_path` varchar(255) DEFAULT NULL COMMENT 'openId路径',
  `scope` varchar(255) DEFAULT NULL COMMENT '授权范围',
  `salt` varchar(255) DEFAULT NULL COMMENT '盐值',
  `status` int(1) DEFAULT NULL COMMENT '状态（0禁用1启用）',
  PRIMARY KEY (`platform_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='社交应用表';

-- ----------------------------
-- Records of sys_social_platform
-- ----------------------------
INSERT INTO `sys_social_platform` VALUES ('dingtalk', 'https://oapi.dingtalk.com', 'dingoanyltk2im3jizxzli', '2t-UgJDbrU1swcbFPFmzH_sJfAEAT7VHi9NwVgjiY0NNXh_zcHHWyW1iTT7NweK3', 'http://localhost:8080/social/callback/dingtalk', '/connect/oauth2/sns_authorize', '/connect/qrconnect', '', null, 'snsapi_login', '!Ws^1', '1');
INSERT INTO `sys_social_platform` VALUES ('gitee', 'https://gitee.com', 'fec0b21dbbdcdb2c9ff53c01efbe542b8fbf966a7382fd82467de9e2950da13a', '3304eee4dc5733d54d307448a68ab9301a5fa42700240384e9c7078fc58e43bf', 'http://x946xm.natappfree.cc/social/gitee/callback', '/oauth/authorize', '/oauth/token', 'https://gitee.com/api/v5/user', null, null, '1xY*)', '1');
INSERT INTO `sys_social_platform` VALUES ('github', 'https://github.com', 'Iv1.3567b61a41654c62', 'f90c7df872e24b8af6e6a0e3ad28909721cf0243', 'http://localhost:8080/social/github/callback', '/login/oauth/authorize', '/login/oauth/access_token', 'https://api.github.com/user', null, null, '~@1au', '1');

-- ----------------------------
-- Table structure for sys_social_user
-- ----------------------------
DROP TABLE IF EXISTS `sys_social_user`;
CREATE TABLE `sys_social_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `platform_name` varchar(255) NOT NULL COMMENT '平台名字',
  `social_id` bigint(20) DEFAULT NULL COMMENT '平台用户id',
  `social_login_name` varchar(255) DEFAULT NULL COMMENT '登录名字',
  `social_name` varchar(255) DEFAULT NULL COMMENT '显示名字',
  `avatar_url` varchar(255) DEFAULT NULL COMMENT '头像地址',
  `sys_user_id` bigint(20) DEFAULT NULL COMMENT '关联用户id',
  `openid` varchar(255) DEFAULT NULL COMMENT '用户在当前开放应用内的唯一标识',
  `unionid` varchar(255) DEFAULT NULL COMMENT '用户在当前开放应用所属企业的唯一标识',
  PRIMARY KEY (`id`),
  KEY `fk_user_platform_name` (`platform_name`),
  CONSTRAINT `fk_user_platform_name` FOREIGN KEY (`platform_name`) REFERENCES `sys_social_platform` (`platform_name`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8mb4 COMMENT='社交登录用户表';

#新加角色和表修改
INSERT INTO `sys_role` VALUES ('3', '社交角色', 'social', '100', '2', '0', '0', '', null, 'admin', '2020-08-22 10:29:41', '通过三方登录的默认角色');
ALTER TABLE `sys_user`
    MODIFY COLUMN `user_name`  varchar(130) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '用户账号' AFTER `dept_id`,
    MODIFY COLUMN `nick_name`  varchar(130) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '用户昵称' AFTER `user_name`;


-- ----------------------------
-- Records of sys_social_user
-- ----------------------------
