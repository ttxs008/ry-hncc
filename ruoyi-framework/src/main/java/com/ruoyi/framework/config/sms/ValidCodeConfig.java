package com.ruoyi.framework.config.sms;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * 读取yml文件中验证码的配置
 * @author 天天向上 （john.yi@qq.com）
 * @date 2020/9/16.
 */
@Configuration
@ConfigurationProperties(prefix = "sms")
@Data
public class ValidCodeConfig {
    private int validCodeLength;
    private String validCodeKeyPrefix;
    private int validCodeExpire;
}
