package com.ruoyi.framework.config.sms;

import com.ruoyi.common.core.domain.entity.SysUser;
import com.ruoyi.common.core.domain.model.LoginUser;
import com.ruoyi.common.enums.UserStatus;
import com.ruoyi.common.exception.BaseException;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.framework.web.service.SysPermissionService;
import com.ruoyi.system.service.ISysUserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

/**
 * @author 天天向上 （john.yi@qq.com）
 * @date 2020/9/16.
 */
@Service
@Slf4j
public class SmsCodeUserDetailsService implements UserDetailsService {
    private final ISysUserService userService;
    private final SysPermissionService permissionService;

    public SmsCodeUserDetailsService(ISysUserService userService, SysPermissionService permissionService) {
        this.userService = userService;
        this.permissionService = permissionService;
    }

    @Override
    public UserDetails loadUserByUsername(String phoneNumber) throws UsernameNotFoundException {
        SysUser user = userService.selectUserByPhone(phoneNumber);
        if (StringUtils.isNull(user)) {
            log.info("登录用户：{} 不存在.", phoneNumber);
            throw new UsernameNotFoundException("登录用户：" + phoneNumber + " 不存在");
        } else if (UserStatus.DELETED.getCode().equals(user.getDelFlag())) {
            log.info("登录用户：{} 已被删除.", phoneNumber);
            throw new BaseException("对不起，您的账号：" + phoneNumber + " 已被删除");
        } else if (UserStatus.DISABLE.getCode().equals(user.getStatus())) {
            log.info("登录用户：{} 已被停用.", phoneNumber);
            throw new BaseException("对不起，您的账号：" + phoneNumber + " 已停用");
        }
        return new LoginUser(user, permissionService.getMenuPermission(user));
    }
}
