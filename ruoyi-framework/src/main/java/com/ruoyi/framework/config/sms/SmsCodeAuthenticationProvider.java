package com.ruoyi.framework.config.sms;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;

/**
 * @author 天天向上 （john.yi@qq.com）
 * @date 2020/9/16.
 */
@Component
public class SmsCodeAuthenticationProvider implements AuthenticationProvider {
    private UserDetailsService smsCodeUserDetailsService;
    private StringRedisTemplate stringRedisTemplate;
    private ValidCodeConfig validCodeConfig;

    @Autowired
    public SmsCodeAuthenticationProvider(@Qualifier("smsCodeUserDetailsService") SmsCodeUserDetailsService smsCodeUserDetailsService,
                                         StringRedisTemplate stringRedisTemplate,
                                         ValidCodeConfig validCodeConfig) {
        this.smsCodeUserDetailsService = smsCodeUserDetailsService;
        this.stringRedisTemplate = stringRedisTemplate;
        this.validCodeConfig = validCodeConfig;
    }

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        checkSmsCode();
        SmsCodeAuthenticationToken authenticationToken = (SmsCodeAuthenticationToken) authentication;
        String phoneNumber = (String) authenticationToken.getPrincipal();
        if(phoneNumber == null) {
            throw new BadCredentialsException("手机号码不存在");
        }
        UserDetails userDetails = smsCodeUserDetailsService.loadUserByUsername(phoneNumber);
        // 此时鉴权成功后，应当重新 new 一个拥有鉴权的 authenticationResult 返回
        SmsCodeAuthenticationToken authenticationResult = new SmsCodeAuthenticationToken(userDetails, userDetails.getAuthorities());
        authenticationResult.setDetails(authenticationToken.getDetails());
        return authenticationResult;
    }

    private void checkSmsCode() {
        HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
        String phoneNumber = request.getParameter("phoneNumber");
        String validCode = request.getParameter("validCode");
        String key = validCodeConfig.getValidCodeKeyPrefix() + ":" + phoneNumber;
        if (!validCode.equals(stringRedisTemplate.opsForValue().get(key))) {
            throw new BadCredentialsException("验证码错误");
        }
    }

    @Override
    public boolean supports(Class<?> authentication) {
        // 判断 authentication 是不是 SmsCodeAuthenticationToken 的子类或子接口
        return SmsCodeAuthenticationToken.class.isAssignableFrom(authentication);
    }


}
